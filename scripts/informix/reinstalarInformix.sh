#!/bin/bash
export INFORMIXDIR=/opt/informix
export INFORMIXSERVER=ifxprodshmuat
export PATH=$INFORMIXDIR/bin:$PATH
export TERMCAP=$INFORMIXDIR/etc/termcap
export IFX_LARGE_PAGES=1
export PSORT_DBTEMP=/opt/informix/data:/tmp
export DBTEMP=$PSORT_DBTEMP
export INFORMIXCONTIME=99999
export DIR_INFORMIX=iif.12.10.FC9TL.linux-x86_64/

cd /root
cd $DIR_INFORMIX
onmode -ky

yes "s" | cp -a $INFORMIXDIR/etc/sqlhosts $INFORMIXDIR/etc/onconfig .

rm -Rf $INFORMIXDIR/etc $INFORMIXDIR/bin $INFORMIXDIR/lib
/root/iif.12.10.FC9TL.linux-x86_64/ids_install -i silent -f /root/iif.12.10.FC9TL.linux-x86_64/install.properties -DUSER_INSTALL_DIR=$INFORMIXDIR -DLICENSE_ACCEPTED=TRUE
cp sqlhosts onconfig $INFORMIXDIR/etc/

oninit -v

