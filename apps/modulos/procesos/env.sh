#!/bin/bash

# properties file
source /home/tomcat/conf/conf.properties

export JAVA_HOME=/java
export CATALINA_HOME=/usr/local/tomcat6
export CATALINA_BASE=/home/tomcat/procesos
export PATH=$PATH:$JAVA_HOME/bin:$CATALINA_HOME/bin
export DBMAXPROC=10
export JAVA_OPTS="-server -Xss2048k -Xms64m -Xmx1084m -XX:MaxPermSize=768m -verbose:gc -XX:+UseConcMarkSweepGC -XX:+CMSIncrementalMode"
export JAVA_OPTS=$JAVA_OPTS" -Djava.net.preferIPv4Stack=true"
export SIS_ENVIRONMENT_NAME=env
