echo "Starting All Basic services"

ROOT_PATH=/home/tomcat/modulos
ROOT_PATH_MS=/home/tomcat/microservices

rm -Rf ${ROOT_PATH}/*/work/Catalina/*

${ROOT_PATH}/scm/start_tomcat.sh cas
${ROOT_PATH}/scm/start_tomcat.sh tesoreria
${ROOT_PATH}/scm/start_tomcat.sh datoscomunes
${ROOT_PATH}/scm/start_tomcat.sh emision
${ROOT_PATH}/scm/start_tomcat.sh siniestros
${ROOT_PATH}/scm/start_tomcat.sh rusmovil
#${ROOT_PATH}/scm/start_tomcat.sh compras
#${ROOT_PATH}/scm/start_tomcat.sh tesoreria-ws
#${ROOT_PATH}/scm/start_tomcat.sh rrhh
#${ROOT_PATH}/scm/start_tomcat.sh desarrollo-territorial
#${ROOT_PATH}/scm/start_tomcat.sh emision-ws
#${ROOT_PATH}/scm/start_tomcat.sh cesvi-ws
#${ROOT_PATH}/scm/start_tomcat.sh auditoria
#${ROOT_PATH}/scm/start_tomcat.sh contabilidad
#${ROOT_PATH}/scm/start_tomcat.sh api-rus
#${ROOT_PATH}/scm/start_tomcat.sh notificaciones
#${ROOT_PATH}/scm/start_tomcat.sh rus-apigateway-interno
#${ROOT_PATH}/scm/start_tomcat.sh api-crm
#${ROOT_PATH}/scm/start_tomcat.sh claims-ws
#${ROOT_PATH}/scm/start_tomcat.sh infoauto-ws
