#!/bin/bash

# properties file
source /home/tomcat/conf/conf.properties

export JAVA_HOME=/java
export CATALINA_HOME=/usr/local/tomcat7
export CATALINA_BASE=/home/tomcat/cas
export CATALINA_OPTS="-Xss1024M -Xmx1024M"
export PATH=$PATH:$JAVA_HOME/bin:$CATALINA_HOME/bin
export JAVA_OPTS="-Xss1024M -Xmx1024M"
export JAVA_OPTS=$JAVA_OPTS" -Dhttps.protocols=TLSv1.2 -Djdk.tls.client.protocols=TLSv1.2"
export JAVA_OPTS=$JAVA_OPTS" -Djava.net.preferIPv4Stack=true"
export SIS_ENVIRONMENT_NAME=env
