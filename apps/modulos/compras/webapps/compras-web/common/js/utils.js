
function productoBienSelect(select) {
	value = select.options[select.selectedIndex].value;
	if(value==0) { //No Aplica
		document.getElementsByName('bien.amortizacion')[0].disabled=true;
		document.getElementsByName('bien.amortizacion')[0].value="";		
		document.getElementsByName('bien.amortizacion')[0].className ='textareagris';
		
		document.getElementsByName('bien.registrable')[0].disabled=true;
		document.getElementsByName('bien.registrable')[0].value="-1";
		document.getElementsByName('bien.registrable')[0].className ='textareagris';
		
		document.getElementsByName('bien.vehiculo')[0].disabled=true;
		document.getElementsByName('bien.vehiculo')[0].value="-1";
		document.getElementsByName('bien.vehiculo')[0].className ='textareagris';
		
		document.getElementsByName('producto.existenciaMinima')[0].disabled=true;
		document.getElementsByName('producto.existenciaMinima')[0].value="";
		document.getElementsByName('producto.existenciaMinima')[0].className ='textareagris';		
		
		document.getElementsByName('producto.reposicionAutomatica')[0].disabled=true;
		document.getElementsByName('producto.reposicionAutomatica')[0].value="0";
		document.getElementsByName('producto.reposicionAutomatica')[0].className ='textareagris';
		
		document.getElementsByName('producto.tipoUnidad.oid')[0].disabled=true;
		document.getElementsByName('producto.tipoUnidad.oid')[0].value="0";
		document.getElementsByName('producto.tipoUnidad.oid')[0].className ='textareagris';
	}
	if(value==1) { //producto
		document.getElementsByName('bien.amortizacion')[0].disabled=true;
		document.getElementsByName('bien.amortizacion')[0].value="";
		document.getElementsByName('bien.amortizacion')[0].className ='textareagris';	
		
		document.getElementsByName('bien.registrable')[0].disabled=true;
		document.getElementsByName('bien.registrable')[0].value="0";
		document.getElementsByName('bien.registrable')[0].className ='textareagris';	
				
		document.getElementsByName('bien.vehiculo')[0].disabled=true;
		document.getElementsByName('bien.vehiculo')[0].value="0";
		document.getElementsByName('bien.vehiculo')[0].className ='textareagris';		
		
		document.getElementsByName('producto.existenciaMinima')[0].disabled=true;
		document.getElementsByName('producto.existenciaMinima')[0].value="";	
		document.getElementsByName('producto.existenciaMinima')[0].className ='textareagris';
		
		document.getElementsByName('producto.reposicionAutomatica')[0].disabled=false;
		document.getElementsByName('producto.reposicionAutomatica')[0].className ='textarea';
		
		document.getElementsByName('producto.tipoUnidad.oid')[0].disabled=false;
		document.getElementsByName('producto.tipoUnidad.oid')[0].className ='textarea';
		
	}
	if(value==2) { //bien	
		document.getElementsByName('producto.existenciaMinima')[0].disabled=true;
		document.getElementsByName('producto.existenciaMinima')[0].value="";
		document.getElementsByName('producto.existenciaMinima')[0].className ='textareagris';
		
		document.getElementsByName('producto.reposicionAutomatica')[0].disabled=true;
		document.getElementsByName('producto.reposicionAutomatica')[0].value="0";
		document.getElementsByName('producto.reposicionAutomatica')[0].className ='textareagris';
		
		document.getElementsByName('bien.registrable')[0].disabled=false;		
		document.getElementsByName('bien.registrable')[0].className ='textarea';
		
		document.getElementsByName('bien.vehiculo')[0].disabled=false;
		document.getElementsByName('bien.vehiculo')[0].className ='textarea';
		
		document.getElementsByName('bien.amortizacion')[0].disabled=false;
		document.getElementsByName('bien.amortizacion')[0].className ='textarea';
		
		document.getElementsByName('producto.tipoUnidad.oid')[0].disabled=true;
		document.getElementsByName('producto.tipoUnidad.oid')[0].value="0";
		document.getElementsByName('producto.tipoUnidad.oid')[0].className ='textareagris';
	}
}


function depositoSelect(select) {
	value = select.options[select.selectedIndex].value;
	if(value==0) { //No Aplica
		document.getElementsByName('deposito.cantidadSeccion')[0].disabled=true;
		document.getElementsByName('deposito.cantidadSeccion')[0].value="";
		document.getElementsByName('deposito.cantidadSeccion')[0].className ='textareagris';
		
		document.getElementsByName('deposito.cantidadColumna')[0].disabled=true;		
		document.getElementsByName('deposito.cantidadColumna')[0].value="";
		document.getElementsByName('deposito.cantidadColumna')[0].className ='textareagris';
		
		document.getElementsByName('deposito.cantidadFila')[0].disabled=true;
		document.getElementsByName('deposito.cantidadFila')[0].value="";
		document.getElementsByName('deposito.cantidadFila')[0].className ='textareagris';
	}
	if(value==1) { //Seccion
		document.getElementsByName('deposito.cantidadSeccion')[0].disabled=false;
		document.getElementsByName('deposito.cantidadSeccion')[0].className ='textarea';
		
		document.getElementsByName('deposito.cantidadColumna')[0].disabled=true;		
		document.getElementsByName('deposito.cantidadColumna')[0].value="";
		document.getElementsByName('deposito.cantidadColumna')[0].className ='textareagris';
		
		document.getElementsByName('deposito.cantidadFila')[0].disabled=true;
		document.getElementsByName('deposito.cantidadFila')[0].value="";
		document.getElementsByName('deposito.cantidadFila')[0].className ='textareagris';
	}
	if(value==2) { //Fila - Columna - Ubicacion
		document.getElementsByName('deposito.cantidadSeccion')[0].disabled=true;
		document.getElementsByName('deposito.cantidadSeccion')[0].value="";
		document.getElementsByName('deposito.cantidadSeccion')[0].className ='textareagris';
		
		document.getElementsByName('deposito.cantidadColumna')[0].disabled=false;		
		document.getElementsByName('deposito.cantidadColumna')[0].className ='textarea';
		
		document.getElementsByName('deposito.cantidadFila')[0].disabled=false;
		document.getElementsByName('deposito.cantidadFila')[0].className ='textarea';
	}
}


function productoReposicionAutomaticaSelect(select) {
	value = select.options[select.selectedIndex].value;
	if(value==0) { //Seleccionar
		document.getElementsByName('producto.existenciaMinima')[0].disabled=true;
		document.getElementsByName('producto.existenciaMinima')[0].value="";		
		document.getElementsByName('producto.existenciaMinima')[0].className ='textareagris';
	}
	if(value==1) { //NO
		document.getElementsByName('producto.existenciaMinima')[0].disabled=true;
		document.getElementsByName('producto.existenciaMinima')[0].value="";
		document.getElementsByName('producto.existenciaMinima')[0].className ='textareagris';
		
	}
	if(value==2) { //SI
		document.getElementsByName('producto.existenciaMinima')[0].disabled=false;
		document.getElementsByName('producto.existenciaMinima')[0].className ='textarea';
	}
}

function tipoDestino(select) {

	value = select.options[select.selectedIndex].value;

	if(value==0) { //Seleccionar

		$('clienteDestinoTexto').innerHTML = "";
		$("clienteDestinoBusqueda").style.display = "none";
		$('fscTexto').innerHTML = "";
		$("fscBusqueda").style.display = "none";

		document.getElementsByName('egresoProducto.depositoDestino.oid')[0].disabled=true;		
		document.getElementsByName('egresoProducto.depositoDestino.oid')[0].value="0";
		document.getElementsByName('egresoProducto.depositoDestino.oid')[0].className ='textareagris';	

		document.getElementsByName('egresoProducto.clienteDestino.descripcion')[0].value="";
		document.getElementsByName('egresoProducto.clienteDestino.descripcion')[0].className ='textareagris';

		document.getElementsByName('egresoProducto.fsc_fss.numero')[0].value="";
		document.getElementsByName('egresoProducto.fsc_fss.numero')[0].className ='textareagris';

		$("seleccionarCliente").className ='linkOcultar';		
		$("seleccionarFormulario").className ='linkOcultar';
	}
	if(value==1) { //Deposito

		$('clienteDestinoTexto').innerHTML = "";
		$("clienteDestinoBusqueda").style.display = "none";
		$('fscTexto').innerHTML = "";
		$("fscBusqueda").style.display = "none";

		document.getElementsByName('egresoProducto.depositoDestino.oid')[0].disabled=false;
		document.getElementsByName('egresoProducto.depositoDestino.oid')[0].className ='textarea';	

		document.getElementsByName('egresoProducto.clienteDestino.descripcion')[0].value="";
		document.getElementsByName('egresoProducto.clienteDestino.descripcion')[0].className ='textareagris';
		
		document.getElementsByName('egresoProducto.fsc_fss.numero')[0].value="";
		document.getElementsByName('egresoProducto.fsc_fss.numero')[0].className ='textareagris';
				
		$("seleccionarCliente").className ='linkOcultar';		
		$("seleccionarFormulario").className ='linkOcultar';
	}
	if(value==2) { //Cliente

		$("clienteDestinoBusqueda").style.display = "block";
		$("fscBusqueda").style.display = "block";

		document.getElementsByName('egresoProducto.depositoDestino.oid')[0].disabled=true;		
		document.getElementsByName('egresoProducto.depositoDestino.oid')[0].value="0";
		document.getElementsByName('egresoProducto.depositoDestino.oid')[0].className ='textareagris';	
						
		document.getElementsByName('egresoProducto.clienteDestino.descripcion')[0].className ='textarea';				
		document.getElementsByName('egresoProducto.fsc_fss.numero')[0].className ='textarea';
		
		$("seleccionarCliente").className ='texto1';
		$("seleccionarFormulario").className ='texto1';
	}
}

function clienteSelect(select) {
	value = select.options[select.selectedIndex].value;
	if(value==0) { //No Aplica		
		$("fondoFijoAsignado").disabled=true;
		$("clasificacion").disabled=true;
		$("duenio").disabled=true;
		
		$("fondoFijoAsignado").className ='textareagris';	
		$("clasificacion").className ='textareagris';
		$("duenio").className = 'textareagris';	
			
		$("fondoFijoAsignado").value="0";	
		$("clasificacion").value="0";
		$("duenio").value="0";
	}
	if(value==1) { //interno
		$("fondoFijoAsignado").disabled=false;
		$("clasificacion").disabled=true;		
		$("duenio").disabled=false;

		$("fondoFijoAsignado").className ='textarea';	
		$("clasificacion").className ='textareagris';					
		$("duenio").className = 'textarea';	

		$("fondoFijoAsignado").value="0";	
		$("clasificacion").value="0";
		$("duenio").value = '1';	
	}
	if(value==2) { //externo
		$("fondoFijoAsignado").disabled=false;
		$("clasificacion").disabled=false;
		$("duenio").disabled = true;

		$("fondoFijoAsignado").className ='textarea';
		$("clasificacion").className ='textarea';
		$("duenio").className = 'textareagris';	

		$("fondoFijoAsignado").value="0";	
		$("clasificacion").value="0";
		$("duenio").value = "0";
	}

	fondoFijoSelect($("fondoFijoAsignado"));
	duenioAplicacionSelect($("duenio"));
}


function duenioAplicacionSelect(select) {

	value = select.options[select.selectedIndex].value;

	// No Aplica
	if(value == 0) {

		$("autorizaFSCFSS").disabled = true;
		$("autorizaFSCFSS").className = "textareagris";
		$("autorizaFSCFSS").value = 0;
	}

	// No
	if(value == 1) {

		$("autorizaFSCFSS").disabled = false;
		$("autorizaFSCFSS").className = "textarea";
		$("autorizaFSCFSS").value = 1;
	}

	// Si
	if(value == 2) {

		$("autorizaFSCFSS").disabled = true;
		$("autorizaFSCFSS").className = "textareagris";
		$("autorizaFSCFSS").value = 2;
	}

	autorizaFSCFSSSelect($("autorizaFSCFSS"));
}

function autorizaFSCFSSSelect(select) {

	value = select.options[select.selectedIndex].value;

	//No Aplica
	if(value==0) {			

		$("montoMinimo").disabled = true;
		$("montoMinimo").className ='textareagris';
		$("montoMinimo").value="";

		$("tieneMontoMaximo").disabled = true;
		$("tieneMontoMaximo").className = "textareagris";
		$("tieneMontoMaximo").value = 0;
	}

	//No
	if(value==1) {

		$("montoMinimo").disabled = true;
		$("montoMinimo").className ='textareagris';	
		$("montoMinimo").value="";

		$("tieneMontoMaximo").disabled = true;
		$("tieneMontoMaximo").className = "textareagris";
		$("tieneMontoMaximo").value = 0;
	}

	//Si
	if(value==2) {	

		$("montoMinimo").disabled = false;
		$("montoMinimo").className ='textarea';

		esDuenio = $("duenio").value

		// No
		if(esDuenio == 1) {

			$("tieneMontoMaximo").disabled = false;
			$("tieneMontoMaximo").className = "textarea";
			$("tieneMontoMaximo").value = 2;
		}

		// Si
		if(esDuenio == 2) {

			$("tieneMontoMaximo").disabled = true;
			$("tieneMontoMaximo").className = "textareagris";
			$("tieneMontoMaximo").value = 2;
		}

	}

	tieneMontoMaximoSelect($("tieneMontoMaximo"));
}

function tieneMontoMaximoSelect(select) {

	value = select.options[select.selectedIndex].value;

	// No Aplica
	if(value == 0) {			

		$("montoMaximo").disabled = true;		
		$("montoMaximo").className ='textareagris';
		$("montoMaximo").value="";		
	}

	// No
	if(value == 1) {

		$("montoMaximo").disabled = true;
		$("montoMaximo").className ='textareagris';
		$("montoMaximo").value="";		
	}

	// Si
	if(value == 2) {	

		$("montoMaximo").disabled = false;		
		$("montoMaximo").className ='textarea';
	}

}


function proveedorUnicoSelect(select) {
	value = select.options[select.selectedIndex].value;


	if(value==0) { //No Aplica		

		$("proveeBusqueda").style.display = "none";
		if (document.getElementsByName('servicio.proveedor.detalleDePersona.razonSocial') != 'undefined') {

			$('servicio.proveedor.detalleDePersona.razonSocial').value = "";
		}

		if (document.getElementsByName('servicio.proveedor.detalleDePersona.nombre') != 'undefined') {

			$('servicio.proveedor.detalleDePersona.nombre').value = "";
		}

		$('provUnico').innerHTML = "&nbsp;";
	}

	if(value==1) { //no

		$("proveeBusqueda").style.display = "none";
		if (document.getElementsByName('servicio.proveedor.detalleDePersona.razonSocial') != 'undefined') {

			$('servicio.proveedor.detalleDePersona.razonSocial').value = "";
		}

		if (document.getElementsByName('servicio.proveedor.detalleDePersona.nombre') != 'undefined') {

			$('servicio.proveedor.detalleDePersona.nombre').value = "";
		}

		$('provUnico').innerHTML = "&nbsp;";
	}

	if(value==2) { //si

		$("proveeBusqueda").style.display = "block";
	}
}


function fondoFijoSelect(select) {
	value = select.options[select.selectedIndex].value;
	if(value==0) { //No Aplica		
		$("importeFondoFijo").disabled=true;	
		$("importeFondoFijo").className ='textareagris';	
		$("importeFondoFijo").value="";	

	}
	if(value==1) { //no
		$("importeFondoFijo").disabled=true;
		$("importeFondoFijo").className ='textareagris';	
		$("importeFondoFijo").value="";	
	

	}
	if(value==2) { //si
		$("importeFondoFijo").disabled=false;
		$("importeFondoFijo").className ='textarea';	

	}
}

function limpiarNivel0(){
	$("nivel2").innerHTML="";
	$("nivel3").innerHTML="";
}
function limpiarNivel1(){
	$("nivel3").innerHTML="";
}


function listaPedidoCotizacionSelect(select){				
		
		var a = $("selectPC").href.split('?');
		var cadenaOriginal= a[0];
		var cadenaParametros=a[1];		
		cadenaParametros=cadenaParametros+"&_";
		var arrayParametros = cadenaParametros.split('&');
		var primerParametro=arrayParametros[0];		
		var cadenaEnvio=cadenaOriginal+"?"+primerParametro;		
		var u='';
		for (i=0; i<document.getElementsByName('pcSelect').length; i++){				
			if (document.getElementsByName('pcSelect')[i].checked==true)
				u=u+'&pedidoCotizacionList='+document.getElementsByName('pcSelect')[i].value;
		}			
		$("selectPC").href=cadenaEnvio + u ;				
	}
	
function listaEntregasParciales(){
	document.getElementsByName('entregaSeleccion')[0].value="";
	for (i=0; i<document.getElementsByName('checkEntregaSeleccion').length; i++){				
		if (document.getElementsByName('checkEntregaSeleccion')[i].checked==true)
			document.getElementsByName('entregaSeleccion')[0].value=document.getElementsByName('entregaSeleccion')[0].value+"#"+
			document.getElementsByName('checkEntregaSeleccion')[i].value;
	}	
	
}

function listaRecepcionesPendientes(){
	document.getElementsByName('recepcionSelecccionList')[0].value="";
	for (i=0; i<document.getElementsByName('checkRecepcionSeleccion').length; i++){				
		if (document.getElementsByName('checkRecepcionSeleccion')[i].checked==true)
			document.getElementsByName('recepcionSelecccionList')[0].value=
			document.getElementsByName('recepcionSelecccionList')[0].value+"#"+
			document.getElementsByName('checkRecepcionSeleccion')[i].value;
	}	
	
}




function listaCantidadesEgresar(){
	document.getElementsByName('cantidadesEgresadas')[0].value="";
	for (i=0; i<document.getElementsByName('cantidadEgresar').length; i++){
		var valor=document.getElementsByName('cantidadEgresar')[i].id+'_'+document.getElementsByName('cantidadEgresar')[i].value;
		document.getElementsByName('cantidadesEgresadas')[0].value=document.getElementsByName('cantidadesEgresadas')[0].value+"/"+valor;
	}
				
			
}
		
	

	function listaOCSelect(){
		document.getElementsByName('ocStringList')[0].value="";
		for (i=0; i<document.getElementsByName('ocSelect').length; i++){				
			if (document.getElementsByName('ocSelect')[i].checked==true)
			{
				var valor=document.getElementsByName('ocSelect')[i].value;
				document.getElementsByName('ocStringList')[0].value=document.getElementsByName('ocStringList')[0].value+"_"+valor;
			}
		}			
				
	}
	
	function listaOCSelectLink(select){				
		
		var cadenaOriginal=$("imprimirOC").href.split('?')[0];		
		var parametros='';
		document.getElementsByName('osStringList')[0].value='';
		for (i=0; i<document.getElementsByName('ocSelect').length; i++){				
			if (document.getElementsByName('ocSelect')[i].checked==true)
			{
				var valor=document.getElementsByName('ocSelect')[i].value;
				document.getElementsByName('ocStringList')[0].value=document.getElementsByName('ocStringList')[0].value+"_"+valor;
				parametros=parametros+"_"+valor;
			}
		}				

		var parametrosFinal="ocStringList="+parametros;				
		$("imprimirOC").href=cadenaOriginal+'?' + parametrosFinal ;				
	}
	
	function listaOSSelect(select){
		document.getElementsByName('osStringList')[0].value="";		
		for (i=0; i<document.getElementsByName('osSelect').length; i++){				
			if (document.getElementsByName('osSelect')[i].checked==true)
			{
				var valor=document.getElementsByName('osSelect')[i].value;
				document.getElementsByName('osStringList')[0].value=document.getElementsByName('osStringList')[0].value+"_"+valor;
			}
		}			
				
	}
	function listaOSSelectLink(select){				
		
		var cadenaOriginal=$("imprimirOS").href.split('?')[0];		
		var parametros='';
		document.getElementsByName('osStringList')[0].value='';
		for (i=0; i<document.getElementsByName('osSelect').length; i++){				
			if (document.getElementsByName('osSelect')[i].checked==true)
			{
				var valor=document.getElementsByName('osSelect')[i].value;
				document.getElementsByName('osStringList')[0].value=document.getElementsByName('osStringList')[0].value+"_"+valor;
				parametros=parametros+"_"+valor;
			}
		}			
		var parametrosFinal="osStringList="+parametros;		
		$("imprimirOS").href=cadenaOriginal+'?' + parametrosFinal;		
		
	}
	
	function listaPCservicioSelect(){
		document.getElementsByName('pcStringList')[0].value="";
		for (i=0; i<document.getElementsByName('pcServicioSelect').length; i++){				
			if (document.getElementsByName('pcServicioSelect')[i].checked==true)
			{
				var valor=document.getElementsByName('pcServicioSelect')[i].value;
				document.getElementsByName('pcStringList')[0].value=document.getElementsByName('pcStringList')[0].value+"_"+valor;
			}
		}			
				
	}
	
	function listaPCproductoBienSelect(){	
	
		document.getElementsByName('pcStringList')[0].value="";
		for (i=0; i<document.getElementsByName('pcProductoBienSelect').length; i++){				
			if (document.getElementsByName('pcProductoBienSelect')[i].checked==true)
			{
				var valor=document.getElementsByName('pcProductoBienSelect')[i].value;
				document.getElementsByName('pcStringList')[0].value=document.getElementsByName('pcStringList')[0].value+"_"+valor;
			}
		}			
				
	}
	
	
	function listaOCproductoBienSelect(){
		document.getElementsByName('ocStringList')[0].value="";
		for (i=0; i<document.getElementsByName('ocSelect').length; i++){				
			if (document.getElementsByName('ocSelect')[i].checked==true)
			{
				var valor=document.getElementsByName('ocSelect')[i].value;
				document.getElementsByName('ocStringList')[0].value=document.getElementsByName('ocStringList')[0].value+"_"+valor;
			}
		}			
				
	}
	
	
function listaPCservicioSelectLink(select){				
		
		var cadenaOriginal=$("imprimirPCservicio").href.split('?')[0];
				
		var parametros='';
		document.getElementsByName('pcStringList')[0].value='';
		for (i=0; i<document.getElementsByName('pcServicioSelect').length; i++){				
			if (document.getElementsByName('pcServicioSelect')[i].checked==true)
			{
				var valor=document.getElementsByName('pcServicioSelect')[i].value;
				document.getElementsByName('pcStringList')[0].value=document.getElementsByName('pcStringList')[0].value+"_"+valor;
				parametros=parametros+"_"+valor;
			}
		}		
		
		var parametrosFinal="pcStringList="+parametros;		
		$("imprimirPCservicio").href=cadenaOriginal+'?pedidoCotizacion.tipo=2&' + parametrosFinal;
						
		
	}
	
	function listaPCproductoBienSelectLink(select){				
		
		var cadenaOriginal=$("imprimirPCproductoBien").href.split('?')[0];
	
		var parametros='';
		document.getElementsByName('pcStringList')[0].value='';
		for (i=0; i<document.getElementsByName('pcProductoBienSelect').length; i++){				
			if (document.getElementsByName('pcProductoBienSelect')[i].checked==true)
			{
				var valor=document.getElementsByName('pcProductoBienSelect')[i].value;
				document.getElementsByName('pcStringList')[0].value=document.getElementsByName('pcStringList')[0].value+"_"+valor;
				parametros=parametros+"_"+valor;
			}
		}			
		var parametrosFinal="pcStringList="+parametros;		
		$("imprimirPCproductoBien").href=cadenaOriginal+'?pedidoCotizacion.tipo=1&' + parametrosFinal;	
							
	}
	
	function tipoPersonaSexo(select) {
		
		value = select.options[select.selectedIndex].value;
		if(value==1) { //Persona Juridica		

			$("docPersonal").disabled = true;
			$("docPersonal").className = 'textareagris';
			$("docPersonal").value = "";
			$("personaDocBusqueda").style.display = "none";

			$("sexo").value=0; //No Aplica
			$("sexo").disabled=true;
		} else { //Seleccionar o Persona Fisica

			$("docPersonal").disabled = false;
			$("docPersonal").className = 'textarea';
			$("personaDocBusqueda").style.display = "block";

			$("sexo").disabled=false;
		}
	}
	
	function condicionFiscalDGR(){
		value = $("situacionDGR").options[$("situacionDGR").selectedIndex].value;
		if(value==0) { // seleccionar						
			$("situacionMultilateral").disabled = true;
			$("situacionMultilateral").className = 'textareagris';
			$("situacionMultilateral").value = -1;
			
			
			$("nroDeIngresosBrutos").disabled = true;
			$("nroDeIngresosBrutos").className ='textareagris';
			$("nroDeIngresosBrutos").value="";		
			
						
			$("fechaDGR").disabled = true;
			$("fechaDGR").className ='textareagris';
			$("fechaDGR").value="";	
			
									
			$("jubilacion").disabled = true;
			$("jubilacion").className ='textareagris';
			$("jubilacion").value="";
		}	
		else if(value==1){ // Responsable Inscripto
		
			$("situacionMultilateral").disabled = false;
			$("situacionMultilateral").className = 'textarea';
			
			
			
			$("nroDeIngresosBrutos").disabled = false;
			$("nroDeIngresosBrutos").className ='textarea';
			
			
						
			$("fechaDGR").disabled = false;
			$("fechaDGR").className ='textarea';				
			
									
			$("jubilacion").disabled = false;
			$("jubilacion").className ='textarea';			
		
		}
		else if(value==2){ // No Inscripto
			$("situacionMultilateral").disabled = false;
			$("situacionMultilateral").value = 0;		
			$("situacionMultilateral").disabled = true;
			$("situacionMultilateral").className = "textareagris";

			
			
			$("nroDeIngresosBrutos").disabled = true;
			$("nroDeIngresosBrutos").className ='textareagris';
			$("nroDeIngresosBrutos").value="";		
			
						
			$("fechaDGR").disabled = true;
			$("fechaDGR").className ='textareagris';
			$("fechaDGR").value="";	
			
									
			$("jubilacion").disabled = true;
			$("jubilacion").className ='textareagris';
			$("jubilacion").value="";
		}
		else if(value==3){// Excento
			$("situacionMultilateral").disabled = false;
			$("situacionMultilateral").value = 1;
			$("situacionMultilateral").disabled = true;
			$("situacionMultilateral").className = "textareagris";
			
			
			
			$("nroDeIngresosBrutos").disabled = false;
			$("nroDeIngresosBrutos").className ='textarea';
			
			
						
			$("fechaDGR").disabled = false;
			$("fechaDGR").className ='textarea';
			
			
									
			$("jubilacion").disabled = true;
			$("jubilacion").className ='textareagris';
			$("jubilacion").value="";
		
		}
		else if(value == 4) {// Regimen Simplificado

			$("situacionMultilateral").disabled = false;
			$("situacionMultilateral").value = 0;
			$("situacionMultilateral").disabled = true;
			$("situacionMultilateral").className = "textareagris";

			$("nroDeIngresosBrutos").disabled = false;
			$("nroDeIngresosBrutos").className ='textarea';

			$("fechaDGR").disabled = false;
			$("fechaDGR").className ='textarea';

			$("jubilacion").disabled = true;
			$("jubilacion").className ='textareagris';
			$("jubilacion").value="";
		}
	}
	
	function advertenciaFechaEmision(select){
		var fechaEmisionValue = $("factura.fechaEmision").value;
		var fechaEmisionParte = fechaEmisionValue.split('/');
		var fechaEmision =  new Date(fechaEmisionParte[2], fechaEmisionParte[1]-1, fechaEmisionParte[0]);

		var fechaIngreso = new Date();
		//var fechaIngresoValue = $("factura.fechaIngreso").value;
		//if (fechaIngresoValue != null){
			//var fechaIngresoParte = fechaIngresoValue.split('/');
			//fechaIngreso =  new Date(fechaIngresoParte[2], fechaIngresoParte[1]-1, fechaIngresoParte[0]);			
		//}
		
		if (fechaEmision.getTime() > fechaIngreso.getTime()) {
			$("fechaEmisionAnterior").style.display = "block";
		} else {
			$("fechaEmisionAnterior").style.display = "none";
		}
	}

function posicionInicialSelect(select) {

	value = select.options[select.selectedIndex].value;
	$("posicionInicial").value = value;
}

function codigoBarraLinkOnClick() {				

	var a = $("codigoBarraLink").href.split('?');
	var cadenaOriginal = a[0];
	var cadenaParametros = a[1];
	var arrayParametros = cadenaParametros.split('&');

	var nuevoArrayParametros = new Array();
	for (i = 0; i < arrayParametros.length; i++) {

		if (arrayParametros[i].indexOf("posicionInicial") == -1) {

			nuevoArrayParametros.push(arrayParametros[i]);
		} else {

			nuevoArrayParametros.push("posicionInicial=" + $("posicionInicial").value);
		}
	}

	var nuevaCadenaParametros = nuevoArrayParametros.join("&");
	var cadenaEnvio = cadenaOriginal + "?" + nuevaCadenaParametros;

	$("codigoBarraLink").href = cadenaEnvio;				
}
function propiedadCartelSelect(select) {

	value = select.options[select.selectedIndex].value;

	//No Aplica
	if(value == 0) {		

		$("proveedorBusqueda").style.display = "none";
		if (document.getElementsByName('carteleria.proveedor.detalleDePersona.razonSocial') != 'undefined') {

			$('carteleria.proveedor.detalleDePersona.razonSocial').value = "";
		}

		if (document.getElementsByName('carteleria.proveedor.detalleDePersona.nombre') != 'undefined') {

			$('carteleria.proveedor.detalleDePersona.nombre').value = "";
		}

		if (document.getElementsByName('carteleria.proveedor.nroProveedor') != 'undefined') {

			$('carteleria.proveedor.nroProveedor').value = "";
		}

		$('proveedorTexto').innerHTML = "&nbsp;";
	}

	// Rio Uruguay
	if(value == 1) {

		$("proveedorBusqueda").style.display = "none";
		if (document.getElementsByName('carteleria.proveedor.detalleDePersona.razonSocial') != 'undefined') {

			$('carteleria.proveedor.detalleDePersona.razonSocial').value = "";
		}

		if (document.getElementsByName('carteleria.proveedor.detalleDePersona.nombre') != 'undefined') {

			$('carteleria.proveedor.detalleDePersona.nombre').value = "";
		}

		if (document.getElementsByName('carteleria.proveedor.nroProveedor') != 'undefined') {

			$('carteleria.proveedor.nroProveedor').value = "";
		}

		$('proveedorTexto').innerHTML = "&nbsp;";
	}

	// Arrendado
	if(value == 2) {

		$('proveedorTexto').innerHTML = "";
		$("proveedorBusqueda").style.display = "block";
	}
}


function tipoEvaluacion(select) {
	
	value = select.options[select.selectedIndex].value;
	
	if($('evaluacionProveedor.esCargaInicial').value == 2){ //esCargaInicial = Si  
		//verifico el valor del combo
		if(value==0) { //Seleccionar
			document.getElementsByName('evaluacionProveedor.ICP')[0].disabled=true;
			document.getElementsByName('evaluacionProveedor.ICP')[0].className ='textareagris';
			document.getElementsByName('evaluacionProveedor.ICS')[0].disabled=true;		
			document.getElementsByName('evaluacionProveedor.ICS')[0].className ='textareagris';
		
			$("proveedorBusqueda").style.display = "none";
			if (document.getElementsByName('evaluacionProveedor.proveedor.detalleDePersona.razonSocial') != 'undefined') {
				$('evaluacionProveedor.proveedor.detalleDePersona.razonSocial').value = "";
			}
			if (document.getElementsByName('evaluacionProveedor.proveedor.detalleDePersona.nombre') != 'undefined') {
				$('evaluacionProveedor.proveedor.detalleDePersona.nombre').value = "";
			}
			if (document.getElementsByName('evaluacionProveedor.proveedor.nroProveedor') != 'undefined') {
				$('evaluacionProveedor.proveedor.nroProveedor').value = "";
			}

			$('proveedorTexto').innerHTML = "&nbsp;";	
			
			//$("iconoCargaInicial").style.display = "none";
			//$("iconoEvaluacionPeriodo").style.display = "none";	
		}
	
		if(value==1) { //Desempenio Proveedor
			
			document.getElementsByName('evaluacionProveedor.ICP')[0].disabled=false;
			document.getElementsByName('evaluacionProveedor.ICP')[0].className ='textarea';
			document.getElementsByName('evaluacionProveedor.ICS')[0].disabled=true;
			document.getElementsByName('evaluacionProveedor.ICS')[0].className ='textareagris';
				
			$('proveedorTexto').innerHTML = "";
			$("proveedorBusqueda").style.display = "block";	
			//$("iconoCargaInicial").style.display = "block";
			//$("iconoEvaluacionPeriodo").style.display = "none";
		}
		if(value==2) { //Post Servicio
			document.getElementsByName('evaluacionProveedor.ICP')[0].disabled=true;
			document.getElementsByName('evaluacionProveedor.ICP')[0].className ='textareagris';
			document.getElementsByName('evaluacionProveedor.ICS')[0].disabled=false;		
			document.getElementsByName('evaluacionProveedor.ICS')[0].className ='textarea';	
		
			$('proveedorTexto').innerHTML = "";
			$("proveedorBusqueda").style.display = "block";	
			//$("iconoCargaInicial").style.display = "block";
			//$("iconoEvaluacionPeriodo").style.display = "none";
		}
	} 
	
	if($('evaluacionProveedor.esCargaInicial').value == 1){ //esCargaInicial = No  
		//verifico el valor del combo
		if(value==0) { //Seleccionar
			if ($('evaluacionProveedor.calificacionEvaluacionProveedor.oid').value != 'undefined') {
				document.getElementsByName('evaluacionProveedor.calificacionEvaluacionProveedor.oid')[0].disabled=true;
				document.getElementsByName('evaluacionProveedor.calificacionEvaluacionProveedor.oid')[0].className ='textareagris';
				}	
			
			$("proveedorBusqueda").style.display = "none";
			if (document.getElementsByName('evaluacionProveedor.proveedor.detalleDePersona.razonSocial') != 'undefined') {
				$('evaluacionProveedor.proveedor.detalleDePersona.razonSocial').value = "";
			}
			if (document.getElementsByName('evaluacionProveedor.proveedor.detalleDePersona.nombre') != 'undefined') {
				$('evaluacionProveedor.proveedor.detalleDePersona.nombre').value = "";
			}
			if (document.getElementsByName('evaluacionProveedor.proveedor.nroProveedor') != 'undefined') {
				$('evaluacionProveedor.proveedor.nroProveedor').value = "";
			}

			$('proveedorTexto').innerHTML = "&nbsp;";
			
			$("tituloFA").style.display = "none";
			$("tituloDP").style.display = "none";
			$("comboCalificacion").style.display = "none";	
			//$("iconoCargaInicial").style.display = "none";
			//$("iconoEvaluacionPeriodo").style.display = "none";
		}
		if(value==1) { //Desempenio Proveedor 
			if ($('evaluacionProveedor.calificacionEvaluacionProveedor.oid').value != 'undefined') {
				document.getElementsByName('evaluacionProveedor.calificacionEvaluacionProveedor.oid')[0].disabled=false;
				document.getElementsByName('evaluacionProveedor.calificacionEvaluacionProveedor.oid')[0].className ='textarea';
			}
			$('proveedorTexto').innerHTML = "";
			$("proveedorBusqueda").style.display = "block";
			$("tituloFA").style.display = "block";
			$("tituloDP").style.display = "none";
			$("comboCalificacion").style.display = "block";	
			//$("iconoCargaInicial").style.display = "none";
			//$("iconoEvaluacionPeriodo").style.display = "block";
		}
		if(value==2) { //Post Servicio 
			if ($('evaluacionProveedor.calificacionEvaluacionProveedor.oid').value != 'undefined') {
				document.getElementsByName('evaluacionProveedor.calificacionEvaluacionProveedor.oid')[0].disabled=false;
				document.getElementsByName('evaluacionProveedor.calificacionEvaluacionProveedor.oid')[0].className ='textarea';
			}
			$('proveedorTexto').innerHTML = "";
			$("proveedorBusqueda").style.display = "block";
			$("tituloFA").style.display = "none";
			$("tituloDP").style.display = "block";
			$("comboCalificacion").style.display = "block";	
			//$("iconoCargaInicial").style.display = "none";
			//$("iconoEvaluacionPeriodo").style.display = "block";
		}
	} 
}	

function cargaInicialEvaluacion(select) {
	
	value = select.options[select.selectedIndex].value;
	//verifico el valor del combo
	if(value==0) { //Es Carga Inicial = Seleccionar
		$("evaluacionProveedor.esCargaInicial").disabled=false;
		
		$("tituloFechaInicio").style.display = "none";
		$("tituloFechaFin").style.display = "none";
		$("campoFechaInicio").style.display = "none";
		$("campoFechaFin").style.display = "none";
		$("tituloTipoEvaluacion").style.display = "none";
		$("campoTipoEvaluacion").style.display = "none";
		$("tituloProveedor").style.display = "none";
		$("campoProveedor").style.display = "none";
		$("tituloICP").style.display = "none";
		$("campoICP").style.display = "none";
		$("tituloICS").style.display = "none";
		$("campoICS").style.display = "none";
		$("tituloEvaluacion").style.display = "none";
		$("campoEvaluacion").style.display = "none";
		
	}
		
	if(value==1) { //Es Carga Inicial = No
		$("evaluacionProveedor.esCargaInicial").disabled=true;
		
		$("tituloFechaInicio").style.display = "block";
		$("tituloFechaFin").style.display = "block";
		$("campoFechaInicio").style.display = "block";
		$("campoFechaFin").style.display = "block";
		$("tituloTipoEvaluacion").style.display = "block";
		$("campoTipoEvaluacion").style.display = "block";
		$("tituloProveedor").style.display = "block";
		$("campoProveedor").style.display = "block";
		$("tituloICP").style.display = "none";
		$("campoICP").style.display = "none";
		$("tituloICS").style.display = "none";
		$("campoICS").style.display = "none";
		$("tituloEvaluacion").style.display = "block";
		$("campoEvaluacion").style.display = "block";	
	}
	
	if(value==2) { //Es Carga Inicial = Si 
		$("evaluacionProveedor.esCargaInicial").disabled=true;
		
		$("tituloFechaInicio").style.display = "none";
		$("tituloFechaFin").style.display = "none";
		$("campoFechaInicio").style.display = "none";
		$("campoFechaFin").style.display = "none";
		$("tituloTipoEvaluacion").style.display = "block";
		$("campoTipoEvaluacion").style.display = "block";
		$("tituloProveedor").style.display = "block";
		$("campoProveedor").style.display = "block";
		$("tituloICP").style.display = "block";
		$("campoICP").style.display = "block";
		$("tituloICS").style.display = "block";
		$("campoICS").style.display = "block";
		$("tituloEvaluacion").style.display = "none";
		$("campoEvaluacion").style.display = "none";			
	}
	 
}	




function hideOrShowRow(obj) {

	if (obj.style.visibility == 'visible') {
		hideTrTd(obj);
	} else {
		showTrTd(obj);
	}
}

function showTrTd(obj) {
	obj.style.visibility = 'visible';
	obj.style.position = 'relative';
}

function hideTrTd(obj) {
	obj.style.visibility = 'hidden';
	obj.style.position = 'absolute';
}

function tipoContratoSelect(select) {

	value = select.options[select.selectedIndex].value;

	if(value == 0) { //No Aplica
		hideTrTd($('bloqueAlquiler'));
		hideTrTd($('bloquePublicidad'));
		hideTrTd($('bloqueDonacion'));
		hideTrTd($('bloqueAlquiler.labelOtro'));
		hideTrTd($('bloqueAlquiler.descripcionOtro'));

		$('tipoAlquiler').value = 0; 
		$('contrato.inmueble.oid').value = ""; 
		$('contrato.inmueble.descripcion').value = ""; 
		$('contrato.cartel.oid').value = ""; 
		$('contrato.cartel.descripcion').value = ""; 
		$('tipoPublicidad').value = 0; 
		$('tipoDonacion').value = 0; 
	}

	if(value == 1) { //Alquiler
		showTrTd($('bloqueAlquiler'));
		hideTrTd($('bloquePublicidad'));
		hideTrTd($('bloqueDonacion'));
		showTrTd($('bloqueAlquiler.labelOtro'));
		showTrTd($('bloqueAlquiler.descripcionOtro'));

		$('tipoAlquiler').value = 0; 
		$('contrato.inmueble.oid').value = ""; 
		$('contrato.inmueble.descripcion').value = ""; 
		$('contrato.cartel.oid').value = ""; 
		$('contrato.cartel.descripcion').value = ""; 
		$('tipoPublicidad').value = 0; 
		$('tipoDonacion').value = 0; 
	}

	if(value == 2) { //Donacion
		hideTrTd($('bloqueAlquiler'));
		hideTrTd($('bloquePublicidad'));
		showTrTd($('bloqueDonacion'));

		$('tipoAlquiler').value = 0; 
		$('contrato.inmueble.oid').value = ""; 
		$('contrato.inmueble.descripcion').value = ""; 
		$('contrato.cartel.oid').value = ""; 
		$('contrato.cartel.descripcion').value = ""; 
		$('tipoPublicidad').value = 0; 
		$('tipoDonacion').value = 0; 
	}

	if(value == 3) { //LocacionDeObra
		hideTrTd($('bloqueAlquiler'));
		hideTrTd($('bloquePublicidad'));
		hideTrTd($('bloqueDonacion'));
		hideTrTd($('bloqueAlquiler.labelOtro'));
		hideTrTd($('bloqueAlquiler.descripcionOtro'));

		$('tipoAlquiler').value = 0; 
		$('contrato.inmueble.oid').value = ""; 
		$('contrato.inmueble.descripcion').value = ""; 
		$('contrato.cartel.oid').value = ""; 
		$('contrato.cartel.descripcion').value = ""; 
		$('tipoPublicidad').value = 0; 
		$('tipoDonacion').value = 0; 
	}

	if(value == 4) { //LocacionDeServicio
		hideTrTd($('bloqueAlquiler'));
		hideTrTd($('bloquePublicidad'));
		hideTrTd($('bloqueDonacion'));
		hideTrTd($('bloqueAlquiler.labelOtro'));
		hideTrTd($('bloqueAlquiler.descripcionOtro'));

		$('tipoAlquiler').value = 0; 
		$('contrato.inmueble.oid').value = ""; 
		$('contrato.inmueble.descripcion').value = ""; 
		$('contrato.cartel.oid').value = ""; 
		$('contrato.cartel.descripcion').value = ""; 
		$('tipoPublicidad').value = 0; 
		$('tipoDonacion').value = 0; 
	}

	if(value == 5) { //Publicidad
		hideTrTd($('bloqueAlquiler'));
		showTrTd($('bloquePublicidad'));
		hideTrTd($('bloqueDonacion'));

		$('tipoAlquiler').value = 0; 
		$('contrato.inmueble.oid').value = ""; 
		$('contrato.inmueble.descripcion').value = ""; 
		$('contrato.cartel.oid').value = ""; 
		$('contrato.cartel.descripcion').value = ""; 
		$('tipoPublicidad').value = 0; 
		$('tipoDonacion').value = 0; 
	}
}

function tipoAlquilerSelect(select) {

	value = select.options[select.selectedIndex].value;

	if(value == 0) { //No Aplica
		showTrTd($('bloqueAlquiler.labelOtro'));
		showTrTd($('bloqueAlquiler.descripcionOtro'));
		hideTrTd($('labelInmueble'));
		hideTrTd($('descripcionInmueble'));
		hideTrTd($('labelCartel'));
		hideTrTd($('descripcionCartel'));

		$('contrato.inmueble.oid').value = ""; 
		$('contrato.inmueble.descripcion').value = ""; 
		$('contrato.cartel.oid').value = ""; 
		$('contrato.cartel.descripcion').value = ""; 

		$('inmuebleTexto').innerHTML = "";
		$('contratoTexto').innerHTML = "";
	}

	if(value == 1) { //Cartel
		hideTrTd($('bloqueAlquiler.labelOtro'));
		hideTrTd($('bloqueAlquiler.descripcionOtro'));
		hideTrTd($('labelInmueble'));
		hideTrTd($('descripcionInmueble'));
		showTrTd($('labelCartel'));
		showTrTd($('descripcionCartel'));

		$('contrato.inmueble.oid').value = ""; 
		$('contrato.inmueble.descripcion').value = ""; 
		$('contrato.cartel.oid').value = ""; 
		$('contrato.cartel.descripcion').value = ""; 

		$('inmuebleTexto').innerHTML = "";
		$('contratoTexto').innerHTML = "";
	}

	if(value == 2) { //Equipo
		showTrTd($('bloqueAlquiler.labelOtro'));
		showTrTd($('bloqueAlquiler.descripcionOtro'));
		hideTrTd($('labelInmueble'));
		hideTrTd($('descripcionInmueble'));
		hideTrTd($('labelCartel'));
		hideTrTd($('descripcionCartel'));

		$('contrato.inmueble.oid').value = ""; 
		$('contrato.inmueble.descripcion').value = ""; 
		$('contrato.cartel.oid').value = ""; 
		$('contrato.cartel.descripcion').value = ""; 

		$('inmuebleTexto').innerHTML = "";
		$('contratoTexto').innerHTML = "";
	}

	if(value == 3) { //Inmueble
		hideTrTd($('bloqueAlquiler.labelOtro'));
		hideTrTd($('bloqueAlquiler.descripcionOtro'));
		showTrTd($('labelInmueble'));
		showTrTd($('descripcionInmueble'));
		hideTrTd($('labelCartel'));
		hideTrTd($('descripcionCartel'));

		$('contrato.inmueble.oid').value = ""; 
		$('contrato.inmueble.descripcion').value = ""; 
		$('contrato.cartel.oid').value = ""; 
		$('contrato.cartel.descripcion').value = ""; 

		$('inmuebleTexto').innerHTML = "";
		$('contratoTexto').innerHTML = "";
	}
}

function tipoBeneficiarioSelect(select) {

	value = select.options[select.selectedIndex].value;

	if(value == 0) { //No Aplica
		showTrTd($('bloqueBeneficiario.labelOtro'));
		showTrTd($('bloqueBeneficiario.descripcionOtro'));
		hideTrTd($('labelProveedor'));
		hideTrTd($('descripcionProveedor'));
		hideTrTd($('labelPersona'));
		hideTrTd($('descripcionPersona'));
		hideTrTd($('bloqueBeneficiario.categoriaBeneficiario'));

		$('contrato.proveedor.nroProveedor').value = 0; 
		$('contrato.proveedor.detalleDePersona.razonSocial').value = ""; 
		$('contrato.proveedor.detalleDePersona.nombre').value = ""; 
		$('contrato.personaSecuencia.pk.identificador').value = 0; 
		$('contrato.personaSecuencia.pk.secuencia').value = -1; 
		$('contrato.personaSecuencia.razonSocial').value = ""; 
		$('contrato.personaSecuencia.nombre').value = ""; 
		$('categoriaBeneficiario').value = 0;

		$('proveedorTexto').innerHTML = "";
		$('personaTexto').innerHTML = "";
	}

	if(value == 1) { //Proveedor
		hideTrTd($('bloqueBeneficiario.labelOtro'));
		hideTrTd($('bloqueBeneficiario.descripcionOtro'));
		showTrTd($('labelProveedor'));
		showTrTd($('descripcionProveedor'));
		hideTrTd($('labelPersona'));
		hideTrTd($('descripcionPersona'));
		showTrTd($('bloqueBeneficiario.categoriaBeneficiario'));

		$('contrato.proveedor.nroProveedor').value = 0; 
		$('contrato.proveedor.detalleDePersona.razonSocial').value = ""; 
		$('contrato.proveedor.detalleDePersona.nombre').value = ""; 
		$('contrato.personaSecuencia.pk.identificador').value = 0; 
		$('contrato.personaSecuencia.pk.secuencia').value = -1; 
		$('contrato.personaSecuencia.razonSocial').value = ""; 
		$('contrato.personaSecuencia.nombre').value = ""; 
		$('categoriaBeneficiario').value = 0;

		$('proveedorTexto').innerHTML = "";
		$('personaTexto').innerHTML = "";
	}

	if(value == 2) { //Persona
		hideTrTd($('bloqueBeneficiario.labelOtro'));
		hideTrTd($('bloqueBeneficiario.descripcionOtro'));
		hideTrTd($('labelProveedor'));
		hideTrTd($('descripcionProveedor'));
		showTrTd($('labelPersona'));
		showTrTd($('descripcionPersona'));
		showTrTd($('bloqueBeneficiario.categoriaBeneficiario'));

		$('contrato.proveedor.nroProveedor').value = 0; 
		$('contrato.proveedor.detalleDePersona.razonSocial').value = ""; 
		$('contrato.proveedor.detalleDePersona.nombre').value = ""; 
		$('contrato.personaSecuencia.pk.identificador').value = 0; 
		$('contrato.personaSecuencia.pk.secuencia').value = -1; 
		$('contrato.personaSecuencia.razonSocial').value = ""; 
		$('contrato.personaSecuencia.nombre').value = ""; 
		$('categoriaBeneficiario').value = 0;

		$('proveedorTexto').innerHTML = "";
		$('personaTexto').innerHTML = "";
	}
}


	function changeOS(select){

		$('linkEnviarOS').style.display ='none';
		$('textoEnviarOS').style.display ='';
		$('linkImprimirOS').style.display ='none';
		$('textoImprimirOS').style.display ='';					
		var existeImpresion=false;
		var existeEnvio=false;
		for (i=0; i<document.getElementsByName('osSelect').length; i++){
			
			if (document.getElementsByName('osSelect')[i].checked==true && $(document.getElementsByName('osSelect')[i].value).name!='')
			{
			
				existeEnvio=true;
				
			}
			if (document.getElementsByName('osSelect')[i].checked==true && $(document.getElementsByName('osSelect')[i].value).name=='')
			{
			
				existeImpresion=true;
								
			}
		}	
		
		if(existeEnvio==true){	
							
				$('linkEnviarOS').style.display ='';
				$('textoEnviarOS').style.display ='none';
								
		}
	
		if(existeImpresion==true){	
							
				$('linkImprimirOS').style.display ='';
				$('textoImprimirOS').style.display ='none';					
					
		}			
	}
	

	function changeOC(select){			
		$('linkEnviarOC').style.display ='none';
		$('textoEnviarOC').style.display ='';
		$('linkImprimirOC').style.display ='none';
		$('textoImprimirOC').style.display ='';		
		var existeImpresion=false;
		var existeEnvio=false;
		for (i=0; i<document.getElementsByName('ocSelect').length; i++){
			if (document.getElementsByName('ocSelect')[i].checked==true && $(document.getElementsByName('ocSelect')[i].value).name!='')
			{
				existeEnvio=true;
				
			}
			if (document.getElementsByName('ocSelect')[i].checked==true && $(document.getElementsByName('ocSelect')[i].value).name=='')
			{
				existeImpresion=true;
								
			}
		}	
		
		if(existeEnvio==true){							
				$('linkEnviarOC').style.display ='';
				$('textoEnviarOC').style.display ='none';
								
		}
	
		if(existeImpresion==true){				
				$('linkImprimirOC').style.display ='';
				$('textoImprimirOC').style.display ='none';							
					
		}
						
	}
	

	function changePCservicio(select){			

		$('linkEnviarPCservicio').style.display ='none';
		$('textoEnviarPCservicio').style.display ='';
		$('linkImprimirPCservicio').style.display ='none';
		$('textoImprimirPCservicio').style.display ='';		
		var existeImpresion=false;
		var existeEnvio=false;
		for (i=0; i<document.getElementsByName('pcServicioSelect').length; i++){
			if (document.getElementsByName('pcServicioSelect')[i].checked==true && $(document.getElementsByName('pcServicioSelect')[i].value).name!='')
			{
				existeEnvio=true;
				
			}
			if (document.getElementsByName('pcServicioSelect')[i].checked==true && $(document.getElementsByName('pcServicioSelect')[i].value).name=='')
			{
				existeImpresion=true;
								
			}
		}	
		
		if(existeEnvio==true){							
				$('linkEnviarPCservicio').style.display ='';
				$('textoEnviarPCservicio').style.display ='none';
								
		}
	
		if(existeImpresion==true){				
				$('linkImprimirPCservicio').style.display ='';
				$('textoImprimirPCservicio').style.display ='none';							
					
		}
						
	}
	
	function changePCproductoBien(select){		
	
		$('linkEnviarPCproductoBien').style.display ='none';
		$('textoEnviarPCproductoBien').style.display ='';
		$('linkImprimirPCproductoBien').style.display ='none';
		//$('textoImprimirPCproductoBien').style.display ='';		
		var existeImpresion=false;
		var existeEnvio=false;
		for (i=0; i<document.getElementsByName('pcProductoBienSelect').length; i++){
			if (document.getElementsByName('pcProductoBienSelect')[i].checked==true && $(document.getElementsByName('pcProductoBienSelect')[i].value).name!='')
			{
				existeEnvio=true;
				
			}
			if (document.getElementsByName('pcProductoBienSelect')[i].checked==true && $(document.getElementsByName('pcProductoBienSelect')[i].value).name=='')
			{
				existeImpresion=true;
								
			}
		}	
		
		if(existeEnvio==true){							
				$('linkEnviarPCproductoBien').style.display ='';
				$('textoEnviarPCproductoBien').style.display ='none';
								
		}
	
		if(existeImpresion==true){				
				$('linkImprimirPCproductoBien').style.display ='';
				//$('textoImprimirPCproductoBien').style.display ='none';							
					
		}
						
	}
	
	
	
	
	
	function calcularMontoIVA(select){		
		
		var indice = $('facturaDetalleAlicuotaIVA.alicuotaIVA.oid').selectedIndex;
		var valor=round(
			 $('facturaDetalleAlicuotaIVA.facturaDetalle.importeGravado').value * 
			 $('facturaDetalleAlicuotaIVA.alicuotaIVA.oid').options[indice].text,4);		
		$('facturaDetalleAlicuotaIVA.montoIVA').value=valor/100;		
							
	}
	
	function calcularPorcentajeMedioCobroFactura(select){		
		
		var indice = $('facturaMedioPago.montoAsignado').value;		
		var importetotal= $('facturaMedioPago.factura.totalFactura').value;
		var valor=round((round(indice,2)/round(importetotal,2)),2)*100;		
		$('facturaMedioPago.porcentaje').value=valor;
						
	}
	
		//Round numbers
		function round(qnum, qdecimal) {
		   var decimal = Math.pow(10, Math.round(qdecimal));
		   var num = Math.round(qnum*decimal)/decimal;
		   
		    num = num.toFixed(2);
		 /*num = String(num);
		 var dot = num.indexOf(".");
		 if (num.substring(++dot).length < 2) e = num+'0';*/
		   
		   return num;
		}
	
		function calcularImporteGravado(select){
		$('facturaDetalle.importeGravado').value=$('facturaDetalle.precioUnitario').value;
		$('facturaDetalle.importeNoGravado').value=0;
							
	}
	
		
		function calcularImporteNoGravado(select){
		$('facturaDetalle.importeNoGravado').value=$('facturaDetalle.precioUnitario').value - $('facturaDetalle.importeGravado').value;
							
	}
	
	
	
	
	function listaFacturaDetalle(){
	document.getElementsByName('facturaDetalleLista')[0].value="";
	for (i=0; i<document.getElementsByName('checkFacturaDetalle').length; i++){				
		if (document.getElementsByName('checkFacturaDetalle')[i].checked==true)
			document.getElementsByName('facturaDetalleLista')[0].value=document.getElementsByName('facturaDetalleLista')[0].value+"#"+
			document.getElementsByName('checkFacturaDetalle')[i].value;
	}	
	
	}
	
	
	
	
	function pendienteSelectOCOS(select) {
	value = select.options[select.selectedIndex].value;
	if(value==0) { //No Aplica		
		$("oc_os.estado").disabled=false;	
		$("oc_os.estado").className ='textarea';
		document.getElementsByName('pendienteRecepcionVencido')[0].disabled=true;
		document.getElementsByName('pendienteRecepcionVencido')[0].value="0";		
		document.getElementsByName('pendienteRecepcionVencido')[0].className ='textareagris';

	}
	if(value==1) { //no
		$("oc_os.estado").disabled=true;
		$("oc_os.estado").className ='textareagris';	
		$("oc_os.estado").value="0";	
		document.getElementsByName('pendienteRecepcionVencido')[0].disabled=true;
		document.getElementsByName('pendienteRecepcionVencido')[0].value="0";		
		document.getElementsByName('pendienteRecepcionVencido')[0].className ='textareagris';

	}
	if(value==2) { //si
		$("oc_os.estado").disabled=true;
		$("oc_os.estado").className ='textareagris';	
		$("oc_os.estado").value="0";
		document.getElementsByName('pendienteRecepcionVencido')[0].disabled=false;
		document.getElementsByName('pendienteRecepcionVencido')[0].value="0";		
		document.getElementsByName('pendienteRecepcionVencido')[0].className ='textarea';

	}
	}
	
	function formaEnvioCorrespondencia(select) {
 
	 value = select.options[select.selectedIndex].value; 
	 if(value==1) { //no
	   $("proveedor.bolsaCorreo.codigo").disabled=false; 
	   $("proveedor.bolsaCorreo.codigo").className ='textarea';  
	 }
	 else{
	   $("proveedor.bolsaCorreo.codigo").disabled=true;
	   $("proveedor.bolsaCorreo.codigo").className ='textareagris';
	   $("proveedor.bolsaCorreo.codigo").value =0;
	 }
 
 }
	
	function ayudaMenu() {
	
		window.open('../common/ayuda/indice.html','mywindow','width=800,height=600,resizable=yes,scrollbars=yes,left=100,top=80,toolbar=yes,menubar=yes');
		
	}
	
	function desplegarDescripcionMaestro() {
		document.getElementsByName('descripcionMaestro')[0].style.display='block';
	}
	
	
	function desplegarMaestroCliente() {
		document.getElementsByName('maestroCliente')[0].style.display='block';
	}
	
	function desplegarMaestroDeposito() {
		document.getElementsByName('maestroDeposito')[0].style.display='block';
	}
	
	function desplegarDescripcionDeposito() {
		document.getElementsByName('descripcionDeposito')[0].style.display='block';
	}
	
	function desplegarDepositoAjusteExistencia() {
		document.getElementsByName('depositoAjusteExistencia')[0].style.display='block';
	}
	
	
	function formaEnvioCorrespondenciaOP(select) {
	
	value = select.options[select.selectedIndex].value;	
	if(value==1) { //no
			$("ordenPago.bolsaCorreo.codigo").disabled=false;	
			$("ordenPago.bolsaCorreo.codigo").className ='textarea';		
	}
	else{
			$("ordenPago.bolsaCorreo.codigo").disabled=true;
			$("ordenPago.bolsaCorreo.codigo").className ='textareagris';
			$("ordenPago.bolsaCorreo.codigo").value =0;
	}
	
	}
	
	
	
		function listaOPSelectLink(select){				
		
		var cadenaOriginal=$("imprimirOP").href.split('?')[0];		
		var parametros='';
		document.getElementsByName('opStringList')[0].value='';
		for (i=0; i<document.getElementsByName('opSelect').length; i++){				
			if (document.getElementsByName('opSelect')[i].checked==true)
			{
				var valor=document.getElementsByName('opSelect')[i].value;
				document.getElementsByName('opStringList')[0].value=document.getElementsByName('opStringList')[0].value+"_"+valor;
				parametros=parametros+"_"+valor;
			}
		}				

		var parametrosFinal="opStringList="+parametros;				
		$("imprimirOP").href=cadenaOriginal+'?' + parametrosFinal ;				
	}	
	
	
	
	
	
	function actualizarFechaVencimFactura(select){
		$("factura.fechaVencimiento").value=$("factura.fechaEmision").value;
	}
	
	
	
function clienteBusquedaSelect(select) {
	value = select.options[select.selectedIndex].value;	
	if(value==1) { //interno		
		$("clasificacion").disabled=true;		
		$("clasificacion").className ='textareagris';
		$("clasificacion").value="0"; 					
	}
	else{ //externo	
		$("clasificacion").disabled=false;
		$("clasificacion").className ='textarea';
	}
}

function checkAll(field) {

	for (i = 0; i < field.length; i++) {

		field[i].checked = true ;
	}
}

function uncheckAll(field) {

	for (i = 0; i < field.length; i++) {
		field[i].checked = false ;
	}
}

function seleccionarOPs() {

	checkAll(document.getElementsByName('opSelect'));
}

function deseleccionarOPs() {

	uncheckAll(document.getElementsByName('opSelect'));
}

function checkAllFacturas() {
	checkAll(document.getElementsByName('selectedFacturas'));
	calcularTotalFacturasAutorizar();
}

function uncheckAllFacturas() {
	uncheckAll(document.getElementsByName('selectedFacturas'));
	calcularTotalFacturasAutorizar();
}


function seleccionarAutorizar() {
	
	var sf = document.getElementsByName('selectedFacturas');
	var sfC = document.getElementById('selectedFacturasChecks');
	sfC.value='';
	for (i = 0; i < sf.length; i++) {
		var f = sf[i]; 
		//if (typeof f.checked == 'undefined'){
		//	f.checked = false ;
		//}
		if(f.checked){
			if(sfC.value == ''){
				sfC.value = f.value; 
			}else{
				sfC.value = sfC.value + "#" + f.value;
			}
		}
	}
}

function condicionFiscalIVASelect(select) {

	value = select.options[select.selectedIndex].value;	

	if (value=="") { // seleccionar		

		$("inscriptoGanancia").disabled=true;		
		$("inscriptoGanancia").className ='textareagris';
		$("inscriptoGanancia").value="-1"; 					

	} else if (value=="RMT") { // responsable monotributo		

		$("inscriptoGanancia").disabled=true;		
		$("inscriptoGanancia").className ='textareagris';
		$("inscriptoGanancia").value="-1"; 					

	} else {

		$("inscriptoGanancia").disabled=false;		
		$("inscriptoGanancia").className ='textarea';
		$("inscriptoGanancia").value="-1"; 					
	}

}





function estadoComprobanteSelect(select) {	
	value = select.options[select.selectedIndex].value;
	if(value==0) { //en preparacion
		$("factura.conOPAutorizada").disabled=false;	
		$("factura.conOPAutorizada").className ='textarea';
		$("factura.conOPAutorizada").value="0";		
		
	}
	if(value==1 || value==2 || value==4 ) { //No Aplica	
		$("factura.conOPAutorizada").disabled=true;
		$("factura.conOPAutorizada").className ='textareagris';	
		$("factura.conOPAutorizada").value="1";			

	}
	if(value==3) { //pendiente pago
		$("factura.conOPAutorizada").disabled=false;	
		$("factura.conOPAutorizada").className ='textarea';		
		$("factura.conOPAutorizada").value="0";			
	}
	
	}
function keypressCodigoBarra(select) {
	alert(select);
	alert(window);	
	alert('evento');
	alert(Event.KEY_RETURN);
	alert(select.keyCode);
	alert(document);	
	alert('On KEY PRESS');
}

function hola(e) {
alert('ON Click');	

}

function noNumbers(e)
{
	var keynum;
	var keychar;
	var numcheck;
	if(window.event) // IE
	{
		keynum = e.keyCode;		
	}
	else if(e.which) // Netscape/Firefox/Opera
	{
		keynum = e.which;
	}	
	if(Event.KEY_RETURN==keynum){		
		 document.getElementById('validarCodigoBarra').focus();
		 var r = document.getElementById('validarCodigoBarra').addEventListener("onClick",true,false);
		 return r;
	}
} 

function clickCodigoBarra(select){
	 document.getElementById('codigoBarra').focus();
}

function periodoEvaluacionSelect(select) {

	value = select.options[select.selectedIndex].value;
	$("periodoEvaluacion.oid").value = value;
}

function fscListLinkOnClick() {				

	var a = $("fscListLink").href.split('?');
	var cadenaOriginal = a[0];
	var cadenaParametros = a[1];
	var arrayParametros = cadenaParametros.split('&');

	var nuevoArrayParametros = new Array();
	for (i = 0; i < arrayParametros.length; i++) {

		if (arrayParametros[i].indexOf("periodoEvaluacion.oid") == -1) {

			nuevoArrayParametros.push(arrayParametros[i]);
		} else {

			nuevoArrayParametros.push("periodoEvaluacion.oid=" + $("periodo").value);
		}
	}

	var nuevaCadenaParametros = nuevoArrayParametros.join("&");
	var cadenaEnvio = cadenaOriginal + "?" + nuevaCadenaParametros;

	$("fscListLink").href = cadenaEnvio;				
}


function frecuenciaPago(select) {
	 
	 value = select.options[select.selectedIndex].value;
	 
    if(value==-1 || value == 0) { //seleccionar o no aplica --> 4 campos inhabilitados 
	    	$("diaPago1").disabled=true;
	    	$("diaPago1").className ='textareagris';
	    	$("diaPago1").value ='';
	    	$("diaPago2").disabled=true;
	    	$("diaPago2").className ='textareagris';
	    	$("diaPago2").value ='';
	    	$("diaPago3").disabled=true;
	    	$("diaPago3").className ='textareagris';
	    	$("diaPago3").value ='';
	    	$("diaPago4").disabled=true;
	    	$("diaPago4").className ='textareagris';
	    	$("diaPago4").value ='';
	    }
	    if(value==1 ) { //semanal --> 4 campos habilitados
	    	$("diaPago1").disabled=false;
	    	$("diaPago1").className ='textarea';
	    	$("diaPago2").disabled=false;
	    	$("diaPago2").className ='textarea';
	    	$("diaPago3").disabled=false;
	    	$("diaPago3").className ='textarea';
	    	$("diaPago4").disabled=false;
	    	$("diaPago4").className ='textarea';
	    }
	    
	    if(value==2 ) { //quincenal --> 2 campos habilitados
	    	$("diaPago1").disabled=false;
	    	$("diaPago1").className ='textarea';
	    	$("diaPago2").disabled=false;
	    	$("diaPago2").className ='textarea';
	    	$("diaPago3").disabled=true;
	    	$("diaPago3").className ='textareagris';
	    	$("diaPago3").value ='';
	    	$("diaPago4").disabled=true;
	    	$("diaPago4").className ='textareagris';
	    	$("diaPago4").value ='';
	    }
	    if(value==3 ) { //mensual --> 1 campo habilitado
	    	$("diaPago1").disabled=false;
	    	$("diaPago1").className ='textarea';
	    	$("diaPago2").disabled=true;
	    	$("diaPago2").className ='textareagris';
	    	$("diaPago2").value ='';
	    	$("diaPago3").disabled=true;
	    	$("diaPago3").className ='textareagris';
	    	$("diaPago3").value ='';
	    	$("diaPago4").disabled=true;
	    	$("diaPago4").className ='textareagris';
	    	$("diaPago4").value ='';
	    }
}

function refreshFacturaFormaPago(){
	
	var cfa = document.getElementById('facturaMedioPago.formaPago.conFechaAsignada');
	fa = document.getElementById('fechaAsignadaTR');
	if (cfa != null && cfa.value=='true'){
		fa.style.display='';
	}else{
		fa.style.display='none';
		document.getElementById('cantidadDias').value = '';
		document.getElementById('fechaAsignada').value = '';
	}
}

function refreshFondoFijoFormaPago(){
	
	var cfa = document.getElementById('fondoFijoMedioPago.formaPago.conFechaAsignada');
	fa = document.getElementById('fechaAsignadaTR');
	if (cfa != null && cfa.value=='true'){
		fa.style.display='';
	}else{
		fa.style.display='none';
		document.getElementById('cantidadDias').value = '';
		document.getElementById('fechaAsignada').value = '';
	}
}

function resizeWindows(){
	if (jQuery( "#capaMenu" ).is(':visible')){
			jQuery( "#contentTrx" ).width(jQuery( "#container" ).width() - jQuery( "#flecha" ).width() - jQuery( "#capaMenu" ).width() - 16 );
			jQuery("#capaMenu").height(jQuery( "#container" ).height());
		}else{
			jQuery( "#contentTrx" ).width(jQuery( "#container" ).width() - jQuery( "#flecha" ).width() - 16 );
		}
}


function calcularTotalFacturasAutorizar(select){	
	calcularTotalFacturasAutorizar();
 }

function calcularTotalFacturasAutorizar(){	
	var totalseleccionado=0;
	for (i=0; i<document.getElementsByName('selectedFacturas').length; i++){				
		if (document.getElementsByName('selectedFacturas')[i].checked==true){			
			var subtotal=0;
			subtotal = parseFloat( $(document.getElementsByName('selectedFacturas')[i].value).value);			
			var tipoFactura =  $(document.getElementsByName('selectedFacturas')[i].value+"_tipoFactura").value			
			if( parseInt(tipoFactura) == 2){
				totalseleccionado = parseFloat(totalseleccionado - subtotal);	
			}
			else{
				totalseleccionado = parseFloat(totalseleccionado + subtotal);
			} 			
		}
	}	
	
	Number.prototype.formatMoney = function(places, symbol, thousand, decimal) {
		places = !isNaN(places = Math.abs(places)) ? places : 2;
		symbol = symbol !== undefined ? symbol : "$";
		thousand = thousand || ",";
		decimal = decimal || ".";
		var number = this, 
		    negative = number < 0 ? "-" : "",
		    i = parseInt(number = Math.abs(+number || 0).toFixed(places), 10) + "",
		    j = (j = i.length) > 3 ? j % 3 : 0;
		return symbol + negative + (j ? i.substr(0, j) + thousand : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousand) + (places ? decimal + Math.abs(number - i).toFixed(places).slice(2) : "");
	};
	
	document.getElementById("tdTotalFacturas").innerHTML=
		'<strong>' +  totalseleccionado.formatMoney(2, "", ".", ",") +'</strong>';	
 }

