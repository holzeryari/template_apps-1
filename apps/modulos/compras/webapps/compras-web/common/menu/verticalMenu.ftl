<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>



<!-- menu -->
<div id="capaItemsMenu" class="capaItemsMenu">
		
		<ul id="listaMenu">
		<@vc.anchors target="contentTrx">
			<li id="limaestros">
				<@security.a
					templateDir="menutemplates" 
					securityCode="CUF0740" 
					id="maestros"				
					cssClass="cursorMenu"
					onclick="javascript:showhide('maestros')">
					<img src="${request.contextPath}/common/images/listaContraida.png" border="0" align="left" id="menuContmaestros">
					<img src="${request.contextPath}/common/images/listaDesplegada.png" border="0" align="left" id="menuDespmaestros" class="hide">
					Maestros 
				</@security.a>
				<div id="areasubmenumaestros" class="hide">
					<div>
						<ul>
							<li>
								<@security.a 
									templateDir="menutemplates" 
									securityCode="CUF0001" 
									label="Clientes"
									href="${request.contextPath}/maestro/cliente/view.action?flowControl=init&amp;navigationId=buscar-clientes">
								</@security.a>
							</li>
								
							<li>
								<@security.a 
									templateDir="menutemplates" 
									securityCode="CUF0020" 
									label="Dep&oacute;sitos"
									href="${request.contextPath}/maestro/deposito/view.action?flowControl=init&amp;navigationId=buscar-depositos">
								</@security.a>
							</li>		
							<li>
								<@security.a 
									templateDir="menutemplates" 
									securityCode="CUF0040" 
									label="Rubros"
									href="${request.contextPath}/maestro/rubro/view.action?flowControl=init&amp;navigationId=buscar-rubros">
								</@security.a>
							</li>
							<li>
								<@security.a 
									templateDir="menutemplates" 
									securityCode="CUF0040" 
									label="Equipo de Ingreso"
									href="${request.contextPath}/maestro/equipo/view.action?flowControl=init&amp;navigationId=buscar-equipos">
								</@security.a>
							</li>
							<li>
								<@security.a 
									templateDir="menutemplates" 
									securityCode="CUF0580" 
									label="Tipo Unidades"
									href="${request.contextPath}/maestro/tipoUnidad/view.action?flowControl=init&amp;navigationId=buscar-tipoUnidad">
								</@security.a>
							</li>
							<li>
								<@security.a 
									templateDir="menutemplates" 
									securityCode="CUF0060" 
									label="Productos/Bienes"
									href="${request.contextPath}/maestro/productoBien/view.action?flowControl=init&amp;navigationId=buscar-productosBienes">
								</@security.a>
							</li>
							<li>		
								<@security.a 
									templateDir="menutemplates" 
									securityCode="CUF0080" 
									label="Servicios"
									href="${request.contextPath}/maestro/servicio/view.action?flowControl=init&amp;navigationId=buscar-servicios">
								</@security.a>	
							</li>
							<li>			
								<@security.a 
									templateDir="menutemplates" 
									securityCode="CUF0100" 
									label="Personas"
									href="${request.contextPath}/maestro/persona/view.action?flowControl=init&amp;navigationId=buscar-personas">
								</@security.a>	
							</li>
							<li>
								<@security.a 
									templateDir="menutemplates" 
									securityCode="CUF0120" 
									label="Proveedores"
									href="${request.contextPath}/maestro/proveedor/view.action?flowControl=init&amp;navigationId=buscar-proveedores">
								</@security.a>	
							</li>
								
							<li>				
								<@security.a 
									templateDir="menutemplates" 
									securityCode="CUF0306" 
									label="Carteles"
									href="${request.contextPath}/maestro/carteleria/view.action?flowControl=init&amp;navigationId=buscar-carteleria">
								</@security.a>
							</li>	
						</ul>	
					</div>
				</div>
			</li>
					
			<li id="lidepositos">
				<@security.a 
					templateDir="menutemplates" 
					securityCode="CUF0741" 
					href=""
					id="depositos"
					cssClass="cursorMenu"
					onclick="javascript:showhide('depositos')">
					<img src="${request.contextPath}/common/images/listaContraida.png" border="0" align="left" id="menuContdepositos">
					<img src="${request.contextPath}/common/images/listaDesplegada.png" border="0" align="left" id="menuDespdepositos" class="hide">
					Dep&oacute;sitos
				</@security.a>
				<div id="areasubmenudepositos" class="hide">
					<div>
						<ul id="submenu2">
						    <li>
							<@security.a 
								templateDir="menutemplates" 
								securityCode="CUF0140" 
								label="Ajustar Existencia de Productos"
								href="${request.contextPath}/deposito/ajusteExistencia/view.action?flowControl=init&amp;navigationId=buscar-ajusteExistencia">
							</@security.a>
							</li>
							
							<li>			
							<@security.a 
								templateDir="menutemplates" 
								securityCode="CUF0160" 
								label="Egresar Productos"
								href="${request.contextPath}/deposito/egresoProducto/view.action?flowControl=init&amp;navigationId=buscar-egresoProducto">
							</@security.a>
							</li>
							
							<li>	
							<@security.a 
								templateDir="menutemplates" 
								securityCode="CUF0180" 
								label="Ingresar Productos"
								href="${request.contextPath}/deposito/ingresoProducto/view.action?flowControl=init&amp;navigationId=buscar-ingresoProducto">
							</@security.a>	
							</li>
							
							<li>			
							<@security.a 
								templateDir="menutemplates" 
								securityCode="CUF0440" 
								label="Recepcionar Productos/Bienes"
								href="${request.contextPath}/deposito/recepcionProductoBien/view.action?flowControl=init&amp;navigationId=buscar-recepcionProductoBien">
							</@security.a>
							</li>
						
							<li>
							<@security.a 
								templateDir="menutemplates" 
								securityCode="CUF0460" 
								label="Devolver Productos/Bienes"
								href="${request.contextPath}/deposito/devolucionProductoBien/view.action?flowControl=init&amp;navigationId=buscar-devolucionProductoBien">
							</@security.a>	
							</li>
							
							<li>	
							<@security.a 
								templateDir="menutemplates" 
								securityCode="CUF0200" 
								label="Consultar Existencias de Productos"
								href="${request.contextPath}/deposito/consultaExistencia/view.action?flowControl=init&amp;navigationId=buscar-existencias">
							</@security.a>	
							</li>
							
							<li>			
							<@security.a 
								templateDir="menutemplates" 
								securityCode="CUF0480" 
								label="Consultar Movimientos de Productos"
								href="${request.contextPath}/deposito/consultaMovimiento/view.action?flowControl=init&amp;navigationId=buscar-movimiento">
							</@security.a>
							</li>
							
							<li>	
							<@security.a 
								templateDir="menutemplates" 
								securityCode="CUF0500" 
								label="Consultar Consumos de Productos"
								href="${request.contextPath}/producto/consumoProducto/view.action?flowControl=init&amp;navigationId=buscar-consumoProducto">
							</@security.a>	
							</li>
						  </ul>		
					</div>
				</div>
			</li>
			<li id="libienes">
				<@security.a 
					templateDir="menutemplates" 
					securityCode="CUF0742" 
					href=""
					id="bienes"
					cssClass="cursorMenu"
					onclick="javascript:showhide('bienes')">
					<img src="${request.contextPath}/common/images/listaContraida.png" border="0" align="left" id="menuContbienes">
					<img src="${request.contextPath}/common/images/listaDesplegada.png" border="0" align="left" id="menuDespbienes" class="hide">
					Bienes 
				</@security.a>
				<div id="areasubmenubienes" class="hide">
					<div>
						<ul id="submenu3">
							<li>
							<@security.a 
								templateDir="menutemplates" 
								securityCode="CUF0220" 
								label="Inventariar Bienes"
								href="${request.contextPath}/bien/inventarioBien/view.action?flowControl=init&amp;navigationId=buscar-inventarioBien">
							</@security.a>
							</li>
							<li>			
							<@security.a 
								templateDir="menutemplates" 
								securityCode="CUF0240" 
								label="Gestionar Bienes Inventariados"
								href="${request.contextPath}/bien/inventarioBien/viewBienInventariado.action?flowControl=init&amp;navigationId=buscar-bienInventariado">
							</@security.a>
							</li>
							<li>	
							<@security.a 
								templateDir="menutemplates" 
								securityCode="CUF0260" 
								label="Transferir Bienes"
								href="${request.contextPath}/bien/transferenciaBien/view.action?flowControl=init&amp;navigationId=buscar-transferenciaBien">
							</@security.a>
							</li>
							<li>	
							<@security.a 
								templateDir="menutemplates" 
								securityCode="CUF0280" 
								label="Embargar Bienes"
								href="${request.contextPath}/bien/embargoBien/view.action?flowControl=init&amp;navigationId=buscar-embargoBien">
							</@security.a>
							</li>
						</ul>			
					</div>
				</div>
			</li>
			<li id="licompras">
				<@security.a 
					templateDir="menutemplates" 
					securityCode="CUF0743" 
					href=""
					id="compras"
					cssClass="cursorMenu"
					onclick="javascript:showhide('compras')">
					<img src="${request.contextPath}/common/images/listaContraida.png" border="0" align="left" id="menuContcompras">
					<img src="${request.contextPath}/common/images/listaDesplegada.png" border="0" align="left" id="menuDespcompras" class="hide">
					Compras y Contrataciones
				</@security.a>
				<div id="areasubmenucompras" class="hide">	
					<div>
						<ul id="submenu4">
						  <li>
							<@security.a 
								templateDir="menutemplates" 
								securityCode="CUF0320" 
								label="Gestionar FSC/FSS"
								href="${request.contextPath}/compraContratacion/fsc_fss/view.action?flowControl=init&amp;navigationId=buscar-fsc_fss">
							</@security.a>	
							</li>
							<li>
							<@security.a 
								templateDir="menutemplates" 
								securityCode="CUF0340" 
								label="Asignar Proveedores a FSC/FSS"
								href="${request.contextPath}/compraContratacion/asignacionProveedor/view.action?flowControl=init&amp;navigationId=buscar-asignacionProveedor">
							</@security.a>
							</li>
							<li>			
							<@security.a 
								templateDir="menutemplates" 
								securityCode="CUF0340" 
								label="Gestionar Pedidos de Cotizaci&oacute;n"
								href="${request.contextPath}/compraContratacion/pedidoCotizacion/view.action?flowControl=init&amp;navigationId=buscar-pedidoCotizacion">
							</@security.a>
							</li>
							<li>
							<@security.a 
								templateDir="menutemplates" 
								securityCode="CUF0380" 
								label="Gestionar OC/OS"
								href="${request.contextPath}/compraContratacion/oc_os/view.action?flowControl=init&amp;navigationId=buscar-oc_os">
							</@security.a>
							</li>
						</ul>				
					</div>
				</div>
			</li>
			<li id="liservicios">
				<@security.a 
					templateDir="menutemplates" 
					securityCode="CUF0744" 
					href=""
					id="servicios"
					cssClass="cursorMenu"
					onclick="javascript:showhide('servicios')">
					<img src="${request.contextPath}/common/images/listaContraida.png" border="0" align="left" id="menuContservicios">
					<img src="${request.contextPath}/common/images/listaDesplegada.png" border="0" align="left" id="menuDespservicios" class="hide"> 
					Servicios
				</@security.a>
				<div id="areasubmenuservicios" class="hide">
					<div>
						<ul id="submenu5">
							<li>
							<@security.a 
								templateDir="menutemplates" 
								securityCode="CUF0620" 
								label="Recepcionar Servicios"
								href="${request.contextPath}/servicio/prestacionServicio/view.action?flowControl=init&amp;navigationId=buscar-prestacionServicio">
							</@security.a>
							</li>
							<li>
							<@security.a 
								templateDir="menutemplates" 
								securityCode="CUF0600" 
								label="Evaluar OS"
								href="${request.contextPath}/servicio/evaluacionOS/view.action?flowControl=init&amp;navigationId=buscar-evaluacionOS">
							</@security.a>	
							</li>
						</ul>			
					</div>
				</div>
			</li>
			<li id="liproveedores">
				<@security.a 
					templateDir="menutemplates" 
					securityCode="CUF0745" 
					href=""
					id="proveedores"
					cssClass="cursorMenu"
					onclick="javascript:showhide('proveedores')">
					<img src="${request.contextPath}/common/images/listaContraida.png" border="0" align="left" id="menuContproveedores">
					<img src="${request.contextPath}/common/images/listaDesplegada.png" border="0" align="left" id="menuDespproveedores" class="hide">
					Proveedores
				</@security.a>
				<div id="areasubmenuproveedores" class="hide">
					<div>
						<ul id="submenu6">
							<li>
							<@security.a 
								templateDir="menutemplates" 
								securityCode="CUF0680" 
								label="Calificaciones de Proveedores"
								href="${request.contextPath}/proveedor/calificacionEvaluacionProveedor/view.action?flowControl=init&amp;navigationId=buscar-calificacionEvaluacionProveedor">
							</@security.a>
							</li>
							<li>
							<@security.a 
								templateDir="menutemplates" 
								securityCode="CUF0400" 
								label="Per&iacute;odos de Evaluaci&oacute;n"
								href="${request.contextPath}/proveedor/periodoEvaluacion/view.action?flowControl=init&amp;navigationId=buscar-periodoEvaluacion">
							</@security.a>
							</li>
							<li>
							<@security.a 
								templateDir="menutemplates" 
								securityCode="CUF0660" 
								label="Advertencias de Proveedores"
								href="${request.contextPath}/proveedor/advertenciaProveedor/view.action?flowControl=init&amp;navigationId=buscar-advertenciaProveedor">
							</@security.a>
							</li>
							<li>
							<@security.a 
								templateDir="menutemplates" 
								securityCode="CUF0420" 
								label="Evaluar Proveedores"
								href="${request.contextPath}/proveedor/evaluacionProveedor/view.action?flowControl=init&amp;navigationId=buscar-evaluacionProveedor">
							</@security.a>
							</li>
							<li>
							<@security.a 
								templateDir="menutemplates" 
								securityCode="CUF0800" 
								label="Consultar Cuentas de Proveedores"
								href="${request.contextPath}/proveedor/consultaCuentaProveedor/view.action?flowControl=init&amp;navigationId=buscar-consultaCuentaProveedor">
							</@security.a>
							</li>
						</ul>	
					</div>
				</div>
			</li>
			 <#--<li>
				<@security.a 
					templateDir="menutemplates" 
					securityCode="CUF0746" 
					href=""
					cssClass="cursorMenu"
					<img src="${request.contextPath}/common/images/listaContraida.png" border="0" align="left" id="menuCont">
					<img src="${request.contextPath}/common/images/listaDesplegada.png" border="0" align="left" id="menuDesp" class="hide">	
					Carteleri&acute;a
				</@security.a>
				<div id="areasubmenu" class="hide">
					<div>
						<@security.a 
							templateDir="menutemplates" 
							securityCode="CUF0306" 
							label="Gestionar Carteles"
							href="${request.contextPath}/carteleria/view.action?flowControl=init&amp;navigationId=buscar-carteleria">
						</@security.a>
					</div>
				</div>
			</li>-->
			
			<li id="licontratos">
				<@security.a 
					templateDir="menutemplates" 
					securityCode="CUF0747" 
					href=""
					id="contratos"
					cssClass="cursorMenu"
					onclick="javascript:showhide('contratos')"> 
					<img src="${request.contextPath}/common/images/listaContraida.png" border="0" align="left" id="menuContcontratos">
					<img src="${request.contextPath}/common/images/listaDesplegada.png" border="0" align="left" id="menuDespcontratos" class="hide">
					Contratos
				</@security.a>
				<div id="areasubmenucontratos" class="hide">
					<div>
						<ul id="submenu8">
							<li>
							<@security.a 
								templateDir="menutemplates" 
								securityCode="CUF0520" 
								label="Inmuebles"
								href="${request.contextPath}/inmueble/view.action?flowControl=init&amp;navigationId=buscar-inmueble">
							</@security.a>
							</li>
							
							<li>
							<@security.a 
								templateDir="menutemplates" 
								securityCode="CUF0306" 
								label="Gestionar Contratos, Acuerdos y Convenios"
								href="${request.contextPath}/contrato/view.action?flowControl=init&amp;navigationId=buscar-contrato">
							</@security.a>
							</li>
						</ul>
					</div>
				</div>
			</li>
			<li id="liclientes">
				<@security.a 
					templateDir="menutemplates" 
					securityCode="CUF0748" 
					href=""
					id="clientes"
					cssClass="cursorMenu"
					onclick="javascript:showhide('clientes')">
					<img src="${request.contextPath}/common/images/listaContraida.png" border="0" align="left" id="menuContclientes">
					<img src="${request.contextPath}/common/images/listaDesplegada.png" border="0" align="left" id="menuDespclientes" class="hide"> 
					Clientes
				</@security.a>
				<div id="areasubmenuclientes" class="hide">
					<div>
						<ul id="submenu9">	
							<li>
							<@security.a 
								templateDir="menutemplates" 
								securityCode="CUF0560" 
								label="Recepcionar Bienes desde Clientes"
								href="${request.contextPath}/cliente/recepcionBien/view.action?flowControl=init&amp;navigationId=buscar-recepcionBien">
							</@security.a>
							</li>
							<li>
							<@security.a 
								templateDir="menutemplates" 
								securityCode="CUF0700" 
								label="Gestionar Fondos Fijos"
								href="${request.contextPath}/comprobante/fondoFijo/view.action?flowControl=init&amp;navigationId=buscar-fondoFijo">
							</@security.a>
							</li>
						</ul>
					</div>
				</div>
			</li>
			<li id="licomprobantes">
				<@security.a 
					templateDir="menutemplates" 
					securityCode="CUF0749" 
					href=""
					id="comprobantes"
					cssClass="cursorMenu"
					onclick="javascript:showhide('comprobantes')">
					<img src="${request.contextPath}/common/images/listaContraida.png" border="0" align="left" id="menuContcomprobantes">
					<img src="${request.contextPath}/common/images/listaDesplegada.png" border="0" align="left" id="menuDespcomprobantes" class="hide"> 
					Comprobantes
				</@security.a>
				<div id="areasubmenucomprobantes" class="hide">
					<div>
						<ul id="submenu10">
							<li>
							<@security.a 
								templateDir="menutemplates" 
								securityCode="CUF0640" 
								label="Gestionar Comprobantes"
								href="${request.contextPath}/comprobante/factura/view.action?flowControl=init&amp;navigationId=buscar-factura">
							</@security.a>
							</li>
							<li>
							<@security.a 
								templateDir="menutemplates" 
								securityCode="CUF0720" 
								label="Rendicion y Reintegro de Gastos"
								href="${request.contextPath}/comprobante/rendicionReintegroGastos/view.action?flowControl=init&amp;navigationId=buscar-rendicionReintegroGastos">
							</@security.a>
							</li>
							<li>
							<@security.a 
								templateDir="menutemplates" 
								securityCode="CUF0780" 
								label="Consultar Imputaciones por Rubro"
								href="${request.contextPath}/comprobante/consultaImputacionRubro/view.action?flowControl=init&amp;navigationId=buscar-consultaImputacionRubro">
							</@security.a>
							</li>
							<li>
							<@security.a 
								templateDir="menutemplates" 
								securityCode="CUF0820" 
								label="Consultar Imputaciones por L&iacute;neas Presupuestarias"
								href="${request.contextPath}/comprobante/consultaImputacionLinea/view.action?flowControl=init&amp;navigationId=buscar-consultaImputacionLinea">
							</@security.a>
							</li>
						</ul>
					</div>
				</div>
			</li>
			<li id="lipagos">
				<@security.a 
					templateDir="menutemplates" 
					securityCode="CUF0751" 
					href=""
					id="pagos"
					cssClass="cursorMenu"
					onclick="javascript:showhide('pagos')">
					<img src="${request.contextPath}/common/images/listaContraida.png" border="0" align="left" id="menuContpagos">
					<img src="${request.contextPath}/common/images/listaDesplegada.png" border="0" align="left" id="menuDesppagos" class="hide">
					Pagos
				</@security.a>
				<div id="areasubmenupagos" class="hide">
					<div>
						<ul id="submenu11">
							<li>
							<@security.a 
								templateDir="menutemplates" 
								securityCode="CUF0760" 
								label="Gestionar Orden de Pago"
								href="${request.contextPath}/pago/view.action?flowControl=init&amp;navigationId=buscar-ordenPago">
							</@security.a>
							</li>
						</ul>
					</div>
				</div>
			</li>
			</@vc.anchors>
			
			<li>
				<a target="_blank" 
				href="${request.contextPath}/ayuda/ayudaIndiceView.action">				
				<img src="${request.contextPath}/common/images/ayudaIndice.gif" border="0" align="left" alt="Manual de Usuario" >Manual de Usuario
				</a>				
			</li>
			<li>
				<a target="_blank" 
				href="${request.contextPath}/ayuda/ayudaView.action">				
				<img src="${request.contextPath}/common/images/ayuda.gif" border="0" align="left" alt="Ayuda">Ayuda Contextual
				</a>				
			</li>
		</ul>
</div>
	
<@vc.anchors target="contentTrx">	
		
<script language="JavaScript" type="text/javascript">

menu_status = new Array();

function showhide(name) {
var menu= document.getElementById(name);
var nameAreasubmenu='areasubmenu'+name;

if(menu_status[name] != 'show') {
           show(name);
           menu_status[name] = 'show';
        }else{
          hide(name);
           menu_status[name] = 'hide';
        }
}

function show(name) {
var menu= document.getElementById(name);
var nameAreasubmenu='areasubmenu'+name;

document.getElementById(nameAreasubmenu).className='show';
document.getElementById(nameAreasubmenu).className = 'capaSubmenus';
document.getElementById('menuDesp'+name).className = 'show';
document.getElementById('menuCont'+name).className = 'hide';
}

function hide(name) {
var menu= document.getElementById(name);
var nameAreasubmenu='areasubmenu'+name;

document.getElementById(nameAreasubmenu).className='hide';
document.getElementById('menuDesp'+name).className = 'hide';
document.getElementById('menuCont'+name).className = 'show';
}

</script>
	
</@vc.anchors>