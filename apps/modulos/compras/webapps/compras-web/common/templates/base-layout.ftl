<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
	<head><title>Sistema de Compras de Rio Uruguay Seguros</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

		<link rel="stylesheet" href="${request.contextPath}/common/styles/jquery-ui.css" type="text/css" media="all" />

		<link rel="stylesheet" href="${request.contextPath}/common/styles/rioUruguay.css" type="text/css" />
		<link rel="stylesheet" href="${request.contextPath}/common/styles/menu.css" type="text/css" />
		<link rel="stylesheet" href="${request.contextPath}/common/styles/lightbox.css" type="text/css" />
		<link rel="stylesheet" href="${request.contextPath}/common/styles/modalbox.css" type="text/css" />
		<link rel="stylesheet" href="${request.contextPath}/common/styles/lightbox.css" type="text/css" />
		<link rel="stylesheet" href="${request.contextPath}/common/styles/displaytag.css" type="text/css" />
		<link rel="stylesheet" href="${request.contextPath}/common/styles/dhtmlgoodies_calendar.css" type="text/css" />
		<link rel="stylesheet" href="${request.contextPath}/common/styles/ajaxtags.css" type="text/css" />
				
		<script type="text/javascript" src="${request.contextPath}/common/js/jquery/jquery.min.js" ></script>
		<script type="text/javascript" src="${request.contextPath}/common/js/jqueryui/jquery-ui.min.js"></script>

		<script type="text/javascript" src="${request.contextPath}/common/js/dhtmlgoodies_calendar.js"></script>
		<script type="text/javascript" src="${request.contextPath}/common/js/date.js"></script>
		<script type="text/javascript" src="${request.contextPath}/common/js/prototype.js"></script>
		<script type="text/javascript" src="${request.contextPath}/common/js/scriptaculous/scriptaculous.js?load=builder,effects,controls"></script>
		<script type="text/javascript" src="${request.contextPath}/common/js/ajax/ajaxtags_parser.js"></script>
		<script type="text/javascript" src="${request.contextPath}/common/js/ajax/ajaxtags_controls.js"></script>
		<script type="text/javascript" src="${request.contextPath}/common/js/ajax/ajaxtags.js"></script>
		<script type="text/javascript" src="${request.contextPath}/common/js/menu/menu.js"></script>
		<script type="text/javascript" src="${request.contextPath}/common/js/modalbox.js"></script>
		<script type="text/javascript" src="${request.contextPath}/common/js/utils.js"></script>
		<script type="text/javascript" src="${request.contextPath}/common/js/lightbox.js"></script>
		<script type="text/javascript" src="${request.contextPath}/common/js/menu/chrome.js"></script>
		<script type="text/javascript" src="${request.contextPath}/common/js/IE7.js"></script>
		
		<script LANGUAGE="JavaScript">
			var pathToImages='${request.contextPath}/common/images/';
			Ajax.Responders.register({
				onCreate: function(request){					
					if(request.url.indexOf('javascript://nop/?ajax=true') == -1){
						jQuery( "#spinnerdiv" ).show();
						showLightbox();
					}
				},
				onComplete: function(){
						hideLightbox();
						resizeWindows();
						jQuery( "#spinnerdiv" ).hide();
				}
			});
		</script>
		
		 <script language="JavaScript" type="text/JavaScript">
<!--
menu_status = new Array();

function hideMenu(theid){

	if (document.getElementById) {
		
		jQuery( "#capaMenu" ).resizable( "destroy" );
	    
	    var switch_id = document.getElementById(theid);
	    var flecha1_id = document.getElementById("colapI");
	    var flecha2_id = document.getElementById("colapD");
	    var content_id = document.getElementById("contentTrx");
	    var flecha_id = document.getElementById("flecha");
	
        switch_id.className = 'hide';
        flecha1_id.className = 'hide';
        flecha_id.className = 'flechaContraido';
        flecha2_id.className = 'show';
        content_id.className = 'menuContraido';
        
        resizeWindows();
    }
}

function showMenu(theid){

    if (document.getElementById) {
    var switch_id = document.getElementById(theid);
    var flecha1_id = document.getElementById("colapI");
    var flecha2_id = document.getElementById("colapD");
    var content_id = document.getElementById("contentTrx");
    var flecha_id = document.getElementById("flecha");
    
           switch_id.className = 'show ui-widget-content';
           flecha1_id.className = 'show';
           flecha2_id.className = 'hide';
           
           	jQuery( "#capaMenu" ).resizable(
			{ minWidth: 80, 
			  maxWidth: 332,	
			  minHeight: 471, 
			  maxHeight: 471,
			  resize: function(event, ui) {
			    resizeWindows();
   			 }
   		});           
           
           content_id.className = 'bodyContraido';
           flecha_id.className = 'flechaDesplegado';
           
           resizeWindows()
    }
    
}

//-->
</script>
			
<script type="text/javascript" language="Javascript">
	document.oncontextmenu = function(){return true}
</script>

<script>
	jQuery(function() {
	
		jQuery(window).bind('resize', function() {
   			resizeWindows();
		});
	
		jQuery( "#capaMenu" ).resizable(
			{ minWidth: 80, 
			  maxWidth: 332,	
			  minHeight: 471, 
			  maxHeight: 471,
			  resize: function(event, ui) {
   				resizeWindows(); 
   			 }
   		});
});
</script>
		
	</head>
	
	<body>
		<div id="capaPrincipal" class="capaPrincipal">	
			
			<div id="contenedor">
				<div id="container">
					<div id="capaMenu" class="show ui-widget-content" style="overflow : hidden;" >

						<div id="cabeceraMenu" class="cabeceraMenu">
								<table>
									<tr>
										<td width="100%" height="22px">Menú Compras- 2.1-21 </td> 
										<td>
											<div align="right">
												<a id="colapI" class="menu1" onclick="hideMenu('capaMenu')" 
													onmouseover="document.images['colapsibleIzq'].src='${request.contextPath}/common/images/izquierdaBrillo.png';" 
													onmouseout="document.images['colapsibleIzq'].src='${request.contextPath}/common/images/izquierdaOpaco.png';">
													<img src="${request.contextPath}/common/images/izquierdaOpaco.png" name="colapsibleIzq" align="absmiddle">
												</a>
											</div>
										</td>
									</tr>
								</table>
						</div>								
						<div style="background: white repeat 0 0; border-left: solid 1px #81adad;" >
							<table><tr><td width="100%"></td></tr></table>
						</div>
						<@tiles.insertAttribute name="verticalMenu"/>
					</div>
					<div id="flecha" class="flechaDesplegado" align="center">
						<a class="hide" id="colapD" class="menu1" onclick="showMenu('capaMenu')" 
							onmouseover="document.images['colapsibleDer'].src='${request.contextPath}/common/images/derechaBrillo.png';" 
							onmouseout="document.images['colapsibleDer'].src='${request.contextPath}/common/images/derechaOpaco.png';">
							<img src="${request.contextPath}/common/images/derechaOpaco.png" name="colapsibleDer" />
						</a>
					</div>

					<div id="spinnerdiv" style="position: fixed; top:30px; left:55%; display:none;" >
						<img  title="spinner" id="spinner" src="${request.contextPath}/common/images/preloader3.gif" />
					</div>
					
					<div class="bodyContraido" width="80%" id="contentTrx" class="capaCuerpo" align="center">
						<@tiles.insertAttribute name="body"/>
					</div>
					
				</div>
				<div id="pie">
  				</div>
  			</div>   				
		</div>
	</body>
</html>
