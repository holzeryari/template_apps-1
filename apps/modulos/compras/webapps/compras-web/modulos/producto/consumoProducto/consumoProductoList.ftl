
<#setting locale="es_AR">

<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>

<@s.hidden id="navigationId" name="navigationId"/>
<@s.hidden id="consumoProducto.producto.rubro.oid" name="consumoProducto.producto.rubro.oid"/>
<@s.hidden id="consumoProducto.producto.rubro.descripcion" name="consumoProducto.producto.rubro.descripcion"/>

<@s.hidden id="consumoProducto.cliente.oid" name="consumoProducto.cliente.oid"/>
<@s.hidden id="consumoProducto.cliente.descripcion" name="consumoProducto.cliente.descripcion"/>

<@s.hidden id="fechaDesdeString" name="fechaDesdeString"/>
<@s.hidden id="fechaHastaString" name="fechaHastaString"/>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
        	<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Datos del Consumo de Producto</b></td>
			</tr>
		</table>
			
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
	    		<td class="textoCampo" colspan="4">&nbsp;</td>
	    	</tr>
			<tr>
  				<td class="textoCampo">C&oacute;digo:</td>
  				<td class="textoDato">
  					<@s.textfield 
						templateDir="custontemplates" 
						id="consumoProducto.producto.oid"
						cssClass="textarea"
						cssStyle="width:160px" 
						name="consumoProducto.producto.oid" 
						title="Codigo" />
				</td>

				<td class="textoCampo">Descripci&oacute;n:</td>
				<td class="textoDato">
					<@s.textfield 
      					templateDir="custontemplates" 
						id="consumoProducto.producto.descripcion"
						cssClass="textarea"
						cssStyle="width:160px" 
						name="consumoProducto.producto.descripcion" 
						title="Descripcion"/>
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Cliente:</td>
      			<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="consumoProducto.cliente.descripcion" />
					<@s.a templateDir="custontemplates" id="seleccionarCliente" name="seleccionarCliente" href="javascript://nop/">
					<img src="${request.contextPath}/common/images/buscar.gif" border="0" align="absmiddle" hspace="3" >
					</@s.a>		
				</td>
									
				<td class="textoCampo">Rubro:</td>
				<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="consumoProducto.producto.rubro.descripcion" />
					<@s.a templateDir="custontemplates" id="seleccionarRubro" name="seleccionarRubro" href="javascript://nop/">
					<img src="${request.contextPath}/common/images/buscar.gif" border="0" align="absmiddle" hspace="3" >
					</@s.a>		
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Fecha Desde Entrega:</td>
      			<td class="textoDato">
				<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="fechaDesde" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="fechaDesde" 
					title="Fecha Entrega Desde" />
				</td>
				<td class="textoCampo">Fecha Hasta Entrega:</td>
				<td class="textoDato">
					<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="fechaHasta" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="fechaHasta" 
					title="Fecha Entrega Hasta" />
				</td>
			</tr>
				<tr>
					<td colspan="4" class="lineaGris" align="right">
	      				<input id="limpiar" type="button" name="btnVolver" value="Limpiar" class="boton"/>		
	      				<input id="buscar" type="button" name="btnVolver" value="Buscar" class="boton"/>
	    			</td>
				</tr>	
			</table>
		</div>
		<!-- Resultado Filtro -->
		<@s.if test="consumoProductoList!=null">
			<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
			<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Productos encontrados</td>
				</tr>
			</table>
			
			<@ajax.anchors target="contentTrx">
				<table class="tablaDetalleCuerpo" cellpadding="3" >	
					<tr>
<#--
						<th align="center"  class="botoneraAnchoCon1" >Acciones</th>
-->
						<th align="center">Codigo</th>						
						<th align="center">Descripcion</th>
						<th align="center">Rubro</th>
						<th align="center">Fecha Entrega</th>
						<th align="center">Cliente</th>
						<th align="center">Cant. Consumida</th>										
					</tr>
									
					<#assign ultimoProducto = 0>
					<#assign ultimoCliente = 0>						
					<#assign total=0>
					<#assign primerFila='true'>
									
					<#assign consumoLista = consumoProductoList>
					<#list consumoLista as consumoProducto>
						<#if consumoProducto.producto.oid!=ultimoProducto>
							<#if primerFila=='false'>
								<tr>										
									<td colspan="5" class="estiloTextoColor">&nbsp;</td>										
									<td class="estiloNumeroColor">${total}</td>
								</tr>
							</#if>
										
							<tr>	
<#--
								<td class="botoneraAnchoCon1">
									<@security.a 
										templateDir="custontemplates" 
										securityCode="CUF0501" 
										enabled="true" 
										cssClass="item" 
										href="${request.contextPath}/producto/consumoProducto/verMovimientos.action?consumoProducto.cliente.oid=${consumoProducto.cliente.oid?c}&consumoProducto.producto.oid=${consumoProducto.producto.oid?c}&fechaDesde={fechaDesdeString}&fechaHasta={fechaHastaString}&navigationId=${navigationId}">
										<img  src="${request.contextPath}/common/images/movimientoDeposito.gif" alt="Ver Movimientos" title="Ver Movimientos"  border="0">
									</@security.a>
								</td>
-->
								<td class="estiloNumero">${consumoProducto.producto.oid}</td>
								<td>${consumoProducto.producto.descripcion}</td>
								<td>${consumoProducto.producto.rubro.descripcion}</td>
								<td>${consumoProducto.fechaEntrega?string("dd/MM/yyyy")}</td>
								<td>${consumoProducto.cliente.descripcion}</td>
								<td class="estiloNumero">${consumoProducto.consumo}</td>												
							</tr>										
							<#assign total=consumoProducto.consumo>
						</#if>
						<#if  consumoProducto.producto.oid==ultimoProducto &&
						consumoProducto.cliente.oid!=ultimoCliente>
							<tr>	
<#--
								<td class="botoneraAnchoCon1">
									<@security.a 
										templateDir="custontemplates" 
										securityCode="CUF0501" 
										enabled="true" 
										cssClass="item" 
										href="${request.contextPath}/producto/consumoProducto/verMovimientos.action?consumoProducto.cliente.oid=${consumoProducto.cliente.oid?c}&consumoProducto.producto.oid=${consumoProducto.producto.oid?c}&fechaDesde={fechaDesdeString}&fechaHasta={fechaHastaString}&navigationId=${navigationId}">
										<img  src="${request.contextPath}/common/images/movimientoDeposito.gif" alt="Ver Movimientos" title="Ver Movimientos"  border="0">
									</@security.a>
								</td>
-->
								<td colspan="3">&nbsp;</td>
								<td>${consumoProducto.fechaEntrega?string("dd/MM/yyyy")}</td>
								<td>${consumoProducto.cliente.descripcion}</td>
								<td class="estiloNumero">${consumoProducto.consumo}</td>													
							</tr>	
							<#assign total=consumoProducto.consumo+total>										
						</#if>
									
						<#assign ultimoProducto = consumoProducto.producto.oid>
						<#assign ultimoCliente = consumoProducto.cliente.oid>	
						<#assign primerFila='false'>								
					</#list>						
								
					<tr>										
						<td colspan="5" class="estiloTextoColor">&nbsp;</td>										
						<td class="estiloNumeroColor">${total}</td>
					</tr>
								
				</table>
			</@ajax.anchors>
		</div>	
	</@s.if>	

<@vc.htmlContent 
  baseUrl="${request.contextPath}/producto/consumoProducto/search.action" 
  source="buscar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=regis,fechaDesde={fechaDesde},fechaHasta={fechaHasta},consumoProducto.producto.oid={consumoProducto.producto.oid},consumoProducto.producto.descripcion={consumoProducto.producto.descripcion},consumoProducto.cliente.oid={consumoProducto.cliente.oid},consumoProducto.cliente.descripcion={consumoProducto.cliente.descripcion},consumoProducto.producto.rubro.oid={consumoProducto.producto.rubro.oid},consumoProducto.producto.rubro.descripcion={consumoProducto.producto.rubro.descripcion}"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/producto/consumoProducto/view.action" 
  source="limpiar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=regis"/>
  

 <@vc.htmlContent 
  baseUrl="${request.contextPath}/producto/consumoProducto/selectRubro.action" 
  source="seleccionarRubro" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=change,fechaDesde={fechaDesde},fechaHasta={fechaHasta},consumoProducto.producto.oid={consumoProducto.producto.oid},consumoProducto.producto.descripcion={consumoProducto.producto.descripcion},consumoProducto.cliente.oid={consumoProducto.cliente.oid},consumoProducto.cliente.descripcion={consumoProducto.cliente.descripcion},consumoProducto.producto.rubro.oid={consumoProducto.producto.rubro.oid},consumoProducto.producto.rubro.descripcion={consumoProducto.producto.rubro.descripcion}"/>
  
 <@vc.htmlContent 
  baseUrl="${request.contextPath}/producto/consumoProducto/selectCliente.action" 
  source="seleccionarCliente" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=change,fechaDesde={fechaDesde},fechaHasta={fechaHasta},consumoProducto.producto.oid={consumoProducto.producto.oid},consumoProducto.producto.descripcion={consumoProducto.producto.descripcion},consumoProducto.cliente.oid={consumoProducto.cliente.oid},consumoProducto.cliente.descripcion={consumoProducto.cliente.descripcion},consumoProducto.producto.rubro.oid={consumoProducto.producto.rubro.oid},consumoProducto.producto.rubro.descripcion={consumoProducto.producto.rubro.descripcion}"/>
  