<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<link rel="stylesheet" href="${request.contextPath}/common/styles/rioUruguay.css" type="text/css" />					


	
	
		<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
			<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
		<tr>
			<@s.form action="/imagenes/doUpload.action" 
				method="post" enctype="multipart/form-data"  theme="ajax"  id="editProject" name="editProject">
		   		<@s.hidden id="navegacionIdBack" name="navegacionIdBack"/>
		   		<@s.hidden id="navigationId" name="navigationId"/>
				<@s.hidden id="carteleria.oid" name="carteleria.oid"/>		
				<@s.hidden id="factura.pkFactura.nro_proveedor" name="factura.pkFactura.nro_proveedor"/>
				<@s.hidden id="factura.pkFactura.nro_factura" name="factura.pkFactura.nro_factura"/>
				<@s.hidden id="factura.pkFactura.sucursal" name="factura.pkFactura.sucursal"/>
				<@s.hidden id="factura.pkFactura.tip_docum" name="factura.pkFactura.tip_docum"/>
				
				<td  class="textoDato">
		       		<@s.file name="upload" label="Path" cssStyle=" border: #3F6891 1px solid; font-size: 11px; color: black; background: #DFE7F1; width:300px;"/>
		       		<@s.submit  theme="ajax" cssStyle=" border:	color: #006699; padding: 1px; font-weight: bold;font-size:11px; cursor: pointer;"/>
				</td>		       
			</@s.form>		

        		        		
        	</tr>
     </table>
		
