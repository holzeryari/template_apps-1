<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>

<@s.hidden id="navigationId" name="navigationId"/>
<@s.hidden id="navegacionIdBack" name="navegacionIdBack"/>
<@s.hidden id="estadoDuro" name="estadoDuro"/>
<@s.hidden id="cuitDuro" name="cuitDuro"/>
<@s.if test="estadoDuro">
	<@s.hidden id="proveedor.estado" name="proveedor.estado.ordinal()"/>	
</@s.if>
<@s.if test="cuitDuro">
	<@s.hidden id="proveedor.detalleDePersona.persona.docFiscal" name="proveedor.detalleDePersona.persona.docFiscal"/>	
</@s.if>


<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
        	<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Datos del Proveedor</b></td>
			</tr>
		</table>
		
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>	
			<tr>
				<td class="textoCampo">N&uacute;mero:</td>
				<td class="textoDato">
					<@s.textfield 
      					templateDir="custontemplates" 
						id="nroProveedor" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="proveedor.nroProveedor" 
						title="N�mero" /></td>
				<td class="textoCampo">Tipo de Persona:</td>

			</tr>
			<tr>
	  			<td class="textoCampo">Nombre de Fantasia:</td>
      			<td class="textoDato">
      				<@s.textfield 
      					templateDir="custontemplates" 
						id="nombreFantasia" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="proveedor.nombreFantasia" 
						title="Nombre de Fantasia" /></td>
														
				<td class="textoCampo">CUIT/CUIL:</td>
      			<td class="textoDato">
      				<@s.textfield 
      					templateDir="custontemplates" 
						id="docFiscal" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="proveedor.detalleDePersona.persona.docFiscal" 
						title="CUIT/CUIL" /></td>
			</tr>
			<tr>	
				<td class="textoCampo">Raz&oacute;n Social/Apellido:</td>
      			<td class="textoDato">
      				<@s.textfield 
      					templateDir="custontemplates" 
						id="razonSocial" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="proveedor.detalleDePersona.razonSocial" 
						title="Raz&oacute;n Social" /></td>
				
				<td class="textoCampo">Nombre:</td>
      			<td class="textoDato">
      				<@s.textfield 
      					templateDir="custontemplates" 
						id="nombre" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="proveedor.detalleDePersona.nombre" 
						title="Nombre" />
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Estado:</td>
				<td class="textoDato">
					<@s.select 
						templateDir="custontemplates" 
						id="proveedor.estado" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="proveedor.estado" 
						list="tipoEstadoList" 
						listKey="key" 
						listValue="description" 
						value="proveedor.estado.ordinal()"
						title="Estado"
						headerKey="-1"
						headerValue="Todos"  
						/>
				</td>
				<td class="textoCampo">N&uacute;mero de Documento:</td>
      			<td class="textoDato">
      				<@s.textfield 
      					templateDir="custontemplates" 
						id="docPersonal" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="proveedor.detalleDePersona.persona.docPersonal" 
						title="N&uacute;mero de Documento" />
				</td>
			</tr>

			<tr>
	    		<td colspan="4" class="lineaGris" align="right">
	      			<input id="limpiar" type="button" name="btnVolver" value="Limpiar" class="boton"/>		
	      			<input id="buscar" type="button" name="btnVolver" value="Buscar" class="boton"/>
	    		</td>
			</tr>	
		</table>
	</div>
					
	<!-- Resultado Filtro -->
	<@s.if test="proveedorList!=null">			
		<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
			<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Proveedores encontrados</td>
				</tr>
			</table>			
			
			<@ajax.anchors target="contentTrx">			
          		<@display.table 
          			class="tablaDetalleCuerpo" 
          			cellpadding="3" 
          			name="proveedorList" 
          			id="proveedorSelect" 
          			pagesize=15 
          			defaultsort=1
          			partialList=true 
          			size="recordSize"
          			keepStatus=true
          			excludedParams="resetFlag">
					
					<@s.if test="nameSpaceSelect!=null && nameSpaceSelect!=''">
						<#assign ref="${request.contextPath}/maestro/proveedor/select.action?">
						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" style=" text-align:center; display:block" title="Acciones">
							<@s.a href="${ref}proveedor.nroProveedor=${proveedorSelect.nroProveedor?c}&navegacionIdBack=${navegacionIdBack}&navigationId=${navigationId}&nameSpaceSelect=${nameSpaceSelect}&actionNameSelect=${actionNameSelect}&oidParameter=${oidParameter}">
								<img src="${request.contextPath}/common/images/seleccionar.gif" alt="Seleccionar" title="Seleccionar" width="16" height="16" border="0">
							</@s.a>
					
						</@display.column>						  															
					</@s.if>
					
					<@s.else>
							<#assign ref="${request.contextPath}/maestro/proveedor/selectBack.action?">
							<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" style=" text-align:center; display:block" title="Acciones">
								<@s.a href="${ref}proveedor.nroProveedor=${proveedorSelect.nroProveedor?c}&navegacionIdBack=${navegacionIdBack}&navigationId=${navigationId}">
									<img src="${request.contextPath}/common/images/seleccionar.gif" alt="Seleccionar" title="Seleccionar" width="16" height="16" border="0">
								</@s.a>
					
							</@display.column>
					</@s.else>
													
					
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="nombreFantasia" title="Nombre de Fantasia" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" title="Direcci&oacute;n">
					<#if proveedorSelect.detalleDePersona.codigoPostal?exists>
						${proveedorSelect.detalleDePersona.codigoPostal.descripcion},
						${proveedorSelect.detalleDePersona.codigoPostal.provincia.descripcion}-
						${proveedorSelect.detalleDePersona.codigoPostal.provincia.pais.descripcion}
					</#if>
					&nbsp;
						${proveedorSelect.direccionString}
					</@display.column>															
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="email" title="Email" />								
					
				</@display.table>
			</@ajax.anchors>
		</div>	
	</@s.if>			
	
	<div id="capaBotonera" class="capaBotonera">
		<table id="tablaBotonera" class="tablaBotonera">
			<tr> 
				<td align="left">
					<input id="cancel" type="button" name="btnVolver" value="Volver" class="boton"/>
				</td>
			</tr>	
		</table>
	</div>
	
<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navegacionIdBack},flowControl=back"/>
							
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/proveedor/selectSearch.action" 
  source="buscar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="resetFlag=true,navigationId={navigationId},flowControl=regis,navegacionIdBack={navegacionIdBack},proveedor.nroProveedor={nroProveedor},proveedor.detalleDePersona.persona.tipoPersona={tipoPersona},proveedor.nombreFantasia={nombreFantasia},proveedor.detalleDePersona.persona.docFiscal={docFiscal},proveedor.detalleDePersona.razonSocial={razonSocial},proveedor.detalleDePersona.nombre={nombre},proveedor.detalleDePersona.persona.docPersonal={docPersonal},proveedor.estado={proveedor.estado},rubro.oid={rubro.oid},nameSpaceSelect=${nameSpaceSelect},actionNameSelect=${actionNameSelect},oidParameter=${oidParameter},estadoDuro={estadoDuro},cuitDuro={cuitDuro}"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/proveedor/selectView.action" 
  source="limpiar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=regis,navegacionIdBack={navegacionIdBack},nameSpaceSelect=${nameSpaceSelect},actionNameSelect=${actionNameSelect},oidParameter=${oidParameter},estadoDuro={estadoDuro},proveedor.estado={proveedor.estado},cuitDuro={cuitDuro}"/>
  
 <@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/persona/codigoPostal/view.action" 
  source="codigoPostalView" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId={navigationId},flowControl=change,navegacionIdBack={navegacionIdBack},proveedor.nroProveedor={nroProveedor},proveedor.detalleDePersona.persona.tipoPersona={tipoPersona},proveedor.nombreFantasia={nombreFantasia},proveedor.detalleDePersona.persona.docFiscal={docFiscal},proveedor.detalleDePersona.razonSocial={razonSocial},proveedor.detalleDePersona.nombre={nombre},proveedor.detalleDePersona.persona.docPersonal={docPersonal},rubro.oid={rubro.oid},nameSpaceSelect=${nameSpaceSelect},actionNameSelect=${actionNameSelect},oidParameter=${oidParameter},estadoDuro={estadoDuro},proveedor.estado={proveedor.estado},cuitDuro={cuitDuro}"/>
  


 