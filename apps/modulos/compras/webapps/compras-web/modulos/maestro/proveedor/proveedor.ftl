<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>

	<#assign convenioDisabled="true">
	<#assign convenioClass="textareagris">
	<#assign bolsaDisabled="true">
	<#assign bolsaClass="textareagris">
	<#assign fechaDisabled="true">
	<#assign fechaClass="textareagris">
	<#assign nroDisabled="true">
	<#assign nroClass="textareagris">
	<#assign sussDisabled="true">
	<#assign sussClass="textareagris">
<@s.if test="proveedor.situacionDGR.ordinal() == 1">
	<#assign convenioDisabled="false">
	<#assign convenioClass="textarea">
	<#assign fechaDisabled="false">
	<#assign fechaClass="textarea">
	<#assign nroDisabled="false">
	<#assign nroClass="textarea">
	<#assign sussDisabled="false">
	<#assign sussClass="textarea">
</@s.if>
<@s.if test="proveedor.situacionDGR.ordinal() == 3">
	<#assign convenioDisabled="true">
	<#assign convenioClass="textareagris">
	<#assign fechaDisabled="false">
	<#assign fechaClass="textarea">
	<#assign nroDisabled="false">
	<#assign nroClass="textarea">
	<#assign sussDisabled="true">
	<#assign sussClass="textareagris">
</@s.if>
<@s.if test="proveedor.situacionDGR.ordinal() == 4">
	<#assign convenioDisabled="true">
	<#assign convenioClass="textareagris">
	<#assign fechaDisabled="false">
	<#assign fechaClass="textarea">
	<#assign nroDisabled="false">
	<#assign nroClass="textarea">
	<#assign sussDisabled="true">
	<#assign sussClass="textareagris">
</@s.if>
<@s.if test="proveedor.formaEnvioCorrespondencia.ordinal()==1">
	<#assign bolsaDisabled="false">
	<#assign bolsaClass="textarea">	
</@s.if>


<#assign diaPago1Disable="false">
<#assign diaPago1CssClass="textarea">
<#assign diaPago2Disable="false">
<#assign diaPago2CssClass="textarea">
<#assign diaPago3Disable="false">
<#assign diaPago3CssClass="textarea">
<#assign diaPago4Disable="false">
<#assign diaPago4CssClass="textarea">

<@s.if test="proveedor.frecuenciaPagoProveedor == null proveedor.frecuenciaPagoProveedor.frecuenciaPago = null || proveedor.frecuenciaPagoProveedor.frecuenciaPago.ordinal() == -1 || proveedor.frecuenciaPagoProveedor.frecuenciaPago.ordinal() == 0">									
	<#assign diaPago1Disable="true">
	<#assign diaPago1CssClass="textareagris">
	<#assign diaPago2Disable="true">
	<#assign diaPago2CssClass="textareagris">
	<#assign diaPago3Disable="true">
	<#assign diaPago3CssClass="textareagris">
	<#assign diaPago4Disable="true">
	<#assign diaPago4CssClass="textareagris">
</@s.if>
<@s.elseif test="proveedor.frecuenciaPagoProveedor.frecuenciaPago.ordinal() == 1">									
	<#assign diaPago1Disable="false">
	<#assign diaPago1CssClass="textarea">
	<#assign diaPago2Disable="false">
	<#assign diaPago2CssClass="textarea">
	<#assign diaPago3Disable="false">
	<#assign diaPago3CssClass="textarea">
	<#assign diaPago4Disable="false">
	<#assign diaPago4CssClass="textarea">
</@s.elseif>
<@s.elseif test="proveedor.frecuenciaPagoProveedor.frecuenciaPago.ordinal() == 2">
	<#assign diaPago1Disable="false">
	<#assign diaPago1CssClass="textarea">
	<#assign diaPago2Disable="false">
	<#assign diaPago2CssClass="textarea">
	<#assign diaPago3Disable="true">
	<#assign diaPago3CssClass="textareagris">
	<#assign diaPago4Disable="true">
	<#assign diaPago4CssClass="textareagris">
</@s.elseif>
<@s.elseif test="proveedor.frecuenciaPagoProveedor.frecuenciaPago.ordinal() == 3">
	<#assign diaPago1Disable="false">
	<#assign diaPago1CssClass="textarea">
	<#assign diaPago2Disable="true">
	<#assign diaPago2CssClass="textareagris">
	<#assign diaPago3Disable="true">
	<#assign diaPago3CssClass="textareagris">
	<#assign diaPago4Disable="true">
	<#assign diaPago4CssClass="textareagris">
</@s.elseif>


	<div id="capaSeparadora" class="capaSeparadora">&nbsp;</div>
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos del Proveedor</b></td>
				<td>
						<div class="alineacionDerecha">	
						<@security.a 
						securityCode="CUF0123" 
						templateDir="custontemplates" 
						id="updateProveedor" 
						name="updateProveedor" 
						href="javascript://nop/" 
						cssClass="ocultarIcono">
							<b>Modificar</b><img src="${request.contextPath}/common/images/modificar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
						</@security.a>	
					</div>
					<#if mostrarCuentas?exists && mostrarCuentas>
					<div class="alineacionDerecha">
						<#if tieneCuentas?exists && tieneCuentas>
							<@vc.anchors target="contentTrx" ajaxFlag="ajax">
								<@s.a href="${request.contextPath}/proveedor/consultaCuentaProveedor/search.action?consultaCuentaProveedor.proveedor.nroProveedor=${proveedor.nroProveedor?c}&proveedorDuro=true&navigationId=consultarCuentas&navegacionIdBack=${navigationId}&flowControl=regis" templateDir="custontemplates" id="agregarProductoBien" name="agregarProductoBien" cssClass="ocultarIcono">
									<b>Consultar Cuenta</b>
									<img src="${request.contextPath}/common/images/verCuentas.gif" border="0" align="absmiddle" hspace="3" >
								</@s.a>		
							</@vc.anchors>
						<#else>
							<b>Consultar Cuenta</b>
							<img src="${request.contextPath}/common/images/verCuentas.gif" border="0" align="absmiddle" hspace="3" style="opacity:0.4; filter:alpha(opacity=40);">	
						</#if>	
					</div>
					</#if>
				</td>
			</tr>
		</table>
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
	        	<td class="textoCampo" colspan="4">&nbsp;</td>
	        </tr>
			<tr>
				<td class="textoCampo">N&uacute;mero:</td>
				<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="proveedor.nroProveedor"/></td>
				<td class="textoCampo">Nombre Fantasia</td>
				<td class="textoDato">
					<@s.textfield 
		  					templateDir="custontemplates" 
							id="nombreFantasia" 
							cssClass="textarea"
							cssStyle="width:160px"
							maxLength="35" 
							name="proveedor.nombreFantasia" 
							title="N�mero" /></td>	      			
			</tr>	
			<tr>
				<td class="textoCampo">Es Cooperativa:</td>
				<td class="textoDato">
	  				<@s.select 
						templateDir="custontemplates" 
						id="esCooperativa" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="proveedor.esCooperativa" 
						list="siNoList" 
						listKey="key" 
						listValue="description" 
						value="proveedor.esCooperativa.ordinal()"
						title="Es Cooperativa"
						headerKey="-1"
						headerValue="Seleccionar"/></td>
				<td class="textoCampo">Actividad en la Regi&oacute;n:</td>
				<td class="textoDato">
	  				<@s.select 
						templateDir="custontemplates" 
						id="adtividadRegion" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="proveedor.adtividadRegion" 
						list="siNoList" 
						listKey="key" 
						listValue="description" 
						value="proveedor.adtividadRegion.ordinal()"
						title="Actividad en la Regi&oacute;n"
						headerKey="-1"
						headerValue="Seleccionar"  
						/></td>
			</tr>
			<tr>
				<td class="textoCampo">Es Socio:</td>
				<td class="textoDato">
					<#if detalleDePersona.socio?exists >
						Si
					<#else>
						&nbsp;
					</#if>	  			
				</td>
				<td class="textoCampo">Nro. Socio:</td>
				<td class="textoDato">
	  				<#if detalleDePersona.socio?exists >
						${detalleDePersona.socio.nroDeSocio}
					<#else>
						&nbsp;
					</#if>
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Solvencia Econ&oacute;mica:</td>
				<td class="textoDato" colspan="3">
				<@s.select 
					templateDir="custontemplates" 
					id="proveedor.solvenciaEconomica" 
					cssClass="textarea"
					cssStyle="width:165px" 
					name="proveedor.solvenciaEconomica" 
					list="solvenciaEconomicaList" 
					listKey="key" 
					listValue="description" 
					value="proveedor.solvenciaEconomica.ordinal()"
					title="Solvencia Economica"
					headerKey="0"
					headerValue="Seleccionar"   
					/>
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Fecha Solvencia Econ. Desde:</td>
      			<td class="textoDato">
					<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="proveedor.fechaSolvenciaEconomicaDesde" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="proveedor.fechaSolvenciaEconomicaDesde" 
					title="Fecha Solvencia Economica Desde" />
				</td>
				<td class="textoCampo">Fecha Solvencia Econ. Hasta:</td>
      			<td class="textoDato">
					<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="proveedor.fechaSolvenciaEconomicaHasta" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="proveedor.fechaSolvenciaEconomicaHasta" 
					title="Fecha Solvencia Economica Hasta" />
				</td>
			</tr>
			<tr>	
				<td class="textoCampo">Email:</td>
				<td class="textoDato">
	  				<@s.textfield 
						templateDir="custontemplates" 
						id="email" 
						cssClass="textarea"
						cssStyle="width:160px"
						maxLength="255" 
						name="proveedor.email"					
						title="Email" /></td>
				<td class="textoCampo">Emite OP:</td>
				<td class="textoDato" colspan="3">
					<@s.select 
					templateDir="custontemplates" 
						id="activo" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="proveedor.activo"
						value="proveedor.activo.ordinal()"
						list="emiteOPList" 
						listKey="key" 
						listValue="description" 
						title="Emite OP"
						headerKey="-1"
						headerValue="Seleccionar"/>
				</td>
			</tr>
			<tr>
				<td class="textoCampo">CAI:</td>
				<td class="textoDato">
		  			<@s.textfield 
						templateDir="custontemplates" 
						id="cai" 
						cssClass="textarea"
						cssStyle="width:160px" 
						maxLength="14"
						name="proveedor.cai" 
						title="N&uacute;mero" /></td>
				<td class="textoCampo">Fecha Vencimiento CAI:</td>
				<td class="textoDato">
					<@vc.rowCalendar 
						templateDir="custontemplates" 
						id="fechaCai" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="proveedor.fechaCai" 
						title="Fecha Vencimiento CAI"/>
					</td>
			</tr>
			<tr>	
				<td class="textoCampo">Evaluado por LyS:</td>
				<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="proveedor.evaluadoPorLyS"/>
				</td>
				<td class="textoCampo">Estado:</td>
				<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="proveedor.estado"/>
				</td>
			</tr>
			<tr>	
				<td class="textoCampo">Situaci&oacute;n ICP:</td>
				<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="proveedor.situacionICP"/>
				</td>
				<td class="textoCampo">Situaci&oacute;n ICS:</td>
				<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="proveedor.situacionICS"/>
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Forma Envio Correspondencia:</td>
				<td class="textoDato">
				<@s.select 
					templateDir="custontemplates" 
					id="proveedor.formaEnvioCorrespondencia" 
					cssClass="textarea"
					cssStyle="width:165px" 
					name="proveedor.formaEnvioCorrespondencia"
					value="proveedor.formaEnvioCorrespondencia.ordinal()"
					list="formaEnvioComunicacionList" 
					listKey="key" 
					listValue="description" 
					title="Forma Envio Correspondencia"
					headerKey="0"
					headerValue="Seleccionar"  
					onchange="javascript:formaEnvioCorrespondencia(this);"
					/>
				</td>
				<td class="textoCampo">Bolsa de Correo:</td>
				<td class="textoDato">
					<@s.select 
						templateDir="custontemplates" 
						id="proveedor.bolsaCorreo.codigo" 
						cssClass="${bolsaClass}"
						cssStyle="width:165px" 
						name="proveedor.bolsaCorreo.codigo" 
						list="bolsaCorreoList" 
						listKey="codigo" 
						listValue="descripcion"
						title="Deposito"
						headerKey="0" 
	                    headerValue="Seleccionar"
	                    value="proveedor.bolsaCorreo.codigo"
	                    disabled="${bolsaDisabled}"	                            
	                    />
		        </td>
			</tr>
			<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>
		</table>			
		</div>

		<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
			<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            	<tr>
					<td><b>Condici&oacute;n Fiscal</b></td>
				</tr>
			</table>
			
			<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
	        	<td class="textoCampo" colspan="4">&nbsp;</td>
	        </tr>
	        <tr>
				<td class="textoCampo">Condici&oacute;n Fiscal DGR:</td>
				<td class="textoDato">
					<@s.select 
					templateDir="custontemplates" 
					id="situacionDGR" 
					cssClass="textarea"
					cssStyle="width:165px" 
					name="proveedor.situacionDGR" 
					value="proveedor.situacionDGR.ordinal()"
					list="situacionDGRList" 
					listKey="key" 
					listValue="description" 
					title="Condici&oacute;n Fiscal DGR"
					headerKey="0"
					headerValue="Seleccionar"				
					/></td>

				<td class="textoCampo">Convenio Multilateral:</td>
				<td class="textoDato"><@s.select 
						templateDir="custontemplates" 
						id="situacionMultilateral" 
						cssClass="${convenioClass}"
						disabled="${convenioDisabled}"
						cssStyle="width:165px" 
						name="proveedor.situacionMultilateral" 
						list="situacionMultilateralList" 
						listKey="key" 
						listValue="description" 
						value="proveedor.situacionMultilateral.ordinal()"
						title="Convenio Multilateral:"
						headerKey="-1"
						headerValue="Seleccionar"  
						/></td>										
			</tr>
			<tr>
				<td class="textoCampo">Nro Ingresos Brutos:</td>
				<td class="textoDato">
					<@s.textfield 
						templateDir="custontemplates" 
						id="nroDeIngresosBrutos" 
						cssClass="${nroClass}"
						disabled="${nroDisabled}"
						cssStyle="width:160px" 
						maxLength="15"
						name="proveedor.nroDeIngresosBrutos" 
						title="Nro Ingresos Brutos" />
				</td>
				
				<td class="textoCampo">Fecha Vto. de Constancia de Inscripcion a DGR:</td>
				<td class="textoDato">
					<@vc.rowCalendar 
						templateDir="custontemplates" 
						id="fechaDGR" 
						cssClass="${fechaClass}"
						disabled="${fechaDisabled}"
						cssStyle="width:160px" 
						name="proveedor.fechaDGR" 
						title="Fecha Vencimiento Ingresos Brutos"/>
				</td>
			</tr>
			<tr>
				<td class="textoCampo">SUSS:</td>
				<td class="textoDato" colspan="3"><@s.textfield 
						templateDir="custontemplates" 
						id="jubilacion" 
						cssClass="${sussClass}"
						disabled="${sussDisabled}"
						cssStyle="width:160px" 
						maxLength="10"
						name="proveedor.jubilacion" 
						title="SUSS" />
						</td>
			</tr>
			<tr>
	        	<td class="textoCampo" colspan="4">&nbsp;</td>
	        </tr>
		</table>	
	</div>
	
		<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Condici&oacute;n de Pago</b></td>
			</tr>
		</table>
		
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
	        	<td class="textoCampo" colspan="4">&nbsp;</td>
	        </tr>
	        <tr>
				<td class="textoCampo">Forma de Pago:</td>
				<td class="textoDato">
					<@s.select 
						templateDir="custontemplates" 
						id="formaPago" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="proveedor.formaPago.pk.idSec" 
						list="formaDepagoList" 
						listKey="pk.idSec" 
						listValue="descripcion" 
						title="Forma de Pago"
						headerKey="-1"
						headerValue="Seleccionar"/>
				</td>	
				<td class="textoCampo">Medio de Pago:</td>
				<td class="textoDato">
					<@s.select 
						templateDir="custontemplates" 
						id="medioPago" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="proveedor.medioPago.pk.idSec" 
						list="medioDePagoList" 
						listKey="pk.idSec" 
						listValue="descripcion" 
						title="Condici&oacute;n de Pago"
						headerKey="-1"
						headerValue="Seleccionar"/>
				</td>
			</tr>
	        <tr>
				<td class="textoCampo">Frecuencia:</td>
				<td class="textoDato" colspan="3">
					<@s.select 
					templateDir="custontemplates" 
					id="frecuenciaPagoId" 
					cssClass="textarea"
					cssStyle="width:165px" 
					name="frecuenciaPago"
					onchange="javascript:frecuenciaPago(this);" 
					value="proveedor.frecuenciaPagoProveedor.frecuenciaPago.ordinal()"
					list="frecuenciaPagoList" 
					listKey="key" 
					listValue="description" 
					title="Frecuencia de pago"
					headerKey="-1"
					headerValue="Seleccionar"				
					/>
				</td>
	        </tr>
			<tr>
				<td class="textoCampo">D&iacute;a de pago 1:</td>
				<td class="textoDato">
					<@s.textfield 
						templateDir="custontemplates" 
						id="diaPago1" 
						disabled="${diaPago1Disable}"
						cssClass="${diaPago1CssClass}"
						cssStyle="width:160px"
						maxLength="50"
						name="proveedor.frecuenciaPagoProveedor.diaPago1" 
						title="D&iacute;a de pago uno" />
				</td>
				<td class="textoCampo">D&iacute;a de pago 2:</td>
				<td class="textoDato">
					<@s.textfield 
						templateDir="custontemplates" 
						id="diaPago2" 
						disabled="${diaPago2Disable}"
						cssClass="${diaPago2CssClass}"
						cssStyle="width:160px"
						maxLength="50"
						name="proveedor.frecuenciaPagoProveedor.diaPago2" 
						title="D&iacute;a de pago dos" />
				</td>
			</tr>	
			<tr>
				<td class="textoCampo">D&iacute;a de pago 3:</td>
				<td class="textoDato">
					<@s.textfield 
						templateDir="custontemplates" 
						id="diaPago3" 
						disabled="${diaPago3Disable}"
						cssClass="${diaPago3CssClass}"
						cssStyle="width:160px"
						maxLength="50"
						name="proveedor.frecuenciaPagoProveedor.diaPago3" 
						title="D&iacute;a de pago tres" />
				</td>
				<td class="textoCampo">D&iacute;a de pago 4:</td>
				<td class="textoDato">
					<@s.textfield 
						templateDir="custontemplates" 
						id="diaPago4" 
						disabled="${diaPago4Disable}"
						cssClass="${diaPago4CssClass}"
						cssStyle="width:160px"
						maxLength="50"
						name="proveedor.frecuenciaPagoProveedor.diaPago4" 
						title="D&iacute;a de pago cuatro" />
				</td>
			</tr>	
			<tr>
	        	<td class="textoCampo" colspan="4">&nbsp;</td>
	        </tr>
		</table>
	</div>
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Direcci&oacute;n de Pago</b></td>
			</tr>
		</table>
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
	        	<td class="textoCampo" colspan="4">&nbsp;</td>
	        </tr>				
			<tr>
				<td class="textoCampo">C&oacute;digo postal:</td>
				<td class="textoDato"><@s.property default="&nbsp;" escape=false value="proveedor.codigoPostalPago.pk.identificador"/>
					<@s.hidden id="identificadorCPP" name="proveedor.codigoPostalPago.pk.identificador"/>
					<@s.hidden id="secuenciaCPP" name="proveedor.codigoPostalPago.pk.secuencia"/>
					<@s.a templateDir="custontemplates" id="selectCodigoPostalView" name="selectCodigoPostalView" href="javascript://nop/" cssClass="ocultarIcono">
						<img src="${request.contextPath}/common/images/buscar.gif" title="C&oacute;digo postal" align="top" border="0"  hspace="3">	
					</@s.a>
					<@vc.htmlContent 
				  		baseUrl="${request.contextPath}/maestro/proveedor/selectCodigoPostalDePago.action"
				  		source="selectCodigoPostalView" 
				  		success="contentTrx" 
				  		failure="errorTrx"  
				  		 parameters="navigationId=${navigationId},navegacionIdBack=${navegacionIdBack},flowControl=change,proveedor.nroProveedor={nroProveedor},detalleDePersona.pk.identificador=${detalleDePersona.pk.identificador?c},detalleDePersona.pk.secuencia=${detalleDePersona.pk.secuencia?c},proveedor.nombreFantasia={nombreFantasia},proveedor.esCooperativa={esCooperativa},proveedor.adtividadRegion={adtividadRegion},proveedor.formaPago={formaPago},proveedor.medioPago={medioPago},proveedor.codigoPostalPago.pk.identificador={identificadorCPP},proveedor.codigoPostalPago.pk.secuencia={secuenciaCPP},proveedor.callePago={callePago},proveedor.numeroPago={numeroPago},proveedor.deptoPago={deptoPago},proveedor.responsable.pk.identificador={identificadorR},proveedor.responsable.pk.secuencia={secuenciaR},proveedor.situacionDGR={situacionDGR},proveedor.situacionMultilateral={situacionMultilateral},proveedor.nroDeIngresosBrutos={nroDeIngresosBrutos},proveedor.fechaDGR={fechaDGR},proveedor.jubilacion={jubilacion},proveedor.cai={cai},proveedor.fechaCai={fechaCai},proveedor.formaEnvioCorrespondencia={proveedor.formaEnvioCorrespondencia},proveedor.bolsaCorreo.codigo={proveedor.bolsaCorreo.codigo},proveedor.email={email}"/>
				</td>
				<td class="textoCampo">Localidad:</td>
				<td class="textoDato"><@s.property default="&nbsp;" escape=false value="proveedor.codigoPostalPago.descripcion"/></td>										
			</tr>
			<tr>
				<td class="textoCampo">Provincia:</td>
				<td class="textoDato"><@s.property default="&nbsp;" escape=false value="proveedor.codigoPostalPago.provincia.descripcion"/></td>
				<td class="textoCampo">Pais:</td>
				<td class="textoDato"><@s.property default="&nbsp;" escape=false value="proveedor.codigoPostalPago.provincia.pais.descripcion"/></td>
			</tr>
			<tr>
				<td class="textoCampo">Secuencia CP:</td>
				<td class="textoDato"><@s.property default="&nbsp;" escape=false value="proveedor.codigoPostalPago.pk.secuencia"/></td>
				<td class="textoCampo">Calle:</td>
				<td class="textoDato">
					<@s.textfield 
						templateDir="custontemplates" 
						id="callePago" 
						cssClass="textarea"
						cssStyle="width:160px"
						maxLength="50"
						name="proveedor.callePago" 
						title="Calle" />
				</td>
			</tr>
			<tr>
				<td class="textoCampo">N&uacute;mero:</td>
				<td class="textoDato">
	  				<@s.textfield 
						templateDir="custontemplates" 
						id="numeroPago" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="proveedor.numeroPago" 
						title="N&uacute;mero" /></td>
				<td class="textoCampo">Depto:</td>
				<td class="textoDato">
					<@s.textfield 
						templateDir="custontemplates" 
						id="deptoPago" 
						cssClass="textarea"
						cssStyle="width:160px"
						maxLength="10" 
						name="proveedor.deptoPago" 
						title="Depto" />
				</td>
			</tr>
			<tr>
	        	<td class="textoCampo" colspan="4">&nbsp;</td>
	        </tr>
		</table>
	</div>
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Responsable de Pago</b></td>
			</tr>
		</table>
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
	        	<td class="textoCampo" colspan="4">&nbsp;</td>
	        </tr>
			<tr>
				<td class="textoCampo">Identificador:</td>
				<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="proveedor.responsable.pk.identificador"/>
					<@s.hidden id="identificadorR" name="proveedor.responsable.pk.identificador"/>
					<@s.hidden id="secuenciaR" name="proveedor.responsable.pk.secuencia"/>
					<@s.a templateDir="custontemplates" id="reponsableView" name="reponsableView" href="javascript://nop/" cssClass="ocultarIcono">
						<img src="${request.contextPath}/common/images/buscar.gif" title="C&oacute;digo postal" align="top" border="0" hspace="3">	
					</@s.a>
					<@vc.htmlContent 
					  	baseUrl="${request.contextPath}/maestro/proveedor/selectRespossableUpdateProveedor.action"
					  	source="reponsableView" 
					  	success="contentTrx" 
					  	failure="errorTrx"  
					  	 parameters="navigationId=${navigationId},navegacionIdBack=${navegacionIdBack},flowControl=change,proveedor.nroProveedor={nroProveedor},detalleDePersona.pk.identificador=${detalleDePersona.pk.identificador?c},detalleDePersona.pk.secuencia=${detalleDePersona.pk.secuencia},proveedor.nombreFantasia={nombreFantasia},proveedor.esCooperativa={esCooperativa},proveedor.adtividadRegion={adtividadRegion},proveedor.formaPago={formaPago},proveedor.medioPago={medioPago},proveedor.codigoPostalPago.pk.identificador={identificadorCPP},proveedor.codigoPostalPago.pk.secuencia={secuenciaCPP},proveedor.callePago={callePago},proveedor.numeroPago={numeroPago},proveedor.deptoPago={deptoPago},proveedor.responsable.pk.identificador={identificadorR},proveedor.responsable.pk.secuencia={secuenciaR},proveedor.situacionDGR={situacionDGR},proveedor.situacionMultilateral={situacionMultilateral},proveedor.nroDeIngresosBrutos={nroDeIngresosBrutos},proveedor.fechaDGR={fechaDGR},proveedor.jubilacion={jubilacion},proveedor.cai={cai},proveedor.fechaCai={fechaCai},proveedor.formaEnvioCorrespondencia={proveedor.formaEnvioCorrespondencia},proveedor.bolsaCorreo.codigo={proveedor.bolsaCorreo.codigo},proveedor.email={email}"/>
				</td>
				<td class="textoCampo">Secuencia</td>
				<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="proveedor.responsable.pk.secuencia"/></td>	      			
			</tr>	
			<tr>
				<td class="textoCampo">Nro Documento:</td>
				<td class="textoDato" colspan="3">
					<@s.property default="&nbsp;" escape=false value="proveedor.responsable.persona.docPersonal"/></td>
			</tr>
			<tr>
				<td class="textoCampo">Apellido:</td>
				<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="proveedor.responsable.nombre"/>
				</td>
				<td class="textoCampo">Nombre:</td>
				<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="proveedor.responsable.razonSocial"/>
				</td>
			</tr>
			<tr>
	        	<td class="textoCampo" colspan="4">&nbsp;</td>
	        </tr>
		</table>
	</div>

<@ajax.updateField
  baseUrl="${request.contextPath}/maestro/proveedor/refrescarNroDeIngresosBrutos.action" 
  source="situacionDGR" 
  target="nroDeIngresosBrutos"
  action="situacionDGR"
  parameters="proveedor.nroProveedor={nroProveedor},proveedor.situacionDGR={situacionDGR}"
  eventType="change"
  postFunction="condicionFiscalDGR"
  parser="new ResponseXmlParser()"/>
  
   