<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>
 
<@s.hidden id="navigationId" name="navigationId"/>
	<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
        	<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Datos del Proveedor</b></td>
			</tr>
		</table>
		
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>	
			<tr>
				<td class="textoCampo">Tipo Evaluaci&oacute;n:</td>
      			<td class="textoDato">
      			<@s.property default="&nbsp;" escape=false value="evaluacionProveedor.tipoEvaluacion" />
      			<@s.hidden id="evaluacionProveedor.tipoEvaluacion" name="evaluacionProveedor.tipoEvaluacion"/>
      			</td>
      		
      			<td class="textoCampo">Es Carga Inicial:</td>
      			<td class="textoDato">
      			<@s.property default="&nbsp;" escape=false value="evaluacionProveedor.esCargaInicial" />
      			<@s.hidden id="evaluacionProveedor.esCargaInicial" name="evaluacionProveedor.esCargaInicial"/>
      			</td>
      		</tr>
				      		
			<#if evaluacionProveedor.esCargaInicial?exists && evaluacionProveedor.esCargaInicial.ordinal()==1>
				<tr>
					<#-- evaluacion de proveedor en el periodo-->
					<td class="textoCampo">Per&iacute;odo de Evaluaci&oacute;n:</td>
  					<td class="textoDato" colspan="3">
  						<@s.property default="&nbsp;" escape=false value="evaluacionProveedor.periodoEvaluacion.fechaInicioStr" /> - <@s.property default="&nbsp;" escape=false value="evaluacionProveedor.periodoEvaluacion.fechaFinStr" />
  						<@s.hidden id="evaluacionProveedor.periodoEvaluacion.fechaInicio" name="evaluacionProveedor.periodoEvaluacion.fechaInicio"/>
  						<@s.hidden id="evaluacionProveedor.periodoEvaluacion.fechaFin" name="evaluacionProveedor.periodoEvaluacion.fechaFin"/>
						<@s.hidden id="evaluacionProveedor.periodoEvaluacion.fechaInicioF" name="evaluacionProveedor.periodoEvaluacion.fechaInicioStr"/>
  						<@s.hidden id="evaluacionProveedor.periodoEvaluacion.fechaFinF" name="evaluacionProveedor.periodoEvaluacion.fechaFinStr"/>
  					</td>
  				</tr>	
  			</#if>
 									
			<tr>
				<td class="textoCampo">N&uacute;mero:</td>
      			<td class="textoDato">
      				<@s.textfield 
      					templateDir="custontemplates" 
						id="nroProveedor" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="proveedor.nroProveedor" 
						title="Numero" /></td>
				<td class="textoCampo">Tipo de Persona:</td>
				<td class="textoDato">
	      			<@s.select 
						templateDir="custontemplates" 
						id="tipoPersona" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="proveedor.detalleDePersona.persona.tipoPersona" 
						list="tipoPersonaList" 
						listKey="key" 
						listValue="description" 
						value="proveedor.detalleDePersona.persona.tipoPersona.ordinal()"
						title="Tipo de Persona"
						headerKey="-1"
						headerValue="Todos"  
						/></td>
			</tr>
			<tr>				
				<td class="textoCampo">Nombre de Fantasia:</td>
      			<td class="textoDato">
      				<@s.textfield 
      					templateDir="custontemplates" 
						id="nombreFantasia" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="proveedor.nombreFantasia" 
						title="Nombre de Fantasia" /></td>
		
	  			<td class="textoCampo">CUIT/CUIL:</td>
      			<td class="textoDato">
      				<@s.textfield 
      					templateDir="custontemplates" 
						id="docFiscal" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="proveedor.detalleDePersona.persona.docFiscal" 
						title="CUIT/CUIL" />
				</td>
			</tr>
			<tr>	
				<td class="textoCampo">N&uacute;mero de Documento:</td>
      			<td class="textoDato">
      				<@s.textfield 
      					templateDir="custontemplates" 
						id="docPersonal" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="proveedor.detalleDePersona.persona.docPersonal" 
						title="N&uacute;mero de Documento" />
				</td>
				<td class="textoCampo">Raz&oacute;n Social:</td>
      			<td class="textoDato">
      				<@s.textfield 
      					templateDir="custontemplates" 
						id="razonSocial" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="proveedor.detalleDePersona.razonSocial" 
						title="Raz&oacute;n Social" /></td>
			</tr>
			<tr>	
				<td class="textoCampo">Nombre:</td>
      			<td class="textoDato">
      				<@s.textfield 
      					templateDir="custontemplates" 
						id="nombre" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="proveedor.detalleDePersona.nombre" 
						title="Nombre" />
				</td>
			
				<td class="textoCampo">Estado:</td>
  						<td class="textoDato">
  						<@s.hidden id="proveedor.estado" name="proveedor.estado.ordinal()"/>
  						<@s.property default="&nbsp;" escape=false value="proveedor.estado"/>
  				</td>
			</tr>	
	
			</tr>		
				<tr>
	    		<td colspan="4" class="lineaGris" align="right">
	      			<input id="limpiar" type="button" name="btnVolver" value="Limpiar" class="boton"/>		
	      			<input id="buscar" type="button" name="btnVolver" value="Buscar" class="boton"/>
	    		</td>
			</tr>	
		</table>
	</div>
	
		
    <!-- Resultado Filtro -->
	<@s.if test="proveedorAEvaluarList!=null">
		<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">			
			<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Proveedores pendientes de evaluaci&oacute;n encontrados</td>
				</tr>
			</table>			
								
			<@vc.anchors target="contentTrx">	
  			<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="proveedorAEvaluarList" id="proveedor" pagesize=15 defaultsort=2>
  				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" class="botoneraAnchoCon1" title="Acciones">
					<#assign ref="${request.contextPath}/maestro/proveedor/selectProveedorAEvaluarBack.action?">
					<@s.a href="${ref}proveedor.nroProveedor=${proveedor.nroProveedor?c}&navegacionIdBack=${navegacionIdBack}&navigationId=${navigationId}"><img src="${request.contextPath}/common/images/seleccionar.gif" alt="Seleccionar" title="Seleccionar" width="16" height="16" border="0"></@s.a>
				</@display.column>
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="nroProveedor" title="N&uacute;mero" />
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="detalleDePersona.persona.tipoPersona" title="Tipo De persona" />
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="nombreFantasia" title="Nombre de Fantasia" />
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="detalleDePersona.razonSocial" title="Raz&oacute;n Social" />
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="detalleDePersona.nombre" title="Nombre" /> 
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="detalleDePersona.persona.docPersonal" title="Documento" />
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="detalleDePersona.persona.docFiscal" title="CUIT/CUIL" />
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="estado" title="Estado" />	
			</@display.table>
			</@vc.anchors>
		</div>	
	</@s.if>
			
	<div id="capaBotonera" class="capaBotonera">
		<table id="tablaBotonera" class="tablaBotonera">
			<tr> 
				<td align="left">
					<input id="cancel" type="button" name="btnVolver" value="Volver" class="boton"/>
				</td>
			</tr>	
		</table>
	</div>		
	
 <@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId=${navegacionIdBack},flowControl=back"/>
	
<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/proveedor/selectProveedorAEvaluarSearch.action" 
  source="buscar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId=${navigationId},flowControl=regis,navegacionIdBack=${navegacionIdBack},proveedor.nroProveedor={proveedor.nroProveedor},persona.tipoPersona={proveedor.detalleDePersona.persona.tipoPersona},proveedor.nombreFantasia={proveedor.nombreFantasia},proveedor.detalleDePersona.persona.docFiscal={proveedor.detalleDePersona.persona.docFiscal},proveedor.detalleDePersona.razonSocial={proveedor.detalleDePersona.razonSocial},proveedor.detalleDePersona.nombre={proveedor.detalleDePersona.nombre},proveedor.detalleDePersona.persona.docPersonal={proveedor.detalleDePersona.persona.docPersonal},proveedor.estado={proveedor.estado}"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/proveedor/selectProveedorAEvaluarView.action" 
  source="limpiar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId=${navigationId},flowControl=regis,proveedor.estado={proveedor.estado},navegacionIdBack=${navegacionIdBack}"/>




  
 
  