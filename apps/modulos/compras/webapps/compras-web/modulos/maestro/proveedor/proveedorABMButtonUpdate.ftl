<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancel" type="button" name="btnVolver" value="Cancelar" class="boton"/>
				<input id="update" type="button" name="btnModificar" value="Modificar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/proveedor/update.action" 
  source="update" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=${navigationId},navegacionIdBack=${navegacionIdBack},proveedor.nroProveedor={nroProveedor},proveedor.nombreFantasia={nombreFantasia},proveedor.esCooperativa={esCooperativa},proveedor.adtividadRegion={adtividadRegion},proveedor.formaPago={formaPago},proveedor.medioPago={medioPago},proveedor.activo={activo},proveedor.codigoPostalPago.pk.identificador={identificadorCPP},proveedor.codigoPostalPago.pk.secuencia={secuenciaCPP},proveedor.callePago={callePago},proveedor.numeroPago={numeroPago},proveedor.deptoPago={deptoPago},proveedor.responsable.pk.identificador={identificadorR},proveedor.responsable.pk.secuencia={secuenciaR},proveedor.situacionDGR={situacionDGR},proveedor.situacionMultilateral={situacionMultilateral},proveedor.nroDeIngresosBrutos={nroDeIngresosBrutos},proveedor.fechaDGR={fechaDGR},proveedor.jubilacion={jubilacion},proveedor.cai={cai},proveedor.fechaCai={fechaCai},proveedor.email={email},proveedor.evaluadoPorLyS={evaluadoPorLyS},proveedor.formaEnvioCorrespondencia={proveedor.formaEnvioCorrespondencia},proveedor.bolsaCorreo.codigo={proveedor.bolsaCorreo.codigo},proveedor.solvenciaEconomica={proveedor.solvenciaEconomica}, proveedor.fechaSolvenciaEconomicaDesde={proveedor.fechaSolvenciaEconomicaDesde}, proveedor.fechaSolvenciaEconomicaHasta={proveedor.fechaSolvenciaEconomicaHasta}, frecuenciaPago={frecuenciaPagoId}, proveedor.frecuenciaPagoProveedor.diaPago1={proveedor.frecuenciaPagoProveedor.diaPago1}, proveedor.frecuenciaPagoProveedor.diaPago2={proveedor.frecuenciaPagoProveedor.diaPago2}, proveedor.frecuenciaPagoProveedor.diaPago3={proveedor.frecuenciaPagoProveedor.diaPago3}, proveedor.frecuenciaPagoProveedor.diaPago4={proveedor.frecuenciaPagoProveedor.diaPago4}"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=${navegacionIdBack},flowControl=back"/>

