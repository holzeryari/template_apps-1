<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>

<@s.hidden id="navigationId" name="navigationId"/>
<@s.hidden id="navegacionIdBack" name="navegacionIdBack"/>
<@s.hidden id="persona.oid" name="persona.oid"/>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
        	<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	

	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Datos de la Persona</b></td>  
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr><td colspan="4" class="campoDato">&nbsp;</td></tr>
			<tr>
				<td class="textoCampo">Identificador:</td>
				<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="persona.oid"/>
				</td>
				<td class="textoCampo">Pa&iacute;s:</td>
				<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="persona.pais.descripcion"/>
		  		</td>
			</tr>	
			<tr>
				<td class="textoCampo">Tipo de Persona:</td>
				<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="persona.tipoPersona"/>
		  		</td>
				<td class="textoCampo">Sexo:</td>
				<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="persona.sexo"/>
		  		</td>
			</tr>	
			<tr>
				<td class="textoCampo">CUIT/CUIL:</td>
				<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="persona.docFiscal"/>					
				</td>
				
				<td class="textoCampo">N&uacute;mero de Documento:</td>
				<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="persona.docPersonal"/>		
				</td>	
			</tr>
			<tr>
				<td class="textoCampo">Condici&oacute;n Fiscal IVA:</td>
				<td class="textoDato">
						<@s.select 
							templateDir="custontemplates" 
							id="situacionFiscal" 
							cssClass="textarea"
							cssStyle="width:165px" 
							name="persona.situacionFiscal.codigo" 
							list="situacionFiscalList" 
							listKey="codigo" 
							listValue="descripcion" 
							title="Documento Fiscal V&aacute;lido"
							headerKey=""
							headerValue="Seleccionar"  
							/>
		  		</td>
				<td class="textoCampo">Condici&oacute;n Fiscal Impuesto Ganancias:</td>
				<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="persona.inscriptoGanancia"/>
				</td>	
			</tr>		
			<tr>
				<td class="textoCampo">Fecha Vencimiento AFIP:</td>
				<td class="textoDato">
					<#if persona.venceFormDGI?exists>
						<#assign venceFormDGI = persona.venceFormDGI> 
						${venceFormDGI?string("dd/MM/yyyy")}	
					</#if>
				</td>
				<td class="textoCampo">Fecha Nacimiento/Inicio:</td>
				<td class="textoDato">
					<#if persona.fechaNacimiento?exists>
						<#assign fechaNacimiento = persona.fechaNacimiento> 
						${fechaNacimiento?string("dd/MM/yyyy")}	
					</#if>
				</td>
			</tr>
			<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>
		
		</table>				
	</div>				
				
	<@s.if test="detallePersonaList!=null">
		<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">			
			<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Detalles de Personas encontradas</td>
					<td>
						<div align="right">
							<@s.a 
								templateDir="custontemplates"
								cssClass="item" 
								id="crearSecuenciaProveedor"										
								name="crearSecuenciaProveedor"
								href="javascript://nop/">
								<b>Agregar Persona Detalle / Proveedor</b>										
								<img src="${request.contextPath}/common/images/agregar.gif" border="0" align="absmiddle" hspace="3" >
							</@s.a>						
						</div>
					</td>
				</tr>
			</table>
						
			<@ajax.anchors target="contentTrx">			
			   		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="detallePersonaList" id="detalleDePersona" defaultsort=1>								
						<#assign ref="${request.contextPath}/maestro/proveedor/selectDetallePersonaFactura.action?">
						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" style=" text-align:center; display:block" title="Acciones">
							<@s.a href="${ref}detalleDePersona.pk.secuencia=${detalleDePersona.pk.secuencia?c}&detalleDePersona.pk.identificador=${detalleDePersona.pk.identificador?c}&navegacionIdBack=${navegacionIdBack}&navigationId=${navigationId}">
								<img src="${request.contextPath}/common/images/seleccionar.gif" alt="Seleccionar" title="Seleccionar" width="16" height="16" border="0">
							</@s.a>
						</@display.column>
						
						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="razonSocial" title="Raz&oacute;n Social" />
						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="nombre" title="Nombre" /> 
						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="persona.docPersonal" title="Documento" />
						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="persona.docFiscal" title="CUIT/CUIL" />	
						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" title="Direcci&oacute;n">
							${detalleDePersona.codigoPostal.descripcion},
							${detalleDePersona.codigoPostal.provincia.descripcion}-
							${detalleDePersona.codigoPostal.provincia.pais.descripcion}
						</@display.column>
					</@display.table>
				</@ajax.anchors>
			</div>	
		</@s.if>
		
		<@s.if test="proveedorList!=null">
		    		<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Proveedores encontrados</td>
					

				</tr>
			</table>	
			<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">						
				<@ajax.anchors target="contentTrx">			
	          		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="proveedorList" id="proveedor" defaultsort=1>								
						
						
					<#assign ref="${request.contextPath}/maestro/proveedor/selectProveedorFactura.action?">
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" style=" text-align:center; display:block" title="Acciones">
						<@s.a href="${ref}proveedor.nroProveedor=${proveedor.nroProveedor?c}&navegacionIdBack=${navegacionIdBack}&navigationId=${navigationId}">
							<img src="${request.contextPath}/common/images/seleccionar.gif" alt="Seleccionar" title="Seleccionar" width="16" height="16" border="0">
						</@s.a>
					
					</@display.column>
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="nombreFantasia" title="Nombre de Fantasia" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="detalleDePersona.razonSocial" title="Raz&oacute;n Social" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="detalleDePersona.nombre" title="Nombre" /> 
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="detalleDePersona.persona.docPersonal" title="Documento" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="detalleDePersona.persona.docFiscal" title="CUIT/CUIL" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" title="Direcci&oacute;n">
					<#if proveedor.detalleDePersona.codigoPostal?exists>
						${proveedor.detalleDePersona.codigoPostal.descripcion},
						${proveedor.detalleDePersona.codigoPostal.provincia.descripcion}-
						${proveedor.detalleDePersona.codigoPostal.provincia.pais.descripcion}
					</#if>
					</@display.column>							

				</@display.table>
				</@ajax.anchors>
			</div>	
		</@s.if>			
										
	<div id="capaBotonera" class="capaBotonera">
		<table id="tablaBotonera" class="tablaBotonera">
			<tr> 
				<td align="left">
					<input id="cancel" type="button" name="btnVolver" value="Volver" class="boton"/>
				</td>
			</tr>	
		</table>
	</div>
	
 <@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navegacionIdBack},flowControl=back"/>
							

<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/proveedor/selectPersonaFactura.action" 
  source="crearSecuenciaProveedor" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navegacionIdBack=${navegacionIdBack},navigationId=${navigationId},persona.oid={persona.oid}"/>