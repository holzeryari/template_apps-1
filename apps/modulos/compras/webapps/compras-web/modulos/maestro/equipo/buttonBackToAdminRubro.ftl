<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="left">
				<input id="cancel" type="button" name="btnVolver" value="Volver" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

  <@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=equipo-administrar,flowControl=back"/>
  
  <@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/equipo/securityRubroList.action" 
  source="buscar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="rubro.codigo={rubro.codigo},rubro.descripcion={rubro.descripcion},rubro.imputable={rubro.imputable},rubroNivel0.oid={rubroNivel0},rubroNivel1.oid={rubroNivel1},rubroNivel2.oid={rubroNivel2},rubroNivel3.oid={rubroNivel3},equipoIngreso.oid={equipoIngreso.oid},navigationId=${navigationId},flowControl=regis"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/equipo/createRubroView.action" 
  source="limpiar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId=${navigationId},flowControl=regis,equipoIngreso.oid={equipoIngreso.oid}"/>
  
