<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="left">
				<input id="cancel" type="button" name="btnVolver" value="Volver" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

  <@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=equipo-administrar,flowControl=back"/>
  
  <@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/equipo/securityUserList.action" 
  source="buscar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="usuarioSeguridad.username={usuarioSeguridad.username},usuarioSeguridad.name={usuarioSeguridad.name},usuarioSeguridad.fullname={usuarioSeguridad.fullname},equipoIngreso.oid={equipoIngreso.oid},navigationId=${navigationId},flowControl=regis"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/equipo/createSecurityUserView.action" 
  source="limpiar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId=${navigationId},flowControl=regis,equipoIngreso.oid={equipoIngreso.oid}"/>
  
