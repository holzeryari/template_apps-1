<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>

<@s.hidden id="webFlow" name="webFlow"/>
<@s.hidden id="viewState" name="viewState"/>
<@s.hidden id="navigationId" name="navigationId"/>
	
	<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
        	<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Datos del Equipo de Ingreso</b></td>
				<td>					
					<div align="right">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0825" 
							enabled="true" 
							cssClass="item" 
							id="crear"										
							href="javascript://nop/">
							<b>Agregar</b>										
							<img src="${request.contextPath}/common/images/agregar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
						</@security.a>
					</div>
				</td>
			</tr>
		</table>
		
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
			<tr>
	      		<td class="textoCampo">Nombre:</td>
	      		<td class="textoDato">
				     <@s.textfield 
	      				templateDir="custontemplates" 
						id="nombre" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="equipoIngreso.nombre" 
						title="Nombre" />
				</td>
	    	</tr>				
			<tr>
	    		<td colspan="4" class="lineaGris" align="right">
	      			<input id="limpiar" type="button" name="btnVolver" value="Limpiar" class="boton"/>		
	      			<input id="buscar" type="button" name="btnVolver" value="Buscar" class="boton"/>
	    		</td>
			</tr>
		</table>
	  </div>  	
	  <@s.if test="equipoIngresoList!=null">
	  	<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">									
			<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Equipos encontrados</td>
				</tr>	
			</table>
			<@vc.anchors target="contentTrx">	
				<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="equipoIngresoList" id="equipoIngreso" pagesize=15 partialList=true size="recordSize" keepStatus=true excludedParams="resetFlag">
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" class="botoneraAnchoCon3" title="Acciones" >
							<div class="alineacion">
								<@security.a 
									templateDir="custontemplates" 
									securityCode="CUF0826" 
									enabled="equipoIngreso.readable" 
									cssClass="item" 
									href="${request.contextPath}/maestro/equipo/readView.action?equipoIngreso.oid=${equipoIngreso.oid?c}&navegacionIdBack=buscar-equipos">
									<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver" border="0">
								</@security.a>
							</div>
							<div class="alineacion">
								<@security.a 
									templateDir="custontemplates" 
									securityCode="CUF0827" 
									name="CUF0043"
									enabled="equipoIngreso.updatable" 
									cssClass="item" 
									href="${request.contextPath}/maestro/equipo/administrarView.action?equipoIngreso.oid=${equipoIngreso.oid?c}&equipoIngreso.versionNumber=${equipoIngreso.versionNumber?c}&navigationId=equipo-administrar&flowControl=regis&navegacionIdBack=buscar-equipos">
									<img  src="${request.contextPath}/common/images/administrar.gif" alt="Administrar" title="Administrar"  border="0">
								</@security.a>
							</div>							
							<div class="alineacion">
								<@security.a 
									templateDir="custontemplates" 
									securityCode="CUF0828" 
									enabled="equipoIngreso.eraseable"
									cssClass="item"  
									href="${request.contextPath}/maestro/equipo/deleteView.action?equipoIngreso.oid=${equipoIngreso.oid}&navigationId=equipo-delete&flowControl=regis&navegacionIdBack=buscar-equipos">
									<img  src="${request.contextPath}/common/images/eliminar.gif" alt="Eliminar" title="Eliminar" border="0">
								</@security.a>									
							</div>
					</@display.column>

					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="nombre" title="Nombre" />
				</@display.table>
			</@vc.anchors>
	  	</div>
	  </@s.if>
	  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/equipo/createView.action" 
  source="crear" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=crear-equipos,navegacionIdBack=buscar-equipos,flowControl=regis"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/equipo/search.action" 
  source="buscar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="resetFlag=true,navigationId=buscar-equipos,flowControl=regis,equipoIngreso.nombre={nombre}"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/equipo/view.action" 
  source="limpiar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId=buscar-equipos,flowControl=regis"/>