<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<@vc.anchors target="contentTrx" ajaxFlag="ajax">
	<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
		<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
			<tr>
				<td>Dep&oacute;sitos</td>
				<td>
					<div align="right">
						<@s.a href="${request.contextPath}/maestro/equipo/createDepositoView.action?equipoIngreso.oid=${equipoIngreso.oid?c}&navegacionIdBack=${navigationId}">
						<b>Agregar</b><img src="${request.contextPath}/common/images/agregar.gif" border="0" align="absmiddle" hspace="3" >
						</@s.a>
					</div>
				</td>	
			</tr>
		</table>
		
		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="equipoIngreso.depositoList" id="deposito">	
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" style=" text-align:center; display:block" title="Acciones">
				<a href="${request.contextPath}/maestro/equipo/deleteDepositoView.action?deposito.oid=${deposito.oid?c}&deposito.descripcion=${deposito.descripcion}&equipoIngreso.oid=${equipoIngreso.oid?c}">
					<img  src="${request.contextPath}/common/images/eliminar.gif" alt="Eliminar" title="Eliminar" border="0"></a>
			</@display.column>		
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="oid" title="C&oacute;digo" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="descripcion" title="Descripci&oacute;n" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service"  class="estiloTexto" property="tipoOrganizacion" title="Tipo Organizaci&oacute;n" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="estado" title="Estado" />
		</@display.table>
	</div>	
</@vc.anchors>