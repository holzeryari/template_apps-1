<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<#--<div id="capaSeparadora" class="capaSeparadora">&nbsp;</div>-->

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancel" type="button" name="btnVolver" value="Cancelar" class="boton"/>
				<input id="store" type="button" name="btnQuitar" value="Eliminar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/equipo/deleteDeposito.action" 
  source="store" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="deposito.oid=${deposito.oid},equipoIngreso.oid=${equipoIngreso.oid}"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/equipo/administrarView.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=equipo-administrar,flowControl=back,deposito.oid=${deposito.oid},equipoIngreso.oid=${equipoIngreso.oid}"/>