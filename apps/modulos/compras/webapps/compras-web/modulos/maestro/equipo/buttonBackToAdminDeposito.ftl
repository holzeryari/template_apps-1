<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="left">
				<input id="cancel" type="button" name="btnVolver" value="Volver" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=equipo-administrar,flowControl=back"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/equipo/securityDepositoList.action" 
  source="buscar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="deposito.oid={deposito.oid},deposito.tipoOrganizacion={deposito.tipoOrganizacion},deposito.descripcion={deposito.descripcion},deposito.estado={deposito.estado},equipoIngreso.oid={equipoIngreso.oid},navigationId=${navigationId},flowControl=regis"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/equipo/createDepositoView.action" 
  source="limpiar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId=${navigationId},flowControl=regis,equipoIngreso.oid={equipoIngreso.oid}"/>
  
