<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos de la Persona</b></td>
				<td>
					<div align="right">
						<@s.a templateDir="custontemplates" id="updatePersonaView" name="updatePersonaView" href="javascript://nop/" cssClass="ocultarIcono">
							<b>Modificar</b><img src="${request.contextPath}/common/images/modificar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
						</@s.a>
						 <@vc.htmlContent 
							baseUrl="${request.contextPath}/maestro/persona/updateView.action" 
							source="updatePersonaView" 
							success="contentTrx" 
							failure="errorTrx"  
							parameters="navigationId=abm-persona-update,navegacionIdBack={navigationId},flowControl=regis,persona.oid=${persona.oid?c}"/>	
					</div>
				</td>
			</tr>
		</table>

		<@tiles.insertAttribute name="persona"/>

		</div>
		
		<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos de la Secuencia</b></td>
				<td>
					<div align="right">
						<@s.a templateDir="custontemplates" id="administarDetallePersonaView" name="administarDetallePersonaView" href="javascript://nop/" cssClass="ocultarIcono">
							<b>Administrar</b><img src="${request.contextPath}/common/images/administrar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
						</@s.a>
						<@vc.htmlContent 
							baseUrl="${request.contextPath}/maestro/persona/detalle/administrarView.action" 
							source="administarDetallePersonaView" 
							success="contentTrx" 
							failure="errorTrx"  
							parameters="detalleDePersona.pk.identificador=${detalleDePersona.pk.identificador?c},detalleDePersona.pk.secuencia=${detalleDePersona.pk.secuencia?c},navigationId=abm-detalleDePersona,flowControl=regis,navegacionIdBack=${navigationId}"/>
					</div>
				</td>
			</tr>
		</table>
							
		<@tiles.insertAttribute name="detalle"/>
		
		</div>

