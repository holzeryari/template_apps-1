<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>


<@ajax.anchors target="contentTrx">	

<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
	<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
		<tr>
			<td>Secuencias de la Persona</td>
			<td>
				<div align="right">
				<@s.a href="${request.contextPath}/maestro/persona/detalle/createView.action?navigationId=abm-detalleDePersona-createView&flowControl=regis&navegacionIdBack=${navigationId}&detalleDePersona.pk.identificador=${persona.oid?c}">
					<b>Agregar</b><img src="${request.contextPath}/common/images/agregar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3">
				</@s.a>
				</div>
			</td>
		</tr>
	</table>	
	
	<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="persona.detalles" id="detalle"  defaultsort=2>
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" class="botoneraAnchoCon4" title="Acciones">
			<#if navigationId != 'administrar-persona' >
				<div class="alineacion">
<#--
					<@security.a 
						templateDir="custontemplates" 
						securityCode="CUF0001" 
						enabled="detalle.readable" 
						cssClass="item" 
						name="selectable"
						href="${request.contextPath}/maestro/persona/detalle/seleccionar.action?detalleDePersona.pk.identificador=${detalle.pk.identificador?c}&detalleDePersona.pk.secuencia=${detalle.pk.secuencia?c}">
						<img  src="${request.contextPath}/common/images/seleccionar.gif" alt="Seleccionar" title="Seleccionar"  border="0">
					</@security.a>
-->
					<@s.a id="selectSecuenciaPersona" 
						name="selectSecuenciaPersona"
						href="${request.contextPath}/maestro/persona/detalle/seleccionar.action?detalleDePersona.pk.identificador=${detalle.pk.identificador?c}&detalleDePersona.pk.secuencia=${detalle.pk.secuencia?c}"
						cssClass="item"
						templateDir="custontemplates">
						<img  src="${request.contextPath}/common/images/seleccionar.gif" alt="Seleccionar" title="Seleccionar"  border="0">
					</@s.a>				

				</div>
			</#if>
		
			<div class="alineacion">
<#--
				<@security.a 
					templateDir="custontemplates" 
					securityCode="CUF0001"
					enabled="detalle.readable" 
					cssClass="item"
					name="readable" 
					href="${request.contextPath}/maestro/persona/detalle/readView.action?detalleDePersona.pk.identificador=${detalle.pk.identificador?c}&detalleDePersona.pk.secuencia=${detalle.pk.secuencia?c}&navigationId=abm-detalleDePersona-read&navegacionIdBack=${navigationId}">
					<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver"  border="0">
				</@security.a>
-->
				<@s.a id="readSecuenciaPersona" 
					name="readSecuenciaPersona"
					href="${request.contextPath}/maestro/persona/detalle/readView.action?detalleDePersona.pk.identificador=${detalle.pk.identificador?c}&detalleDePersona.pk.secuencia=${detalle.pk.secuencia?c}&navigationId=abm-detalleDePersona-read&navegacionIdBack=${navigationId}"
					cssClass="item"
					templateDir="custontemplates">
					<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver"  border="0">
				</@s.a>				

			</div>
			<div class="alineacion">
<#--
				<@security.a 
					templateDir="custontemplates" 
					securityCode="CUF0001" 
					enabled="detalle.updatable"
					cssClass="item"  
					name="updatable"
					href="${request.contextPath}/maestro/persona/detalle/administrarView.action?detalleDePersona.pk.identificador=${detalle.pk.identificador?c}&detalleDePersona.pk.secuencia=${detalle.pk.secuencia?c}&navigationId=abm-detalleDePersona&flowControl=regis&navegacionIdBack=${navigationId}">
					<img  src="${request.contextPath}/common/images/administrar.gif" alt="Administrar" title="Administrar"  border="0">
				</@security.a>
-->
				<@s.a id="adminSecuenciaPersona" 
					name="adminSecuenciaPersona"
					href="${request.contextPath}/maestro/persona/detalle/administrarView.action?detalleDePersona.pk.identificador=${detalle.pk.identificador?c}&detalleDePersona.pk.secuencia=${detalle.pk.secuencia?c}&navigationId=abm-detalleDePersona&flowControl=regis&navegacionIdBack=${navigationId}"
					cssClass="item"
					templateDir="custontemplates">
					<img  src="${request.contextPath}/common/images/administrar.gif" alt="Administrar" title="Administrar"  border="0">
				</@s.a>				
			</div>
<#-- no implementado
			<div class="alineacion">
				<@security.a 
					templateDir="custontemplates" 
					securityCode="CUF0104" 
					enabled="detalle.eraseable"
					cssClass="item"  
					name="eraseable"
					href="${request.contextPath}/maestro/persona/detalle/deleteView.action?detalleDePersona.pk.identificador=${detalle.pk.identificador?c}&detalleDePersona.pk.secuencia=${detalle.pk.secuencia?c}">
					<img  src="${request.contextPath}/common/images/eliminar.gif" alt="Eliminar" title="Eliminar"  border="0">
				</@security.a>
			</div>
-->
		</@display.column>			
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="pk.secuencia" title="Secuencia" />
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="razonSocial" title="Raz&oacute;n Social" />
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="nombre" title="Nombre" /> 
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="codigoPostal.pk.identificador" title="CP" />
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="codigoPostal.descripcion" title="Localidad" />
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="codigoPostal.provincia.descripcion" title="Provincia" />
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="codigoPostal.provincia.pais.descripcion" title="Pais" />
	</@display.table>
	</div>
</@ajax.anchors>
