<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>
<@s.hidden id="navigationId" name="navigationId"/>
	
	<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
        	<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Datos de la Persona</b></td>
				<td>					
					<div align="right">
						<@security.a id="create" 
							name="create"
							href="javascript://nop/" 
							cssClass="ocultarIcono" 
							enabled="true" 
							templateDir="custontemplates" 
							securityCode="CUF0103">
							<b>Agregar</b>
							<img src="${request.contextPath}/common/images/agregar.gif" border="0" align="absmiddle" hspace="3" >
						</@security.a>
					</div>
				</td>
			</tr>
		</table>
	
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
			<tr>
				<td class="textoCampo">Identificador:</td>
				<td class="textoDato">
					<@s.textfield 
     					templateDir="custontemplates" 
						id="oid" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="persona.oid" 
						title="Identificador" />
				</td>
				<td class="textoCampo">Tipo de Persona:</td>
				<td class="textoDato" align="left" width="30%">
					<@s.select 
						templateDir="custontemplates" 
						id="tipoPersona" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="persona.tipoPersona" 
						list="tipoPersonaList" 
						listKey="key" 
						listValue="description" 
						value="persona.tipoPersona.ordinal()"
						title="Tipo de Persona"
						headerKey="-1"
						headerValue="Todos"  
						/>
				</td>
			</tr>
			<tr>	
				<td class="textoCampo">Raz&oacute;n Social:</td>
				<td class="textoDato">
					<@s.textfield 
      					templateDir="custontemplates" 
						id="razonSocial" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="detalleDePersona.razonSocial" 
						title="Raz&oacute;n Social" /></td>
				<td class="textoCampo">Nombre:</td>
				<td class="textoDato">
					<@s.textfield 
	    				templateDir="custontemplates" 
						id="nombre" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="detalleDePersona.nombre" 
						title="Nombre" />
				</td>
			</tr>
			<tr>	
				<td class="textoCampo">CUIT/CUIL:</td>
      			<td class="textoDato">
      				<@s.textfield 
      					templateDir="custontemplates" 
						id="docFiscal" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="persona.docFiscal" 
						title="CUIT/CUIL" />
				</td>
				<td class="textoCampo">N&uacute;mero de Documento:</td>
      			<td class="textoDato">
      				<@s.textfield 
      					templateDir="custontemplates" 
						id="docPersonal" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="persona.docPersonal" 
						title="N&uacute;mero de Documento" />
				</td>
			</tr>
			<tr>
    			<td class="textoCampo">Pa&iacute;s:</td>
				<td class="textoDato">
	      			<@s.select 
						templateDir="custontemplates" 
						id="detalleDePersona.codigoPostal.provincia.pais.codigo" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="detalleDePersona.codigoPostal.provincia.pais.codigo" 
						list="paisList" 
						listKey="codigo" 
						listValue="descripcion" 
						title="Pais"
						headerKey="-1"
						headerValue="Todos"  
						value="detalleDePersona.codigoPostal.provincia.pais.codigo"
						/>
				</td>
				<td class="textoCampo">Provincia:</td>
      			<td class="textoDato">
      				<@s.select 
						templateDir="custontemplates" 
						id="detalleDePersona.codigoPostal.provincia.codigo" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="detalleDePersona.codigoPostal.provincia.codigo" 
						list="provinciaList" 
						listKey="codigo" 
						listValue="descripcion"  
						title="Provincia"
						headerKey="-1"
						headerValue="Todos"
						value="detalleDePersona.codigoPostal.provincia.codigo"
						/>
				</td>
			</tr>
			<tr>
				<td class="textoCampo">C&oacute;digo Postal:</td>
				<td class="textoDato">
					<#if (detalleDePersona?exists && detalleDePersona.codigoPostal?exists && detalleDePersona.codigoPostal.pk?exists && detalleDePersona.codigoPostal.pk.identificador?exists)>
					
						<@s.property default="&nbsp;" escape=false value="detalleDePersona.codigoPostal.pk.identificador"/>
							-									 
						<@s.property default="&nbsp;" escape=false value="detalleDePersona.codigoPostal.descripcion" />										
						<@s.hidden id="secuenciaCP" name="detalleDePersona.codigoPostal.pk.secuencia"/>
						<@s.hidden id="descripcionCodigoPostal" name="detalleDePersona.codigoPostal.descripcion"/>
						<@s.hidden id="codigoPostal" name="detalleDePersona.codigoPostal.pk.identificador"/>
					
					</#if>
					<@s.a templateDir="custontemplates" id="codigoPostalView" name="codigoPostalView" href="javascript://nop/" cssClass="ocultarIcono">
						<img src="${request.contextPath}/common/images/buscar.gif" title="C&oacute;digo postal" align="top" border="0" hspace="3">	
					</@s.a>
				</td>
				<td class="textoCampo">Sexo:</td>
      			<td class="textoDato">
	      			<@s.select 
						templateDir="custontemplates" 
						id="sexo" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="persona.sexo" 
						list="sexoList" 
						listKey="key" 
						listValue="description" 
						value="persona.sexo.ordinal()"
						title="Sexo"
						headerKey="0"
						headerValue="Todos"  
						/>
				</td>
			</tr>
			<tr>
	    		<td colspan="4" class="lineaGris" align="right">
	      			<input id="limpiar" type="button" name="btnVolver" value="Limpiar" class="boton"/>		
	      			<input id="buscar" type="button" name="btnVolver" value="Buscar" class="boton"/>
	    		</td>
			</tr>	
		</table>
	</div>
	
	<!-- Resultado Filtro -->
	<@s.if test="detalleDePersonaList!=null">
		<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">			
			<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Personas encontradas</td>
				</tr>
			</table>
		
		
		<@ajax.anchors target="contentTrx">			
    		<@display.table 
  				class="tablaDetalleCuerpo" 
  				cellpadding="3" 
  				name="detalleDePersonaList" 
  				id="detalleDePersona" 
  				pagesize=15 
  				defaultsort=2
  				partialList=true 
  				size="recordSize"
  				keepStatus=true
  				excludedParams="resetFlag">
		
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" class="botoneraAnchoCon2" title="Acciones">
							
					<div class="alineacion">
						<@security.a templateDir="custontemplates" 
							securityCode="CUF0101" 
							enabled="persona.readable"
							cssClass="item"
							id="read" 
							name="read" 
							href="${request.contextPath}/maestro/persona/readView.action?persona.oid=${detalleDePersona.persona.oid?c}&navigationId=abm-persona&flowControl=regis">
							<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver"  border="0">
						</@security.a>
					</div>
					<div class="alineacion">
						<@security.a templateDir="custontemplates" 
							securityCode="CUF0102" 
							enabled="persona.updatable"
							id="admin" 
							name="admin"
							cssClass="item" 
							href="${request.contextPath}/maestro/persona/administrarView.action?persona.oid=${detalleDePersona.persona.oid?c}&navigationId=administrar-persona&flowControl=regis"
							><img src="${request.contextPath}/common/images/administrar.gif" alt="Administrar" title="Administrar"  border="0"
							></@security.a>
					</div>
				</@display.column>
		
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="persona.oid" title="Identificador"/>
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="persona.tipoPersona" title="Tipo de Persona"/>
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="razonSocial" title="Raz&oacute;n Social" />
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="nombre" title="Nombre" /> 
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="persona.sexo" title="Sexo"/>
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="persona.docPersonal" title="Documento"/>
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="persona.docFiscal" title="CUIT/CUIL"/>
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="codigoPostal.pk.identificador" title="CP"/>
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="codigoPostal.provincia.descripcion" title="Provincia"/>
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="codigoPostal.provincia.pais.descripcion" title="Pa&iacute;s"/>
			</@display.table>
		</@ajax.anchors>
		</div>	
	</@s.if>


<@ajax.select
  baseUrl="${request.contextPath}/maestro/persona/codigoPostal/provinciasView.action"
  source="detalleDePersona.codigoPostal.provincia.pais.codigo"
  target="detalleDePersona.codigoPostal.provincia.codigo"
  parameters="pais.codigo={detalleDePersona.codigoPostal.provincia.pais.codigo}" 
  parser="new ResponseXmlParser()"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/persona/search.action" 
  source="buscar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="resetFlag=true,navigationId={navigationId},flowControl=regis,persona.tipoPersona={tipoPersona},detalleDePersona.nombre={nombre},persona.docFiscal={docFiscal},detalleDePersona.razonSocial={razonSocial},persona.sexo={sexo},persona.docPersonal={docPersonal},detalleDePersona.codigoPostal.provincia.codigo={detalleDePersona.codigoPostal.provincia.codigo},detalleDePersona.codigoPostal.provincia.pais.codigo={detalleDePersona.codigoPostal.provincia.pais.codigo},detalleDePersona.codigoPostal.pk.identificador={codigoPostal},detalleDePersona.codigoPostal.pk.secuencia={secuenciaCP},detalleDePersona.codigoPostal.descripcion={descripcionCodigoPostal},persona.oid={persona.oid}"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/persona/view.action" 
  source="limpiar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=regis"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/persona/createView.action" 
  source="create" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=administrar-persona-create,navegacionIdBack={navigationId},flowControl=regis"/>
  
  
  <@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/persona/selectCodigoPostal.action" 
  source="codigoPostalView" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=change,persona.tipoPersona={tipoPersona},detalleDePersona.nombre={nombre},persona.docFiscal={docFiscal},detalleDePersona.razonSocial={razonSocial},persona.sexo={sexo},persona.docPersonal={docPersonal},detalleDePersona.codigoPostal.provincia.codigo={detalleDePersona.codigoPostal.provincia.codigo},detalleDePersona.codigoPostal.provincia.pais.codigo={detalleDePersona.codigoPostal.provincia.pais.codigo},detalleDePersona.codigoPostal.pk.identificador={codigoPostal},detalleDePersona.codigoPostal.pk.secuencia={secuenciaCP},detalleDePersona.codigoPostal.descripcion={descripcionCodigoPostal}"/>
  
 <@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=back"/>
  