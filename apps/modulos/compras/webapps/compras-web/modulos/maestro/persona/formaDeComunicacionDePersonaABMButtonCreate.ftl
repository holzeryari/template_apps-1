<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancel" type="button" name="btnVolver" value="Cancelar" class="boton"/>
				<input id="store" type="button" name="btnAgregar" value="Agregar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/persona/formaDeComunicacionDePersona/create.action" 
  source="store" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=${navigationId},navegacionIdBack=${navegacionIdBack},formaDeComunicacionDePersona.id.identificador={identificador},formaDeComunicacionDePersona.id.secuencia={secuencia},formaDeComunicacionDePersona.formaDeComunicacion.codigo={formaDeComunicacion},formaDeComunicacionDePersona.id.descripcion={formaDeComunicacionDePersona}"/>
  
  <@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=${navegacionIdBack},flowControl=back"/>

  