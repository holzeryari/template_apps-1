<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>

<@s.hidden id="navigationId" name="navigationId"/>
<@s.hidden id="navegacionIdBack" name="navegacionIdBack"/>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
        	<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Datos del C&oacute;digo Postal</b></td>
			</tr>
		</table>
			
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>		
        	<tr>	
				<td class="textoCampo">C&oacute;digo:</td>
      			<td class="textoDato">
      				<@s.textfield 
      					templateDir="custontemplates" 
						id="codigo" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="codigoPostal.pk.identificador" 
						title="C�digo" />
				</td>
				<td class="textoCampo">Descripci&oacute;n:</td>
      			<td class="textoDato">
      				<@s.textfield
      					templateDir="custontemplates" 
						id="descripcion" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="codigoPostal.descripcion" 
						title="N&uacute;mero de Documento" />
				</td>
			</tr>
			<tr>
    			<td class="textoCampo">Pa&iacute;s:</td>
				<td class="textoDato">
	      			<@s.select 
						templateDir="custontemplates" 
						id="codigoPostal.provincia.pais.codigo" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="codigoPostal.provincia.pais.codigo" 
						value="codigoPostal.provincia.pais.codigo"
						list="paisList" 
						listKey="codigo" 
						listValue="descripcion" 
						title="Pais"
						headerKey="-1"
						headerValue="Todos"  
						/>
				</td>
				<td class="textoCampo">Provincia:</td>
      			<td class="textoDato">
      				<@s.select 
						templateDir="custontemplates" 
						id="codigoPostal.provincia.codigo" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="codigoPostal.provincia.codigo"
						value="codigoPostal.provincia.codigo" 
						list="provinciaList" 
						listKey="codigo" 
						listValue="descripcion"  
						title="Provincia"	
						headerKey="-1"									
						headerValue="Seleccionar"
						/>
				</td>
			</tr>
				<tr>
	    		<td colspan="4" class="lineaGris" align="right">
	      			<input id="limpiar" type="button" name="btnVolver" value="Limpiar" class="boton"/>		
	      			<input id="buscar" type="button" name="btnVolver" value="Buscar" class="boton"/>
	    		</td>
			</tr>	
		</table>
	</div>
	<!-- Resultado Filtro -->
	<@s.if test="codigoPostalList!=null">
		<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">			
			<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>C&oacute;digos Postales encontrados</td>
				</tr>
			</table>
			<@ajax.anchors target="contentTrx">			
				<@display.table class="tablaDetalleCuerpo" 
          			cellpadding="3" 
          			name="codigoPostalList" 
          			id="codigoPostal" 
          			pagesize=15 
          			defaultsort=1
          			partialList=true 
          			size="recordSize"
          			keepStatus=true
          			excludedParams="resetFlag" >								
								
					<@s.if test="nameSpaceSelect!=''">								
						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" style=" text-align:center; display:block" title="Acciones">
							<div align="center" nowrap>	
							<@s.a 
								templateDir="custontemplates"
								cssClass="item" 
								href="${request.contextPath}/maestro/persona/codigoPostal/selectCodigoPostalRedirec.action?navigationId=${navigationId}&codigoPostal.pk.identificador=${codigoPostal.pk.identificador?c}&codigoPostal.pk.secuencia=${codigoPostal.pk.secuencia?c}&nameSpaceSelect=${nameSpaceSelect}&actionNameSelect=${actionNameSelect}&oidParameter=${oidParameter}"
								><img  src="${request.contextPath}/common/images/seleccionar.gif" alt="Ver" title="Ver"  border="0"
								></@s.a>
						
							</div>
						</@display.column>
					</@s.if>
					<@s.else>
						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" style=" text-align:center; display:block" title="Acciones">
						<div align="center" nowrap>	
						<@s.a 
							templateDir="custontemplates"										
							cssClass="item"
							href="${request.contextPath}/maestro/persona/codigoPostal/select.action?navigationId=${navigationId}&navegacionIdBack=${navegacionIdBack}&codigoPostal.pk.identificador=${codigoPostal.pk.identificador?c}&codigoPostal.pk.secuencia=${codigoPostal.pk.secuencia?c}&codigoPostal.provincia.codigo=${codigoPostal.provincia.codigo}&codigoPostal.provincia.pais.codigo=${codigoPostal.provincia.pais.codigo}">
							<img  src="${request.contextPath}/common/images/seleccionar.gif" alt="Ver" title="Ver"  border="0">
							</@s.a>
					
						</div>
						</@display.column>
					</@s.else>
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="pk.identificador" title="C&oacute;digo" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="pk.secuencia" title="Secuencia" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="descripcion" title="Descripci&oacute;n" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="zonaRiesgo" title="Zona Riesgo" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="provincia.descripcion" title="Provincia" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="provincia.pais.descripcion" title="Pa&iacute;s" />
					
				</@display.table>
			</@ajax.anchors>
			</div>
		</@s.if>   

		<div id="capaBotonera" class="capaBotonera">
			<table id="tablaBotonera" class="tablaBotonera">
			<tr> 
				<td align="left">
					<input id="cancel" type="button" name="btnVolver" value="Volver" class="boton"/>
				</td>
			</tr>	
			</table>
		</div>

<@ajax.select
  baseUrl="${request.contextPath}/maestro/persona/codigoPostal/provinciasView.action"
  source="codigoPostal.provincia.pais.codigo"
  target="codigoPostal.provincia.codigo"
  parameters="pais.codigo={codigoPostal.provincia.pais.codigo}" 
  parser="new ResponseXmlParser()"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/persona/codigoPostal/search.action" 
  source="buscar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="resetFlag=true,navigationId={navigationId},navegacionIdBack={navegacionIdBack},codigoPostal.pk.identificador={codigo},codigoPostal.descripcion={descripcion},codigoPostal.provincia.codigo={codigoPostal.provincia.codigo},codigoPostal.provincia.pais.codigo={codigoPostal.provincia.pais.codigo},nameSpaceSelect=${nameSpaceSelect},actionNameSelect=${actionNameSelect},oidParameter=${oidParameter}"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/persona/codigoPostal/view.action" 
  source="limpiar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},navegacionIdBack={navegacionIdBack},nameSpaceSelect=${nameSpaceSelect},actionNameSelect=${actionNameSelect},oidParameter=${oidParameter}"/>

 <@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId=${navegacionIdBack},flowControl=back"/>
