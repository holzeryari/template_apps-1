<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<#assign sexoDisabled="false">
<@s.if test="persona.tipoPersona.ordinal()==1">
	<#assign sexoDisabled="true">
</@s.if>

	<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
        	<tr>
				<td class="textoCampo">Identificador:</td>
				<td class="textoDato"><@s.property default="&nbsp;" escape=false value="persona.oid"/></td>
				<td class="textoCampo">Pa&iacute;s:</td>
				<td class="textoDato">
  					<@s.select 
						templateDir="custontemplates" 
						id="pais" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="persona.pais.codigo" 
						list="paisList" 
						listKey="codigo" 
						listValue="descripcion" 
						title="Pais"
						headerKey="-1"
						headerValue="Seleccionar"/>
				</td>
			</tr>	
			<tr>
				<td class="textoCampo">Tipo de Persona:</td>
				<td class="textoDato">
  					<@s.select 
						templateDir="custontemplates" 
						id="tipoPersona" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="persona.tipoPersona" 
						list="tipoPersonaList" 
						listKey="key" 
						listValue="description" 
						value="persona.tipoPersona.ordinal()"
						title="Tipo de Persona"
						headerKey="-1"
						headerValue="Seleccionar" 
						onchange="javascript:tipoPersonaSexo(this);" 
						/>
				</td>
				<td class="textoCampo">Sexo:</td>
				<td class="textoDato">
  					<@s.select 
						templateDir="custontemplates" 
						id="sexo" 
						cssClass="textarea"
						cssStyle="width:165px"
						disabled="${sexoDisabled}" 
						value=sexoNoAplica
						name="persona.sexo" 
						list="sexoList" 
						listKey="key" 
						listValue="description" 
						value="persona.sexo.ordinal()"
						title="S1exo"
						headerKey="-1"
						headerValue="Seleccionar"
						/>
				</td>
			</tr>	
			<tr>
				<td class="textoCampo">CUIT/CUIL:</td>
				<td class="textoDato">
					<@s.textfield 
						templateDir="custontemplates" 
						id="docFiscal" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="persona.docFiscal" 
						title="CUIT/CUIL" />
						<@s.a templateDir="custontemplates" id="personaDocFisList" name="personaDocFisList" href="javascript://nop/" cssClass="ocultarIcono">
							<img src="${request.contextPath}/common/images/validar.gif" title="Validar Existencia de CUIT/CUIL" align="top" border="0" hspace="3">	
						</@s.a>
				</td>
		
				<td class="textoCampo">N&uacute;mero de Documento:</td>
				<td class="textoDato">

					<div id="docPersonalTexto" style="float:left;">
						<@s.textfield 
							templateDir="custontemplates" 
							id="docPersonal" 
							cssClass="textarea"
							cssStyle="width:160px" 
							name="persona.docPersonal" 
							title="N&uacute;mero de Documento" />
					</div>

					<#assign personaDocBusquedaStyle="display: block;">
					<div id="personaDocBusqueda" style="${personaDocBusquedaStyle}">
						<@s.a templateDir="custontemplates" id="personaDocList" name="personaDocList" href="javascript://nop/" cssClass="ocultarIcono">
							<img src="${request.contextPath}/common/images/validar.gif" title="Validar Existencia de Nro Documento" align="absmiddle" border="0" hspace="3">	
						</@s.a>
					</div>
				</td>	
			</tr>
			<tr>
				<td class="textoCampo">Condici&oacute;n Fiscal IVA:</td>
				<td class="textoDato">
  					<@s.select 
						templateDir="custontemplates" 
						id="situacionFiscal" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="persona.situacionFiscal.codigo" 
						list="situacionFiscalList" 
						listKey="codigo" 
						listValue="descripcion" 
						title="Documento Fiscal V&aacute;lido"
						onchange="javascript:condicionFiscalIVASelect(this);"
						headerKey=""
						headerValue="Seleccionar"  
						/>
				</td>


				<#assign inscriptoGananciaClass="textarea">
				<#assign inscriptoGananciaDisabled="false">

				<@s.if test='persona.situacionFiscal.codigo.equals("")'>
					<#assign inscriptoGananciaClass="textareagris">
					<#assign inscriptoGananciaDisabled="true">
				</@s.if>
				<@s.if test='persona.situacionFiscal.codigo.equals("RMT")'>
					<#assign inscriptoGananciaClass="textareagris">
					<#assign inscriptoGananciaDisabled="true">
				</@s.if>

				<td class="textoCampo">Condici&oacute;n Fiscal Impuesto Ganancias:</td>
				<td class="textoDato">
					<@s.select 
						templateDir="custontemplates" 
						id="inscriptoGanancia" 
						cssClass="${inscriptoGananciaClass}"
<#--						cssClass="textarea" -->
						cssStyle="width:165px" 
						name="persona.inscriptoGanancia"
						value="persona.inscriptoGanancia.ordinal()" 
						list="inscriptoGananciaList" 
						listKey="key" 
						listValue="description"  
						title="Condici�n Fiscal Impuesto Ganancias"	
						disabled="${inscriptoGananciaDisabled}"
						headerKey="-1"
						headerValue="Seleccionar"
						/>
				</td>	
			</tr>		
			<tr>
				<td class="textoCampo">Fecha Vencimiento AFIP:</td>
				<td class="textoDato">
					<@vc.rowCalendar 
						templateDir="custontemplates" 
						id="venceFormDGI" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="persona.venceFormDGI" 
						title="Fecha Vencimiento DGI" />
				</td>
				<td class="textoCampo">Fecha Nacimiento/Inicio:</td>
				<td class="textoDato">
					<@vc.rowCalendar 
						templateDir="custontemplates" 
						id="fechaNacimiento" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="persona.fechaNacimiento" 
						title="Fecha de Nacimiento" 
						/>
				</td>
			</tr>
			<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>
		</table>				

<@ajax.updateField
  baseUrl="${request.contextPath}/maestro/persona/newSelectBox.action"  
  source="docFiscal" 
  target="docPersonal"
  parameters="persona.docFiscal={docFiscal},persona.pais.codigo={pais},persona.tipoPersona={tipoPersona},persona.sexo={sexo},persona.docPersonal={docPersonal},persona.situacionFiscal.codigo={situacionFiscal}, persona.inscriptoGanancia={inscriptoGanancia},persona.venceFormDGI={venceFormDGI}, persona.fechaNacimiento={fechaNacimiento}"
  eventType="blur"
  action="docFiscal" 
  parser="new ResponseXmlParser()"/>
