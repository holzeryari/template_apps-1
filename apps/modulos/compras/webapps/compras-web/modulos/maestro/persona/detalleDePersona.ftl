<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>


	<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
		<tr>
        	<td class="textoCampo" colspan="4">&nbsp;</td>
        </tr>
		<tr>
			<td class="textoCampo">Secuencia:</td>
			<td class="textoDato" colspan="3">
				<@s.property default="&nbsp;" escape=false value="detalleDePersona.pk.secuencia"/></td>
		</tr>	
		<tr>
			<td class="textoCampo">Raz&oacute;n social/Apellido:</td>
			<td class="textoDato">
  				<@s.textfield 
					templateDir="custontemplates" 
					id="razonSocial" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="detalleDePersona.razonSocial" 
					title="Razon social" />
			</td>
			<td class="textoCampo">Nombre:</td>
			<td class="textoDato">
  				<@s.textfield 
					templateDir="custontemplates" 
					id="nombre" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="detalleDePersona.nombre" 
					title="Nombre" />
					<@s.a templateDir="custontemplates" id="personaNomList" name="personaNomList" href="javascript://nop/" cssClass="ocultarIcono">
						<img src="${request.contextPath}/common/images/validar.gif" title="Validar Existencia de Raz&oacute;n Social/Apellido y/o Nombre" align="top" border="0" height="18" hspace="3">	
					</@s.a>
			</td>
		</tr>
		<tr>
			<td class="textoCampo">C&oacute;digo postal:</td>
			<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="detalleDePersona.codigoPostal.pk.identificador"/>
				<@s.hidden id="identificadorCP" name="detalleDePersona.codigoPostal.pk.identificador"/>
				<@s.hidden id="secuenciaCP" name="detalleDePersona.codigoPostal.pk.secuencia"/>
				<@s.a templateDir="custontemplates" id="codigoPostalView" name="codigoPostalView" href="javascript://nop/" cssClass="ocultarIcono">
					<img src="${request.contextPath}/common/images/buscar.gif" title="C&oacute;digo postal" align="top" border="0" hspace="3">	
				</@s.a>	
			</td>
			<td class="textoCampo">Secuencia CP:</td>
			<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="detalleDePersona.codigoPostal.pk.secuencia"/>
				<@s.hidden id="secuenciaCP" name="detalleDePersona.codigoPostal.pk.secuencia"/></td>
		</tr>
		<tr>
			<td class="textoCampo">Localidad:</td>
			<td class="textoDato"><@s.property default="&nbsp;" escape=false value="detalleDePersona.codigoPostal.descripcion"/></td>
			<td class="textoCampo">Provincia:</td>
			<td class="textoDato"><@s.property default="&nbsp;" escape=false value="detalleDePersona.codigoPostal.provincia.descripcion"/></td>
		</tr>
		<tr>
			<td class="textoCampo">Pa&iacute;s:</td>
			<td class="textoDato"><@s.property default="&nbsp;" escape=false value="detalleDePersona.codigoPostal.provincia.pais.descripcion"/></td>
			<td class="textoCampo">Letra CP:</td>
			<td class="textoDato">
				<@s.textfield 
					templateDir="custontemplates" 
					id="letrasCP" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="detalleDePersona.letrasCP" 
					title="C�digo postal" />
			</td>		
		</tr>
		<tr>
			<td class="textoCampo">Calle:</td>
			<td class="textoDato">
  				<@s.textfield 
					templateDir="custontemplates" 
					id="calle" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="detalleDePersona.calle" 
					title="Calle" />
			</td>
			<td class="textoCampo">N&uacute;mero de Finca:</td>
			<td class="textoDato">
				<@s.textfield 
					templateDir="custontemplates" 
					id="numeroFinca" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="detalleDePersona.numeroFinca" 
					title="N�mero de Finca" />
			</td>
		</tr>
		<tr>	
			<td class="textoCampo">Depto:</td>
			<td class="textoDato">
  				<@s.textfield 
					templateDir="custontemplates" 
					id="aptoCasa" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="detalleDePersona.aptoCasa" 
					title="Depto" />
			</td>
			<td class="textoCampo">Entre Calles:</td>
			<td class="textoDato">
				<@s.textfield 
					templateDir="custontemplates" 
					id="entreCalles" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="detalleDePersona.entreCalles" 
					title="Depto" />
			</td>
		</tr>
		<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>
	</table>