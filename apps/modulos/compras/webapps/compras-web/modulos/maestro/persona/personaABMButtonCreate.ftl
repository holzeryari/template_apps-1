<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancel" type="button" name="btnVolver" value="Cancelar" class="boton"/>
				<input id="store" type="button" name="btnAgregar" value="Agregar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/persona/create.action" 
  source="store" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=${navigationId},navegacionIdBack=${navegacionIdBack},nameSpaceSelect=${nameSpaceSelect},actionNameSelect=${actionNameSelect},persona.tipoPersona={tipoPersona},persona.pais.codigo={pais},persona.fechaNacimiento={fechaNacimiento},persona.sexo={sexo},persona.venceFormDGI={venceFormDGI},persona.docFiscal={docFiscal},persona.inscriptoGanancia={inscriptoGanancia},persona.docPersonal={docPersonal},persona.situacionFiscal.codigo={situacionFiscal},detalleDePersona.razonSocial={razonSocial},detalleDePersona.nombre={nombre},detalleDePersona.letrasCP={letrasCP},detalleDePersona.calle={calle},detalleDePersona.numeroFinca={numeroFinca},detalleDePersona.aptoCasa={aptoCasa},detalleDePersona.entreCalles={entreCalles},formaDeComunicacionDePersona.formaDeComunicacion.codigo={formaDeComunicacion},formaDeComunicacionDePersona.id.descripcion={formaDeComunicacionDePersona},detalleDePersona.codigoPostal.pk.secuencia={secuenciaCP},detalleDePersona.codigoPostal.pk.identificador={identificadorCP}"/>
  
  <@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=${navegacionIdBack},flowControl=back"/>
  
   <@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/persona/selectCodigoPostalABM.action" 
  source="codigoPostalView" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=${navigationId},navegacionIdBack=${navegacionIdBack},flowControl=change,nameSpaceSelect=${nameSpaceSelect},actionNameSelect=${actionNameSelect},persona.tipoPersona={tipoPersona},persona.pais.codigo={pais},persona.fechaNacimiento={fechaNacimiento},persona.sexo={sexo},persona.venceFormDGI={venceFormDGI},persona.docFiscal={docFiscal},persona.inscriptoGanancia={inscriptoGanancia},persona.docPersonal={docPersonal},persona.situacionFiscal.codigo={situacionFiscal},detalleDePersona.razonSocial={razonSocial},detalleDePersona.nombre={nombre},detalleDePersona.letrasCP={letrasCP},detalleDePersona.calle={calle},detalleDePersona.numeroFinca={numeroFinca},detalleDePersona.aptoCasa={aptoCasa},detalleDePersona.entreCalles={entreCalles},formaDeComunicacionDePersona.formaDeComunicacion.codigo={formaDeComunicacion},formaDeComunicacionDePersona.descripcion={formaDeComunicacionDePersona},detalleDePersona.codigoPostal.pk.secuencia={secuenciaCP},detalleDePersona.codigoPostal.pk.identificador={identificadorCP},codigoPostal.provincia.pais.codigo={pais}"/>  
  
  <@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/persona/validateDocSearch.action" 
  source="personaDocList" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=${navigationId},navegacionIdBack=${navegacionIdBack},flowControl=change,nameSpaceSelect=${nameSpaceSelect},actionNameSelect=${actionNameSelect},persona.tipoPersona={tipoPersona},persona.pais.codigo={pais},persona.fechaNacimiento={fechaNacimiento},persona.sexo={sexo},persona.venceFormDGI={venceFormDGI},persona.docFiscal={docFiscal},persona.inscriptoGanancia={inscriptoGanancia},persona.docPersonal={docPersonal},persona.situacionFiscal.codigo={situacionFiscal},detalleDePersona.razonSocial={razonSocial},detalleDePersona.nombre={nombre},detalleDePersona.letrasCP={letrasCP},detalleDePersona.calle={calle},detalleDePersona.numeroFinca={numeroFinca},detalleDePersona.aptoCasa={aptoCasa},detalleDePersona.entreCalles={entreCalles},formaDeComunicacionDePersona.formaDeComunicacion.codigo={formaDeComunicacion},formaDeComunicacionDePersona.descripcion={formaDeComunicacionDePersona},detalleDePersona.codigoPostal.pk.secuencia={secuenciaCP},detalleDePersona.codigoPostal.pk.identificador={identificadorCP},detalleDePersona.codigoPostal.provincia.pais.codigo={pais}"/>
  
  <@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/persona/validateCUITSearch.action" 
  source="personaDocFisList" 
  success="contentTrx" 
  failure="errorTrx"
  parameters="navigationId=${navigationId},navegacionIdBack=${navegacionIdBack},flowControl=change,nameSpaceSelect=${nameSpaceSelect},actionNameSelect=${actionNameSelect},persona.tipoPersona={tipoPersona},persona.pais.codigo={pais},persona.fechaNacimiento={fechaNacimiento},persona.sexo={sexo},persona.venceFormDGI={venceFormDGI},persona.docFiscal={docFiscal},persona.inscriptoGanancia={inscriptoGanancia},persona.docPersonal={docPersonal},persona.situacionFiscal.codigo={situacionFiscal},detalleDePersona.razonSocial={razonSocial},detalleDePersona.nombre={nombre},detalleDePersona.letrasCP={letrasCP},detalleDePersona.calle={calle},detalleDePersona.numeroFinca={numeroFinca},detalleDePersona.aptoCasa={aptoCasa},detalleDePersona.entreCalles={entreCalles},formaDeComunicacionDePersona.formaDeComunicacion.codigo={formaDeComunicacion},formaDeComunicacionDePersona.descripcion={formaDeComunicacionDePersona},detalleDePersona.codigoPostal.pk.secuencia={secuenciaCP},detalleDePersona.codigoPostal.pk.identificador={identificadorCP},detalleDePersona.codigoPostal.provincia.pais.codigo={pais}"/>


  <@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/persona/validateNomSearch.action" 
  source="personaNomList" 
  success="contentTrx" 
  failure="errorTrx"
  parameters="navigationId=${navigationId},navegacionIdBack=${navegacionIdBack},flowControl=change,nameSpaceSelect=${nameSpaceSelect},actionNameSelect=${actionNameSelect},persona.tipoPersona={tipoPersona},persona.pais.codigo={pais},persona.fechaNacimiento={fechaNacimiento},persona.sexo={sexo},persona.venceFormDGI={venceFormDGI},persona.docFiscal={docFiscal},persona.inscriptoGanancia={inscriptoGanancia},persona.docPersonal={docPersonal},persona.situacionFiscal.codigo={situacionFiscal},detalleDePersona.razonSocial={razonSocial},detalleDePersona.nombre={nombre},detalleDePersona.letrasCP={letrasCP},detalleDePersona.calle={calle},detalleDePersona.numeroFinca={numeroFinca},detalleDePersona.aptoCasa={aptoCasa},detalleDePersona.entreCalles={entreCalles},formaDeComunicacionDePersona.formaDeComunicacion.codigo={formaDeComunicacion},formaDeComunicacionDePersona.descripcion={formaDeComunicacionDePersona},detalleDePersona.codigoPostal.pk.secuencia={secuenciaCP},detalleDePersona.codigoPostal.pk.identificador={identificadorCP},detalleDePersona.codigoPostal.provincia.pais.codigo={pais}"/>