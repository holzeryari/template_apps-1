<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancel" type="button" name="btnVolver" value="Cancelar" class="boton"/>
				<input id="update" type="button" name="btnModificar" value="Modificar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/persona/personaFull/update.action" 
  source="update" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="detalleDePersona.pk.secuencia={secuencia},detalleDePersona.pk.identificador={identificador},detalleDePersona.persona.tipoPersona={tipoPersona},detalleDePersona.razonSocial={razonSocial},detalleDePersona.nombre={nombre},detalleDePersona.persona.docPersonal={docPersonal},detalleDePersona.persona.docFiscal={docFiscal},detalleDePersona.persona.fechaNacimiento={fechaNacimiento},detalleDePersona.persona.sexo={sexo},detalleDePersona.codigoPostal.pk.identificador={identificadorCP},detalleDePersona.codigoPostal.pk.secuencia={secuenciaCP},detalleDePersona.letrasCP={letrasCP},detalleDePersona.calle={calle},detalleDePersona.numeroFinca={numeroFinca},detalleDePersona.aptoCasa={aptoCasa},detalleDePersona.entreCalles={entreCalles},detalleDePersona.persona.venceFormDGI={venceFormDGI},detalleDePersona.persona.inscriptoGanancia={inscriptoGanancia},detalleDePersona.persona.situacionFiscal.codigo={situacionFiscal},navegacionIdBack=${navegacionIdBack}"/>
  
  <@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=${navegacionIdBack},flowControl=back"/>
  
  <@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/persona/personaFull/selectCodigoPostal.action" 
  source="codigoPostalDPView" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=${navigationId},flowControl=change,detalleDePersona.pk.secuencia={secuencia},detalleDePersona.pk.identificador={identificador},detalleDePersona.persona.tipoPersona={tipoPersona},detalleDePersona.razonSocial={razonSocial},detalleDePersona.nombre={nombre},detalleDePersona.persona.docPersonal={docPersonal},detalleDePersona.persona.docFiscal={docFiscal},detalleDePersona.persona.fechaNacimiento={fechaNacimiento},detalleDePersona.persona.sexo={sexo},detalleDePersona.codigoPostal.pk.identificador={identificadorCP},detalleDePersona.codigoPostal.pk.secuencia={secuenciaCP},detalleDePersona.letrasCP={letrasCP},detalleDePersona.calle={calle},detalleDePersona.numeroFinca={numeroFinca},detalleDePersona.aptoCasa={aptoCasa},detalleDePersona.entreCalles={entreCalles},detalleDePersona.persona.venceFormDGI={venceFormDGI},detalleDePersona.persona.inscriptoGanancia={inscriptoGanancia},detalleDePersona.persona.situacionFiscal.codigo={situacionFiscal},navegacionIdBack=${navegacionIdBack}"/>
  
   <@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/persona/validateNomSearch.action" 
  source="personaNomList" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=${navigationId},flowControl=change,detalleDePersona.pk.secuencia={secuencia},detalleDePersona.pk.identificador={identificador},detalleDePersona.persona.tipoPersona={tipoPersona},detalleDePersona.razonSocial={razonSocial},detalleDePersona.nombre={nombre},detalleDePersona.persona.docPersonal={docPersonal},detalleDePersona.persona.docFiscal={docFiscal},detalleDePersona.persona.fechaNacimiento={fechaNacimiento},detalleDePersona.persona.sexo={sexo},detalleDePersona.codigoPostal.pk.identificador={identificadorCP},detalleDePersona.codigoPostal.pk.secuencia={secuenciaCP},detalleDePersona.letrasCP={letrasCP},detalleDePersona.calle={calle},detalleDePersona.numeroFinca={numeroFinca},detalleDePersona.aptoCasa={aptoCasa},detalleDePersona.entreCalles={entreCalles},detalleDePersona.persona.venceFormDGI={venceFormDGI},detalleDePersona.persona.inscriptoGanancia={inscriptoGanancia},detalleDePersona.persona.situacionFiscal.codigo={situacionFiscal},navegacionIdBack=${navegacionIdBack}"/>
  