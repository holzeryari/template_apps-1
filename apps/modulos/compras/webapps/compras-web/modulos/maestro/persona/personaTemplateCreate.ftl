<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>

<#-- bloque Persona -->
<@tiles.insertAttribute name="persona"/>

<#-- bloque Detalle de Persona -->
<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Secuencia de la Persona</b></td>
				<td>
			</tr>
		</table>
		
		<@tiles.insertAttribute name="detalle"/>
</div>		

<#-- bloque Forma de Comunicacion de la Persona -->
<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
	<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
		<tr>
			<td><b>Forma de Comunicaci&oacute;n de la Persona</b></td>
		</tr>
	</table>

	<@tiles.insertAttribute name="formaDeComunicacion"/>

</div>									

<#-- bloque Botones -->
<@tiles.insertAttribute name="buttons"/>
