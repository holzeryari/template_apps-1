<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancel" type="button" name="btnVolver" value="Cancelar" class="boton"/>
				<input id="store" type="button" name="btnAgregar" value="Agregar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/persona/detalle/create.action" 
  source="store" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=${navigationId},navegacionIdBack=${navegacionIdBack},detalleDePersona.pk.identificador={identificador},detalleDePersona.razonSocial={razonSocial},detalleDePersona.nombre={nombre},detalleDePersona.letrasCP={letrasCP},detalleDePersona.calle={calle},detalleDePersona.numeroFinca={numeroFinca},detalleDePersona.aptoCasa={aptoCasa},detalleDePersona.entreCalles={entreCalles},detalleDePersona.codigoPostal.pk.secuencia={secuenciaCP},detalleDePersona.codigoPostal.pk.identificador={identificadorCP}"/>
  
  <@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=administrar-persona,flowControl=back"/>

  <@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/persona/detalle/selectCodigoPostal.action" 
  source="codigoPostalView" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=${navigationId},navegacionIdBack=${navegacionIdBack},flowControl=change,detalleDePersona.pk.secuencia={secuencia},detalleDePersona.pk.identificador={identificador},detalleDePersona.razonSocial={razonSocial},detalleDePersona.nombre={nombre},detalleDePersona.letrasCP={letrasCP},detalleDePersona.calle={calle},detalleDePersona.numeroFinca={numeroFinca},detalleDePersona.aptoCasa={aptoCasa},detalleDePersona.entreCalles={entreCalles},detalleDePersona.codigoPostal.pk.secuencia={secuenciaCP},detalleDePersona.codigoPostal.pk.identificador={identificadorCP}"/>
  
   <@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/persona/validateNomSearch.action" 
  source="personaNomList" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=${navigationId},flowControl=change,detalleDePersona.pk.secuencia={secuencia},detalleDePersona.pk.identificador={identificador},detalleDePersona.razonSocial={razonSocial},detalleDePersona.nombre={nombre},detalleDePersona.letrasCP={letrasCP},detalleDePersona.calle={calle},detalleDePersona.numeroFinca={numeroFinca},detalleDePersona.aptoCasa={aptoCasa},detalleDePersona.entreCalles={entreCalles},detalleDePersona.codigoPostal.pk.secuencia={secuenciaCP},detalleDePersona.codigoPostal.pk.identificador={identificadorCP}"/>
  