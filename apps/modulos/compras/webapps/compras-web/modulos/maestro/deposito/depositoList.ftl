<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>

<@s.hidden id="navigationId" name="navigationId"/>
	
	<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
        	<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Datos del Dep&oacute;sito</b></td>
				<td>					
					<div align="right">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0026"
							enabled="deposito.readable" 
							cssClass="item" 
							id="crear"										
							href="javascript://nop/">
							<b>Agregar</b>										
							<img src="${request.contextPath}/common/images/agregar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
						</@security.a>
					</div>
				</td>
			</tr>
		</table>
		
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
			<tr>
				<td class="textoCampo">C&oacute;digo:</td>
				<td class="textoDato">
					<@s.textfield 
 						templateDir="custontemplates" 
						id="oid" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="deposito.oid" 
						title="C&oacute;digo"/>
				</td>
				<td class="textoCampo">Descripci&oacute;n:</td>
      			<td class="textoDato">
      				<@s.textfield 
      					templateDir="custontemplates" 
						id="descripcion" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="deposito.descripcion" 
						title="Descripcion" />
				</td>
			</tr>
			<tr>
				<td class="textoCampo"  align="right">Tipo de Organizaci&oacute;n:</td>
  				<td class="textoDato" align="left" >
      				<@s.select 
						templateDir="custontemplates" 
						id="tipoOrganizacion" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="deposito.tipoOrganizacion" 
						list="tipoOrganizacionList" 
						listKey="key" 
						listValue="description" 
						value="deposito.tipoOrganizacion.ordinal()"
						title="Tipo de Organizaci&oacute;n"
						headerKey="0"
						headerValue="Todos"/>
				</td>	
				<td class="textoCampo"  align="right">Estado:</td>
  				<td class="textoDato" align="left" >
  					<@s.select 
						templateDir="custontemplates" 
						id="deposito.estado" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="deposito.estado" 
						list="estadoList" 
						listKey="key" 
						listValue="description" 
						value="deposito.estado.ordinal()"
						title="Estado"
						headerKey="0"
						headerValue="Todos"/>
				</td>
			</tr>
			<tr>
	    		<td colspan="4" class="lineaGris" align="right">
	      			<input id="limpiar" type="button" name="btnVolver" value="Limpiar" class="boton"/>		
	      			<input id="buscar" type="button" name="btnVolver" value="Buscar" class="boton"/>
	    		</td>
			</tr>	
		</table>
	</div>
	<!-- Resultado Filtro -->
	<@s.if test="depositoList!=null">
		<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
			<#assign depositoOid="0" />
			<@s.if test="deposito.oid!=null">
				<#assign depositoOid="${deposito.oid}" />
			</@s.if>
			<#assign depositoTipo="${deposito.tipoOrganizacion.ordinal()}" />
			<#assign depositoDescripcion="${deposito.descripcion}" />
			<#assign depositoEstado = "${deposito.estado.ordinal()}" />
			
			<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Dep&oacute;sitos encontrados</td>
					<td>
						<div class="alineacionDerecha">
							<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0027" 
								cssClass="item" 
								href="${request.contextPath}/maestro/deposito/printPDF.action?deposito.oid=${depositoOid}&deposito.tipoOrganizacion=${depositoTipo}&deposito.descripcion=${depositoDescripcion}&deposito.estado=${depositoEstado}">
									<img src="${request.contextPath}/common/images/imprimirExcel.gif" title="Exportar a PDF" align="absmiddle" border="0" hspace="3">
							</@security.a>
						</div>
						<div class="alineacionDerecha">
							<b>Imprimir</b>
							<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0027" 
								cssClass="item" 
								href="${request.contextPath}/maestro/deposito/printPDF.action?deposito.oid=${depositoOid}&deposito.tipoOrganizacion=${depositoTipo}&deposito.descripcion=${depositoDescripcion}&deposito.estado=${depositoEstado}">
								<img src="${request.contextPath}/common/images/imprimirPdf.gif" title="Exportar a Excel" align="absmiddle" border="0" hspace="3">
							</@security.a>
						</div>	
					</td>
				</tr>
			</table>		
			
			<@ajax.anchors target="contentTrx">			
		    	<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="depositoList" id="deposito" pagesize=15 defaultsort=2 partialList=true size="recordSize" keepStatus=true excludedParams="resetFlag">
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" title="Acciones" class="botoneraAnchoCon4">
						<div class="alineacion">
								<@security.a 
									templateDir="custontemplates" 
									securityCode="CUF0021" 
									enabled="deposito.readable" 
									cssClass="item" 
									href="${request.contextPath}/maestro/deposito/readView.action?deposito.oid=${deposito.oid?c}&deposito.versionNumber=${deposito.versionNumber?c}">
									<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver"  border="0">
								</@security.a>
						</div>
						<div class="alineacion">
								<@security.a 
									templateDir="custontemplates" 
									securityCode="CUF0022" 
									enabled="deposito.updatable"
									cssClass="item"  
									href="${request.contextPath}/maestro/deposito/administrarView.action?deposito.oid=${deposito.oid?c}&navigationId=administrar&flowControl=regis">
									<img  src="${request.contextPath}/common/images/administrar.gif" alt="Administrar" title="Administrar"  border="0">
								</@security.a>									
						</div>
						<div class="alineacion">
								<@security.a 
									templateDir="custontemplates" 
									securityCode="CUF0023" 
									enabled="deposito.eraseable"
									cssClass="item"  
									href="${request.contextPath}/maestro/deposito/deleteView.action?deposito.oid=${deposito.oid?c}&deposito.versionNumber=${deposito.versionNumber?c}">
									<img  src="${request.contextPath}/common/images/eliminar.gif" alt="Eliminar" title="Eliminar"  border="0">
								</@security.a>
						</div>
						<div class="alineacion">
							<#if deposito.estado == "Activo">
								<@security.a 
									templateDir="custontemplates" 
									securityCode="CUF0024" 
									enabled="true"
									cssClass="item"  
									href="${request.contextPath}/maestro/deposito/desactivarView.action?deposito.oid=${deposito.oid?c}&deposito.versionNumber=${deposito.versionNumber?c}">
									<img src="${request.contextPath}/common/images/desactivar.gif" alt="Desactivar" title="Desactivar" border="0">
								</@security.a>			
							<#else>
								<@security.a 
									templateDir="custontemplates" 
									securityCode="CUF0025" 
									enabled="true"
									cssClass="item"  
									href="${request.contextPath}/maestro/deposito/activarView.action?deposito.oid=${deposito.oid?c}&deposito.versionNumber=${deposito.versionNumber?c}">
									<img src="${request.contextPath}/common/images/activar.gif" alt="Activar" title="Activar" border="0">
								</@security.a>								
							</#if>		
						</div>		
					</@display.column>
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="oid" title="C&oacute;digo" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="descripcion" title="Descripci&oacute;n" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service"  class="estiloTexto" property="tipoOrganizacion" title="Tipo Organizaci&oacute;n" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="estado" title="Estado" />					
				</@display.table>
			</@ajax.anchors>
		</div>
	</@s.if>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/deposito/search.action" 
  source="buscar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="resetFlag=true,navigationId={navigationId},flowControl=regis,deposito.oid={oid},deposito.tipoOrganizacion={tipoOrganizacion},deposito.descripcion={descripcion},deposito.estado={deposito.estado}"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/deposito/view.action" 
  source="limpiar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=regis"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/deposito/createView.action" 
  source="crear" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters=""/>
