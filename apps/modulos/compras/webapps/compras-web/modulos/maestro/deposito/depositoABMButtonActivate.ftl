<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<#if deposito.estado == "Activo">	
			<#assign titulo="Desactivar">
		<#elseif deposito.estado=="Inactivo">
			<#assign titulo="Activar">
		<#else>			
			<#assign titulo="Activar">						
</#if>	

<div id="capaSeparadora" class="capaSeparadora">&nbsp;</div>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancelar" type="button" name="btnCancelar" value="Cancelar" class="boton"/>
				<input id="store" type="button" name="btnActivarDesactivar" value="${titulo}" class="boton"/>
			</td>
		</tr>	
	</table>
</div>


<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/deposito/cambiarEstado.action" 
  source="store" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="deposito.oid={oid}"/>
  
  <@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action"  
  source="cancelar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=buscar-depositos,flowControl=back"/>