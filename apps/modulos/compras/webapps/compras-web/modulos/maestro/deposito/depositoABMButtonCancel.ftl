<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<table width="100%" border="0" cellspacing="0" cellpadding="3">
	<tr> 
		<td class="blue-linea-bot" align="left">
			<input id="cancelar" type="button" name="btnVolver" value="Cancelar" class="boton"/>
		</td>
		<td class="blue-linea-bot" align="right">
			
		</td>
	</tr>	
</table>
  
  
  <@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action"  
  source="cancelar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=buscar-depositos,flowControl=back"/>
  
  