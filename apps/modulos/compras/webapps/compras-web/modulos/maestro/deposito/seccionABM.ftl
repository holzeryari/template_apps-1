<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<@s.hidden id="navigationId" name="navigationId"/>
<@s.hidden id="seccion.oid" name="seccion.oid"/>
<@s.hidden id="seccion.deposito.oid" name="seccion.deposito.oid"/>

	<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
			<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	

	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos del Dep&oacute;sito</b></td>
			</tr>
		</table>
	
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr><td class="textoCampo" colspan="4">&nbsp;</td></tr>
			<tr>
			   	<td class="textoCampo">C&oacute;digo:</td>
			    <td class="textoDato">
			    	<@s.property default="&nbsp;" escape=false value="seccion.deposito.oid"/>
			    </td>
			    <td  class="textoCampo">Descripci&oacute;n: </td>
				<td class="textoDato">					
					<@s.property default="&nbsp;" escape=false value="seccion.deposito.descripcion"/>										
				</td>	      			
			</tr>	
			<tr>
			 	<td class="textoCampo">Tipo de Organizaci&oacute;n:</td>
				<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="seccion.deposito.tipoOrganizacion"/>
				</td>
				<td  class="textoCampo">Secciones: </td>
				<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="seccion.deposito.cantidadSeccion"/>
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Estado:</td>
			    <td  class="textoDato" colspan="3">
			    	<@s.property default="&nbsp;" escape=false value="seccion.deposito.estado"/>
				</td>		
			</tr>
		</table>	
		
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos de la Secci&oacute;n</b></td>
			</tr>
		</table>						
	
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr><td class="textoCampo" colspan="4">&nbsp;</td></tr>	
			<tr>					
				<td class="textoCampo">C&oacute;digo:</td>
				<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="seccion.oid"/>
				</td>
				<td  class="textoCampo">Secci&oacute;n: </td>
				<td  class="textoDato">
					<@s.textfield 
						templateDir="custontemplates" 
						id="seccion" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="seccion.seccion" 
						title="Secci&oacute;n"
						maxLength="1" />
					</td>
			</tr>
			<tr><td class="textoCampo" colspan="4">&nbsp;</td></tr>	
		</table>
	</div>	
