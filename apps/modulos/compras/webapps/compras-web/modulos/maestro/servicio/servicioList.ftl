
<#setting locale="es_AR">

<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>

<@s.hidden id="navigationId" name="navigationId"/>
<@s.hidden id="servicio.rubro.oid" name="servicio.rubro.oid"/>

<@s.hidden id="periodoEvaluacion.oid" name="periodoEvaluacion.oid"/>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
        	<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Datos del Servicio</b></td>
				<td>					
					<div align="right">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0086"
							enabled="true" 
							cssClass="item" 
							id="crear"										
							href="javascript://nop/">
							<b>Agregar</b>										
							<img src="${request.contextPath}/common/images/agregar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
						</@security.a>
					</div>
				</td>
			</tr>
		</table>
		
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
			<tr>
				<td class="textoCampo">C&oacute;digo:</td>
      			<td class="textoDato">
      			<@s.textfield 
  					templateDir="custontemplates" 
					id="servicio.oid" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="servicio.oid" 
					title="C&oacute;digo" /></td>
      		
      			<td class="textoCampo">Descripci&oacute;n</td>
      			<td class="textoDato">
      				<@s.textfield 
  					templateDir="custontemplates" 
					id="servicio.descripcion" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="servicio.descripcion" 
					title="Descripcion" />
				</td>
    		</tr>					
			<tr>
				<td class="textoCampo">Rubro:</td>
      			<td class="textoDato">
						<@s.hidden id="servicio.rubro.descripcion" name="servicio.rubro.descripcion"/>
						<@s.property default="&nbsp;" escape=false value="servicio.rubro.descripcion" />									
					<@s.a templateDir="custontemplates" id="seleccionarRubro" name="seleccionarRubro" href="javascript://nop/">
						<img src="${request.contextPath}/common/images/buscar.gif"  border="0" align="absmiddle" hspace="3" >
					</@s.a>		
					</td>
				<td class="textoCampo">Tipo:</td>
      			<td class="textoDato">
      			<@s.select 
						templateDir="custontemplates" 
						id="servicio.tipo" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="servicio.tipo" 
						list="tipoList" 
						listKey="key" 
						listValue="description" 
						value="servicio.tipo.ordinal()"										
						headerKey="0" 
	                    headerValue="Todos"
	                    title="Tipo de servicio"
			       		 />
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Es Cr&iacute;tico: </td>
				<td  class="textoDato">
	      			<@s.select 
						templateDir="custontemplates" 
						id="servicio.critico" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="servicio.critico" 
						list="criticoList" 
						listKey="key" 
						listValue="description" 
						value="servicio.critico.ordinal()"
						title="Es Critico"
						headerKey="0"
						headerValue="Todos" 
						 />
				</td>
				<td class="textoCampo">Estado:</td>
      			<td class="textoDato">
      			<@s.select 
							templateDir="custontemplates" 
							id="servicio.estado" 
							cssClass="textarea"
							cssStyle="width:165px" 
							name="servicio.estado" 
							list="estadoList" 
							listKey="key" 
							listValue="description" 
							value="servicio.estado.ordinal()"
							title="Estado"
							headerKey="0"
							headerValue="Todos"							
							/>
				</td>
			</tr>
			<tr>
	    		<td colspan="4" class="lineaGris" align="right">
	      			<input id="limpiar" type="button" name="btnVolver" value="Limpiar" class="boton"/>		
	      			<input id="buscar" type="button" name="btnVolver" value="Buscar" class="boton"/>
	    		</td>
			</tr>	
		</table>
	</div>
						
	<!-- Resultado Filtro -->
	<@s.if test="servicioList!=null">
		<#assign servicioOid="0" />
		<#assign servicioRubroOid="0" />
		<#assign servicioRubroDescripcion="" />		
		<@s.if test="servicio.oid!=null">
			<#assign servicioOid="${servicio.oid}" />
		</@s.if>
		<@s.if test="servicio.rubro!=null">						
			<#assign servicioRubroDescripcion="${servicio.rubro.descripcion}" />
			<@s.if test="servicio.rubro.oid!=null">		
				<#assign servicioRubroOid="${servicio.rubro.oid}" />
			</@s.if>						
		</@s.if>						
		<#assign servicioDescripcion="${servicio.descripcion}" />
		<#assign servicioTipo="${servicio.tipo.ordinal()}" />
		<#assign servicioEstado = "${servicio.estado.ordinal()}" />
		<#assign servicioCritico = "${servicio.critico.ordinal()}" />
		
		<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
		
		<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Servicios encontrados</td>
					<td>
						<div class="alineacionDerecha">
							<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0087" 
								cssClass="item" 
								href="${request.contextPath}/maestro/servicio/printXLS.action?servicio.tipo=${servicioTipo}&servicio.descripcion=${servicioDescripcion}&servicio.oid=${servicioOid}&servicio.rubro.oid=${servicioRubroOid}&servicio.rubro.descripcion=${servicioRubroDescripcion}&servicio.estado=${servicioEstado}&servicio.critico=${servicioCritico}">
								<img src="${request.contextPath}/common/images/imprimirExcel.gif" title="Exportar a Excel" align="absmiddle" border="0" hspace="3" >
							</@security.a>
						</div>
						<div class="alineacionDerecha">
							<b>Imprimir</b>
							<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0087" 
								cssClass="item" 
								href="${request.contextPath}/maestro/servicio/printPDF.action?servicio.tipo=${servicioTipo}&servicio.descripcion=${servicioDescripcion}&servicio.oid=${servicioOid}&servicio.rubro.oid=${servicioRubroOid}&servicio.rubro.descripcion=${servicioRubroDescripcion}&servicio.estado=${servicioEstado}&servicio.critico=${servicioCritico}">
								<img src="${request.contextPath}/common/images/imprimirPdf.gif" title="Exportar a PDF" align="absmiddle" border="0" hspace="3">
							</@security.a>
						</div>	
							<div class="alineacionDerecha">
								<b>Per&iacute;odos:</b>		
									<@s.select 
										templateDir="custontemplates" 
										id="periodo" 
										cssClass="textarea"
										cssStyle="width:50px" 
										cssStyle="width:165px" 
										name="periodo" 
										list="periodoEvaluacionList" 
										listKey="oid" 
										listValue="fechasPeriodoEvaluacion" 
										title="Periodos"
										onchange="javascript:periodoEvaluacionSelect(this);" 
										/>									
									<@security.a 
									templateDir="custontemplates" 
									securityCode="CUF0067" 
									cssClass="item" 
									id="fscListLink"
									name="fscListLink"		
									onclick="javascript:fscListLinkOnClick();"
									href="${request.contextPath}/maestro/servicio/printFSSPorServicioPDF.action?periodoEvaluacion.oid=${periodoEvaluacion.oid}&servicio.tipo=${servicioTipo}&servicio.descripcion=${servicioDescripcion}&servicio.oid=${servicioOid}&servicio.rubro.oid=${servicioRubroOid}&servicio.rubro.descripcion=${servicioRubroDescripcion}&servicio.estado=${servicioEstado}&servicio.critico=${servicioCritico}">									
									<img src="${request.contextPath}/common/images/imprimirPdf.gif" title="Listado de FSS por Servicio" align="absmiddle" border="0" hspace="3">
									</@security.a> 		<b> - </b>&nbsp;
						</div>	
					</td>
				</tr>
			</table>
									
			<@vc.anchors target="contentTrx" ajaxFlag="ajax">
				<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="servicioList" id="servicio" pagesize=15 defaultsort=2 partialList=true size="recordSize" keepStatus=true excludedParams="resetFlag">
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select"  class="botoneraAnchoCon4" title="Acciones">
						<div class="alineacion">				
							<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0081" 
								enabled="servicio.readable" 
								cssClass="item" 
								href="${request.contextPath}/maestro/servicio/readView.action?servicio.oid=${servicio.oid?c}">
								<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver" border="0">
							</@security.a>
							</div>
							<div class="alineacion">
							<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0082" 
								enabled="servicio.updatable"
								cssClass="item"  
								href="${request.contextPath}/maestro/servicio/updateView.action?servicio.oid=${servicio.oid?c}&navigationId=servicio-update&flowControl=regis">
								<img  src="${request.contextPath}/common/images/modificar.gif" alt="Modificar" title="Modificar" border="0">
							</@security.a>									
							</div>
							<div class="alineacion">
							<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0083" 
								enabled="servicio.eraseable"
								cssClass="item"  
								href="${request.contextPath}/maestro/servicio/deleteView.action?servicio.oid=${servicio.oid?c}">
								<img  src="${request.contextPath}/common/images/eliminar.gif" alt="Eliminar" title="Eliminar" border="0">
							</@security.a>									
							</div>
						<div class="alineacion">
						<#if servicio.estado == "Activo">
							<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0084" 
								enabled="true"
								cssClass="item"  
								href="${request.contextPath}/maestro/servicio/desactivarView.action?servicio.oid=${servicio.oid?c}">
								<img src="${request.contextPath}/common/images/desactivar.gif" alt="Desactivar" title="Desactivar" border="0">
							</@security.a>			
						<#else>
							<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0085" 
								enabled="true"
								cssClass="item"  
								href="${request.contextPath}/maestro/servicio/activateView.action?servicio.oid=${servicio.oid?c}">
								<img src="${request.contextPath}/common/images/activar.gif" alt="Activar" title="Activar" border="0">
							</@security.a>								
						</#if>	
						</div>							
						</@display.column>
						
						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="oid" title="C&oacute;digo" />
						
						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="descripcion" title="Descripci&oacute;n" />
						
						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="rubro.descripcion" title="Rubro" />
						
						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="tipo" title="Tipo" />
						
						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="estado" title="Estado" />					
						
					</@display.table>
					</@vc.anchors>
					</div>		
				</@s.if>
					
<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/servicio/search.action" 
  source="buscar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="resetFlag=true,navigationId={navigationId},flowControl=regis,servicio.tipo={servicio.tipo},servicio.descripcion={servicio.descripcion},servicio.oid={servicio.oid},servicio.rubro.oid={servicio.rubro.oid},servicio.rubro.descripcion={servicio.rubro.descripcion},servicio.estado={servicio.estado},servicio.critico={servicio.critico}"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/servicio/selectRubroSearch.action" 
  source="seleccionarRubro" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=change,navegacionIdBack={navigationId},servicio.tipo={servicio.tipo},servicio.descripcion={servicio.descripcion},servicio.oid={servicio.oid},servicio.rubro.oid={servicio.rubro.oid},servicio.rubro.descripcion={servicio.rubro.descripcion},servicio.estado={servicio.estado},servicio.critico={servicio.critico}"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/servicio/view.action" 
  source="limpiar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=regis"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/servicio/createView.action" 
  source="crear" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=servicio-create,flowControl=regis"/>
