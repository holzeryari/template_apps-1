<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<@ajax.anchors target="contentTrx">	
<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
	<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
		<tr>
			<td>Ciudades</td>
		</tr>
	</table>			
	<!-- Resultado Filtro -->						
	<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="carteleria.listaCodigoPostal" id="carteleriaCodigoPostal" defaultsort=2>
					
	<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="codigoPostal.pk.identificador" title="C&oacute;digo Postal" defaultorder="ascending"/>
	
	<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="codigoPostal.descripcion" title="Descripci&oacute;n" />
	
	<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="kmCiudad" title="Km Ciudad" />
	
	<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="puntoCardinal" title="Punto Cardinal" />
	

</@display.table>
</div>
</@ajax.anchors>