<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>


<@s.hidden id="webFlow" name="webFlow"/>
<@s.hidden id="viewState" name="viewState"/>

<@s.hidden id="carteleriaCodigoPostal.carteleria.oid" name="carteleriaCodigoPostal.carteleria.oid"/>
<@s.hidden id="carteleriaCodigoPostal.oid" name="carteleriaCodigoPostal.oid"/>
<@s.hidden id="carteleriaCodigoPostal.codigoPostal.pk.identificador" name="carteleriaCodigoPostal.codigoPostal.pk.identificador"/>
<@s.hidden id="carteleriaCodigoPostal.codigoPostal.pk.secuencia" name="carteleriaCodigoPostal.codigoPostal.pk.secuencia"/>
<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
			
			<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos del Cartel</b></td>
			</tr>
		</table>
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
			<tr>
      			<td class="textoCampo">N&uacute;mero:</td>
      			<td class="textoDato"><@s.property default="&nbsp;" escape=false value="carteleriaCodigoPostal.carteleria.oid"/></td>
      			<td  class="textoCampo">Fecha: </td>
				<td class="textoDato">
				<@s.if test="carteleriaCodigoPostal.carteleria.fecha != null">
					<#assign fechaIngreso = carteleriaCodigoPostal.carteleria.fecha> 
					${fechaIngreso?string("dd/MM/yyyy")}	
				</@s.if>
					      			
			</tr>	

			<tr>
      			<td class="textoCampo">Descripci&oacute;n:</td>
	      			<td class="textoDato">
	      			<@s.property default="&nbsp;" escape=false value="carteleriaCodigoPostal.carteleria.descripcion"/>	                         
	      		</td>		      		
				
				<td class="textoCampo">Fotografia Digital:</td>
      			<td class="textoDato">
      			
      									<a
								templateDir="custontemplates"
								id="imagenView" 
								name="imagenView"
								target="_blank"
								cssClass="ocultarIcono" 
								href="${request.contextPath}/maestro/carteleria/carteleriaRedirectImagenView.action?carteleria.oid=${carteleriaCodigoPostal.carteleria.oid?c}">
									<img src="${request.contextPath}/common/images/ver.gif" border="0" align="absmiddle" hspace="3" alt="Ver Imagen del Cartel" title="Ver Imagen del Cartel"  >
						</a>
				</td>
    		</tr>
    		<tr>
      			<td class="textoCampo">Propiedad:</td>
	      		<td class="textoDato">
	      			<@s.property default="&nbsp;" escape=false value="carteleriaCodigoPostal.carteleria.propiedadCartel"/>	                         
	      		</td>		      		
				
				<td class="textoCampo">Proveedor:</td>
      			<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="carteleriaCodigoPostal.carteleria.proveedor.detalleDePersona.razonSocial" /><@s.property default="&nbsp;" escape=false value="carteleriaCodigoPostal.carteleria.proveedor.detalleDePersona.nombre" />
				</td>
    		</tr>
    		<tr>
				<td class="textoCampo">Observaciones:</td>
      			<td  class="textoDato" colspan="3">
      			<@s.property default="&nbsp;" escape=false value="carteleriaCodigoPostal.carteleria.observaciones"/>
				</td>					
    		</tr>

    		<tr>
				<td class="textoCampo">Estado:</td>
      			<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="carteleriaCodigoPostal.carteleria.estado"/></td>
				</td>

				<td class="textoCampo">&nbsp;</td>
      			<td class="textoDato">&nbsp;</td>
    		</tr>
	    	<tr><td class="textoCampo" colspan="4">&nbsp;</td></tr>
	    </table>			
		</div>
	
		<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos de la Ubicaci&oacute;n del Cartel</b></td>
			</tr>
		</table>
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>		
				<td class="textoCampo">C&oacute;digo Postal:</td>
      			<td class="textoDato"><@s.property default="&nbsp;" escape=false value="carteleriaCodigoPostal.codigoPostal.pk.identificador"/></td>
				<td class="textoCampo">Secuencia:</td>
      			<td class="textoDato"><@s.property default="&nbsp;" escape=false value="carteleriaCodigoPostal.codigoPostal.pk.secuencia"/></td>					      			
	      	</tr>
	      	<tr>
      			<td  class="textoCampo">Localidad: </td>
				<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="carteleriaCodigoPostal.codigoPostal.descripcion"/>					
				</td>	      			
				<td  class="textoCampo">Provincia: </td>
				<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="carteleriaCodigoPostal.codigoPostal.provincia.descripcion"/>					
				</td>	      			
			</tr>

				<tr>
					<td class="textoCampo">Km de la Ciudad: </td>
					<td  class="textoDato">
					<@s.textfield 
						templateDir="custontemplates"
						template="textMoney"   
						id="carteleriaCodigoPostal.kmCiudad" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="carteleriaCodigoPostal.kmCiudad"
						title="Existencia" />									
																								
					</td>
												
					<td class="textoCampo">Punto Cardinal: </td>
					<td  class="textoDato">
					<@s.select 
						templateDir="custontemplates" 
						id="carteleriaCodigoPostal.puntoCardinal" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="carteleriaCodigoPostal.puntoCardinal" 
						list="puntoCardinalList" 
						listKey="key" 
						listValue="description" 
						value="carteleriaCodigoPostal.puntoCardinal.ordinal()"
						title="Punto Cardinal"
						headerKey="0" 
                		headerValue="Seleccionar"
            		/>								
	    		<td>
	    	</tr>
	    	<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>
		</table>
	</div>			    	
		