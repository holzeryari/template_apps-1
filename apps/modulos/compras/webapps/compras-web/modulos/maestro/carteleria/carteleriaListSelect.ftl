
<#setting locale="es_AR">

<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>


<@s.hidden id="navigationId" name="navigationId"/>
<@s.hidden id="estadoDuro" name="estadoDuro"/>
<@s.if test="estadoDuro == true">
	<@s.hidden id="carteleria.estado" name="carteleria.estado.ordinal()"/>
</@s.if>
<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
        	<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Datos del Cartel</b></td>
				<td>&nbsp;</td>
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
	    		<td class="textoCampo" colspan="4">&nbsp;</td>
	    	</tr>
			<tr>
				<td class="textoCampo">N&uacute;mero:</td>
      			<td class="textoDato">
      			<@s.textfield 
      					templateDir="custontemplates" 
						id="carteleria.oid"	
						cssClass="textarea"
						cssStyle="width:160px" 
						name="carteleria.oid" 
						title="N&uacute;mero" />
				</td>
					
				<td class="textoCampo">Descripci&oacute;n:</td>
      			<td class="textoDato">
      			<@s.textfield 
      					templateDir="custontemplates" 
						id="carteleria.descripcion"
						cssClass="textarea"
						cssStyle="width:160px" 
						name="carteleria.descripcion" 
						title="Descripci&oacute;n" />
				</td>	
			</tr>
				<tr>
				<td class="textoCampo">N&uacute;mero Ruta:</td>
      			<td class="textoDato">
      			<@s.textfield 
      					templateDir="custontemplates" 
						id="carteleria.numeroRuta" 						
						cssClass="textarea"
						cssStyle="width:160px" 
						name="carteleria.numeroRuta" 
						title="N&uacute;mero Ruta" />
				</td>
				
				<td class="textoCampo">Km Ruta:</td>
      			<td class="textoDato">
      			<@s.textfield 
      					templateDir="custontemplates" 
						id="carteleria.kmRuta" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="carteleria.kmRuta" 
						title="Km Ruta" />
				</td>	
			</tr>
			<tr>					
				<td class="textoCampo">Propiedad:</td>
      			<td class="textoDato">
	      			<@s.select 
						templateDir="custontemplates" 
						id="carteleria.propiedadCartel" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="carteleria.propiedadCartel" 
						list="propiedadCartelList" 
						listKey="key" 
						listValue="description" 
						value="carteleria.propiedadCartel.ordinal()"
						title="Propiedad Cartel"
						headerKey="0"
						headerValue="Todos"							
						/>
				</td>
				
				<td class="textoCampo">Proveedor:</td>
      			<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="carteleria.proveedor.detalleDePersona.razonSocial" /><@s.property default="&nbsp;" escape=false value="carteleria.proveedor.detalleDePersona.nombre" />
					<@s.a templateDir="custontemplates" id="seleccionarProveedor" name="seleccionarProveedor" href="javascript://nop/" cssClass="ocultarIcono">
						<img src="${request.contextPath}/common/images/buscar.gif" border="0" align="absmiddle" hspace="3" >
					</@s.a>
				</td>	
				
			</tr>
			<tr>
				<td class="textoCampo">Fecha Desde:</td>
      			<td class="textoDato">
					<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="fechaDesde" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="fechaDesde" 
					title="Fecha Desde" />
				</td>
				<td class="textoCampo">Fecha Hasta:</td>
      			<td class="textoDato">
				<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="fechaHasta" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="fechaHasta" 
					title="Fecha Hasta" />
				</td>
				
			</tr>
			<tr>
				<td class="textoCampo">Estado:</td>
      			<td class="textoDato" colspan="3">
					<@s.select 
						templateDir="custontemplates" 
						id="carteleria.estado" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="carteleria.estado" 
						list="estadoList" 
						listKey="key" 
						listValue="description" 
						value="carteleria.estado.ordinal()"
						headerKey="0" 
	                    headerValue="Todos"
						title="Estado" />
				</td>
			</tr>
				<tr>
	    			<td colspan="4" class="lineaGris" align="right">
	      			<input id="limpiar" type="button" name="btnVolver" value="Limpiar" class="boton"/>		
	      			<input id="buscar" type="button" name="btnVolver" value="Buscar" class="boton"/>
	    			</td>
				</tr>	
			</table>
		</div>	
						
		<!-- Resultado Filtro -->
		<@s.if test="carteleriaList!=null">
					
			<#assign carteleriaOid="0" />
			<@s.if test="carteleria.oid!=null">
				<#assign carteleriaOid="${carteleria.oid}" />
				<#assign carteleriaDescripcion="${carteleria.descripcion}" />
				<#assign carteleriaEstado = "${carteleria.estado.ordinal()}" />
			</@s.if>
			<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
				<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Carteles encontrados</td>
				</tr>
			</table>	


			<@vc.anchors target="contentTrx" ajaxFlag="ajax">
	          		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="carteleriaList" id="carteleriaSelect" pagesize=15 defaultsort=2 partialList=true size="recordSize" keepStatus=true excludedParams="resetFlag">
						
						<#assign ref="${request.contextPath}/maestro/carteleria/selectCarteleriaBack.action?">											
						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" style=" text-align:center; display:block" title="Acciones">			
							<@s.a id="selectCarteleria" name="selectCarteleria" href="${ref}carteleria.oid=${carteleriaSelect.oid?c}&carteleria.descripcion=${carteleriaSelect.descripcion}&navegacionIdBack=${navegacionIdBack}&navigationId=${navigationId}"><img src="${request.contextPath}/common/images/seleccionar.gif" alt="Seleccionar" title="Seleccionar" width="16" height="16" border="0"></@s.a>
							&nbsp;
						</@display.column>	
						
						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="oid" title="N&uacute;mero" />
					
						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="descripcion" title="Descripci&oacute;n" />				
						
						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="fecha" format="{0,date,dd/MM/yyyy}" title="Fecha Ingreso" />
						
						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="numeroRuta" title="N&uacute;mero Ruta" />
						
						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="kmRuta" title="Km Ruta" />
						
						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="propiedadCartel" title="Propiedad" />	
						
						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" title="Proveedor" >											
	
							<#if carteleriaSelect.proveedor?exists>
								<#if carteleriaSelect.proveedor.detalleDePersona.nombre?exists>
									${carteleriaSelect.proveedor.detalleDePersona.razonSocial} ${carteleriaSelect.proveedor.detalleDePersona.nombre}
								<#else>
									${carteleriaSelect.proveedor.detalleDePersona.razonSocial}
								</#if>
							</#if>
	
						</@display.column>

						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="estado" title="Estado" />		
					
					</@display.table>
				</@vc.anchors>
			</div>	
		</@s.if>
		
		<div id="capaBotonera" class="capaBotonera">
				<table id="tablaBotonera" class="tablaBotonera">
					<tr> 
						<td align="left">
							<input id="cancel" type="button" name="btnVolver" value="Volver" class="boton"/>
						
						</td>
					</tr>	
				</table>
			</div>	
		
			
<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/carteleria/selectSearchCarteleria.action" 
  source="buscar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="resetFlag=true,navigationId={navigationId},navegacionIdBack=${navegacionIdBack},flowControl=regis,carteleria.oid={carteleria.oid}, carteleria.descripcion={carteleria.descripcion}, carteleria.numeroRuta={carteleria.numeroRuta}, carteleria.kmRuta={carteleria.kmRuta}, carteleria.propiedadCartel={carteleria.propiedadCartel}, carteleria.estado={carteleria.estado},fechaDesde={fechaDesde},fechaHasta={fechaHasta},estadoDuro={estadoDuro}"/>


<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/carteleria/selectViewCarteleria.action" 
  source="limpiar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=regis,navegacionIdBack=${navegacionIdBack},estadoDuro={estadoDuro}"/>
  
    
 <@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId=${navegacionIdBack},flowControl=back"/>
 
 
