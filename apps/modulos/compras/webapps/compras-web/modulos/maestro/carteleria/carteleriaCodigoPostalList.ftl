<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<@ajax.anchors target="contentTrx">
	<#assign carteleriaDescripcion="" />
	<@s.if test="carteleria.descripcion!=null">
		<#assign carteleriaDescripcion="${carteleria.descripcion}" />
	</@s.if>

	<#assign carteleriaPropiedadCartel="0" />
	<@s.if test="carteleria.propiedadCartel!=null">
		<#assign carteleriaPropiedadCartel="${carteleria.propiedadCartel.ordinal()}" />
	</@s.if>

	<#assign carteleriaObservaciones="" />
	<@s.if test="carteleria.observaciones!=null">
		<#assign carteleriaObservaciones="${carteleria.observaciones}" />
	</@s.if>

	<#assign carteleriaNumeroRuta="0" />
	<@s.if test="carteleria.numeroRuta!=null">
		<#assign carteleriaNumeroRuta="{carteleria.numeroRuta}" />
	</@s.if>

	<#assign carteleriaKmRuta="0" />
	<@s.if test="carteleria.kmRuta!=null">
		<#assign carteleriaKmRuta="{carteleria.kmRuta}" />
	</@s.if>

	<#assign carteleriaAlto="0" />
	<@s.if test="carteleria.alto!=null">
		<#assign carteleriaAlto="${carteleria.alto}" />
	</@s.if>

	<#assign carteleriaAncho="0" />
	<@s.if test="carteleria.ancho!=null">
		<#assign carteleriaAncho="${carteleria.ancho}" />
	</@s.if>

	<#assign carteleriaSuperficie="0" />
	<@s.if test="carteleria.superficie!=null">
		<#assign carteleriaSuperficie="${carteleria.superficie}" />
	</@s.if>

	<#assign carteleriaEstructura="" />
	<@s.if test="carteleria.estructura!=null">
		<#assign carteleriaEstructura="${carteleria.estructura}" />
	</@s.if>

	<#assign carteleriaCondicionEstructura="0" />
	<@s.if test="carteleria.condicionEst!=null">
		<#assign carteleriaCondicionEstructura="${carteleria.condicionEst.ordinal()}" />
	</@s.if>

	<#assign carteleriaFundacion="" />
	<@s.if test="carteleria.fundacion!=null">
		<#assign carteleriaFundacion="${carteleria.fundacion}" />
	</@s.if>

	<#assign carteleriaCondicionFundacion="0" />
	<@s.if test="carteleria.condicionFundacion!=null">
		<#assign carteleriaCondicionFundacion="${carteleria.condicionFundacion.ordinal()}" />
	</@s.if>

	<#assign carteleriaPintura="" />
	<@s.if test="carteleria.pintura!=null">
		<#assign carteleriaPintura="${carteleria.pintura}" />
	</@s.if>

	<#assign carteleriaCondicionPintura="0" />
	<@s.if test="carteleria.condicionPintura!=null">
		<#assign carteleriaCondicionPintura="${carteleria.condicionPintura.ordinal()}" />
	</@s.if>

	<#assign carteleriaTipoLetra="0" />
	<@s.if test="carteleria.tipoLetra!=null">
		<#assign carteleriaTipoLetra="${carteleria.tipoLetra.ordinal()}" />
	</@s.if>

	<#assign carteleriaNroProveedor="0" />
	<@s.if test="carteleria.proveedor.nroProveedor!=null">
		<#assign carteleriaNroProveedor="${carteleria.proveedor.nroProveedor}" />
	</@s.if>

	<#assign carteleriaRazonSocialProveedor="" />
	<@s.if test="carteleria.proveedor.detalleDePersona.razonSocial!=null">
		<#assign carteleriaRazonSocialProveedor="${carteleria.proveedor.detalleDePersona.razonSocial}" />
	</@s.if>

	<#assign carteleriaNombreProveedor="" />
	<@s.if test="carteleria.proveedor.detalleDePersona.nombre!=null">
		<#assign carteleriaNombreProveedor="${carteleria.proveedor.detalleDePersona.nombre}" />
	</@s.if>	
	
	<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
		<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
			<tr>
				<td>Ciudades</td>
				<td>
					<div align="right">
						<@s.a href="${request.contextPath}/maestro/carteleria/selectCodigoPostal.action?navigationId=${navigationId}&flowControl=change&carteleria.oid=${carteleria.oid?c}" templateDir="custontemplates" id="agregarCarteleriaCodigoPostal" name="agregarCarteleriaCodigoPostal" cssClass="ocultarIcono">
							<b>Agregar</b><img src="${request.contextPath}/common/images/agregar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
						</@s.a>	
					</div>
				</td>
			</tr>
	</table>			
<!-- Resultado Filtro -->						
<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="carteleria.listaCodigoPostal" id="carteleriaCodigoPostal" defaultsort=2>

	<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" class="botoneraAnchoCon3" title="Acciones">
		
				
		<a href="${request.contextPath}/maestro/carteleria/readCarteleriaCodigoPostalView.action?carteleriaCodigoPostal.oid=${carteleriaCodigoPostal.oid?c}&navegacionIdBack=${navigationId}&navigationId=ver-carteleriaCodigoPostal"><img src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver" border="0"></a>
		&nbsp;
		
		<a href="${request.contextPath}/maestro/carteleria/updateCarteleriaCodigoPostalView.action?carteleriaCodigoPostal.oid=${carteleriaCodigoPostal.oid?c}&navegacionIdBack=${navigationId}&navigationId=modificar-carteleriaCodigoPostal"><img src="${request.contextPath}/common/images/modificar.gif" alt="Modificar" title="Modificar" border="0"></a>
		&nbsp;
		
		<a href="${request.contextPath}/maestro/carteleria/deleteCarteleriaCodigoPostalView.action?carteleriaCodigoPostal.oid=${carteleriaCodigoPostal.oid?c}&navegacionIdBack=${navigationId}&navigationId=eliminar-carteleriaCodigoPostal"><img src="${request.contextPath}/common/images/eliminar.gif" alt="Eliminar" title="Eliminar" border="0"></a>
									
	</@display.column>
						
	<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="codigoPostal.pk.identificador" title="C&oacute;digo Postal" defaultorder="ascending"/>
	
	<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="codigoPostal.descripcion" title="Descripci&oacute;n" />
	
	<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="kmCiudad" title="Km Ciudad" />
	
	<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="puntoCardinal" title="Punto Cardinal" />
	

</@display.table>
</div>
</@ajax.anchors>