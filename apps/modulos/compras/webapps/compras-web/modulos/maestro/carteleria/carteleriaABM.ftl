<#setting locale="es_AR">

<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<@s.hidden id="carteleria.oid" name="carteleria.oid"/>
<@s.hidden id="carteleria.estado" name="carteleria.estado.ordinal()"/>
<@s.hidden id="carteleria.versionNumber" name="carteleria.versionNumber"/>
<@s.hidden id="carteleria.proveedor.nroProveedor" name="carteleria.proveedor.nroProveedor"/>
<@s.hidden id="carteleria.proveedor.detalleDePersona.razonSocial" name="carteleria.proveedor.detalleDePersona.razonSocial" />
<@s.hidden id="carteleria.proveedor.detalleDePersona.nombre" name="carteleria.proveedor.detalleDePersona.nombre" />

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
			
			<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos del Cartel</b></td>
				<td>
					<div align="right">
							<@s.a templateDir="custontemplates" id="modificarCarteleria" name="modificarCarteleria" href="javascript://nop/" cssClass="ocultarIcono">
								<b>Modificar</b><img src="${request.contextPath}/common/images/modificar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
							</@s.a>
					</div>
				</td>
			</tr>
		</table>
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        		
        	</tr>
			<tr>
      			<td class="textoCampo">N&uacute;mero:</td>
      			<td class="textoDato"><@s.property default="&nbsp;" escape=false value="carteleria.oid"/></td>
      			<td  class="textoCampo">Fecha: </td>
				<td class="textoDato">
					<@s.if test="carteleria.fecha != null">
						<#assign fecha = carteleria.fecha> 
						${fecha?string("dd/MM/yyyy")}	
					</@s.if>									
					&nbsp;
				</td>	      			
			</tr>	
			<tr>
				<td  class="textoCampo">Descripci&oacute;n: </td>
				<td class="textoDato">
				<@s.textfield 
					templateDir="custontemplates" 
					id="carteleria.descripcion" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="carteleria.descripcion" 
					title="Descripci&oacute;n"
					maxLength="100"  />					
				</td>	 
      			
      			<td class="textoCampo">Fotografia Digital:</td>
      			<td class="textoDato">				
				<#if !(modoCrear?exists)>
						<a
								templateDir="custontemplates"
								id="imagenView" 
								name="imagenView"
								target="_blank"
								cssClass="ocultarIcono" 
								href="${request.contextPath}/maestro/carteleria/carteleriaRedirectImagenView.action?carteleria.oid=${carteleria.oid?c}">
									<img src="${request.contextPath}/common/images/ver.gif" border="0" align="absmiddle" hspace="3" alt="Ver Imagen del Cartel" title="Ver Imagen del Cartel"  >
						</a>
						
				</#if>			
					
				<#if modoModificar?exists && modoModificar>
						<#--		
						<a templateDir="custontemplates" 
							id="seleccionarFoto" 
							name="seleccionarFoto" 
							target="_blank" 
							href="${request.contextPath}/maestro/carteleria/carteleriaUploadImageView.action?carteleria.oid=${carteleria.oid?c}&navigationId=factura-uploadImagen&flowControl=regis&navegacionIdBack=${navigationId}" 
						>
						-->
						<a templateDir="custontemplates" 
							id="seleccionarFoto" 
							name="seleccionarFoto" 
							target="_blank" 
							href="${request.contextPath}/imagenes/uploadImagenView.action?carteleria.oid=${carteleria.oid?c}&navigationId=factura-uploadImagen&flowControl=regis&navegacionIdBack=${navigationId}" 
						>
								<img src="${request.contextPath}/common/images/imagenUP.gif" border="0" align="absmiddle" hspace="3" >
						</a>
								
				</#if> &nbsp;
				
				
					</td>	
				
			</tr>
			<tr>
      			<td class="textoCampo">Propiedad:</td>
      			<td class="textoDato">		      						

					<@s.select 
						templateDir="custontemplates" 
						id="carteleria.propiedadCartel" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="carteleria.propiedadCartel" 
						list="propiedadCartelList" 
						listKey="key" 
						listValue="description" 
						value="carteleria.propiedadCartel.ordinal()"
						title="Propiedad Cartel"
						headerKey="0" 
                        headerValue="Seleccionar"
						onchange="javascript:propiedadCartelSelect(this);"
               		/>
                </td>		      		

				<td class="textoCampo">Proveedor:</td>
      			<td class="textoDato">
					<#assign proveedorBusquedaStyle="display:none;float:left;">
					<@s.if test="carteleria.propiedadCartel.ordinal() == 2">																		
						<#assign proveedorBusquedaStyle="display:block;float:left;">
					</@s.if>

					<div id="proveedorTexto" style="float:left;">
						<@s.property default="&nbsp;" escape=false value="carteleria.proveedor.detalleDePersona.razonSocial" /><@s.property default="&nbsp;" escape=false value="carteleria.proveedor.detalleDePersona.nombre" />
					</div>

					<div id="proveedorBusqueda" style="${proveedorBusquedaStyle}">
						<@s.a templateDir="custontemplates" id="seleccionarProveedor" name="seleccionarProveedor" href="javascript://nop/" cssClass="ocultarIcono">
							<img src="${request.contextPath}/common/images/buscar.gif"  border="0" align="absmiddle" hspace="3" >
						</@s.a>		
					</div>
				</td>				
					
    		</tr>
	    	<tr>
				<td class="textoCampo">Observaciones:</td>
      			<td  class="textoDato" colspan="3">
					<@s.textarea	
						templateDir="custontemplates"
						cols="89" rows="2"
						cssClass="textarea"
						id="carteleria.observaciones"
						name="carteleria.observaciones"
						label="Observaciones"
						/>
				</td>
    		</tr>

    		<tr>
				<td class="textoCampo">Estado:</td>
      			<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="carteleria.estado"/></td>
				</td>

				<td class="textoCampo">&nbsp;</td>
      			<td class="textoDato">&nbsp;</td>
    		</tr>
	    	<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>	
	    </div>
	    
	    <div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Dimensiones</b></td>
			</tr>
		</table>
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
	    	    		
    		<tr>
    			<td  class="textoCampo">Ancho[m]: </td>
				<td class="textoDato">
					<@s.textfield 
						templateDir="custontemplates" 
						id="carteleria.ancho" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="carteleria.ancho" 
						title="Ancho"
						maxLength="100"  />					
				</td>	

				<td  class="textoCampo">Alto[m]: </td>
				<td class="textoDato">
					<@s.textfield 
						templateDir="custontemplates" 
						id="carteleria.alto" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="carteleria.alto" 
						title="Alto"
						maxLength="100"  />					
				</td>	
    		</tr>

    		<tr>
    			<td  class="textoCampo">Superficie[m2]: </td>
				<td class="textoDato">
					<@s.textfield 
						templateDir="custontemplates" 
						id="carteleria.superficie" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="carteleria.superficie" 
						title="Superficie"
						maxLength="100"  />					
				</td>	
				
				<td class="textoCampo">&nbsp;</td>
      			<td class="textoDato" >&nbsp;</td>
    		</tr>
    		<tr><td colspan="4"class="textoCampo">&nbsp;</td></tr>
    	</table>
    </div>
    <div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Condiciones F&iacute;sicas</b></td>
			</tr>
		</table>
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>		
    		<tr>
    			<td  class="textoCampo">Estructura de: </td>
				<td class="textoDato">
					<@s.textfield 
						templateDir="custontemplates" 
						id="carteleria.estructura" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="carteleria.estructura" 
						title="Estructura"
						maxLength="100"  />					
				</td>	
    		
    			<td class="textoCampo">Condici&oacute;n:</td>
      			<td class="textoDato">		      						
					<@s.select 
						templateDir="custontemplates" 
						id="carteleria.condicionEst" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="carteleria.condicionEst" 
						list="condicionEstList" 
						listKey="key" 
						listValue="description" 
						value="carteleria.condicionEst.ordinal()"
						title="Condici&oacute;n"
						headerKey="0" 
                        headerValue="Seleccionar"
                    />
                </td>	
            </tr>    

			<tr>
    			<td  class="textoCampo">Fundaci&oacute;n de: </td>
				<td class="textoDato" align="left" width="30%">
					<@s.textfield 
						templateDir="custontemplates" 
						id="carteleria.fundacion" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="carteleria.fundacion" 
						title="Fundacion"
						maxLength="100"  />					
				</td>	
    		
    			<td class="textoCampo">Condici&oacute;n:</td>
      			<td class="textoDato">		      						
					<@s.select 
						templateDir="custontemplates" 
						id="carteleria.condicionEst" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="carteleria.condicionEst" 
						list="condicionEstList" 
						listKey="key" 
						listValue="description" 
						value="carteleria.condicionEst.ordinal()"
						title="Condici&oacute;n"
						headerKey="0" 
                        headerValue="Seleccionar"
                    />
                </td>	
            </tr>
	            
            <tr>
    			<td  class="textoCampo">Pintura de Fondo: </td>
				<td class="textoDato">
					<@s.textfield 
						templateDir="custontemplates" 
						id="carteleria.pinturaFondo" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="carteleria.pinturaFondo" 
						title="Pintura de Fondo"
						maxLength="100"  />					
				</td>	
    		
    			<td class="textoCampo">Condici&oacute;n:</td>
      			<td class="textoDato">		      						
					<@s.select 
						templateDir="custontemplates" 
						id="carteleria.condicionPintura" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="carteleria.condicionPintura" 
						list="condicionPinturaList" 
						listKey="key" 
						listValue="description" 
						value="carteleria.condicionPintura.ordinal()"
						title="Condici&oacute;n"
						headerKey="0" 
                        headerValue="Seleccionar"
                        />
                </td>	
            </tr>
	    		
    		<tr>
				<td class="textoCampo">Letras:</td>
      			<td class="textoDato" colspan="3">		      						
					<@s.select 
						templateDir="custontemplates" 
						id="carteleria.tipoLetra" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="carteleria.tipoLetra" 
						list="tipoLetraList" 
						listKey="key" 
						listValue="description" 
						value="carteleria.tipoLetra.ordinal()"
						title="Tipo Letra"
						headerKey="0" 
                        headerValue="Seleccionar"
                        />
                </td>
			</tr>
			<tr>
                <td class="textoCampo" colspan="4">&nbsp;</td>
      		</tr>
           </table>
          </div>  
	    	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
				<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            		<tr>
						<td><b>Ubicaci&oacute;n</b></td>
					</tr>
				</table>
				<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
					<tr>
        				<td class="textoCampo" colspan="4">&nbsp;</td>
        			</tr>	
	    			<tr>
		    			<td  class="textoCampo">N&uacute;mero Ruta: </td>
						<td class="textoDato">
							<@s.textfield 
								templateDir="custontemplates" 
								id="carteleria.numeroRuta" 
								cssClass="textarea"
								cssStyle="width:160px" 
								name="carteleria.numeroRuta" 
								title="N&uacute;mero Ruta"
								maxLength="100"  />					
						</td>	
					
						<td  class="textoCampo">Km Ruta: </td>
						<td class="textoDato">
							<@s.textfield 
								templateDir="custontemplates" 
								id="carteleria.kmRuta" 
								cssClass="textarea"
								cssStyle="width:160px" 
								name="carteleria.kmRuta" 
								title="Km Ruta"
								maxLength="100"  />					
						</td>	
		    		</tr>
		    		<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>
		    </table>		
		</div>				
		
<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/carteleria/updateCarteleriaView.action" 
  source="modificarCarteleria" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=update-carteleria,flowControl=regis,carteleria.oid={carteleria.oid},carteleria.versionNumber={carteleria.versionNumber}"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/carteleria/selectProveedor.action" 
  source="seleccionarProveedor" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId=${navigationId},flowControl=change,navegacionIdBack=${navigationId},carteleria.oid={carteleria.oid},carteleria.estado={carteleria.estado},carteleria.descripcion={carteleria.descripcion},carteleria.propiedadCartel={carteleria.propiedadCartel},carteleria.proveedor.nroProveedor={carteleria.proveedor.nroProveedor},carteleria.proveedor.detalleDePersona.razonSocial={carteleria.proveedor.detalleDePersona.razonSocial},carteleria.proveedor.detalleDePersona.nombre={carteleria.proveedor.detalleDePersona.nombre},carteleria.observaciones={carteleria.observaciones},carteleria.ancho={carteleria.ancho},carteleria.alto={carteleria.alto},carteleria.superficie={carteleria.superficie},carteleria.estructura={carteleria.estructura},carteleria.condicionEst={carteleria.condicionEst},carteleria.fundacion={carteleria.fundacion},carteleria.condicionFundacion={carteleria.condicionFundacion},carteleria.pinturaFondo={carteleria.pinturaFondo},carteleria.condicionPintura={carteleria.condicionPintura},carteleria.tipoLetra={carteleria.tipoLetra},carteleria.numeroRuta={carteleria.numeroRuta},carteleria.kmRuta={carteleria.kmRuta}"/>
