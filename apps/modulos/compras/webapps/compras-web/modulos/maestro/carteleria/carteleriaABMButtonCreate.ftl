<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>


<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancel" type="button" name="btnCancelar" value="Cancelar" class="boton"/>
				<input id="store" type="button" name="btnAgregar" value="Agregar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/carteleria/createCarteleria.action" 
  source="store" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="carteleria.oid={carteleria.oid},carteleria.descripcion={carteleria.descripcion}, carteleria.propiedadCartel={carteleria.propiedadCartel},carteleria.observaciones={carteleria.observaciones},carteleria.alto={carteleria.alto},carteleria.ancho={carteleria.ancho},carteleria.superficie={carteleria.superficie},carteleria.estructura={carteleria.estructura},carteleria.condicionEst={carteleria.condicionEst},carteleria.fundacion={carteleria.fundacion},carteleria.condicionFundacion={carteleria.condicionFundacion},carteleria.pintura={carteleria.pintura},carteleria.condicionPintura={carteleria.condicionPintura},carteleria.tipoLetra={carteleria.tipoLetra},carteleria.kmRuta={carteleria.kmRuta},carteleria.numeroRuta={carteleria.numeroRuta},carteleria.proveedor.nroProveedor={carteleria.proveedor.nroProveedor} "/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action"  
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=buscar-carteleria,flowControl=back"/>
