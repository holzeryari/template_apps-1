<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>

<@s.hidden id="navigationId" name="navigationId"/>
	<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
        	<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Datos del Cliente</b></td>
				<!-- <td>					
					<div align="right">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0007" 
							enabled="true" 
							cssClass="item" 
							id="crear"										
							href="javascript://nop/">
							<b>Agregar</b>										
							<img src="${request.contextPath}/common/images/agregar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
						</@security.a>
					</div>
				</td> -->
			</tr>
		</table>
		<#assign clasificacionDisabled="false">				
		<#assign clasificacionClass="textarea">
		<@s.if test="cliente.tipo.ordinal() == 1">				
				<#assign clasificacionDisabled="true">				
				<#assign clasificacionClass="textareagris">				
		</@s.if>
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
			<tr>
				<td class="textoCampo">C&oacute;digo:</td>
	      		<td class="textoDato">
	      			<@s.textfield 
	      				templateDir="custontemplates" 
						id="codigo" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="cliente.oid" 
						title="C&oacute;digo" />
				</td>
	      		<td class="textoCampo">Descripci&oacute;n:</td>
	      		<td class="textoDato"><@s.textfield 
	      				templateDir="custontemplates" 
						id="descripcion" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="cliente.descripcion" 
						title="Descripcion" />
				</td>
	    	</tr>					
			<tr>
				<td class="textoCampo">Tipo:</td>
	      		<td class="textoDato">
		      		<@s.select 
						templateDir="custontemplates" 
						id="tipo" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="cliente.tipo" 
						list="tipoList" 
						listKey="key" 
						listValue="description" 
						value="cliente.tipo.ordinal()"
						title="Tipo de cliente"
						headerKey="0"
						headerValue="Todos"  
						onchange="javascript:clienteBusquedaSelect(this);"
						 />
				</td>
				<td class="textoCampo">Tipo Cliente SIS:</td>
	      		<td class="textoDato">
	      			<@s.select 
						templateDir="custontemplates" 
						id="tipoClienteSIS" 
						cssClass="textarea"						
						cssStyle="width:165px" 
						name="cliente.tipoClienteSIS" 
						list="tipoClienteSISList"
						listKey="key" 
						listValue="description" 
						value="cliente.tipoClienteSIS.ordinal()"
						title="Tipo Cliente SIS"
						headerKey="0"
						headerValue="Todos"											
						/>
	      		</td>
			</tr>
			<tr>
				<td class="textoCampo">Estado:</td>
	  			<td class="textoDato" >
	  			<@s.select 
					templateDir="custontemplates" 
					id="cliente.estado" 
					cssClass="textarea"
					cssStyle="width:165px" 
					name="cliente.estado" 
					list="estadoList" 
					listKey="key" 
					listValue="description" 
					value="cliente.estado.ordinal()"
					title="Estado"
					headerKey="0"
					headerValue="Todos"							
					/>
				</td>

			</tr>		
			<tr>
	    		<td colspan="4" class="lineaGris" align="right">
	      			<input id="limpiar" type="button" name="btnVolver" value="Limpiar" class="boton"/>		
	      			<input id="buscar" type="button" name="btnVolver" value="Buscar" class="boton"/>
	    		</td>
			</tr>	
		</table>
	</div>
	
	<!-- Resultado Filtro -->
	
	<@s.if test="clienteRusSisViewList!=null"> 
		<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
			
			<#assign clienteOid="0" />
			<@s.if test="cliente.oid!=null">
				<#assign clienteOid="${cliente.oid}" />
			</@s.if>
			
			<#assign clienteTipo="0" />
			<@s.if test="cliente.tipo.oid!=null">
					<#assign clienteTipo="${cliente.tipo.ordinal()}"/>
			</@s.if>
			<#assign clienteDescripcion="${cliente.descripcion}" />
			
			<#assign clienteClasificacion="0" />
			<@s.if test="cliente.clasificacion!=null">
				<#assign clienteClasificacion="${cliente.clasificacion.ordinal()}" />
			</@s.if>
			
			<#assign clienteEstado="0" />
			<@s.if test="clienteEstado.estado!=null">
				<#assign clienteEstado = "${cliente.estado.ordinal()}" />
			</@s.if>
			
		    <#assign clienteTipoClienteSis="0" />
			<@s.if test="clienteEstado.tipoClienteSis!=null">
				<#assign clienteTipoClienteSis = "${cliente.tipoClienteSis}" />
			</@s.if>
						
			<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Clientes encontrados</td>
					<td>
						<div class="alineacionDerecha">
							<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0008" 
								cssClass="item" 
								href="${request.contextPath}/maestro/cliente/printXLS.action?cliente.tipo=${clienteTipo}&cliente.descripcion=${clienteDescripcion}&cliente.oid=${clienteOid}&cliente.clasificacion=${clienteClasificacion}&cliente.estado=${clienteEstado}">												
								<img src="${request.contextPath}/common/images/imprimirExcel.gif" title="Exportar a Excel" align="absmiddle" border="0" hspace="3">
							</@security.a>	
						</div>
						<div class="alineacionDerecha">
							<b>Imprimir</b>
							<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0008" 
								cssClass="item" 
								href="${request.contextPath}/maestro/cliente/printPDF.action?cliente.tipo=${clienteTipo}&cliente.descripcion=${clienteDescripcion}&cliente.oid=${clienteOid}&cliente.clasificacion=${clienteClasificacion}&cliente.estado=${clienteEstado}">
								<img src="${request.contextPath}/common/images/imprimirPdf.gif" title="Exportar a PDF" align="absmiddle" border="0" hspace="3">
							</@security.a>
						</div>	
						
					</td>
				</tr>
			</table>
			
		
			<@vc.anchors target="contentTrx" ajaxFlag="ajax">                    
				<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="clienteRusSisViewList" id="cliente" pagesize=15 defaultsort=2 partialList=true size="recordSize" keepStatus=true excludedParams="resetFlag">
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select"  class="botoneraAnchoCon2" title="Acciones">
						<div class="alineacion">
							<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0003" 
								enabled=".readable" 
								cssClass="item" 
								href="${request.contextPath}/maestro/cliente/readView.action?cliente.oid=${cliente.oid?c}&navigationId=cliente-ver">
								<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver" border="0">
							</@security.a>
						</div>
						<div class="alineacion">
							<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0004" 
								enabled=".updatable" 
								cssClass="item" 
								href="${request.contextPath}/maestro/cliente/administrarView.action?cliente.oid=${cliente.oid?c}&navigationId=cliente-administrar&flowControl=regis">
								<img  src="${request.contextPath}/common/images/administrar.gif" alt="Administrar" title="Administrar"  border="0">
							</@security.a>
						</div>
				<!-- no se elimina mas desde Compras los clientes
						<div class="alineacion">
							<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0005" 
								enabled="cliente.eraseable" 
								cssClass="item" 
								href="${request.contextPath}/maestro/cliente/deleteView.action?cliente.oid=${cliente.oid}&navigationId=cliente-eliminar">
								<img  src="${request.contextPath}/common/images/eliminar.gif" alt="Eliminar" title="Eliminar" border="0">
							</@security.a>
						</div>
						<div class="alineacion">
							<#if cliente.estado == "Activo">
								<#assign imagen="desactivar.gif">
								<#assign alternativa="Desactivar">
								<#assign titulo="Desactivar">
								<@security.a 
									templateDir="custontemplates" 
									securityCode="CUF0006" 
									enabled="true" 
									cssClass="item" 
									href="${request.contextPath}/maestro/cliente/desactivarView.action?cliente.oid=${cliente.oid}&navigationId=cliente-desactivar">
									<img src="${request.contextPath}/common/images/${imagen}" alt="${alternativa}" title="${titulo}" border="0">
								</@security.a>
							<#else>
								<#assign imagen="activar.gif">
								<#assign alternativa="Activar">
								<#assign titulo="Activar">
								<@security.a 
									templateDir="custontemplates" 
									securityCode="CUF0006" 
									enabled="true" 
									cssClass="item" 
									href="${request.contextPath}/maestro/cliente/activarView.action?cliente.oid=${cliente.oid}&navigationId=cliente-actualizar">
									<img src="${request.contextPath}/common/images/${imagen}" alt="${alternativa}" title="${titulo}" border="0">
								</@security.a>
							</#if>
							-->
						</div>
					</@display.column>
				
				    <@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="codi" title="C&oacute;digo" />				
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="descripcion" title="Descripci&oacute;n" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="tipoClienteSIS" title="Tipo Sis" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="tipo" title="Tipo" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="clasificacion" title="Clasificaci&oacute;n" />					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="estado" title="Estado" />					
					</@display.table>
				</@vc.anchors>
			</div>
		</@s.if> 
<!-- parameters="resetFlag=true,navigationId={navigationId},flowControl=regis,cliente.tipo={tipo},cliente.descripcion={descripcion},cliente.oid={codigo},cliente.clasificacion={clasificacion},cliente.estado={cliente.estado}"/> -->
			
<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/cliente/search.action" 
  source="buscar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="resetFlag=true,navigationId={navigationId},flowControl=regis,cliente.tipo={tipo},cliente.descripcion={descripcion},cliente.oid={codigo},cliente.clasificacion={clasificacion},cliente.estado={cliente.estado},cliente.tipoClienteSIS={tipoClienteSIS}"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/cliente/view.action" 
  source="limpiar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=regis"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/cliente/createView.action" 
  source="crear" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters=""/>  
