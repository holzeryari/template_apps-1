<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<@s.hidden id="cliente.oid" name="cliente.oid"/>
<@s.hidden id="cliente.estado" name="cliente.estado.ordinal()"/>
<@s.hidden id="cliente.versionNumber" name="cliente.versionNumber"/>

	<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
			<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos del Cliente</b></td>
				<td>
					<div align="right">
						<@s.a templateDir="custontemplates" id="modificarCliente" name="modificarCliente" href="javascript://nop/" cssClass="ocultarIcono">
						<b>Modificar</b><img src="${request.contextPath}/common/images/modificar.gif" border="0" align="absmiddle" hspace="3" >
						</@s.a>
					</div>
				</td>
			</tr>
		</table>

		<@s.if test="accion == 'createCliente' || accion == 'updateCliente'">
			<#assign fondoFijoAsignadoDisabled="true">
			<#assign clasificacionDisabled="true">
			<#assign esDuenioAplicacionDisabled="true">
			<#assign importeFondoFijoDisabled="true">
			<#assign autorizaFSCFSSDisabled="true">
			<#assign tieneMontoMaximoDisabled="true">
			<#assign montoMinimoDisabled="true">
			<#assign montoMaximoDisabled="true">
		
			<#assign fondoFijoAsignadoClass="textareagris">
			<#assign clasificacionClass="textareagris">
			<#assign esDuenioAplicacionClass="textaregris">
			<#assign importeFondoFijoClass="textareagris">
			<#assign autorizaFSCFSSClass="textareagris">
			<#assign tieneMontoMaximoClass="textareagris">
			<#assign montoMinimoClass="textareagris">
			<#assign montoMaximoClass="textareagris">
		</@s.if>
		<@s.else>
			<#assign fondoFijoAsignadoDisabled="true">
			<#assign clasificacionDisabled="true">
			<#assign esDuenioAplicacionDisabled="true">
			<#assign importeFondoFijoDisabled="true">
			<#assign autorizaFSCFSSDisabled="true">
			<#assign tieneMontoMaximoDisabled="true">
			<#assign montoMinimoDisabled="true">
			<#assign montoMaximoDisabled="true">
		
			<#assign fondoFijoAsignadoClass="textarea">
			<#assign clasificacionClass="textarea">
			<#assign esDuenioAplicacionClass="textarea">
			<#assign importeFondoFijoClass="textarea">
			<#assign autorizaFSCFSSClass="textarea">
			<#assign tieneMontoMaximoClass="textarea">
			<#assign montoMinimoClass="textarea">
			<#assign montoMaximoClass="textarea">
		</@s.else>
		
		<@s.if test="accion == 'createCliente' || accion == 'updateCliente'">
			<@s.if test="cliente.tipo == null || cliente.tipo.ordinal() == 0">
				<#assign fondoFijoAsignadoDisabled="true">
				<#assign clasificacionDisabled="true">
				<#assign esDuenioAplicacionDisabled="true">
			
				<#assign fondoFijoAsignadoClass="textareagris">
				<#assign clasificacionClass="textareagris">
				<#assign esDuenioAplicacionClass="textareagris">
			
				<@s.if test="cliente.fondoFijoAsignado.ordinal() == 0">
					<#assign importeFondoFijoDisabled="true">
					<#assign importeFondoFijoClass="textareagris">
				</@s.if>
				<@s.if test="cliente.fondoFijoAsignado.ordinal() == 1">
					<#assign importeFondoFijoDisabled="true">
					<#assign importeFondoFijoClass="textareagris">
				</@s.if>
				<@s.if test="cliente.fondoFijoAsignado.ordinal() == 2">
					<#assign importeFondoFijoDisabled="false">
					<#assign importeFondoFijoClass="textarea">
				</@s.if>
			
			
				<@s.if test="cliente.esDuenioAplicacion.ordinal() == 0">
					<#assign autorizaFSCFSSDisabled="true">
					<#assign autorizaFSCFSSClass="textareagris">
				</@s.if>
				<@s.if test="cliente.esDuenioAplicacion.ordinal() == 1">
					<#assign autorizaFSCFSSDisabled="true">
					<#assign autorizaFSCFSSClass="textareagris">
				</@s.if>
				<@s.if test="cliente.esDuenioAplicacion.ordinal() == 2">
					<#assign autorizaFSCFSSDisabled="true">
					<#assign autorizaFSCFSSClass="textareagris">
				</@s.if>
			
			
				<@s.if test="cliente.autorizaFSCFSS.ordinal() == 0">
					<#assign montoMinimoDisabled="true">
					<#assign montoMinimoClass="textareagris">
					<#assign montoMaximoDisabled="true">
					<#assign montoMaximoClass="textareagris">
					<#assign tieneMontoMaximoDisabled="true">
					<#assign tieneMontoMaximoClass="textareagris">
				</@s.if>
				<@s.if test="cliente.autorizaFSCFSS.ordinal() == 1">
					<#assign montoMinimoDisabled="true">
					<#assign montoMinimoClass="textareagris">
					<#assign montoMaximoDisabled="true">
					<#assign montoMaximoClass="textareagris">
					<#assign tieneMontoMaximoDisabled="true">
					<#assign tieneMontoMaximoClass="textareagris">
				</@s.if>
				<@s.if test="cliente.autorizaFSCFSS.ordinal() == 2">
					<#assign montoMinimoDisabled="false">
					<#assign montoMinimoClass="textarea">
					<@s.if test="cliente.esDuenioAplicacion.ordinal() == 1">
						<#assign tieneMontoMaximoDisabled="false">
						<#assign tieneMontoMaximoClass="textarea">
					</@s.if>
					<@s.if test="cliente.esDuenioAplicacion.ordinal() == 2">
						<#assign tieneMontoMaximoDisabled="true">
						<#assign tieneMontoMaximoClass="textareagris">
					</@s.if>
		
					<@s.if test="cliente.tieneMontoMaximo.ordinal() == 0">
						<#assign montoMaximoDisabled="true">
						<#assign montoMaximoClass="textareagris">
					</@s.if>
					<@s.if test="cliente.tieneMontoMaximo.ordinal() == 1">
						<#assign montoMaximoDisabled="true">
						<#assign montoMaximoClass="textareagris">
					</@s.if>
					<@s.if test="cliente.tieneMontoMaximo.ordinal() == 2">
						<#assign montoMaximoDisabled="false">
						<#assign montoMaximoClass="textarea">
					</@s.if>
				</@s.if>
			</@s.if>
			<@s.if test="cliente.tipo.ordinal() == 1">
				<#assign fondoFijoAsignadoDisabled="false">
				<#assign clasificacionDisabled="true">
				<#assign esDuenioAplicacionDisabled="false">
			
				<#assign fondoFijoAsignadoClass="textareagris">
				<#assign clasificacionClass="textareagris">
				<#assign esDuenioAplicacionClass="textarea">
			
			
				<@s.if test="cliente.fondoFijoAsignado.ordinal() == 0">
					<#assign importeFondoFijoDisabled="true">
					<#assign importeFondoFijoClass="textareagris">
				</@s.if>
				<@s.if test="cliente.fondoFijoAsignado.ordinal() == 1">
					<#assign importeFondoFijoDisabled="true">
					<#assign importeFondoFijoClass="textareagris">
				</@s.if>
				<@s.if test="cliente.fondoFijoAsignado.ordinal() == 2">
					<#assign importeFondoFijoDisabled="false">
					<#assign importeFondoFijoClass="textarea">
				</@s.if>
			
			
				<@s.if test="cliente.esDuenioAplicacion.ordinal() == 0">
					<#assign autorizaFSCFSSDisabled="false">
					<#assign autorizaFSCFSSClass="textarea">
				</@s.if>
				<@s.if test="cliente.esDuenioAplicacion.ordinal() == 1">
					<#assign autorizaFSCFSSDisabled="false">
					<#assign autorizaFSCFSSClass="textarea">
				</@s.if>
				<@s.if test="cliente.esDuenioAplicacion.ordinal() == 2">
					<#assign autorizaFSCFSSDisabled="true">
					<#assign autorizaFSCFSSClass="textareagris">
				</@s.if>
			
			
				<@s.if test="cliente.autorizaFSCFSS.ordinal() == 0">
					<#assign montoMinimoDisabled="true">
					<#assign montoMinimoClass="textareagris">
					<#assign montoMaximoDisabled="true">
					<#assign montoMaximoClass="textareagris">
					<#assign tieneMontoMaximoDisabled="true">
					<#assign tieneMontoMaximoClass="textareagris">
				</@s.if>
				<@s.if test="cliente.autorizaFSCFSS.ordinal() == 1">
					<#assign montoMinimoDisabled="true">
					<#assign montoMinimoClass="textareagris">
					<#assign montoMaximoDisabled="true">
					<#assign montoMaximoClass="textareagris">
					<#assign tieneMontoMaximoDisabled="true">
					<#assign tieneMontoMaximoClass="textareagris">
				</@s.if>
				<@s.if test="cliente.autorizaFSCFSS.ordinal() == 2">
					<#assign montoMinimoDisabled="false">
					<#assign montoMinimoClass="textarea">
					<#assign montoMaximoDisabled="false">
					<#assign montoMaximoClass="textarea">
					<@s.if test="cliente.esDuenioAplicacion.ordinal() == 1">
						<#assign tieneMontoMaximoDisabled="false">
						<#assign tieneMontoMaximoClass="textarea">
					</@s.if>
					<@s.if test="cliente.esDuenioAplicacion.ordinal() == 2">
						<#assign tieneMontoMaximoDisabled="true">
						<#assign tieneMontoMaximoClass="textareagris">
					</@s.if>
		
					<@s.if test="cliente.tieneMontoMaximo.ordinal() == 0">
						<#assign montoMaximoDisabled="true">
						<#assign montoMaximoClass="textareagris">
					</@s.if>
					<@s.if test="cliente.tieneMontoMaximo.ordinal() == 1">
						<#assign montoMaximoDisabled="true">
						<#assign montoMaximoClass="textareagris">
					</@s.if>
					<@s.if test="cliente.tieneMontoMaximo.ordinal() == 2">
						<#assign montoMaximoDisabled="false">
						<#assign montoMaximoClass="textarea">
					</@s.if>
				</@s.if>
			</@s.if>
			<@s.if test="cliente.tipo.ordinal() == 2">
				<#assign fondoFijoAsignadoDisabled="false">
				<#assign clasificacionDisabled="false">
				<#assign esDuenioAplicacionDisabled="true">
			
				<#assign fondoFijoAsignadoClass="textarea">
				<#assign clasificacionClass="textarea">
				<#assign esDuenioAplicacionClass="textareagris">
			
			
				<@s.if test="cliente.fondoFijoAsignado.ordinal() == 0">
					<#assign importeFondoFijoDisabled="true">
					<#assign importeFondoFijoClass="textareagris">
				</@s.if>
				<@s.if test="cliente.fondoFijoAsignado.ordinal() == 1">
					<#assign importeFondoFijoDisabled="true">
					<#assign importeFondoFijoClass="textareagris">
				</@s.if>
				<@s.if test="cliente.fondoFijoAsignado.ordinal() == 2">
					<#assign importeFondoFijoDisabled="false">
					<#assign importeFondoFijoClass="textarea">
				</@s.if>
			
			
				<@s.if test="cliente.esDuenioAplicacion.ordinal() == 0">
					<#assign autorizaFSCFSSDisabled="true">
					<#assign autorizaFSCFSSClass="textareagris">
				</@s.if>
				<@s.if test="cliente.esDuenioAplicacion.ordinal() == 1">
					<#assign autorizaFSCFSSDisabled="true">
					<#assign autorizaFSCFSSClass="textareagris">
				</@s.if>
				<@s.if test="cliente.esDuenioAplicacion.ordinal() == 2">
					<#assign autorizaFSCFSSDisabled="true">
					<#assign autorizaFSCFSSClass="textareagris">
				</@s.if>
			
			
				<@s.if test="cliente.autorizaFSCFSS.ordinal() == 0">
					<#assign montoMinimoDisabled="true">
					<#assign montoMinimoClass="textareagris">
					<#assign montoMaximoDisabled="true">
					<#assign montoMaximoClass="textareagris">
					<#assign tieneMontoMaximoDisabled="true">
					<#assign tieneMontoMaximoClass="textareagris">
				</@s.if>
				<@s.if test="cliente.autorizaFSCFSS.ordinal() == 1">
					<#assign montoMinimoDisabled="true">
					<#assign montoMinimoClass="textareagris">
					<#assign montoMaximoDisabled="true">
					<#assign montoMaximoClass="textareagris">
					<#assign tieneMontoMaximoDisabled="true">
					<#assign tieneMontoMaximoClass="textareagris">
				</@s.if>
				<@s.if test="cliente.autorizaFSCFSS.ordinal() == 2">
					<#assign montoMinimoDisabled="false">
					<#assign montoMinimoClass="textarea">
					<#assign montoMaximoDisabled="false">
					<#assign montoMaximoClass="textarea">
					<@s.if test="cliente.esDuenioAplicacion.ordinal() == 1">
						<#assign tieneMontoMaximoDisabled="false">
						<#assign tieneMontoMaximoClass="textarea">
					</@s.if>
					<@s.if test="cliente.esDuenioAplicacion.ordinal() == 2">
						<#assign tieneMontoMaximoDisabled="true">
						<#assign tieneMontoMaximoClass="textareagris">
					</@s.if>
		
					<@s.if test="cliente.tieneMontoMaximo.ordinal() == 0">
						<#assign montoMaximoDisabled="true">
						<#assign montoMaximoClass="textareagris">
					</@s.if>
					<@s.if test="cliente.tieneMontoMaximo.ordinal() == 1">
						<#assign montoMaximoDisabled="true">
						<#assign montoMaximoClass="textareagris">
					</@s.if>
					<@s.if test="cliente.tieneMontoMaximo.ordinal() == 2">
						<#assign montoMaximoDisabled="false">
						<#assign montoMaximoClass="textarea">
					</@s.if>
				</@s.if>
			</@s.if>
		</@s.if>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
			<tr>
	      		<td class="textoCampo">C&oacute;digo:</td>
	      		<td class="textoDato"><@s.property default="&nbsp;" escape=false value="cliente.oid"/>&nbsp;</td>
	      		<td class="textoCampo">Tipo:</td>
				<td class="textoDato">
		      		<@s.select 
						templateDir="custontemplates" 
						id="tipo" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="cliente.tipo" 
						list="tipoList" 
						listKey="key" 
						listValue="description" 
						value="cliente.tipo.ordinal()"
						title="Tipo de cliente"
						headerKey="0"
						headerValue="Seleccionar"   
						onchange="javascript:clienteSelect(this);"
						/>
				</td>	      			
			</tr>	
			<tr>
	      		<td class="textoCampo"><@s.text name="cliente.descripcion" /></td>
	      		<td class="textoDato">
					<@s.textfield 
						templateDir="custontemplates" 
						id="descripcion" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="cliente.descripcion" 
						title="Descripci&oacute;n" />					
		      	</td>
	      		<td class="textoCampo">Clasificaci&oacute;n:</td>
				<td class="textoDato">
					<@s.select 
						templateDir="custontemplates" 
						id="clasificacion" 
						cssClass="${clasificacionClass}"
						disabled="${clasificacionDisabled}"
						cssStyle="width:165px" 
						name="cliente.clasificacion" 
						list="clasificacionList" 
						listKey="key" 
						listValue="description" 
						value="cliente.clasificacion.ordinal()"
						title="Clasificacion"
						headerKey="0"
						headerValue="Seleccionar"
						/>
				</td>
	    	</tr>
			
			<tr>
				<td class="textoCampo">Fondo Fijo Asignado:</td>
	      		<td class="textoDato">
					<@s.select 
						templateDir="custontemplates" 
						id="fondoFijoAsignado" 
						cssClass="${fondoFijoAsignadoClass}"
						disabled="${fondoFijoAsignadoDisabled}"
						cssStyle="width:165px" 
						name="cliente.fondoFijoAsignado" 
						list="fondoFijoAsignadoList" 
						listKey="key" 
						listValue="description" 
						value="cliente.fondoFijoAsignado.ordinal()"
						title="Fondo Fijo Asignado"
						onchange="javascript:fondoFijoSelect(this);"
						headerKey="0"
						headerValue="Seleccionar"   
						/>
				</td>

				<td class="textoCampo">Importe Fondo Fijo: </td>
				<td class="textoDato">
					<@s.textfield 
						templateDir="custontemplates"
						template="textMoney" 
						id="importeFondoFijo" 
						disabled="${importeFondoFijoDisabled}"
						cssClass="${importeFondoFijoClass}"
						cssStyle="width:160px" 
						name="cliente.importeFondoFijo"
						title="Importe Fondo Fijo"
						/>
				</td>
	    	</tr>
			<tr>
				<td class="textoCampo">Due&ntilde;o de la aplicaci&oacute;n: </td>
				<td class="textoDato" colspan="3">
		      		<@s.select 
						templateDir="custontemplates" 
						id="duenio" 
						disabled="${esDuenioAplicacionDisabled}"
						cssClass="${esDuenioAplicacionClass}"
						cssStyle="width:165px" 
						name="cliente.esDuenioAplicacion" 
						list="esDuenioAplicacionList" 
						listKey="key" 
						listValue="description" 
						value="cliente.esDuenioAplicacion.ordinal()"
						title="Due�o de la aplicaci�n"
						onchange="javascript:duenioAplicacionSelect(this);"
						headerKey="0"
						headerValue="Seleccionar"			
						/>
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Autoriza FSC/FSS: </td>
				<td class="textoDato">
					<@s.select 
						templateDir="custontemplates" 
						id="autorizaFSCFSS" 
						cssClass="${autorizaFSCFSSClass}"
						disabled="${autorizaFSCFSSDisabled}"
						cssStyle="width:165px" 
						name="cliente.autorizaFSCFSS" 
						list="autorizaFSCFSSList" 
						listKey="key" 
						listValue="description" 
						value="cliente.autorizaFSCFSS.ordinal()"
						title="Autoriza FSC/FSS"
						onchange="javascript:autorizaFSCFSSSelect(this);"
						headerKey="0"
						headerValue="Seleccionar"   
						/>
				</td>
				<td class="textoCampo">Tiene Monto M&aacute;ximo: </td>
				<td class="textoDato">
					<@s.select 
						templateDir="custontemplates" 
						id="tieneMontoMaximo" 
						cssClass="${tieneMontoMaximoClass}"
						disabled="${tieneMontoMaximoDisabled}"
						cssStyle="width:165px" 
						name="cliente.tieneMontoMaximo" 
						list="tieneMontoMaximoList" 
						listKey="key" 
						listValue="description" 
						value="cliente.tieneMontoMaximo.ordinal()"
						title="Tiene Monto Maximo"
						onchange="javascript:tieneMontoMaximoSelect(this);"
						headerKey="0"
						headerValue="Seleccionar"   
						/>
				</td>
			</tr>
			<tr>
	      		<td  class="textoCampo">Monto M&iacute;nimo:</td>
	      		<td  class="textoDato">
					<@s.textfield 
						templateDir="custontemplates" 
						template="textMoney"
						id="montoMinimo" 
						disabled="${montoMinimoDisabled}"
						cssClass="${montoMinimoClass}"
						cssStyle="width:160px" 
						name="cliente.montoMinimo" 
						title="Monto M&iacute;nimo" 
						/>
				</td>
      			<td  class="textoCampo">Monto M&aacute;ximo:</td>
	      		<td  class="textoDato">
					<@s.textfield 
						templateDir="custontemplates" 
						template="textMoney" 
						id="montoMaximo" 
						disabled="${montoMaximoDisabled}"
						cssClass="${montoMaximoClass}"
						cssStyle="width:160px" 
						name="cliente.montoMaximo" 
						title="Monto M&aacute;ximo:" 
						/>							
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Estado:</td>
	      		<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="cliente.estado"/>
				</td>
				
				<td class="textoCampo">Nro. Bolsa:</td>
	      		<td class="textoDato">
					<@s.textfield 
	  					templateDir="custontemplates" 
						id="bolsaCorreo" 
						cssClass="textarea" 
						cssStyle="width:160px" 
						name="cliente.bolsaCorreo" 
						title="Nro. Bolsa" />
				</td>
			</tr>
			
			<#-- renglon de separacion -->
			<tr><td colspan="4">&nbsp;</td></tr>
		
		</table>
	</div>

	<@s.if test="banderaTipo == 'tipo'">
		<@s.hidden id="tipo" name="cliente.tipo.ordinal()"/>
	</@s.if>
	<@s.if test="banderaDuenio == 'duenio'">
		<@s.hidden id="cliente.esDuenioAplicacion" name="cliente.esDuenioAplicacion.ordinal()"/>
	</@s.if>
	<@s.if test="banderaTieneMontoMaximo == 'tieneMontoMaximo'">
		<@s.hidden id="cliente.tieneMontoMaximo" name="cliente.tieneMontoMaximo.ordinal()"/>
	</@s.if>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/cliente/updateView.action" 
  source="modificarCliente" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="cliente.oid={cliente.oid},cliente.versionNumber={cliente.versionNumber}"/>
