<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancel" type="button" name="btnVolver" value="Cancelar" class="boton"/>
				<input id="store" type="button" name="btnAgregar" value="Agregar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/cliente/create.action" 
  source="store" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="cliente.descripcion={descripcion},cliente.tipo={tipo},cliente.clasificacion={clasificacion},cliente.fondoFijoAsignado={fondoFijoAsignado},cliente.importeFondoFijo={importeFondoFijo},cliente.autorizaFSCFSS={autorizaFSCFSS},cliente.montoMinimo={montoMinimo},cliente.montoMaximo={montoMaximo},cliente.estado={cliente.estado},cliente.oid={oid},cliente.versionNumber={versionNumber},cliente.esDuenioAplicacion={cliente.esDuenioAplicacion},cliente.tieneMontoMaximo={cliente.tieneMontoMaximo}"/>
  
  <@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action"  
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=buscar-clientes,flowControl=back"/>