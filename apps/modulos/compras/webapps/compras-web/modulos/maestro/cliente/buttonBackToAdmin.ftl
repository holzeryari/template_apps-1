<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="left">
				<input id="cancel" type="button" name="btnVolver" value="Volver" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

  <@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=cliente-administrar,flowControl=back"/>
  
  <@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/cliente/securityUserList.action" 
  source="buscar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="usuarioSeguridad.username={usuarioSeguridad.username},usuarioSeguridad.name={usuarioSeguridad.name},usuarioSeguridad.fullname={usuarioSeguridad.fullname},cliente.oid={cliente.oid},navigationId=${navigationId},flowControl=regis"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/cliente/createSecurityUserView.action" 
  source="limpiar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId=${navigationId},flowControl=regis,cliente.oid={cliente.oid}"/>
  
