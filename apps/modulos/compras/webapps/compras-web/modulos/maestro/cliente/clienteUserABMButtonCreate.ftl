<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancel" type="button" name="btnVolver" value="Cancelar" class="boton"/>
				<input id="store" type="button" name="btnAgregar" value="Agregar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/cliente/createClienteUsuario.action" 
  source="store" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="usuario.descripcion=${usuario.descripcion},usuario.nombre=${usuario.nombre},usuario.apellido=${usuario.apellido},usuario.nombreUsuario=${usuario.nombreUsuario},cliente.oid=${cliente.oid}"/>
  
  <@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action"  
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=cliente-administrar,flowControl=back,cliente.oid=${cliente.oid}"/>