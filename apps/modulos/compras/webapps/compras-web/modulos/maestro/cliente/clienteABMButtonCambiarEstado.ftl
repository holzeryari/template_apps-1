<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
	<#if cliente.estado == "Activo">
		<#assign titulo="Desactivar">
	<#else>
		<#assign titulo="Activar">
	</#if>

<div id="capaSeparadora" class="capaSeparadora">&nbsp;</div>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancel" type="button" name="btnVolver" value="Cancelar" class="boton"/>
				<input id="update" type="button" name="btnModificar" value="${titulo}" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/cliente/activar.action" 
  source="update" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="cliente.tipo={tipo},cliente.oid={cliente.oid},cliente.versionNumber={cliente.versionNumber}"/>
  
  <@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action"  
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=buscar-clientes,flowControl=back"/>
  
  