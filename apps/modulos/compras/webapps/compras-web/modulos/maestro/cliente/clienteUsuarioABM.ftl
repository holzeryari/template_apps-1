<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>


<@s.hidden id="webFlow" name="webFlow"/>
<@s.hidden id="viewState" name="viewState"/>
<@s.hidden id="cliente.oid" name="cliente.oid"/>
<@s.hidden id="usuario.oid" name="usuario.oid"/>

	<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
			<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td colspan="4"><b>Datos del Cliente</b></td>
			</tr>
		</table>
	
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>	
			<tr>
      			<td class="textoCampo">C&oacute;digo:</td>
      			<td class="textoDato"><@s.property default="&nbsp;" escape=false value="cliente.oid"/></td>
      			<td class="textoCampo">Tipo:</td>
				<td class="textoDato">${cliente.tipo}</td>	      			
			</tr>	
			<tr>
      			<td class="textoCampo"><@s.text name="cliente.descripcion" /></td>
	  			<td class="textoDato">${cliente.descripcion}</td>
      			<td class="textoCampo">Clasificaci&oacute;n:</td>
				<td class="textoDato">${cliente.clasificacion}</td>
			</tr>
			<tr>
				<td class="textoCampo">Fondo Fijo Asignado:</td>
      			<td  class="textoDato">${cliente.fondoFijoAsignado}</td>
				<td  class="textoCampo">Importe Fondo Fijo: </td>
				<td  class="textoDato">
					<#if cliente.importeFondoFijo?exists>
						${cliente.importeFondoFijo}
					<#else>
					&nbsp;
					</#if>
				</td>
    		</tr>
			<tr>
				<td class="textoCampo">Due&ntilde;o de la aplicaci&oacute;n: </td>
				<td class="textoDato" colspan="3">
					${cliente.esDuenioAplicacion} &nbsp;
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Autoriza FSC/FSS: </td>
				<td class="textoDato">
					${cliente.autorizaFSCFSS} &nbsp;
				</td>
				<td class="textoCampo">Tiene Monto M&aacute;ximo: </td>
				<td class="textoDato">
					${cliente.tieneMontoMaximo} &nbsp;
				</td>
			</tr>
			<tr>
      			<td class="textoCampo">Monto M&iacute;nimo:</td>
      			<td  class="textoDato">
	      			<#if cliente.montoMinimo?exists>
						${cliente.montoMinimo}
					<#else>
					&nbsp;
					</#if>
				</td>
      			<td class="textoCampo">Monto M&aacute;ximo:</td>
      			<td  class="textoDato">
	      			<#if cliente.montoMaximo?exists>
						${cliente.montoMaximo}
					<#else>
					&nbsp;
					</#if>
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Estado:</td>
      			<td  class="textoDato" colspan="3">
					${cliente.estado} &nbsp;
				</td>
    		</tr>
    		<#-- renglon de separacion -->
			<tr><td colspan="4">&nbsp;</td></tr>
		</table>
	</div>
	
	<!--datos de usuario -->
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td colspan="4"><b>Datos del Usuario</b></td>
			</tr>
		</table>
	
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
				<td class="textoCampo">C&oacute;digo:</td>
	    		<td class="textoDato"><@s.property default="&nbsp;" escape=false value="usuario.oId"/></td>
	      		<td class="textoCampo">Descripci&oacute;n</td>
				<td class="textoDato">
					${usuario.descripcion}					
				</td>	      			
			</tr>	
			<tr>
	      		<td class="textoCampo">Nombre:</td>
    	  		<td class="textoDato">
		      		${usuario.nombre}
		      	</td>
	      		<td class="textoCampo">Apellido:</td>
				<td class="textoDato">
					${usuario.apellido}
				</td>
    		</tr>
    		<tr><td class="textoCampo" colspan="4">&nbsp;</td></tr>
		</table>
	</div>
				
	
		