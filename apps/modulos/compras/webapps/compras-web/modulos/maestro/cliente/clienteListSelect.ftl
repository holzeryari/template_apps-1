<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>

<@s.hidden id="navigationId" name="navigationId"/>
<@s.hidden id="navegacionIdBack" name="navegacionIdBack"/>
<@s.hidden id="estadoDuro" name="estadoDuro"/>
<@s.hidden id="tipoDuro" name="tipoDuro"/>
<@s.hidden id="fondoFijoDuro" name="fondoFijoDuro"/>

<@s.if test="estadoDuro == true">
	<@s.hidden id="cliente.estado" name="cliente.estado.ordinal()"/>
</@s.if>

<@s.if test="fondoFijoDuro == true">
	<@s.hidden id="cliente.fondoFijoAsignado" name="cliente.fondoFijoAsignado.ordinal()"/>
</@s.if>
  
<@s.if test="tipoDuro == true">
	<@s.hidden id="cliente.tipo" name="cliente.tipo.ordinal()"/>
</@s.if>

	<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
        	<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
		<#assign clasificacionDisabled="false">				
		<#assign clasificacionClass="textarea">
		<@s.if test="cliente.tipo.ordinal() == 1">				
				<#assign clasificacionDisabled="true">				
				<#assign clasificacionClass="textareagris">				
		</@s.if>
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Datos del Cliente</b></td>  
			</tr>
		</table>
		
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
				<td class="textoCampo">C&oacute;digo:</td>
	      		<td class="textoDato">
	      			<@s.textfield 
	      				templateDir="custontemplates" 
						id="codigo" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="cliente.oid" 
						title="C&oacute;digo" />
				</td>
	      		<td class="textoCampo">Descripci&oacute;n:</td>
	      		<td class="textoDato">
	      			<@s.textfield 
	      				templateDir="custontemplates" 
						id="descripcion" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="cliente.descripcion" 
						title="Descripcion" />
				</td>
	    	</tr>			
    		
					
			<tr>
				<td class="textoCampo">Tipo:</td>
	      		<td class="textoDato">
	      			<@s.select 
						templateDir="custontemplates" 
						id="cliente.tipo" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="cliente.tipo" 
						list="tipoList" 
						listKey="key" 
						listValue="description" 
						value="cliente.tipo.ordinal()"
						title="Tipo de cliente"
						headerKey="0"
						headerValue="Todos"
						onchange="javascript:clienteBusquedaSelect(this);"
						/>
				</td>
					<td class="textoCampo">Tipo Cliente SIS:</td>
	      			<td class="textoDato">
	      			<@s.select 
						templateDir="custontemplates" 
						id="tipoClienteSIS" 
						cssClass="textarea"						
						cssStyle="width:165px" 
						name="cliente.tipoCLienteSis" 
						list="tipoClienteSISList"
						listKey="key" 
						listValue="description" 
						value="cliente.tipoClienteSIS.ordinal()"
						title="Tipo Cliente SIS"
						headerKey="0"
						headerValue="Todos"											
						/>
	      			</td>
			</tr>
			<tr>
				<td class="textoCampo">Fondo Fijo Asignado:</td>
	      		<td class="textoDato">
	      			<@s.select 
						templateDir="custontemplates" 
						id="cliente.fondoFijoAsignado" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="cliente.fondoFijoAsignado" 
						list="fondoFijoAsignadoList" 
						listKey="key" 
						listValue="description" 
						value="cliente.fondoFijoAsignado.ordinal()"
						title="Fondo Fijo Asignado"
						headerKey="0"
						headerValue="Todos"/>
				</td>
				<td class="textoCampo">Estado:</td>
				<td class="textoDato" colspan="3">
	      			<@s.select 
						templateDir="custontemplates" 
						id="cliente.estado" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="cliente.estado" 
						list="estadoList" 
						listKey="key" 
						listValue="description" 
						value="cliente.estado.ordinal()"
						title="Estado"
						headerKey="0"
						headerValue="Todos"/>
				</td>
			</tr>		
				<tr>
	    		<td colspan="4" class="lineaGris" align="right">
	      			<input id="limpiar" type="button" name="btnVolver" value="Limpiar" class="boton"/>		
	      			<input id="buscar" type="button" name="btnVolver" value="Buscar" class="boton"/>
	    		</td>
			</tr>	
		</table>
	</div>
	<!-- Resultado Filtro -->
	<@s.if test="clienteRusSisViewList!=null">
		<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">			
			<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Clientes encontrados</td>
				</tr>
			</table>
			
			<@vc.anchors target="contentTrx" ajaxFlag="ajax">
				<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="clienteRusSisViewList" id="clienteSelect" pagesize=15 defaultsort=2 partialList=true size="recordSize" keepStatus=true excludedParams="resetFlag">
					<#assign ref="${request.contextPath}/maestro/cliente/selectCliente.action?">										
				
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" style=" text-align:center; display:block" title="Acciones">
						<@s.a href="${ref}cliente.oid=${clienteSelect.oid?c}&cliente.descripcion=${clienteSelect.descripcion}&cliente.tipo=${clienteSelect.tipo.ordinal()}&navegacionIdBack=${navegacionIdBack}&navigationId=${navigationId}"><img src="${request.contextPath}/common/images/seleccionar.gif" alt="Seleccionar" title="Seleccionar" width="16" height="16" border="0"></@s.a>
						&nbsp;
					</@display.column>
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="codi" title="C&oacute;digo" />					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="descripcion" title="Descripci&oacute;n" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="tipoClienteSIS" title="Tipo Sis" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="tipo" title="Tipo" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="clasificacion" title="Clasificaci&oacute;n" />					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="estado" title="Estado" />					
				</@display.table>
			</@vc.anchors>
		</div>	
	</@s.if>
	
	<div id="capaBotonera" class="capaBotonera">
		<table id="tablaBotonera" class="tablaBotonera">
			<tr> 
				<td align="left">
					<input id="cancel" type="button" name="btnVolver" value="Volver" class="boton"/>
				</td>
			</tr>	
		</table>
	</div>


<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/cliente/selectClienteSearch.action" 
  source="buscar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="resetFlag=true,navigationId={navigationId},flowControl=regis,navegacionIdBack={navegacionIdBack},cliente.tipo={cliente.tipo},cliente.descripcion={descripcion},cliente.oid={codigo},cliente.clasificacion={clasificacion},cliente.estado={cliente.estado},estadoDuro={estadoDuro},fondoFijoDuro={fondoFijoDuro},cliente.fondoFijoAsignado={cliente.fondoFijoAsignado},tipoDuro={tipoDuro},cliente.tipoClienteSIS={tipoClienteSIS}"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/cliente/selectClienteView.action" 
  source="limpiar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=regis,navegacionIdBack={navegacionIdBack},fondoFijoDuro={fondoFijoDuro},cliente.fondoFijoAsignado={cliente.fondoFijoAsignado},cliente.estado={cliente.estado},cliente.tipo={cliente.tipo},estadoDuro={estadoDuro},tipoDuro={tipoDuro}"/>
  
 <@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navegacionIdBack},flowControl=back"/>
 
 