
<#setting locale="es_AR">

<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>


<@s.hidden id="navigationId" name="navigationId"/>
<@s.hidden id="tipoUnidad.rubro.oid" name="tipoUnidad.rubro.oid"/>

	<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
        	<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Datos del Tipo de Unidad</b></td>
				<td>					
					<div align="right">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0581" 
							enabled="true" 
							cssClass="item" 
							id="crear"										
							href="javascript://nop/">
							<b>Agregar</b>										
							<img src="${request.contextPath}/common/images/agregar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
						</@security.a>
					</div>
				</td>
			</tr>
		</table>
		
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
			<tr>
				<td class="textoCampo">C&oacute;digo:</td>
	  			<td class="textoDato">
	  			<@s.textfield 
	  					templateDir="custontemplates" 
						id="tipoUnidad.codigo" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="tipoUnidad.codigo" 
						title="C&oacute;digo" /></td>
	  		
	  			<td class="textoCampo">Descripci&oacute;n:</td>
	  			<td class="textoDato"><@s.textfield 
	  					templateDir="custontemplates" 
						id="tipoUnidad.descripcion" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="tipoUnidad.descripcion" 
						title="Descripcion" /></td>
			</tr>					
			<tr>
				<td class="textoCampo">Estado:</td>
	  			<td class="textoDato" colspan="3">
	  			<@s.select 
							templateDir="custontemplates" 
							id="tipoUnidad.estado" 
							cssClass="textarea"
							cssStyle="width:165px" 
							name="tipoUnidad.estado" 
							list="estadoList" 
							listKey="key" 
							listValue="description" 
							value="tipoUnidad.estado.ordinal()"
							title="Estado"
							headerKey="0"
							headerValue="Todos"							
							/>
				</td>
			</tr>
			<tr>
	    		<td colspan="4" class="lineaGris" align="right">
	      			<input id="limpiar" type="button" name="btnVolver" value="Limpiar" class="boton"/>		
	      			<input id="buscar" type="button" name="btnVolver" value="Buscar" class="boton"/>
	    		</td>
			</tr>		
		</table>
	</div>					
	<!-- Resultado Filtro -->
	<@s.if test="tipoUnidadList!=null">					
		<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">			
			<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Tipos de Unidades encontrados</td>
				</tr>
			</table>
			
			<@vc.anchors target="contentTrx" ajaxFlag="ajax">
         		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="tipoUnidadList" id="tipoUnidad" pagesize=15 defaultsort=2 partialList=true size="recordSize" keepStatus=true excludedParams="resetFlag">
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select"  class="botoneraAnchoCon4" title="Acciones">
					<div class="alineacion">				
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0583" 
							enabled="tipoUnidad.readable" 
							cssClass="item" 
							href="${request.contextPath}/maestro/tipoUnidad/readView.action?tipoUnidad.oid=${tipoUnidad.oid?c}">
							<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver" border="0">
						</@security.a>
						</div>
						<div class="alineacion">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0584" 
							enabled="tipoUnidad.updatable"
							cssClass="item"  
							href="${request.contextPath}/maestro/tipoUnidad/updateView.action?tipoUnidad.oid=${tipoUnidad.oid?c}&navigationId=tipoUnidad-update&flowControl=regis">
							<img  src="${request.contextPath}/common/images/modificar.gif" alt="Modificar" title="Modificar" border="0">
						</@security.a>									
						</div>
						<div class="alineacion">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0585" 
							enabled="tipoUnidad.eraseable"
							cssClass="item"  
							href="${request.contextPath}/maestro/tipoUnidad/deleteView.action?tipoUnidad.oid=${tipoUnidad.oid}">
							<img  src="${request.contextPath}/common/images/eliminar.gif" alt="Eliminar" title="Eliminar" border="0">
						</@security.a>									
						</div>
					<div class="alineacion">
					<#if tipoUnidad.estado == "Activo">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0586" 
							enabled="true"
							cssClass="item"  
							href="${request.contextPath}/maestro/tipoUnidad/desactivarView.action?tipoUnidad.oid=${tipoUnidad.oid?c}">
							<img src="${request.contextPath}/common/images/desactivar.gif" alt="Desactivar" title="Desactivar" border="0">
						</@security.a>			
					<#else>
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0586" 
							enabled="true"
							cssClass="item"  
							href="${request.contextPath}/maestro/tipoUnidad/activateView.action?tipoUnidad.oid=${tipoUnidad.oid?c}">
							<img src="${request.contextPath}/common/images/activar.gif" alt="Activar" title="Activar" border="0">
						</@security.a>								
					</#if>	
					</div>							
					</@display.column>
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="codigo" title="C&oacute;digo" />
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="descripcion" title="Descripci&oacute;n" />
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="estado" title="Estado" />					
					
				</@display.table>
			</@vc.anchors>
			</div>
		</@s.if>
					
<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/tipoUnidad/search.action" 
  source="buscar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="resetFlag=true,navigationId={navigationId},flowControl=regis,tipoUnidad.descripcion={tipoUnidad.descripcion},tipoUnidad.codigo={tipoUnidad.codigo},tipoUnidad.estado={tipoUnidad.estado}"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/tipoUnidad/view.action" 
  source="limpiar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=regis"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/tipoUnidad/createView.action" 
  source="crear" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=tipoUnidad-create,flowControl=regis"/>
