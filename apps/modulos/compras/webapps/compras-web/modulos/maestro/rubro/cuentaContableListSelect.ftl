
<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<@s.hidden id="cuentaContable.codigo" name="cuentaContable.codigo"/>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
        	<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Datos de la Cuenta contable</b></td>
			</tr>
		</table>
		
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
			<tr>
	      		<td class="textoCampo">Codigo del cuenta contable:</td>
	      		<td class="textoDato">
				     <@s.textfield 
	      				templateDir="custontemplates" 
						id="codigo" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="cuentaContable.codigo" 
						title="C&oacute;digo" />
				</td>
	      		<td class="textoCampo">Descripci&oacute;n</td>
	      		<td class="textoDato">
	      			<@s.textfield 
	      				templateDir="custontemplates" 
						id="descripcion" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="cuentaContable.descripcion" 
						title="Descripcion" />
				</td>
	    	</tr>				
			<tr>
	    		<td colspan="4" class="lineaGris" align="right">
	      			<input id="limpiar" type="button" name="btnVolver" value="Limpiar" class="boton"/>		
	      			<input id="buscar" type="button" name="btnVolver" value="Buscar" class="boton"/>
	    		</td>
			</tr>	
		</table>
		</div>	
			
		<@s.if test="cuentaContableList!=null">
		<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">	
			<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Cuentas Contables encontradas</td>
				</tr>
			</table>			
			<@vc.anchors target="contentTrx" ajaxFlag="ajax">
          		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="cuentaContableList" id="cuentaContableSelect" pagesize=15 defaultsort=2 partialList=true size="recordSize" keepStatus=true excludedParams="resetFlag">
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" style=" text-align:center; display:block" title="Acciones">
					<#assign ref="${request.contextPath}/maestro/rubro/selectCuentaContable.action?">											
						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" style=" text-align:center; display:block" title="Acciones">			
						<@s.a href="${ref}cuentaContable.codigo=${cuentaContableSelect.codigo?c}&cuentaContable.descripcion=${cuentaContableSelect.descripcion}&navegacionIdBack=${navegacionIdBack}&navigationId=${navigationId}">
						<img src="${request.contextPath}/common/images/seleccionar.gif" alt="Seleccionar" title="Seleccionar" width="16" height="16" border="0"></@s.a>
								&nbsp;			
						</@display.column>	
					</@display.column>
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="codigo" title="C&oacute;digo" />
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="descripcion" title="Descripci&oacute;n" />
				</@display.table>
			</@vc.anchors>
	</div>	
	</@s.if>
	
		<div id="capaBotonera" class="capaBotonera">
		<table id="tablaBotonera" class="tablaBotonera">
			<tr> 
				<td align="left">
					<input id="ok" type="button" name="btnVolver" value="Volver" class="boton"/>
				</td>
			</tr>	
		</table>
	</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="ok" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=${navegacionIdBack},flowControl=back"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/rubro/cuentaContableView.action" 
  source="limpiar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="rubro.oid={rubro.oid},navegacionIdBack=${navegacionIdBack},navigationId=${navigationId}"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/rubro/cuentaContableSearch.action" 
  source="buscar" 
  success="contentTrx" 
  failure="errorTrx" 
 parameters="resetFlag=true,cuentaContable.codigo={codigo},cuentaContable.descripcion={descripcion},rubro.oid={rubro.oid},navegacionIdBack=${navegacionIdBack},navigationId=${navigationId}"/>
