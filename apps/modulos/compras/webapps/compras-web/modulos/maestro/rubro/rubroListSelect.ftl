<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>

<@s.hidden id="navigationId" name="navigationId"/>
<@s.hidden id="navegacionIdBack" name="navegacionIdBack"/>
<@s.hidden id="estadoDuro" name="estadoDuro"/>
<@s.hidden id="imputableDuro" name="imputableDuro"/>
<@s.hidden id="fondoFijoDuro" name="fondoFijoDuro"/>
<@s.hidden id="usuario.oid" name="usuario.oid"/>
<@s.hidden id="filtraPorUsuario" name="filtraPorUsuario"/>


  <@s.if test="estadoDuro == true">
	<@s.hidden id="rubro.estado" name="rubro.estado.ordinal()"/>
  </@s.if>
  
    <@s.if test="imputableDuro == true">
	<@s.hidden id="rubro.imputable" name="rubro.imputable.ordinal()"/>
  </@s.if>
  
      <@s.if test="fondoFijoDuro == true">
	<@s.hidden id="rubro.fondoFijo" name="rubro.fondoFijo.ordinal()"/>
  </@s.if>


<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
        	<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	

	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Datos del Rubro</b></td>
			</tr>
		</table>
		
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
			<tr>
	      		<td class="textoCampo">C&oacute;digo:</td>
	      		<td class="textoDato">
				     <@s.textfield 
	      				templateDir="custontemplates" 
						id="codigo" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="rubro.codigo" 
						title="C&oacute;digo" />
				</td>
	      		<td class="textoCampo">Descripci&oacute;n:</td>
	      		<td class="textoDato">
	      			<@s.textfield 
	      				templateDir="custontemplates" 
						id="descripcion" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="rubro.descripcion" 
						title="Descripcion" />
				</td>
	    	</tr>
	    	<tr>
	      		<td class="textoCampo">Es imputable:</td>
	      		<td class="textoDato">
					<@s.select 
						templateDir="custontemplates" 
						id="rubro.imputable" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="rubro.imputable" 
						list="imputableList" 
						listKey="key" 
						listValue="description" 
						value="rubro.imputable.ordinal()"
						title="Es Imputable"
						headerKey="0"
						headerValue="Todos"  
						 />
				</td>
				<td class="textoCampo">Utilizable con Fondo Fijo:</td>
	      			<td class="textoDato">
				      <@s.select 
						templateDir="custontemplates" 
						id="cliente.fondoFijo" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="rubro.fondoFijo" 
						list="fondoFijoList" 
						listKey="key" 
						listValue="description" 
						value="rubro.fondoFijo.ordinal()"
						title="Utilizable con Fondo Fijo"
						headerKey="0"
						headerValue="Todos"  
						 />
					</td>
				</tr>
				<tr>					
	      			<td class="textoCampo">Rubro:</td>
	      			<td class="textoDato">
				     <@s.select 
						templateDir="custontemplates" 
						id="rubroNivel0" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="rubroNivel0.oid" 
						list="rubroNivel0List" 
						listKey="oid" 
						listValue="descripcion" 
						value="rubroNivel0.oid"
						title="Rubro nivel 0"
						 /> 
					</td>
	    			<td class="textoCampo">Rubro nivel 1:</td>
					<td class="textoDato">
				     	<@s.select 
							templateDir="custontemplates" 
							id="rubroNivel1" 
							cssClass="textarea"
							cssStyle="width:165px" 
							name="rubroNivel1.oid" 
							list="rubroNivel1List" 
							listKey="oid" 
							listValue="descripcion" 
							title="Rubro nivel 1"
						 	/> 
					</td>
				</tr>
				<tr>
					<td class="textoCampo">Rubro nivel 2:</td>
					<td class="textoDato">
				     <@s.select 
						templateDir="custontemplates" 
						id="rubroNivel2" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="rubroNivel2.oid" 
						list="rubroNivel2List" 
						listKey="oid" 
						listValue="descripcion" 
						title="Rubro nivel 2"
					 /> 
					</td>
					<td class="textoCampo">Rubro nivel 3:</td>
					<td class="textoDato">
				     <@s.select 
						templateDir="custontemplates" 
						id="rubroNivel3" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="rubroNivel3.oid" 
						list="rubroNivel3List" 
						listKey="oid" 
						listValue="descripcion" 
						title="Rubro nivel 3"
					 /> 
					</td>
				</tr>
				<tr>						 
					<td class="textoCampo">Estado:</td>
	      			<td class="textoDato" colspan="3">
	      				<@s.select 
								templateDir="custontemplates" 
								id="rubro.estado" 
								cssClass="textarea"
								cssStyle="width:165px" 
								name="rubro.estado" 
								list="estadoList" 
								listKey="key" 
								listValue="description" 
								value="rubro.estado.ordinal()"
								title="Estado"
								headerKey="0"
								headerValue="Todos"							
								/>
					</td>
				</tr>
				<tr>
	    		<td colspan="4" class="lineaGris" align="right">
	      			<input id="limpiar" type="button" name="btnVolver" value="Limpiar" class="boton"/>		
	      			<input id="buscar" type="button" name="btnVolver" value="Buscar" class="boton"/>
	    		</td>
			</tr>
		</table>
	  </div>  	
				
	  <@s.if test="rubroList!=null">									
			<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">			
			<#assign rubroCodigo="${rubro.codigo}" />
			<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Rubros encontrados</td>
				</tr>	
			</table>
			
			<@vc.anchors target="contentTrx">	
					<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="rubroList" id="rubroSelect" pagesize=15 partialList=true size="recordSize" keepStatus=true excludedParams="resetFlag">
					<@s.if test="namespace!=''">
						<#assign ref="${request.contextPath}/maestro/rubro/selectRubro.action?">
						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" style=" text-align:center; display:block" title="Acciones">
							<@s.a href="${ref}rubro.oid=${rubroSelect.oid?c}&rubro.descripcion=${rubroSelect.descripcion}&navegacionIdBack=${navegacionIdBack}&navigationId=${navigationId}&namespace=${namespace}&actionName=${actionName}">
								<img src="${request.contextPath}/common/images/seleccionar.gif" alt="Seleccionar" title="Seleccionar" width="16" height="16" border="0">
							</@s.a>
							
						</@display.column>						  															
					</@s.if>
					<@s.else>
						<#assign ref="${request.contextPath}/maestro/rubro/selectBackRubro.action?">
						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" style=" text-align:center; display:block" title="Acciones">
							<@s.a href="${ref}rubro.oid=${rubroSelect.oid?c}&rubro.descripcion=${rubroSelect.descripcion}&navegacionIdBack=${navegacionIdBack}&navigationId=${navigationId}">
								<img src="${request.contextPath}/common/images/seleccionar.gif" alt="Seleccionar" title="Seleccionar" width="16" height="16" border="0">
							</@s.a>
							
						</@display.column>
					</@s.else>
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="codigo" title="C&oacute;digo" />
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="descripcion" title="Descripci&oacute;n" />
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="imputable" title="Imputable" />
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="cuentaContableListAsString" title="Cuenta Contable" />

					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="estado" title="Estado" />					
					
				</@display.table>
			</@vc.anchors>
			</div>
			</@s.if>
			
			<div id="capaBotonera" class="capaBotonera">
				<table id="tablaBotonera" class="tablaBotonera">
					<tr> 
						<td align="left">
							<input id="cancel" type="button" name="btnVolver" value="Volver" class="boton"/>
						</td>
					</tr>	
				</table>
			</div>
  

<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/rubro/selectSearchRubro.action" 
  source="buscar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="resetFlag=true,navigationId={navigationId},navegacionIdBack={navegacionIdBack},flowControl=regis,namespace=${namespace},actionName=${actionName},rubro.descripcion={descripcion},rubro.codigo={codigo},rubroNivel0.oid={rubroNivel0},rubroNivel1.oid={rubroNivel1},rubroNivel2.oid={rubroNivel2},rubroNivel3.oid={rubroNivel3},rubro.estado={rubro.estado},rubro.estado={rubroEstado},rubro.imputable={rubro.imputable},estadoDuro={estadoDuro},imputableDuro={imputableDuro},rubro.fondoFijo={rubro.fondoFijo},fondoFijoDuro={fondoFijoDuro},rubroPadreDuro={rubroPadreDuro},filtraPorUsuario={filtraPorUsuario},usuario.oid={usuario.oid}"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/rubro/selectViewRubro.action" 
  source="limpiar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},navegacionIdBack={navegacionIdBack},flowControl=regis,namespace=${namespace},actionName=${actionName},estadoDuro={estadoDuro},imputableDuro={imputableDuro},rubro.fondoFijo={rubro.fondoFijo},fondoFijoDuro={fondoFijoDuro},rubro.imputable={rubro.imputable},rubro.estado={rubro.estado}"/>
 
   
 <@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navegacionIdBack},flowControl=back"/>
 

<@ajax.select
  baseUrl="${request.contextPath}/maestro/rubro/refrescarSubrubro1.action" 
  source="rubroNivel0" 
  target="rubroNivel1"
  parameters="rubroNivel0.oid={rubroNivel0},navegacionIdBack={navegacionIdBack}"
  parser="new ResponseXmlParser()"/>

<@ajax.select
  baseUrl="${request.contextPath}/maestro/rubro/refrescarSubrubro2.action" 
  source="rubroNivel1" 
  target="rubroNivel2"
  parameters="rubroNivel1.oid={rubroNivel1},navegacionIdBack={navegacionIdBack}"
  parser="new ResponseXmlParser()"/>
  
<@ajax.select
  baseUrl="${request.contextPath}/maestro/rubro/refrescarSubrubro3.action" 
  source="rubroNivel2" 
  target="rubroNivel3"
  parameters="rubroNivel2.oid={rubroNivel2},navegacionIdBack={navegacionIdBack}"
  parser="new ResponseXmlParser()"/>