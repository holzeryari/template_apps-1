<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>


<@s.if test="rubroList!=null">									
	
	<#assign rubroCodigo="${rubro.codigo}" />
	<#assign rubroDescripcion="${rubro.descripcion}" />
	<#assign rubroImputable="${rubro.imputable.ordinal()}" />

	<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">	
		<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Rubros encontrados</td>
					<td>
						<div class="alineacionDerecha">
								<@security.a 
									templateDir="custontemplates" 
									securityCode="CUF0047" 
									name="CUF0047"
									cssClass="item" 
									href="${request.contextPath}/maestro/rubro/printXLS.action?rubro.descripcion=${rubroDescripcion}&rubro.codigo=${rubroCodigo}&rubro.imputable=${rubroImputable}&rubroNivel0.oid=${rubroNivel0Oid}&rubroNivel1.oid=${rubroNivel1Oid}&rubroNivel2.oid=${rubroNivel2Oid}&rubroNivel3.oid=${rubroNivel3Oid}&rubro.estado=${rubroEstado}">
									<img src="${request.contextPath}/common/images/imprimirExcel.gif" title="Exportar a Excel" align="absmiddle" border="0" hspace="3" >
								</@security.a>
						</div>
						<div class="alineacionDerecha">
							<b>Imprimir</b>
								<@security.a 
									templateDir="custontemplates" 
									securityCode="CUF0047" 
									name="CUF0047"
									cssClass="item" 
									href="${request.contextPath}/maestro/rubro/printPDF.action?rubro.descripcion=${rubroDescripcion}&rubro.codigo=${rubroCodigo}&rubro.imputable=${rubroImputable}&rubroNivel0.oid=${rubroNivel0Oid}&rubroNivel1.oid=${rubroNivel1Oid}&rubroNivel2.oid=${rubroNivel2Oid}&rubroNivel3.oid=${rubroNivel3Oid}&rubro.estado=${rubroEstado}">
									<img src="${request.contextPath}/common/images/imprimirPdf.gif" title="Exportar a PDF" align="absmiddle" border="0" hspace="3" >
								</@security.a>
							</div>										
					</td>
				</tr>	
			</table>


	<@ajax.anchors target="contentTrx">	
		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="rubroList" id="rubro" pagesize=15 defaultsort=2 >
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" class="botoneraAnchoCon4" title="Acciones">
			
			<div class="alineacion">
			<@security.a 
				templateDir="custontemplates" 
				securityCode="CUF0042" 
				name="CUF0042"
				enabled="rubro.readable" 
				cssClass="item" 
				href="${request.contextPath}/maestro/rubro/readView.action?rubro.oid=${rubro.oid?c}&rubro.versionNumber=${rubro.versionNumber}&navigationId=ver-rubro">
				<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver" border="0">
			</@security.a>
			</div>
			<div class="alineacion">
			<@security.a 
				templateDir="custontemplates" 
				securityCode="CUF0043" 
				name="CUF0043"
				enabled="rubro.updatable" 
				cssClass="item" 
				href="${request.contextPath}/maestro/rubro/administrarView.action?rubro.oid=${rubro.oid?c}&rubro.versionNumber=${rubro.versionNumber}&navigationId=administrar&flowControl=regis">
				<img  src="${request.contextPath}/common/images/administrar.gif" alt="Administrar" title="Administrar"  border="0">
			</@security.a>
			</div>
			<div class="alineacion">
			<@security.a 
				templateDir="custontemplates" 
				securityCode="CUF0044" 
				name="CUF0044"
				enabled="rubro.eraseable" 
				cssClass="item" 
				href="${request.contextPath}/maestro/rubro/deleteView.action?rubro.oid=${rubro.oid?c}&rubro.versionNumber=${rubro.versionNumber?c}&navigationId=eliminar-rubro">
				<img  src="${request.contextPath}/common/images/eliminar.gif" alt="Eliminar" title="Eliminar" border="0">
			</@security.a>
			</div>
			<div class="alineacion">
			<#if rubro.estado == "Activo">
				<#assign imagen="desactivar.gif">
				<#assign alternativa="Desactivar">
				<#assign titulo="Desactivar">
				<#assign link="${request.contextPath}/maestro/rubro/activateView.action?rubro.oid=${rubro.oid?c}">
			<#else>
				<#assign imagen="activar.gif">
				<#assign alternativa="Activar">
				<#assign titulo="Activar">
				<#assign link="${request.contextPath}/maestro/rubro/activateView.action?rubro.oid=${rubro.oid?c}">
			</#if>
			<#if rubro.estado == "En preparacion" >
				<#assign imagen="activargris.gif">
				<#assign alternativa="Activar">
				<#assign titulo="Activar">
				<#assign link="">
			</#if>
			<#if (rubro.codigo?length > 1) >
				<#assign imagen="activargris.gif">
				<#assign alternativa="Activar">
				<#assign titulo="Activar">
				<#assign link="">
			</#if>
			
			
			<@security.a 
				templateDir="custontemplates" 
				securityCode="CUF0045" 
				name="CUF0045"
				enabled="true" 
				cssClass="item" 
				href="${link}"><img src="${request.contextPath}/common/images/${imagen}" alt="${alternativa}" title="${titulo}" border="0">
			</@security.a>
			</div>
		</@display.column>
		
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="codigo" title="C&oacute;digo" />
		
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="descripcion" title="Descripci&oacute;n" />
		
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="imputable" title="Imputable" />
		
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="cuentaContableListAsString" title="Cuenta Contable" />
		
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="estado" title="Estado" />					
		
	</@display.table>
</@ajax.anchors>
</div>
</@s.if>

