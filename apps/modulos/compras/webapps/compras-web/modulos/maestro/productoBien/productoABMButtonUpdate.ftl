<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>


<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancel" type="button" name="btnVolver" value="Cancelar" class="boton"/>
				<input id="update" type="button" name="btModificar" value="Modificar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/productoBien/update.action" 
  source="update" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="productoBien.estado={productoBien.estado},productoBien.oid={productoBien.oid},productoBien.rubro.oid={productoBien.rubro.oid},productoBien.descripcion={productoBien.descripcion},productoBien.tipo={productoBien.tipoProductoBien},productoBien.marca={productoBien.marca},productoBien.modelo={productoBien.modelo},productoBien.critico={productoBien.critico},productoBien.codigoBarra={productoBien.codigoBarra},producto.reposicionAutomatica={producto.reposicionAutomatica},producto.existenciaMinima={producto.existenciaMinima},bien.amortizacion={bien.amortizacion},bien.registrable={bien.registrable},bien.vehiculo={bien.vehiculo},productoBien.valorMercado={productoBien.valorMercado},productoBien.fechaValorMercado={productoBien.fechaValorMercado},productoBien.versionNumber={productoBien.versionNumber},producto.tipoUnidad.oid={producto.tipoUnidad.oid}"/>
  
  <@vc.htmlContent 
baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=producto-administrar,flowControl=back"/>
  
      
<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/productoBien/selectRubroCreateUpdate.action" 
  source="seleccionarRubro" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId=producto-update,flowControl=change,navegacionIdBack=producto-update,productoBien.estado={productoBien.estadoProductoBien},productoBien.oid={productoBien.oid},productoBien.rubro.oid={productoBien.rubro.oid},productoBien.rubro.descripcion={productoBien.rubro.descripcion},productoBien.descripcion={productoBien.descripcion},productoBien.tipo={productoBien.tipoProductoBien},productoBien.marca={productoBien.marca},productoBien.modelo={productoBien.modelo},productoBien.critico={productoBien.critico},productoBien.codigoBarra={productoBien.codigoBarra},producto.reposicionAutomatica={producto.reposicionAutomatica},producto.existenciaMinima={producto.existenciaMinima},bien.amortizacion={bien.amortizacion},bien.registrable={bien.registrable},bien.vehiculo={bien.vehiculo},productoBien.valorMercado={productoBien.valorMercado},productoBien.fechaValorMercado={productoBien.fechaValorMercado},producto.tipoUnidad.oid={producto.tipoUnidad.oid}"/>
  