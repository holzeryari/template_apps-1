<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<@s.hidden id="bien.rubro.oid" name="bien.rubro.oid"/>
<@s.hidden id="bien.rubro.descripcion" name="bien.rubro.descripcion"/>
<@s.hidden id="estadoDuro" name="estadoDuro"/>

<@s.if test="estadoDuro == true">
	<@s.hidden id="bien.estado" name="bien.estado.ordinal()"/>
</@s.if>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
        	<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Datos del Bien</b></td>
			</tr>		
		</table>				
		
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
			<tr>
			  	<td class="textoCampo">C&oacute;digo:</td>
			    <td class="textoDato">
			      	<@s.textfield 
						templateDir="custontemplates" 
						id="bien.oid"
						cssClass="textarea"
						cssStyle="width:160px" 
						name="bien.oid" 
						title="Codigo" />
				</td>
				<td class="textoCampo">Descripci&oacute;n:</td>
				<td class="textoDato">
					<@s.textfield 
				    	templateDir="custontemplates" 
						id="bien.descripcion"
						cssClass="textarea"
						cssStyle="width:160px" 
						name="bien.descripcion" 
						title="Descripcion"/>
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Rubro:</td>
      			<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="bien.rubro.descripcion"/>
					<@s.a templateDir="custontemplates" id="seleccionarRubro" name="seleccionarRubro" href="javascript://nop/">
						<img src="${request.contextPath}/common/images/buscar.gif"  border="0" align="absmiddle" hspace="3" >
					</@s.a>		
				</td>
				<td class="textoCampo">Estado:</td>
      			<td class="textoDato" align="left" colspan="3">
      				<@s.select 
							templateDir="custontemplates" 
							id="bien.estado" 
							cssClass="textarea"
							cssStyle="width:160px" 
							name="bien.estado" 
							list="estadoList" 
							listKey="key" 
							listValue="description" 
							value="bien.estado.ordinal()"
							title="Estado"
							headerKey="0"
							headerValue="Todos"							
							/>
				</td>							
			</tr>							
			<tr>
	    		<td colspan="4" class="lineaGris" align="right">
	      			<input id="limpiar" type="button" name="btnVolver" value="Limpiar" class="boton"/>		
	      			<input id="buscar" type="button" name="btnVolver" value="Buscar" class="boton"/>
	    		</td>
			</tr>	
		</table>
	</div>

	<!-- Resultado Filtro -->
	<@s.if test="productoBienList!=null">
		<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
		<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Bienes encontrados</td>
				</tr>
		</table>
		<@ajax.anchors target="contentTrx">
			<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="productoBienList" id="bienSelect" pagesize=15 defaultsort=1 partialList=true size="recordSize" keepStatus=true excludedParams="resetFlag">
			
				<@s.if test="nameSpaceSelect!=''">								
					<#assign ref="${request.contextPath}/maestro/productoBien/selectBien.action?">										
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" style=" text-align:center; display:block" title="Acciones">			
							<@s.a href="${ref}bien.oid=${bienSelect.oid?c}&navegacionIdBack=${navegacionIdBack}&navigationId=${navigationId}&nameSpaceSelect=${nameSpaceSelect}&actionNameSelect=${actionNameSelect}&oidParameter=${oidParameter?c}"><img src="${request.contextPath}/common/images/seleccionar.gif" alt="Seleccionar" title="Seleccionar" width="16" height="16" border="0"></@s.a>
								
					</@display.column>								  															
				</@s.if>
				
				<@s.else>
					<#assign ref="${request.contextPath}/maestro/productoBien/selectBienBack.action?">											
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" style=" text-align:center; display:block" title="Acciones">			
						<@s.a href="${ref}bien.oid=${bienSelect.oid?c}&bien.descripcion=${bienSelect.descripcion}&navegacionIdBack=${navegacionIdBack}&navigationId=${navigationId}"><img src="${request.contextPath}/common/images/seleccionar.gif" alt="Seleccionar" title="Seleccionar" width="16" height="16" border="0"></@s.a>
							
					</@display.column>	
					
				</@s.else>
				
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="oid" title="C&oacute;digo" />
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="descripcion" title="Descripci&oacute;n" />
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="rubro.descripcion" title="Rubro" />								
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="estado" title="Estado" />
				
			
			</@display.table>								
		</@ajax.anchors>
		</div>	
	</@s.if>
					
	<div id="capaBotonera" class="capaBotonera">
		<table id="tablaBotonera" class="tablaBotonera">
			<tr> 
				<td align="left">
					<input id="cancel" type="button" name="btnVolver" value="Volver" class="boton"/>
				</td>
			</tr>	
		</table>
	</div>  


<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/productoBien/selectBienSearch.action" 
  source="buscar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="resetFlag=true,navegacionIdBack=${navegacionIdBack},navigationId=${navigationId},flowControl=regis,bien.oid={bien.oid},bien.descripcion={bien.descripcion},bien.rubro.oid={bien.rubro.oid},bien.rubro.descripcion={bien.rubro.descripcion},nameSpaceSelect=${nameSpaceSelect},actionNameSelect=${actionNameSelect},oidParameter=${oidParameter?c},bien.estado={bien.estado},estadoDuro={estadoDuro}"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/productoBien/selectBienView.action"
  source="limpiar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navegacionIdBack=${navegacionIdBack},navigationId=${navigationId},flowControl=regis,nameSpaceSelect=${nameSpaceSelect},actionNameSelect=${actionNameSelect},oidParameter=${oidParameter?c},estadoDuro={estadoDuro},bien.estado={bien.estado}"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/productoBien/selectRubroSearch.action" 
  source="seleccionarRubro" 
  success="contentTrx" 
  failure="errorTrx"   
  parameters="navigationId=${navigationId},flowControl=change,navegacionIdBack=${navegacionIdBack},nameSpaceSelect=${nameSpaceSelect},actionNameSelect=${actionNameSelect},oidParameter=${oidParameter?c},bien.oid={bien.oid},bien.descripcion={bien.descripcion},bien.rubro.oid={bien.rubro.oid},bien.rubro.descripcion={bien.rubro.descripcion},bien.estado={bien.estado},estadoDuro={estadoDuro}"/>
  
    
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId=${navegacionIdBack},flowControl=back"/>
 