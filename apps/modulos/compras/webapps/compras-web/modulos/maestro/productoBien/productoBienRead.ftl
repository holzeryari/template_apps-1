
<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
	<tr>
		<td class="textoCampo" colspan="4">&nbsp;</td>
	</tr>	
	<tr>	
  		<td class="textoCampo">C&oacute;digo:</td>
  		<td class="textoDato">
  		<@s.property default="&nbsp;" escape=false value="productoBien.oid"/>
  		</td>
  		<td class="textoCampo">Tipo:</td>
  		<td class="textoDato">
  		<@s.property default="&nbsp;" escape=false value="productoBien.tipo"/>						      			
  		</td>		      		
	</tr>	
	<tr>
  		<td  class="textoCampo">Descripci&oacute;n: </td>
		<td class="textoDato">
		<@s.property default="&nbsp;" escape=false value="productoBien.descripcion"/>											
		</td>	      			
								
		<td class="textoCampo">Rubro:</td>
  		<td class="textoDato">
		<@s.property default="&nbsp;" escape=false value="productoBien.rubro.descripcion"/>				
		</td>
	</tr>
	<tr>
		<td class="textoCampo">Marca: </td>
		<td  class="textoDato">
		<@s.property default="&nbsp;" escape=false value="productoBien.marca"/>
		</td>
		<td class="textoCampo">Modelo: </td>
		<td  class="textoDato">
		<@s.property default="&nbsp;" escape=false value="productoBien.modelo"/>										
		</td>
	</tr>
	<tr>
		<td class="textoCampo">Valor Mercado: </td>
		<td  class="textoDato">
		<@s.property default="&nbsp;" escape=false value="productoBien.valorMercado"/>										
		</td>
		<td class="textoCampo">Fecha Valor Mercado: </td>
		<td  class="textoDato">
		<@s.property default="&nbsp;" escape=false value="productoBien.fechaValorMercado"/>										
		</td>
	</tr>
	<tr>
		<td class="textoCampo">C&oacute;digo de Barra: </td>
		<td  class="textoDato">
		<@s.property default="&nbsp;" escape=false value="productoBien.codigoBarra"/>										
		</td>
		<td class="textoCampo">Es Cr&iacute;tico: </td>
		<td  class="textoDato">
		<@s.property default="&nbsp;" escape=false value="productoBien.critico"/>						      		
		</td>
	</tr>
	<tr>
		<td class="textoCampo">Reposici&oacute;n Autm&aacute;tica: </td>
		<td  class="textoDato">
		<@s.property default="&nbsp;" escape=false value="productoBien.reposicionAutomatica"/>						      														
		</td>
		<td class="textoCampo">Existencia M&iacute;nima: </td>
		<td  class="textoDato">
		<@s.property default="&nbsp;" escape=false value="productoBien.existenciaMinima"/>
		</td>
	</tr>
	<tr>
		<td class="textoCampo">Tipo Unidad:</td>
		<td class="textoDato" colspan="3" >		      						
		<@s.property default="&nbsp;" escape=false value="productoBien.tipoUnidad.descripcion"/>										 
		</td>	
	</tr>
	<tr>
		<td class="textoCampo">Es Registrable: </td>
		<td  class="textoDato" colspan="3">
		<@s.property default="&nbsp;" escape=false value="bien.registrable"/>						      			
		</td>
	</tr>
	<tr>
		<td class="textoCampo">Es Veh&iacute;culo: </td>
		<td  class="textoDato">
		<@s.property default="&nbsp;" escape=false value="bien.vehiculo"/>
  		</td>
		<td class="textoCampo">Amortizaci&oacute;n (%): </td>
		<td  class="textoDato">
		<@s.property default="&nbsp;" escape=false value="bien.amortizacion"/>										
		</td>
	</tr>
	<tr>
		<td class="textoCampo">Estado: </td>
  		<td class="textoDato">
  			<@s.property default="&nbsp;" escape=false value="productoBien.estado"/></td>
		<td class="textoCampo">&nbsp;</td>
		<td  class="textoDato">&nbsp;</td>
	</tr>
</table>		