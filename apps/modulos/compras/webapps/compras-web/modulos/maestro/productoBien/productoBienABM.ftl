<#setting locale="es_AR">

<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>

<@s.hidden id="productoBien.oid" name="productoBien.oid"/>
<@s.hidden id="productoBien.versionNumber" name="productoBien.versionNumber"/>

<@s.hidden id="productoBien.tipoProductoBien" name="productoBien.tipo.ordinal()"/>
<@s.hidden id="productoBien.estadoProductoBien" name="productoBien.estado.ordinal()"/>

<@s.hidden id="productoBien.rubro.oid" name="productoBien.rubro.oid"/>
<@s.hidden id="productoBien.rubro.descripcion" name="productoBien.rubro.descripcion"/>

	<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
			<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos del Producto/Bien</b></td>
				<td>
					<div align="right">
						<@s.a templateDir="custontemplates" id="modificarProductoBien" name="modificarProductoBien" href="javascript://nop/" cssClass="ocultarIcono">
						<b>Modificar</b><img src="${request.contextPath}/common/images/modificar.gif" border="0" align="absmiddle" hspace="3" >
						</@s.a>
					</div>
				</td>
			</tr>
		</table>
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>							
			<tr>
				<#assign productoEnable="true">
				<#assign bienEnable="true">
				<#assign existenciaEnable="true">
				<#assign tipoUnidadEnable="true">
				<#assign precioEnable="true">
			
				<#assign productoClass="textareagris">
				<#assign bienClass="textareagris">
				<#assign bienClass2="textareagris">
				<#assign existenciaClass="textareagris">
				<#assign tipoUnidadClass="textareagris">
				<#assign precioClass="textareagris">
									
				<@s.if test="productoBien.tipo.ordinal() == 1">
					<#assign productoEnable="false">
					<#assign productoClass="textarea">
					<#assign bienEnable="true">
					<#assign tipoUnidadClass="textarea">
					<#assign tipoUnidadEnable="false">
				</@s.if>
				<@s.if test="productoBien.tipo.ordinal() == 2">
					<#assign productoEnable="true">
					<#assign bienEnable="false">
					<#assign bienClass="textarea">
					<#assign bienClass2="textarea">
				</@s.if>
				<@s.if test="producto.reposicionAutomatica.ordinal() == 2">
					<#assign existenciaEnable="false">
					<#assign existenciaClass="textarea">
				</@s.if>
				
				<@s.if test="bien.vehiculo.ordinal() == 2">
					<#assign precioEnable="false">
					<#assign precioClass="textarea">
				</@s.if>
					
				<td class="textoCampo">C&oacute;digo:</td>
				<td class="textoDato"><@s.property default="&nbsp;" escape=false value="productoBien.oid"/></td>
				<td class="textoCampo">Tipo:</td>
				<td class="textoDato">
					<@s.select 
						templateDir="custontemplates" 
						id="productoBien.tipo" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="productoBien.tipo" 
						list="tipoList" 
						listKey="key" 
						listValue="description" 
						value="productoBien.tipo.ordinal()"
						title="Tipo de Producto/Bien"
						onchange="javascript:productoBienSelect(this);" 
						headerKey="0"
						headerValue="Seleccionar"  
						/>
				</td>		      		
			</tr>	
			<tr>
				<td  class="textoCampo">Descripci&oacute;n: </td>
				<td class="textoDato">
					<@s.textfield 
						templateDir="custontemplates" 
						id="productoBien.descripcion" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="productoBien.descripcion" 
						title="Descripci&oacute;n"
						maxLength="100" 
						/>					
				</td>	      			
				<td class="textoCampo">Rubro:</td>
				<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="productoBien.rubro.descripcion"/>
					<@s.a templateDir="custontemplates" id="seleccionarRubro" name="seleccionarRubro" href="javascript://nop/" cssClass="ocultarIcono">
						<img src="${request.contextPath}/common/images/buscar.gif" border="0" align="absmiddle" hspace="3" >
					</@s.a>		
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Marca: </td>
				<td  class="textoDato">
					<@s.textfield 
						templateDir="custontemplates" 
						id="productoBien.marca" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="productoBien.marca" 
						title="Marca" />
				</td>
				<td class="textoCampo">Modelo: </td>
				<td  class="textoDato">
					<@s.textfield 
						templateDir="custontemplates" 
						id="productoBien.modelo" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="productoBien.modelo" 
						title="Modelo" />
				</td>
    		</tr>
			<tr>
				<td class="textoCampo">Valor Mercado: </td>
				<td  class="textoDato">
					<@s.textfield 
						templateDir="custontemplates" 
						id="productoBien.valorMercado" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="productoBien.valorMercado" 
						title="Valor Mercado" />
				</td>
				<td class="textoCampo">Fecha Valor Mercado: </td>
				<td  class="textoDato">
					<@vc.rowCalendar 
						templateDir="custontemplates" 
						id="productoBien.fechaValorMercado" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="productoBien.fechaValorMercado" 
						title="Fecha Valor Mercado" />
				</td>
    		</tr>
			<tr>
				<td class="textoCampo">C&oacute;digo de Barra: </td>
				<td  class="textoDato">
					<@s.textfield 
						templateDir="custontemplates" 
						id="productoBien.codigoBarra" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="productoBien.codigoBarra" 
						title="Codigo de Barra" />
				</td>
				<td class="textoCampo">Es Cr&iacute;tico: </td>
				<td  class="textoDato">
	      			<@s.select 
						templateDir="custontemplates" 
						id="productoBien.critico" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="productoBien.critico" 
						list="criticoList" 
						listKey="key" 
						listValue="description" 
						value="productoBien.critico.ordinal()"
						title="Es Critico"
						headerKey="0"
						headerValue="Seleccionar" 
						 />
				</td>
    		</tr>
			<tr>
				<td class="textoCampo">Reposici&oacute;n Autm&aacute;tica: </td>
				<td  class="textoDato">
	      			<@s.select 
						templateDir="custontemplates" 
						id="producto.reposicionAutomatica" 
						cssClass="${productoClass}"
						cssStyle="width:165px" 
						name="producto.reposicionAutomatica" 
						list="reposicionAutomaticaList" 
						listKey="key" 
						listValue="description" 
						value="producto.reposicionAutomatica.ordinal()"
						title="Reposicion Automatica"
						headerKey="0"
						headerValue="Seleccionar"											
						disabled="${productoEnable}"
						onchange="javascript:productoReposicionAutomaticaSelect(this);" 
						 />
						
				</td>
				<td class="textoCampo">Existencia M&iacute;nima: </td>
				<td  class="textoDato">
					<@s.textfield 
						templateDir="custontemplates" 
						id="producto.existenciaMinima" 
						cssClass="${existenciaClass}"
						cssStyle="width:160px" 
						name="producto.existenciaMinima" 
						title="Existencia Minima"
						disabled="${existenciaEnable}" 
						/>
				</td>
    		</tr>
    		<tr>
    		
  			<td class="textoCampo">Tipo Unidad:</td>
      			<td class="textoDato" colspan="3" >		      						
					<@s.select 
						templateDir="custontemplates" 
						id="producto.tipoUnidad.oid" 
						cssClass="${tipoUnidadClass}"
						disabled="${tipoUnidadEnable}" 
						cssStyle="width:165px" 
						name="producto.tipoUnidad.oid" 
						list="tipoUnidadList" 
						listKey="oid" 
						listValue="codigo" 
						value="oid"
						title="Deposito"
						headerKey="0" 
                        headerValue="Seleccionar"
                        value="producto.tipoUnidad.oid"/>
            	</td>	
    		</tr>
			<tr>
				<td class="textoCampo">Es Registrable: </td>
				<td  class="textoDato" colspan="3">
	      			<@s.select 
						templateDir="custontemplates" 
						id="bien.registrable" 
						cssClass="${bienClass}"
						cssStyle="width:165px" 
						name="bien.registrable" 
						list="registrableList" 
						listKey="key" 
						listValue="description" 
						value="bien.registrable.ordinal()"
						title="Es Registrable"
						headerKey="0"
						headerValue="Seleccionar"  
						disabled="${bienEnable}"
						/>
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Es Veh&iacute;culo: </td>
				<td  class="textoDato">
	      			<@s.select 
						templateDir="custontemplates" 
						id="bien.vehiculo" 
						cssClass="${bienClass}"
						cssStyle="width:165px" 
						name="bien.vehiculo" 
						list="vehiculoList" 
						listKey="key" 
						listValue="description" 
						value="bien.vehiculo.ordinal()"
						title="Es Vehiculo"
						headerKey="0"
						headerValue="Seleccionar"
						disabled="${bienEnable}"/>
				</td>
				<td class="textoCampo">Amortizaci&oacute;n (%): </td>
				<td  class="textoDato">
					<@s.textfield 
						templateDir="custontemplates" 
						id="bien.amortizacion" 
						cssClass="${bienClass2}"
						cssStyle="width:160px" 
						name="bien.amortizacion" 
						title="Amortizacion"
						disabled="${bienEnable}" 
						/>
				</td>
    		</tr>
			<tr>
				<td class="textoCampo">Estado: </td>
      			<td class="textoDato" colspan="3"><@s.property default="&nbsp;" escape=false value="productoBien.estado"/></td>
				
    		</tr>
    		<tr><td colspan="4">&nbsp;</td></tr>
		</table>
	</div>
	<@tiles.insertAttribute name="detalle"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/productoBien/updateView.action" 
  source="modificarProductoBien" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=producto-update,flowControl=regis,productoBien.oid={productoBien.oid},productoBien.versionNumber={productoBien.versionNumber},productoBien.tipo={productoBien.tipoProductoBien}"/>
	