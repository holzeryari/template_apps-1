<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancel" type="button" name="btnVolver" value="Cancelar" class="boton"/>
				<input id="store" type="button" name="btnAgregar" value="Agregar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/productoBien/create.action" 
  source="store" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="producto.tipoUnidad.oid={producto.tipoUnidad.oid},productoBien.descripcion={productoBien.descripcion},productoBien.tipo={productoBien.tipo},productoBien.marca={productoBien.marca},productoBien.modelo={productoBien.modelo},productoBien.critico={productoBien.critico},productoBien.codigoBarra={productoBien.codigoBarra},productoBien.rubro.oid={productoBien.rubro.oid},producto.reposicionAutomatica={producto.reposicionAutomatica},producto.existenciaMinima={producto.existenciaMinima},bien.amortizacion={bien.amortizacion},bien.registrable={bien.registrable},bien.vehiculo={bien.vehiculo},productoBien.valorMercado={productoBien.valorMercado},productoBien.fechaValorMercado={productoBien.fechaValorMercado}"/>

  <@vc.htmlContent 
baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=buscar-productosBienes,flowControl=back"/>
  
    
<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/productoBien/selectRubroCreateUpdate.action" 
  source="seleccionarRubro" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId=productoBien-create,flowControl=change,navegacionIdBack=productoBien-create,productoBien.rubro.oid={productoBien.rubro.oid},productoBien.rubro.descripcion={productoBien.rubro.descripcion},productoBien.descripcion={productoBien.descripcion},productoBien.tipo={productoBien.tipo},productoBien.marca={productoBien.marca},productoBien.modelo={productoBien.modelo},productoBien.critico={productoBien.critico},productoBien.codigoBarra={productoBien.codigoBarra},producto.reposicionAutomatica={producto.reposicionAutomatica},producto.existenciaMinima={producto.existenciaMinima},bien.amortizacion={bien.amortizacion},bien.registrable={bien.registrable},bien.vehiculo={bien.vehiculo},productoBien.valorMercado={productoBien.valorMercado},productoBien.fechaValorMercado={productoBien.fechaValorMercado},productoBien.estado={productoBien.estadoProductoBien},producto.tipoUnidad.oid={producto.tipoUnidad.oid}"/>
  