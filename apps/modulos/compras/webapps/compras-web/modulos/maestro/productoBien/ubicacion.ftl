<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<@s.hidden id="tipo" name="deposito.tipoOrganizacion.ordinal()"/>
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
	<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>	
			<tr>
				<td class="textoCampo">Tipo de Organizaci&oacute;n:</td>
				<td class="textoDato"colspan="3">
				<@s.property default="&nbsp;" escape=false value="deposito.tipoOrganizacion"/></td>
		</tr>

		<tr>
			<td class="textoCampo">Secci&oacute;n:</td>
			<td class="textoDato" colspan="3">
	  			<@s.select 
					templateDir="custontemplates" 
					id="seccion" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="seccion" 
					list="seccionList" 
					listKey="oid" 
					listValue="seccion" 
					value="oid"
					title="Seccion"
					headerKey="" 
			        headerValue="Seleccionar" />
	  		</td>
	  	</tr>	
		<tr>
			<td class="textoCampo">Fila Desde:</td>
			<td class="textoDato">
	  			<@s.select 
					templateDir="custontemplates" 
					id="filaDesde" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="filaDesde" 
					list="filaColumnaWrapper.filaList" 
					listKey="fila" 
					listValue="fila" 
					value="fila"
					title="Fila Desde"
					headerKey="" 
			        headerValue="Seleccionar" />
	  		</td>
			<td class="textoCampo">Fila Hasta:</td>
			<td class="textoDato">
	  			<@s.select 
					templateDir="custontemplates" 
					id="filaHasta" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="filaHasta" 
					list="filaColumnaWrapper.filaList" 
					listKey="fila" 
					listValue="fila" 
					value="fila"
					title="Fila Hasta"
					headerKey="" 
			        headerValue="Seleccionar" />
	  		</td>
		</tr>
		<tr>
			<td class="textoCampo">Columna Desde:</td>
			<td class="textoDato">
	  			<@s.select 
					templateDir="custontemplates" 
					id="columnaDesde" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="columnaDesde" 
					list="filaColumnaWrapper.columnaList" 
					listKey="columna" 
					listValue="columna" 
					value="columna"
					title="Columna Desde"
					headerKey="" 
			        headerValue="Seleccionar" />
	  		</td>
			<td class="textoCampo">Columna Hasta:</td>
			<td class="textoDato">
	  			<@s.select 
					templateDir="custontemplates" 
					id="columnaHasta" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="columnaHasta" 
					list="filaColumnaWrapper.columnaList" 
					listKey="columna" 
					listValue="columna" 
					value="columna"
					title="Columna Hasta"
					headerKey="" 
			        headerValue="Seleccionar" />
	  		</td>
		</tr>
		<tr>
			<td class="textoCampo">Ubicaci&oacute;n Desde:</td>
			<td class="textoDato">
	  			<@s.select 
					templateDir="custontemplates" 
					id="ubicacionDesde" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="ubicacionDesde" 
					list="filaColumnaWrapper.ubicacionList" 
					listKey="ubicacion" 
					listValue="ubicacion" 
					value="ubicacion"
					title="Ubicacion Desde"
					headerKey="" 
			        headerValue="Seleccionar" />
	  		</td>
			<td class="textoCampo">Ubicaci&oacute;n Hasta:</td>
			<td class="textoDato">
	  			<@s.select 
					templateDir="custontemplates" 
					id="ubicacionHasta" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="ubicacionHasta" 
					list="filaColumnaWrapper.ubicacionList" 
					listKey="ubicacion" 
					listValue="ubicacion" 
					value="ubicacion"
					title="Ubicacion Hasta"
					headerKey="" 
			        headerValue="Seleccionar" />
	  		</td>
		</tr>
		<tr><td class="textoCampo" colspan="4">&nbsp;</td></tr>
	</table>
	</div>
