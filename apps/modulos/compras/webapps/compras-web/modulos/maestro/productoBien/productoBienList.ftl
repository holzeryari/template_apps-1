
<#setting locale="es_AR">

<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>

<@s.hidden id="navigationId" name="navigationId"/>
<@s.hidden id="productoBien.rubro.oid" name="productoBien.rubro.oid"/>
<@s.hidden id="productoBien.rubro.descripcion" name="productoBien.rubro.descripcion"/>

<@s.hidden id="periodoEvaluacion.oid" name="periodoEvaluacion.oid"/>

		<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
        	<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Datos del Producto/Bien</b></td>
				<td>					
					<div align="right">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0066"
							enabled="true" 
							cssClass="item" 
							id="crear"										
							href="javascript://nop/">
							<b>Agregar</b>										
							<img src="${request.contextPath}/common/images/agregar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
						</@security.a>
					</div>
				</td>
			</tr>
		</table>
		
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
        	<tr>
  				<td class="textoCampo">C&oacute;digo:</td>
  				<td class="textoDato">
  					<@s.textfield 
					templateDir="custontemplates" 
					id="productoBien.oid"
					cssClass="textarea"
					cssStyle="width:160px" 
					name="productoBien.oid" 
					title="Codigo" />
			</td>
			<td class="textoCampo">Descripci&oacute;n:</td>
			<td class="textoDato">
				<@s.textfield 
  					templateDir="custontemplates" 
					id="productoBien.descripcion"
					cssClass="textarea"
					cssStyle="width:160px" 
					name="productoBien.descripcion" 
					title="Descripcion"/>
			</td>
		</tr>
		<tr>
			<td class="textoCampo">Rubro:</td>
  			<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="productoBien.rubro.descripcion" />
				<@s.a templateDir="custontemplates" id="seleccionarRubro" name="seleccionarRubro" href="javascript://nop/">
				<img src="${request.contextPath}/common/images/buscar.gif" border="0" align="absmiddle" hspace="3" >
				</@s.a>		
			</td>
			<td class="textoCampo">Tipo:</td>
			<td class="textoDato">
				<@s.select 
					templateDir="custontemplates" 
					id="productoBien.tipo" 
					cssClass="textarea"
					cssStyle="width:165px" 
					name="productoBien.tipo" 
					list="tipoList" 
					listKey="key" 
					listValue="description" 
					value="productoBien.tipo.ordinal()"
					title="Tipo de Producto/Bien"
					headerKey="0"
					headerValue="Todos"   
					/>
			</td>
		</tr>
		<tr>
			<td class="textoCampo">Es Cr&iacute;tico: </td>
				<td  class="textoDato">
	      			<@s.select 
						templateDir="custontemplates" 
						id="productoBien.critico" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="productoBien.critico" 
						list="criticoList" 
						listKey="key" 
						listValue="description" 
						value="productoBien.critico.ordinal()"
						title="Es Critico"
						headerKey="0"
						headerValue="Todos" 
						 />
			</td>
			<td class="textoCampo">Estado:</td>
  				<td class="textoDato">
  				<@s.select 
					templateDir="custontemplates" 
					id="productoBien.estado" 
					cssClass="textarea"
					cssStyle="width:165px" 
					name="productoBien.estado" 
					list="estadoList" 
					listKey="key" 
					listValue="description" 
					value="productoBien.estado.ordinal()"
					title="Estado"
					headerKey="0"
					headerValue="Todos"							
					/>
			</td>
		</tr>	
		<tr>
	    	<td colspan="4" class="lineaGris" align="right">
	      		<input id="limpiar" type="button" name="btnVolver" value="Limpiar" class="boton"/>		
	      		<input id="buscar" type="button" name="btnVolver" value="Buscar" class="boton"/>
	    	</td>
		</tr>	
	</table>
	</div>

	<!-- Resultado Filtro -->
	<@s.if test="productoBienList!=null">
		<#assign productoBienOid="0" />
		<#assign productoBienRubroOid="0" />
		<#assign productoBienRubroDescripcion="" />		
		<@s.if test="productoBien.oid!=null">
			<#assign productoBienOid="${productoBien.oid}" />
		</@s.if>
		<@s.if test="productoBien.rubro!=null">						
			<#assign productoBienRubroDescripcion="${productoBien.rubro.descripcion}" />
			<@s.if test="productoBien.rubro.oid!=null">		
				<#assign productoBienRubroOid="${productoBien.rubro.oid}" />
			</@s.if>						
		</@s.if>						
		<#assign productoBienDescripcion="${productoBien.descripcion}" />
		<#assign productoBienTipo="${productoBien.tipo.ordinal()}" />
		<#assign productoBienEstado = "${productoBien.estado.ordinal()}" />
		<#assign productoBienCritico = "${productoBien.critico.ordinal()}" />
										
		
		<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
		<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Productos/Bienes encontrados</td>
					<td>
						<div class="alineacionDerecha">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0067" 
							cssClass="item" 
							href="${request.contextPath}/maestro/productoBien/printXLS.action?productoBien.oid=${productoBienOid}&productoBien.descripcion=${productoBienDescripcion}&productoBien.tipo=${productoBienTipo}&productoBien.rubro.oid=${productoBienRubroOid}&productoBien.rubro.descripcion=${productoBienRubroDescripcion}&productoBien.estado=${productoBienEstado}&productoBien.critico=${productoBienCritico}">
							<img src="${request.contextPath}/common/images/imprimirExcel.gif" title="Exportar a Excel" align="absmiddle" border="0"  hspace="3" >
						</@security.a>	
						</div>
						<div class="alineacionDerecha">
							<b>Imprimir</b>
							<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0067" 
								cssClass="item" 
								href="${request.contextPath}/maestro/productoBien/printPDF.action?productoBien.oid=${productoBienOid}&productoBien.descripcion=${productoBienDescripcion}&productoBien.tipo=${productoBienTipo}&productoBien.rubro.oid=${productoBienRubroOid}&productoBien.rubro.descripcion=${productoBienRubroDescripcion}&productoBien.estado=${productoBienEstado}&productoBien.critico=${productoBienCritico}">
								<img src="${request.contextPath}/common/images/imprimirPdf.gif" title="Exportar a PDF" align="absmiddle" border="0" hspace="3" >
							</@security.a>
						</div>	
						
						<div class="alineacionDerecha">
								<b>Per&iacute;odos:</b>	
									<@s.select 
										templateDir="custontemplates" 
										id="periodo" 
										cssClass="textarea"
										cssStyle="width:50px" 
										cssStyle="width:165px" 
										name="periodo" 
										list="periodoEvaluacionList" 
										listKey="oid" 
										listValue="fechasPeriodoEvaluacion" 
										title="Periodos"
										onchange="javascript:periodoEvaluacionSelect(this);" 
										/>
									<@security.a 
									templateDir="custontemplates" 
									securityCode="CUF0067" 
									cssClass="item" 
									id="fscListLink"
									name="fscListLink"		
									onclick="javascript:fscListLinkOnClick();"					
									href="${request.contextPath}/maestro/productoBien/printFSCPorProductoBienPDF.action?periodoEvaluacion.oid=${periodoEvaluacion.oid}&productoBien.oid=${productoBienOid}&productoBien.descripcion=${productoBienDescripcion}&productoBien.tipo=${productoBienTipo}&productoBien.rubro.oid=${productoBienRubroOid}&productoBien.rubro.descripcion=${productoBienRubroDescripcion}&productoBien.estado=${productoBienEstado}&productoBien.critico=${productoBienCritico}">
									<img src="${request.contextPath}/common/images/imprimirPdf.gif" title="Listado de FSC por Producto/Bien" align="absmiddle" border="0" hspace="3">
									</@security.a>
											<b> - </b>&nbsp;
						</div>	
						
						
					</td>
				</tr>
			</table>
		
			<@ajax.anchors target="contentTrx">
				<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="productoBienList" id="productoBien" pagesize=15 defaultsort=2 partialList=true size="recordSize" keepStatus=true excludedParams="resetFlag">		
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select"  class="botoneraAnchoCon4" title="Acciones">
						<div class="alineacion">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0061" 
							enabled="productoBien.readable" 
							cssClass="item" 
							href="${request.contextPath}/maestro/productoBien/readView.action?productoBien.oid=${productoBien.oid?c}&productoBien.tipo=${productoBien.tipo.ordinal()?c}&navigationId=productobien-ver&navegacionIdBack=${navigationId}">
							<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver"  border="0">
						</@security.a>
						</div>
						<div class="alineacion">
						<#if productoBien.tipo == "Producto">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0062" 
							enabled="productoBien.updatable"
							cssClass="item"  
							href="${request.contextPath}/maestro/productoBien/administrarView.action?productoBien.oid=${productoBien.oid?c}&productoBien.tipo=${productoBien.tipo.ordinal()?c}&navigationId=producto-administrar&flowControl=regis&navegacionIdBack=${navigationId}">
							<img  src="${request.contextPath}/common/images/administrar.gif" alt="Administrar" title="Administrar"  border="0">
						</@security.a>									
						<#else>
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0062" 
							enabled="productoBien.updatable"
							cssClass="item"  
							href="${request.contextPath}/maestro/productoBien/updateView.action?productoBien.oid=${productoBien.oid?c}&productoBien.tipo=${productoBien.tipo.ordinal()?c}&navigationId=bien-update&flowControl=regis">
							<img  src="${request.contextPath}/common/images/modificar.gif" alt="Modificar" title="Modificar"  border="0">
						</@security.a>									
						</#if>	
						</div>
						<div class="alineacion">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0063" 
							enabled="productoBien.eraseable"
							cssClass="item"  
							href="${request.contextPath}/maestro/productoBien/deleteView.action?productoBien.oid=${productoBien.oid?c}&productoBien.tipo=${productoBien.tipo.ordinal()?c}&navigationId=productobien-eliminar">
							<img  src="${request.contextPath}/common/images/eliminar.gif" alt="Eliminar" title="Eliminar"  border="0">
						</@security.a>
						</div>
						<div class="alineacion">
					<#if productoBien.estado == "Activo">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0064" 
							enabled="true"
							cssClass="item"  
							href="${request.contextPath}/maestro/productoBien/desactivarView.action?productoBien.oid=${productoBien.oid?c}&productoBien.tipo=${productoBien.tipo.ordinal()?c}&navigationId=productobien-desactivar">
							<img src="${request.contextPath}/common/images/desactivar.gif" alt="Desactivar" title="Desactivar" border="0">
						</@security.a>			
					<#else>
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0065" 
							enabled="true"
							cssClass="item"  
							href="${request.contextPath}/maestro/productoBien/activarView.action?productoBien.oid=${productoBien.oid?c}&productoBien.tipo=${productoBien.tipo.ordinal()?c}&&navigationId=productobien-activar">
							<img src="${request.contextPath}/common/images/activar.gif" alt="Activar" title="Activar" border="0">
						</@security.a>								
					</#if>	
					</div>								
					</@display.column>

						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="oid" title="C&oacute;digo" />
						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="descripcion" title="Descripci&oacute;n" />
						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="rubro.descripcion" title="Rubro" />
						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="tipo" title="Tipo Producto/Bien" />
						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="estado" title="Estado" />

					</@display.table>
				</@ajax.anchors>
			</div>
		</@s.if>
		

<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/productoBien/search.action" 
  source="buscar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="resetFlag=true,navigationId={navigationId},flowControl=regis,productoBien.oid={productoBien.oid},productoBien.descripcion={productoBien.descripcion},productoBien.tipo={productoBien.tipo},productoBien.rubro.oid={productoBien.rubro.oid},productoBien.rubro.descripcion={productoBien.rubro.descripcion},productoBien.estado={productoBien.estado}, productoBien.critico={productoBien.critico}"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/productoBien/view.action" 
  source="limpiar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=regis"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/productoBien/createView.action" 
  source="crear" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=productoBien-create,flowControl=regis"/>
  
 <@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/productoBien/selectRubroSearch.action" 
  source="seleccionarRubro" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=change,productoBien.oid={productoBien.oid},productoBien.descripcion={productoBien.descripcion},productoBien.tipo={productoBien.tipo},productoBien.rubro.oid={productoBien.rubro.oid},productoBien.rubro.descripcion={productoBien.rubro.descripcion},productoBien.estado={productoBien.estado},productoBien.critico={productoBien.critico}"/>
  
