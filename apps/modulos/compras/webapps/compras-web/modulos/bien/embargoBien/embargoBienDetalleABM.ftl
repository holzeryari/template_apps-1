<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<@s.hidden id="embargoBienDetalle.oid" name="embargoBienDetalle.oid"/>
<@s.hidden id="embargoBienDetalle.embargoBien.oid" name="embargoBienDetalle.embargoBien.oid"/>
<@s.hidden id="embargoBienDetalle.bienInventariado.oid" name="embargoBienDetalle.bienInventariado.oid"/>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
			<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos del Inventario</b></td>
			</tr>
		</table>
	
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
			<tr>
      			<td class="textoCampo">N&uacute;mero:</td>
      			<td class="textoDato"><@s.property default="&nbsp;" escape=false value="embargoBienDetalle.bienInventariado.inventarioBien.numero"/></td>
      			<td  class="textoCampo">Fecha: </td>
				<td class="textoDato">
					<@s.if test="embargoBienDetalle.bienInventariado.inventarioBien.fecha != null">
						<#assign fecha = embargoBienDetalle.bienInventariado.inventarioBien.fecha>
						${fecha?string("dd/MM/yyyy")}	
					</@s.if>									
					&nbsp;
				</td>
			</tr>	
			<tr>
				<td class="textoCampo">Tipo Inventario:</td>
      			<td class="textoDato"colspan="3"><@s.property default="&nbsp;" escape=false value="embargoBienDetalle.bienInventariado.inventarioBien.tipoInventario"/></td>
    		</tr>
    		<tr>
				<td class="textoCampo">Observaciones:</td>
      			<td class="textoDato">
      				<@s.property default="&nbsp;" escape=false value="embargoBienDetalle.bienInventariado.inventarioBien.observaciones"/>&nbsp;
      			</td>
      			<td class="textoCampo">&nbsp;</td>
      			<td class="textoDato">&nbsp;</td>					
    		</tr>
    		<tr>
				<td class="textoCampo">Estado:</td>
      			<td  class="textoDato" colspan="3"><@s.property default="&nbsp;" escape=false value="embargoBienDetalle.bienInventariado.inventarioBien.estado"/></td>
			</tr>
			<tr><td class="textoCampo" colspan="4">&nbsp;</td></tr>
		</table>
	</div>		
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos del Bien</b></td>
			</tr>
		</table>
	
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>			
			<tr>
  			<td class="textoCampo">C&oacute;digo:</td>
  			<td class="textoDato"><@s.property default="&nbsp;" escape=false value="embargoBienDetalle.bienInventariado.bien.oid"/></td>
  			<td class="textoCampo">Descripci&oacute;n: </td>
			<td class="textoDato"><@s.property default="&nbsp;" escape=false value="embargoBienDetalle.bienInventariado.bien.descripcion"/></td>	      			
		</tr>	
		<tr>
			<td class="textoCampo">Marca:</td>
  			<td class="textoDato">
  				<@s.property default="&nbsp;" escape=false value="embargoBienDetalle.bienInventariado.bien.marca"/>
  					&nbsp;
			</td>
			<td class="textoCampo">Modelo:</td>
  			<td  class="textoDato">
      			<@s.property default="&nbsp;" escape=false value="embargoBienDetalle.bienInventariado.bien.modelo"/>
      			&nbsp;
			</td>
		</tr>
		<tr>
			<td class="textoCampo">Valor Mercado:</td>
  			<td  class="textoDato">
				<@s.property default="&nbsp;" escape=false value="embargoBienDetalle.bienInventariado.bien.valorMercado"/></td>
			</td>
			<td class="textoCampo">Fecha Valor Mercado:</td>
  			<td  class="textoDato">
				<@s.property default="&nbsp;" escape=false value="embargoBienDetalle.bienInventariado.bien.fechaValorMercado"/></td>
			</td>
		</tr>
		<tr>
			<td class="textoCampo">Rubro:</td>
  			<td  class="textoDato">
				<@s.property default="&nbsp;" escape=false value="embargoBienDetalle.bienInventariado.bien.rubro.descripcion"/></td>
			</td>

			<td class="textoCampo">Es cr&iacute;tico:</td>
  			<td  class="textoDato">
				<@s.property default="&nbsp;" escape=false value="embargoBienDetalle.bienInventariado.bien.critico"/></td>
			</td>
		</tr>
		<tr>
			<td class="textoCampo">C&oacute;digo de barras:</td>
  			<td  class="textoDato">
				<@s.property default="&nbsp;" escape=false value="embargoBienDetalle.bienInventariado.bien.codigoBarra"/></td>
			</td>

			<td class="textoCampo">Amortizaci&oacute;n:</td>
  			<td  class="textoDato">
				<@s.property default="&nbsp;" escape=false value="embargoBienDetalle.bienInventariado.bien.amortizacion"/> (%)</td>
			</td>
		</tr>
		<tr>
			<td class="textoCampo">Es registrable:</td>
  			<td  class="textoDato" colsapn="3">
				<@s.property default="&nbsp;" escape=false value="embargoBienDetalle.bienInventariado.bien.registrable"/></td>
			</td>
		</tr>
		<tr>
			<td class="textoCampo">Es veh&iacute;culo:</td>
  			<td  class="textoDato">
				<@s.property default="&nbsp;" escape=false value="embargoBienDetalle.bienInventariado.bien.vehiculo"/></td>
			</td>

			<td class="textoCampo">Estado:</td>
  			<td  class="textoDato">
				<@s.property default="&nbsp;" escape=false value="embargoBienDetalle.bienInventariado.bien.estado"/>
			</td>
		</tr>
		<tr><td class="textoCampo" colspan="4">&nbsp;</td></tr>
	</table>
	</div>					
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos del Bien Inventariado</b></td>
			</tr>
		</table>
	
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>					    
			<tr>
      			<td class="textoCampo">N&uacute;mero del bien inventariado:</td>
      			<td class="textoDato">
      				<@s.property default="&nbsp;" escape=false value="embargoBienDetalle.bienInventariado.numeroInventario"/>
      			</td>
      			<td class="textoCampo">Fecha Alta:</td>
				<td class="textoDato">
					<@s.if test="embargoBienDetalle.bienInventariado.fechaAlta != null">
						<#assign fechaAlta = embargoBienDetalle.bienInventariado.fechaAlta>
						${fechaAlta?string("dd/MM/yyyy")}	
					</@s.if>									
					&nbsp;
				</td>
			</tr>	
			<tr>
				<td class="textoCampo">N&uacute;mero de Factura:</td>
      			<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="embargoBienDetalle.bienInventariado.numeroFactura"/>
				</td>
				<td class="textoCampo">Valor Adquisici&oacute;n: </td>
				<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="embargoBienDetalle.bienInventariado.valorAdquisicion"/>
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Valor Amortizado: </td>
				<td class="textoDato" colspan="3">
					<@s.property default="&nbsp;" escape=false value="embargoBienDetalle.bienInventariado.valorAmortizado"/>
				</td>
			</tr>
			<tr>
      			<td class="textoCampo">Identificador: </td>
				<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="embargoBienDetalle.bienInventariado.numeroSerie"/>
				</td>
				<td class="textoCampo">Cliente:</td>
				<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="embargoBienDetalle.bienInventariado.cliente.descripcion"/>
				</td>
    		</tr>
			<tr>
				<td class="textoCampo">Estado:</td>
				<td class="textoDato" colspan="3">
					<@s.property default="&nbsp;" escape=false value="embargoBienDetalle.bienInventariado.estado"/>
				</td>
			</tr>
			<tr><td class="textoCampo" colspan="4">&nbsp;</td></tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos del Embargo</b></td>
			</tr>
		</table>
	
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>		
			<tr>
      			<td class="textoCampo">Monto:</td>
      			<td class="textoDato">
	      			<@s.textfield 
      					templateDir="custontemplates" 
						id="embargoBienDetalle.montoEmbargo" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="embargoBienDetalle.montoEmbargo" 
						title="Monto" />
      			</td>   			
      			<td class="textoCampo">&nbsp;</td>
      			<td class="textoDato">&nbsp;</td>
      			
			</tr>
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>		
		</table>
	</div>		

