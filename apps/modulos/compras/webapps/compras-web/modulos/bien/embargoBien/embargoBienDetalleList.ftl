<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<@vc.anchors target="contentTrx" ajaxFlag="ajax">
	<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
		<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
			<tr>
				<td>Bienes a Embargar</td>
				<td>
					<div align="right">
						<#if embargoBien.estado.ordinal()==1>
  								<@s.a href="${request.contextPath}/bien/embargoBien/selectBienInventariado.action?embargoBien.oid=${embargoBien.oid?c}&navegacionIdBack=${navigationId}" templateDir="custontemplates" id="agregarBienInventariado" name="agregarBienInventariado" cssClass="ocultarIcono">
									<b>Agregar</b><img src="${request.contextPath}/common/images/agregar.gif"  border="0" align="absmiddle" hspace="3" >
								</@s.a>	
						<#else>
							&nbsp;				
						</#if>	
					</div>
				</td>	
			</tr>
		</table>
		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="embargoBien.embargoBienDetalleList" id="embargoBienDetalle" defaultsort=2>	
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" class="botoneraAnchoCon3" title="Acciones">
				<a href="${request.contextPath}/bien/embargoBien/readEmbargoBienDetalleView.action?embargoBienDetalle.oid=${embargoBienDetalle.oid?c}&navegacionIdBack=${navigationId}"><img src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver" border="0"></a>
				&nbsp;
				<a href="${request.contextPath}/bien/embargoBien/updateEmbargoBienDetalleView.action?embargoBienDetalle.oid=${embargoBienDetalle.oid?c}&navegacionIdBack=${navigationId}"><img src="${request.contextPath}/common/images/modificar.gif" alt="Modificar" title="Modificar" border="0"></a>
				&nbsp;
				<#if embargoBien.estado.ordinal()==1>
  					
					<a href="${request.contextPath}/bien/embargoBien/deleteEmbargoBienDetalleView.action?embargoBienDetalle.oid=${embargoBienDetalle.oid?c}&navegacionIdBack=${navigationId}"><img src="${request.contextPath}/common/images/eliminar.gif" alt="Eliminar" title="Eliminar" border="0"></a>
				</#if>
				
			</@display.column>
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="numero" title="N&uacute;mero Item" defaultorder="ascending"/>
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="bienInventariado.numeroInventario" title="Nro. Inventario" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="bienInventariado.bien.descripcion" title="Descripci&oacute;n" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="bienInventariado.bien.marca" title="Marca" />					
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="bienInventariado.bien.modelo" title="Modelo" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="bienInventariado.numeroSerie" title="Identificador" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="montoEmbargo" title="Monto" />
		</@display.table>
	</div>
</@vc.anchors>
