<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancelar" type="button" name="btnVolver" value="Cancelar" class="boton"/>
				<input id="crear" type="button" name="btnAgregar" value="Agregar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>


<@vc.htmlContent 
  baseUrl="${request.contextPath}/bien/inventarioBien/createBienInventariado.action" 
  source="crear" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="bienInventariado.inventarioBien.oid={bienInventariado.inventarioBien.oid},bienInventariado.bien.oid={bienInventariado.bien.oid},bienInventariado.cliente.oid={bienInventariado.cliente.oid},cantidadInventariar={cantidadInventariar},bienInventariado.factura.pkFactura.nro_proveedor={bienInventariado.factura.pkFactura.nro_proveedor},bienInventariado.factura.pkFactura.nro_factura={bienInventariado.factura.pkFactura.nro_factura},bienInventariado.factura.pkFactura.sucursal={bienInventariado.factura.pkFactura.sucursal},bienInventariado.factura.pkFactura.tip_docum={bienInventariado.factura.pkFactura.tip_docum},bienInventariado.numeroFactura={bienInventariado.numeroFactura},bienInventariado.factura.numeroString={bienInventariado.factura.numeroString}"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancelar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=inventarioBien-administrar,flowControl=back"/>
  
 <@vc.htmlContent 
  baseUrl="${request.contextPath}/bien/inventarioBien/selectCliente.action" 
  source="seleccionarCliente" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId=${navigationId},flowControl=change,navegacionIdBack=administrar-createDetalle,bienInventariado.inventarioBien.oid={bienInventariado.inventarioBien.oid},bienInventariado.bien.oid={bienInventariado.bien.oid},bienInventario.cliente.oid={bienInventariado.cliente.oid},bienInventariado.cliente.descripcion={bienInventariado.cliente.descripcion},cantidadInventariar={cantidadInventariar},bienInventariado.factura.pkFactura.nro_proveedor={bienInventariado.factura.pkFactura.nro_proveedor},bienInventariado.factura.pkFactura.nro_factura={bienInventariado.factura.pkFactura.nro_factura},bienInventariado.factura.pkFactura.sucursal={bienInventariado.factura.pkFactura.sucursal},bienInventariado.factura.pkFactura.tip_docum={bienInventariado.factura.pkFactura.tip_docum},bienInventariado.numeroFactura={bienInventariado.numeroFactura},bienInventariado.factura.numeroString={bienInventariado.factura.numeroString}"/>
 
 
 <@vc.htmlContent 
  baseUrl="${request.contextPath}/bien/inventarioBien/selectComprobante.action" 
  source="seleccionarFactura" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=${navigationId},flowControl=change,bienInventariado.inventarioBien.oid={bienInventariado.inventarioBien.oid},bienInventariado.bien.oid={bienInventariado.bien.oid},bienInventariado.cliente.oid={bienInventariado.cliente.oid},cantidadInventariar={cantidadInventariar},bienInventariado.factura.pkFactura.nro_proveedor={bienInventariado.factura.pkFactura.nro_proveedor},bienInventariado.factura.pkFactura.nro_factura={bienInventariado.factura.pkFactura.nro_factura},bienInventariado.factura.pkFactura.sucursal={bienInventariado.factura.pkFactura.sucursal},bienInventariado.factura.pkFactura.tip_docum={bienInventariado.factura.pkFactura.tip_docum},bienInventariado.cliente.descripcion={bienInventariado.cliente.descripcion},bienInventariado.numeroFactura={bienInventariado.numeroFactura},bienInventariado.factura.numeroString={bienInventariado.factura.numeroString}"/>
 