<#setting locale="es_AR">

<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>

<@s.hidden id="navigationId" name="navigationId"/>
<@s.hidden id="navegacionIdBack" name="navegacionIdBack"/>
<@s.hidden id="bienInventariado.bien.descripcion" name="bienInventariado.bien.descripcion"/>
<@s.hidden id="bienInventariado.bien.rubro.oid" name="bienInventariado.bien.rubro.oid"/>
<@s.hidden id="bienInventariado.bien.rubro.descripcion" name="bienInventariado.bien.rubro.descripcion"/>
<@s.hidden id="bienInventariado.cliente.oid" name="bienInventariado.cliente.oid"/>
<@s.hidden id="bienInventariado.cliente.descripcion" name="bienInventariado.cliente.descripcion"/>
<@s.hidden id="bienInventariado.estado" name="bienInventariado.estado.ordinal()"/>
<@s.hidden id="clienteDuro" name="clienteDuro"/>
<@s.hidden id="estaEmbargadoDuro" name="estaEmbargadoDuro"/>
<@s.hidden id="bienInventariado.bien.oid" name="bienInventariado.bien.oid"/>


<@s.if test="clienteDuro == true">
	<@s.hidden id="bienInventariado.cliente.descripcion" name="bienInventariado.cliente.descripcion"/>
</@s.if>

<@s.if test="estaEmbargadoDuro == true">
	<@s.hidden id="bienInventariado.estaEmbargado" name="bienInventariado.estaEmbargado.ordinal()"/>
</@s.if>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
        	<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Datos del Bien</b></td>
			</tr>
		</table>
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
	    		<td class="textoCampo" colspan="4">&nbsp;</td>
	    	</tr>	
			<tr>
				<td class="textoCampo">Bien:</td>
      			<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="bienInventariado.bien.descripcion"/>
					<@s.a templateDir="custontemplates" id="seleccionarBien" name="seleccionarBien" href="javascript://nop/">
						<img src="${request.contextPath}/common/images/buscar.gif"  border="0" align="absmiddle" hspace="3" >
					</@s.a>	
				</td>
      			<td class="textoCampo">N&uacute;mero de inventario:</td>
      			<td class="textoDato">
      				<@s.textfield 
	  					templateDir="custontemplates" 
						id="bienInventariado.numeroInventario" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="bienInventariado.numeroInventario" 
						title="N&uacute;mero de Inventario" />
				</td>
    		</tr>					
			<tr>
				<td class="textoCampo">Rubro:</td>
      			<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="bienInventariado.bien.rubro.descripcion"/>
					<@s.a templateDir="custontemplates" id="seleccionarRubro" name="seleccionarRubro" href="javascript://nop/">
						<img src="${request.contextPath}/common/images/buscar.gif" border="0" align="absmiddle" hspace="3" >
					</@s.a>		
				</td>
				<td class="textoCampo">Embargado:</td>
				<td class="textoDato">
					<@s.select 
							templateDir="custontemplates" 
							id="bienInventariado.estaEmbargado" 
							cssClass="textarea"
							cssStyle="width:165px" 
							name="bienInventariado.estaEmbargado" 
							list="estaEmbargadoList" 
							listKey="key" 
							listValue="description" 
							value="bienInventariado.estaEmbargado.ordinal()"										
							headerKey="0" 
		                    headerValue="Seleccionar"
		                    title="Embargado"
				       		 />
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Cliente:</td>
				<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="bienInventariado.cliente.descripcion"/>
					<@s.a templateDir="custontemplates" id="seleccionarCliente" name="seleccionarCliente" href="javascript://nop/" cssClass="ocultarIcono">
						<img src="${request.contextPath}/common/images/buscar.gif"  border="0" align="absmiddle" hspace="3" >
					</@s.a>		
				</td>
				<td class="textoCampo">Estado: </td>
				<td class="textoDato">
					<@s.select 
							templateDir="custontemplates" 
							id="bienInventariado.estado" 
							cssClass="textarea"
							cssStyle="width:160px" 
							name="bienInventariado.estado" 
							list="estadoList" 
							listKey="key" 
							listValue="description" 
							value="bienInventariado.estado.ordinal()"
							headerKey="0" 
		                    headerValue="Seleccionar"										
							title="Estado" />
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Fecha Desde:</td>
				<td class="textoDato">
					<@vc.rowCalendar 
						templateDir="custontemplates" 
						id="fechaDesde" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="fechaDesde" 
						title="Fecha Desde" />
				</td>
				<td class="textoCampo">Fecha Hasta:</td>
				<td class="textoDato">
					<@vc.rowCalendar 
						templateDir="custontemplates" 
						id="fechaHasta" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="fechaHasta" 
						title="Fecha Hasta" />
				</td>
			</tr>
			<tr>
	    		<td colspan="4" class="lineaGris" align="right">
	      			<input id="limpiar" type="button" name="btnVolver" value="Limpiar" class="boton"/>		
	      			<input id="buscar" type="button" name="btnVolver" value="Buscar" class="boton"/>
	    		</td>
			</tr>	
		</table>
	</div>			
						
	<!-- Resultado Filtro -->
	<@s.if test="bienInventariadoList!=null">
		<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
			<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Bienes encontrados</td>
				</tr>
			</table>
			
			<@vc.anchors target="contentTrx" ajaxFlag="ajax">
				<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="bienInventariadoList" id="bienInventariadoSelect" pagesize=15 defaultsort=1 partialList=true size="recordSize" keepStatus=true excludedParams="resetFlag">
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" style=" text-align:center; display:block" title="Acciones">
						<@s.if test="nameSpaceSelect!=''">								
							<@s.a id="select${bienInventariadoSelect.oid?c}"href="javascript://nop/" name="select${bienInventariadoSelect.oid?c}" cssClass="no-rewrite" >
								<img src="${request.contextPath}/common/images/seleccionar.gif" alt="Seleccionar" title="Seleccionar" border="0">
							</@s.a>
							<@vc.htmlContent 
							  baseUrl="${request.contextPath}/bien/inventarioBien/selectBienInventariado.action" 
							  source="select${bienInventariadoSelect.oid?c}" 
							  success="contentTrx" 
							  failure="errorTrx" 
							  parameters="bienInventariado.oid=${bienInventariadoSelect.oid?c},bienInventariado.bien.descripcion=${bienInventariadoSelect.bien.descripcion},oidParameter=${oidParameter},nameSpaceSelect=${nameSpaceSelect},actionNameSelect=${actionNameSelect},navegacionIdBack=${navegacionIdBack},navigationId=${navigationId}"/>
						</@s.if>

						<@s.else>
							<@s.a id="select${bienInventariadoSelect.oid?c}"href="javascript://nop/" name="select${bienInventariadoSelect.oid?c}" cssClass="no-rewrite" >
								<img src="${request.contextPath}/common/images/seleccionar.gif" alt="Seleccionar" title="Seleccionar"  border="0">
							</@s.a>
							<@vc.htmlContent 
							  baseUrl="${request.contextPath}/bien/inventarioBien/selectBienInventariadoBack.action" 
							  source="select${bienInventariadoSelect.oid?c}" 
							  success="contentTrx" 
							  failure="errorTrx" 
							  parameters="bienInventariado.oid=${bienInventariadoSelect.oid?c},bienInventariado.bien.descripcion=${bienInventariadoSelect.bien.descripcion},oidParameter=${oidParameter},navegacionIdBack=${navegacionIdBack},navigationId=${navigationId}"/>
						</@s.else>

					</@display.column>
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="numeroInventario" title="Nro. Inventario" />
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="bien.descripcion" title="Bien" />
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="fechaAlta" format="{0,date,dd/MM/yyyy}" title="Fecha Alta" />
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="cliente.descripcion" title="Cliente" />
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="estaEmbargado" title="Embargado" />
										
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="estado" title="Estado" />
					
				</@display.table>
			</@vc.anchors>
		</div>
	</@s.if>

	<div id="capaBotonera" class="capaBotonera">
		<table id="tablaBotonera" class="tablaBotonera">
			<tr> 
				<td align="left">
					<input id="cancel" type="button" name="btnVolver" value="Volver" class="boton"/>
				</td>
			</tr>	
		</table>
	</div>
		  

<@vc.htmlContent 
  baseUrl="${request.contextPath}/bien/inventarioBien/searchBienInventariadoSelect.action" 
  source="buscar" 
  success="contentTrx" 
  failure="errorTrx" 
 parameters="resetFlag=true,flowControl=regis,bienInventariado.numeroInventario={bienInventariado.numeroInventario},bienInventariado.bien.descripcion={bienInventariado.bien.descripcion},bienInventariado.estaEmbargado={bienInventariado.estaEmbargado.ordinal()},bienInventariado.bien.rubro.oid={bienInventariado.bien.rubro.oid},bienInventariado.bien.rubro.descripcion={bienInventariado.bien.rubro.descripcion},bienInventariado.cliente.oid={bienInventariado.cliente.oid},bienInventariado.cliente.descripcion={bienInventariado.cliente.descripcion},bienInventariado.estado={bienInventariado.estado},clienteDuro={clienteDuro},estaEmbargadoDuro={estaEmbargadoDuro},fechaDesde={fechaDesde},fechaHasta={fechaHasta},nameSpaceSelect=${nameSpaceSelect},actionNameSelect=${actionNameSelect},oidParameter=${oidParameter},navigationId={navigationId},navegacionIdBack=${navegacionIdBack},bienInventariado.bien.oid={bienInventariado.bien.oid}"/>
 
<@vc.htmlContent 
  baseUrl="${request.contextPath}/bien/inventarioBien/viewBienInventariadoSelect.action" 
  source="limpiar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="flowControl=regis,bienInventariado.cliente.oid={bienInventariado.cliente.oid},bienInventariado.cliente.descripcion={bienInventariado.cliente.descripcion},bienInventariado.estado={bienInventariado.estado},bienInventariado.estaEmbargado={bienInventariado.estaEmbargado.ordinal()},clienteDuro={clienteDuro},estaEmbargadoDuro={estaEmbargadoDuro},nameSpaceSelect=${nameSpaceSelect},actionNameSelect=${actionNameSelect},oidParameter=${oidParameter},navigationId={navigationId},navegacionIdBack=${navegacionIdBack}"/>
  
  
 <@vc.htmlContent 
  baseUrl="${request.contextPath}/bien/inventarioBien/selectClienteBienSearch.action" 
  source="seleccionarCliente" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=change,bienInventariado.numeroInventario={bienInventariado.numeroInventario},bienInventariado.bien.descripcion={bienInventariado.bien.descripcion},bienInventariado.estaEmbargado={bienInventariado.estaEmbargado.ordinal()},bienInventariado.bien.rubro.oid={bienInventariado.bien.rubro.oid},bienInventariado.bien.rubro.descripcion={bienInventariado.bien.rubro.descripcion},bienInventariado.cliente.oid={bienInventariado.cliente.oid},bienInventariado.cliente.descripcion={bienInventariado.cliente.descripcion},bienInventariado.estado={bienInventariado.estado},clienteDuro={clienteDuro},estaEmbargadoDuro={estaEmbargadoDuro},fechaDesde={fechaDesde},fechaHasta={fechaHasta},nameSpaceSelect=${nameSpaceSelect},actionNameSelect=${actionNameSelect},oidParameter=${oidParameter},navegacionIdBack={navegacionIdBack},bienInventariado.bien.oid={bienInventariado.bien.oid}"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/bien/inventarioBien/selectBienSearchSelectBienInventariado.action" 
  source="seleccionarBien" 
  success="contentTrx" 
  failure="errorTrx" 
 parameters="navigationId={navigationId},flowControl=change,bienInventariado.numeroInventario={bienInventariado.numeroInventario},bienInventariado.bien.descripcion={bienInventariado.bien.descripcion},bienInventariado.estaEmbargado={bienInventariado.estaEmbargado.ordinal()},bienInventariado.bien.rubro.oid={bienInventariado.bien.rubro.oid},bienInventariado.bien.rubro.descripcion={bienInventariado.bien.rubro.descripcion},bienInventariado.cliente.oid={bienInventariado.cliente.oid},bienInventariado.cliente.descripcion={bienInventariado.cliente.descripcion},bienInventariado.estado={bienInventariado.estado},clienteDuro={clienteDuro},estaEmbargadoDuro={estaEmbargadoDuro},fechaDesde={fechaDesde},fechaHasta={fechaHasta},nameSpaceSelect=${nameSpaceSelect},actionNameSelect=${actionNameSelect},oidParameter=${oidParameter},navegacionIdBack={navegacionIdBack},bienInventariado.bien.oid={bienInventariado.bien.oid}"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/bien/inventarioBien/selectRubroSelectBienInventariado.action" 
  source="seleccionarRubro" 
  success="contentTrx" 
  failure="errorTrx" 
 parameters="navigationId={navigationId},flowControl=change,bienInventariado.numeroInventario={bienInventariado.numeroInventario},bienInventariado.bien.descripcion={bienInventariado.bien.descripcion},bienInventariado.estaEmbargado={bienInventariado.estaEmbargado.ordinal()},bienInventariado.bien.rubro.oid={bienInventariado.bien.rubro.oid},bienInventariado.bien.rubro.descripcion={bienInventariado.bien.rubro.descripcion},bienInventariado.cliente.oid={bienInventariado.cliente.oid},bienInventariado.cliente.descripcion={bienInventariado.cliente.descripcion},bienInventariado.estado={bienInventariado.estado},clienteDuro={clienteDuro},estaEmbargadoDuro={estaEmbargadoDuro},fechaDesde={fechaDesde},fechaHasta={fechaHasta},nameSpaceSelect=${nameSpaceSelect},actionNameSelect=${actionNameSelect},oidParameter=${oidParameter},navegacionIdBack={navegacionIdBack},bienInventariado.bien.oid={bienInventariado.bien.oid}"/>
    
 <@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId=${navegacionIdBack},flowControl=back"/>
 
 
  