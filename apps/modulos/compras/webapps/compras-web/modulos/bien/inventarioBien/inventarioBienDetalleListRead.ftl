<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<@ajax.anchors target="contentTrx">	
		
	<#-- Recepcion-->
	<#if inventarioBien.tipoInventario.ordinal()==1>
			<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
				<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
					<tr>
						<td>Recepciones de Bienes</td>
					</tr>
				</table>
			    <@display.table class="tablaDetalleCuerpo" cellpadding="3" name="inventarioBien.recepcionProductoBienList" id="recepcionBien" defaultsort=2 >
          		  	<@display.column headerClass="tbl-contract-service-select" class="botoneraAnchoCon1" title="Acciones">
						<div class="alineacion">
						<@s.a 
							templateDir="custontemplates"
							cssClass="item" 
							name="ver"
							id="ver"
							href="${request.contextPath}/deposito/recepcionProductoBien/readView.action?recepcionProductoBien.oid=${recepcionBien.oid?c}&navigationId=recepcionBien-visualizar&flowControl=regis&navegacionIdBack=${navigationId}">
							<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver"  border="0">
						</@s.a>
						</div>	
					</@display.column>
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="numero" title="Nro." />				
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" title="Proveedor">  
						${recepcionBien.proveedor.detalleDePersona.razonSocial} 
						<#if recepcionBien.proveedor.detalleDePersona.nombre?exists> 
						   ${recepcionBien.proveedor.detalleDePersona.nombre}
						</#if>
					</@display.column>
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="fechaRecepcion" format="{0,date,dd/MM/yyyy}"  title="Fecha Recepci&oacute;n" /> 
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="deposito.descripcion" title="Dep&oacute;sito" />
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="cliente.descripcion" title="Cliente" />

					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="estado" title="Estado" />		

				</@display.table>
				</div>
				<div id="capaSeparadora" class="capaSeparadora">&nbsp;</div>
			</#if>

			<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
				<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
					<tr>
						<td>Bienes a Inventariar</td>
					</tr>
				</table>
						
				<!-- Resultado Filtro -->						
				<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="inventarioBien.bienInventariadoList" id="bienInventariado" defaultsort=2>	
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select"  class="botoneraAnchoCon1" title="Acciones">
						<a href="${request.contextPath}/bien/inventarioBien/readBienInventariadoView.action?bienInventariado.oid=${bienInventariado.oid?c}&navegacionIdBack=inventarioBien-visualizar"><img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver" border="0"></a>
						&nbsp;					
					</@display.column>
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="bien.oid" title="C&oacute;digo" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="numeroInventario" title="N&uacute;mero Inventario" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="bien.descripcion" title="Descripci&oacute;n" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="bien.rubro.descripcion" title="Rubro" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="bien.marca" title="Marca" />					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="bien.modelo" title="Modelo" />
				</@display.table>
		</div>				
	</@ajax.anchors>

