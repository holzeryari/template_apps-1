<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<@s.hidden id="bienInventariado.cliente.descripcion" name="bienInventariado.cliente.descripcion"/>
<@s.hidden id="bienInventariado.factura.pkFactura.nro_proveedor" name="bienInventariado.factura.pkFactura.nro_proveedor"/>
<@s.hidden id="bienInventariado.factura.pkFactura.nro_factura" name="bienInventariado.factura.pkFactura.nro_factura"/>
<@s.hidden id="bienInventariado.factura.pkFactura.sucursal" name="bienInventariado.factura.pkFactura.sucursal"/>
<@s.hidden id="bienInventariado.factura.pkFactura.tip_docum" name="bienInventariado.factura.pkFactura.tip_docum"/>
<@s.hidden id="bienInventariado.factura.numeroString" name="bienInventariado.factura.numeroString"/>


<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos de los Bienes a Inventariar</b></td>
			</tr>
		</table>
		
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
		<tr>
        	<td class="textoCampo" colspan="4">&nbsp;</td>
        </tr>	
		<tr>
			<td class="textoCampo">Cantidad a Inventariar:</td>
	  		<td class="textoDato">
	  			<@s.textfield 
	  					templateDir="custontemplates" 
						id="cantidadInventariar" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="cantidadInventariar" 
						title="Cantidad a inventariar" />
			</td>	
			<td class="textoCampo">Cliente:</td>
	  		<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="bienInventariado.cliente.descripcion"/>
				<@s.a templateDir="custontemplates" id="seleccionarCliente" name="seleccionarCliente" href="javascript://nop/" cssClass="ocultarIcono">
					<img src="${request.contextPath}/common/images/buscar.gif" border="0" align="absmiddle" hspace="3" >
				</@s.a>	
			</td>
		</tr>
		<tr>
			<td class="textoCampo">Nro. Factura Anterior al Sistema:</td>
	  		<td class="textoDato">
	  			<@s.textfield 
	  					templateDir="custontemplates" 
						id="bienInventariado.numeroFactura" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="bienInventariado.numeroFactura" 
						title="Nro. de Factura Anterior al Sistema" />
			</td>	
			<td class="textoCampo">Factura:</td>
	  		<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="bienInventariado.factura.numeroString"/>
				<@s.a templateDir="custontemplates" id="seleccionarFactura" name="seleccionarFactura" href="javascript://nop/" cssClass="ocultarIcono">
					<img src="${request.contextPath}/common/images/buscar.gif" border="0" align="absmiddle" hspace="3" >
				</@s.a>	
			</td>
		<tr>
		<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>
	</table>
</div>	
