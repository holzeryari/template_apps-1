<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancelar" type="button" name="btnVolver" value="Cancelar" class="boton"/>
				<input id="update" type="button" name="btnModificar" value="Modificar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/bien/inventarioBien/updateInventarioBien.action" 
  source="update" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="inventarioBien.oid={inventarioBien.oid},inventarioBien.versionNumber={inventarioBien.versionNumber},inventarioBien.fechaAlta={inventarioBien.fechaAlta},inventarioBien.observaciones={inventarioBien.observaciones}"/>
  

<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancelar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=inventarioBien-administrar,flowControl=back"/>
  
  