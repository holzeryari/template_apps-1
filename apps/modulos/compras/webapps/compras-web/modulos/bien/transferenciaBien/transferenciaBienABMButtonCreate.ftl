<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancel" type="button" name="btnCancelar" value="Cancelar" class="boton"/>
				<input id="store" type="button" name="btnAgregar" value="Agregar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/bien/transferenciaBien/abmSelectClienteOrigen.action" 
  source="seleccionarClienteOrigen" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId=abm-transferenciaBien-create,flowControl=change,transferenciaBien.clienteOrigen.oid={transferenciaBien.clienteOrigen.oid},transferenciaBien.clienteOrigen.descripcion={transferenciaBien.clienteOrigen.descripcion},transferenciaBien.clienteDestino.oid={transferenciaBien.clienteDestino.oid},transferenciaBien.clienteDestino.descripcion={transferenciaBien.clienteDestino.descripcion},transferenciaBien.observaciones={transferenciaBien.observaciones}"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/bien/transferenciaBien/abmSelectClienteDestino.action" 
  source="seleccionarClienteDestino" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId=abm-transferenciaBien-create,flowControl=change,transferenciaBien.clienteOrigen.oid={transferenciaBien.clienteOrigen.oid},transferenciaBien.clienteOrigen.descripcion={transferenciaBien.clienteOrigen.descripcion},transferenciaBien.clienteDestino.oid={transferenciaBien.clienteDestino.oid},transferenciaBien.clienteDestino.descripcion={transferenciaBien.clienteDestino.descripcion},transferenciaBien.observaciones={transferenciaBien.observaciones}"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/bien/transferenciaBien/createTransferenciaBien.action" 
  source="store" 
  success="contentTrx" 
  failure="errorTrx"
  parameters="transferenciaBien.oid={transferenciaBien.oid},transferenciaBien.clienteOrigen.oid={transferenciaBien.clienteOrigen.oid},transferenciaBien.clienteDestino.oid={transferenciaBien.clienteDestino.oid},transferenciaBien.observaciones={transferenciaBien.observaciones}"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action"
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=buscar-transferenciaBien,flowControl=back"/>
