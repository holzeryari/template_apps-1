<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaSeparadora" class="capaSeparadora">&nbsp;</div>
<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancel" type="button" name="btnCancelar" value="Cancelar" class="boton"/>
				<input id="delete" type="button" name="btnEliminar" value="Eliminar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/bien/transferenciaBien/deleteTransferenciaBien.action" 
  source="delete" 
  success="contentTrx" 
  failure="errorTrx"
  parameters="transferenciaBien.oid={transferenciaBien.oid}"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action"
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=buscar-transferenciaBien,flowControl=back"/>
