<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>


<@s.hidden id="navigationId" name="navigationId"/>
<@s.hidden id="transferenciaBienDetalle.oid" name="transferenciaBienDetalle.oid"/>
<@s.hidden id="transferenciaBienDetalle.bienInventariado.inventarioBien.oid" name="transferenciaBienDetalle.bienInventariado.inventarioBien.oid"/>
<@s.hidden id="transferenciaBienDetalle.bienInventariado.oid" name="transferenciaBienDetalle.bienInventariado.oid"/>
<@s.hidden id="transferenciaBienDetalle.bienInventariado.bien.oid" name="transferenciaBienDetalle.bienInventariado.bien.oid"/>
<@s.hidden id="transferenciaBienDetalle.bienInventariado.cliente.oid" name="transferenciaBienDetalle.bienInventariado.cliente.oid"/>
<@s.hidden id="transferenciaBienDetalle.bienInventariado.versionNumber" name="transferenciaBienDetalle.bienInventariado.versionNumber"/>
<@s.hidden id="transferenciaBienDetalle.bienInventariado.rubro.oid" name="transferenciaBienDetalle.bienInventariado.rubro.oid"/>
<@s.hidden id="transferenciaBienDetalle.bienInventariado.estadoServicio" name="transferenciaBienDetalle.bienInventariado.estado.ordinal()"/>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
			<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos del Inventario</b></td>
			</tr>
		</table>
	
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
			<tr>
      			<td class="textoCampo">N&uacute;mero:</td>
      			<td class="textoDato"><@s.property default="&nbsp;" escape=false value="transferenciaBienDetalle.bienInventariado.inventarioBien.numero"/></td>
      			<td  class="textoCampo">Fecha: </td>
				<td class="textoDato">
					<@s.if test="transferenciaBienDetalle.bienInventariado.inventarioBien.fecha != null">
						<#assign fecha = transferenciaBienDetalle.bienInventariado.inventarioBien.fecha>
						${fecha?string("dd/MM/yyyy")}	
					</@s.if>									
				</td>
			</tr>	
			<tr>
				<td class="textoCampo">Tipo Inventario:</td>
      			<td class="textoDato" colspan="3"><@s.property default="&nbsp;" escape=false value="transferenciaBienDetalle.bienInventariado.inventarioBien.tipoInventario"/></td>
    		</tr>
    		<tr>
				<td class="textoCampo">Observaciones:</td>
      			<td  class="textoDato" colspan="3"><@s.property default="&nbsp;" escape=false value="transferenciaBienDetalle.bienInventariado.inventarioBien.observaciones"/>&nbsp;</td>					
    		</tr>
    		<tr>
				<td class="textoCampo">Estado:</td>
      			<td  class="textoDato" colspan="3"><@s.property default="&nbsp;" escape=false value="transferenciaBienDetalle.bienInventariado.inventarioBien.estado"/></td>
			</tr>
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
		</table>
	</div>	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos del Bien</b></td>
			</tr>
		</table>
	
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>					
			<tr>
      			<td class="textoCampo">C&oacute;digo:</td>
      			<td class="textoDato"><@s.property default="&nbsp;" escape=false value="transferenciaBienDetalle.bienInventariado.bien.oid"/></td>
      			<td class="textoCampo">Descripci&oacute;n: </td>
				<td class="textoDato"><@s.property default="&nbsp;" escape=false value="transferenciaBienDetalle.bienInventariado.bien.descripcion"/></td>	      			
			</tr>	
			<tr>
				<td class="textoCampo">Marca:</td>
      			<td class="textoDato">
  					<@s.property default="&nbsp;" escape=false value="transferenciaBienDetalle.bienInventariado.bien.marca"/>
				</td>
				<td class="textoCampo">Modelo:</td>
      			<td  class="textoDato">
	      			<@s.property default="&nbsp;" escape=false value="transferenciaBienDetalle.bienInventariado.bien.modelo"/>
				</td>
    		</tr>
			<tr>
				<td class="textoCampo">Valor Mercado:</td>
      			<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="transferenciaBienDetalle.bienInventariado.bien.valorMercado"/></td>
				</td>

				<td class="textoCampo">Fecha Valor Mercado:</td>
      			<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="transferenciaBienDetalle.bienInventariado.bien.fechaValorMercado"/></td>
				</td>
    		</tr>
			<tr>
				<td class="textoCampo">Rubro:</td>
      			<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="transferenciaBienDetalle.bienInventariado.bien.rubro.descripcion"/></td>
				</td>

				<td class="textoCampo">Es cr&iacute;tico:</td>
      			<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="transferenciaBienDetalle.bienInventariado.bien.critico"/></td>
				</td>
    		</tr>
    		<tr>
				<td class="textoCampo">C&oacute;digo de barras:</td>
      			<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="transferenciaBienDetalle.bienInventariado.bien.codigoBarra"/></td>
				</td>

				<td class="textoCampo">Amortizaci&oacute;n:</td>
      			<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="transferenciaBienDetalle.bienInventariado.bien.amortizacion"/> (%)</td>
				</td>
    		</tr>
    		<tr>
				<td class="textoCampo">Es registrable:</td>
      			<td  class="textoDato" colsapn="3">
					<@s.property default="&nbsp;" escape=false value="transferenciaBienDetalle.bienInventariado.bien.registrable"/></td>
				</td>
				
    		</tr>
    		<tr>
				<td class="textoCampo">Es veh&iacute;culo:</td>
      			<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="transferenciaBienDetalle.bienInventariado.bien.vehiculo"/></td>
				</td>
				<td class="textoCampo">Estado:</td>
      			<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="transferenciaBienDetalle.bienInventariado.bien.estado"/>
				</td>
    		</tr>
    		<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>	
		</table>				
	</div>
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos del Bien Inventariado</b></td>
			</tr>
		</table>
	
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>		
			<tr>
		  		<td class="textoCampo">Inventario:</td>
		  		<td class="textoDato">
		  				<@s.property default="&nbsp;" escape=false value="transferenciaBienDetalle.bienInventariado.numeroInventario"/>
		  		</td>
		  		<td class="textoCampo">Fecha Alta:</td>
				<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="transferenciaBienDetalle.bienInventariado.fechaAlta"/>
				</td>	      			
			</tr>	
			<tr>
				<td class="textoCampo">N&uacute;mero de Factura:</td>
		  		<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="transferenciaBienDetalle.bienInventariado.numeroFactura"/>
				</td>
				<td class="textoCampo">Valor Adquisici&oacute;n: </td>
				<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="transferenciaBienDetalle.bienInventariado.valorAdquisicion"/>
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Valor Amortizado: </td>
				<td class="textoDato" colspan="3">
					<@s.property default="&nbsp;" escape=false value="transferenciaBienDetalle.bienInventariado.valorAmortizado"/>
				</td>
			</tr>
			<tr>
		  		<td class="textoCampo">N&uacute;mero de serie: </td>
				<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="transferenciaBienDetalle.bienInventariado.numeroSerie"/>
					&nbsp;
				</td>
				<td class="textoCampo" >Cliente:</td>
				<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="transferenciaBienDetalle.bienInventariado.cliente.descripcion"/>
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Estado:</td>
				<td class="textoDato" colspan="3">
					<@s.property default="&nbsp;" escape=false value="transferenciaBienDetalle.bienInventariado.estado"/>
				</td>
			</tr>
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
		</table>
	</div>
