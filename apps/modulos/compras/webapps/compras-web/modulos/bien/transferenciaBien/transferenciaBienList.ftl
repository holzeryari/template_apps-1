
<#setting locale="es_AR">

<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>

<@s.hidden id="navigationId" name="navigationId"/>
<@s.hidden id="transferenciaBien.clienteOrigen.oid" name="transferenciaBien.clienteOrigen.oid"/>
<@s.hidden id="transferenciaBien.clienteOrigen.descripcion" name="transferenciaBien.clienteOrigen.descripcion"/>
<@s.hidden id="transferenciaBien.clienteDestino.oid" name="transferenciaBien.clienteDestino.oid"/>
<@s.hidden id="transferenciaBien.clienteDestino.descripcion" name="transferenciaBien.clienteDestino.descripcion"/>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
        	<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Datos de la Transferencia de Bienes</b></td>
				<td>
					<div align="right">
						<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0261" 
								enabled="transferenciaBien.readable" 
								cssClass="item" 
								id="crear"										
								href="javascript://nop/">
								<b >Agregar</b>										
								<img src="${request.contextPath}/common/images/agregar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
							</@security.a>					
					</div>
				</td>
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
	    		<td class="textoCampo" colspan="4">&nbsp;</td>
	    	</tr>		
			<tr>
	  			<td class="textoCampo">N&uacute;mero:</td>
	      		<td class="textoDato">
	      			<@s.textfield 
	      				templateDir="custontemplates" 
						id="transferenciaBien.numero" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="transferenciaBien.numero" 
						title="N&uacute;mero"/>
				</td>							
				<td class="textoCampo">Estado:</td>
      			<td class="textoDato">
	      			<@s.select 
						templateDir="custontemplates" 
						id="estado" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="estado" 
						list="estadoList" 
						listKey="key" 
						listValue="description" 
						value="transferenciaBien.estado.ordinal()"
						title="Estado"
						headerKey="0"
						headerValue="Todos"/>
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Cliente Origen:</td>
      			<td class="textoDato" align="left">
					<@s.property default="&nbsp;" escape=false value="transferenciaBien.clienteOrigen.descripcion"/>
					<@s.a templateDir="custontemplates" id="seleccionarClienteOrigen" name="seleccionarClienteOrigen" href="javascript://nop/">
						<img src="${request.contextPath}/common/images/buscar.gif"  border="0" align="absmiddle" hspace="3" >
					</@s.a>
				</td>
				<td class="textoCampo">Cliente Destino:</td>
      			<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="transferenciaBien.clienteDestino.descripcion"/>
					<@s.a templateDir="custontemplates" id="seleccionarClienteDestino" name="seleccionarClienteDestino" href="javascript://nop/">
						<img src="${request.contextPath}/common/images/buscar.gif" border="0" align="absmiddle" hspace="3" >
					</@s.a>
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Fecha Desde:</td>
      			<td class="textoDato" align="left" >
					<@vc.rowCalendar 
						templateDir="custontemplates" 
						id="fechaDesde" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="fechaDesde" 
						title="Fecha Desde" />
				</td>
				<td class="textoCampo">Fecha Hasta:</td>
      			<td class="textoDato">
					<@vc.rowCalendar 
						templateDir="custontemplates" 
						id="fechaHasta" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="fechaHasta" 
						title="Fecha Hasta" />
				</td>
			</tr>
				<tr>
	    		<td colspan="4" class="lineaGris" align="right">
	      			<input id="limpiar" type="button" name="btnVolver" value="Limpiar" class="boton"/>		
	      			<input id="buscar" type="button" name="btnVolver" value="Buscar" class="boton"/>
	    		</td>
			</tr>	
		</table>
	</div>	
	<!-- Resultado Filtro -->
	<@s.if test="transferenciaBienList!=null">

		<#assign transferenciaBienNumero = "0" />
		<@s.if test="transferenciaBien.numero != null">
			<#assign transferenciaBienNumero = "${transferenciaBien.numero}" />
		</@s.if>

		<#assign transferenciaBienEstado = "0" />
		<@s.if test="transferenciaBien.estado != null">
				<#assign transferenciaBienEstado = "${transferenciaBien.estado.ordinal()}" />
		</@s.if>

		<#assign transferenciaBienClienteOrigen = "0" />
		<@s.if test="transferenciaBien.clienteOrigen != null && transferenciaBien.clienteOrigen.oid != null">
			<#assign transferenciaBienClienteOrigen = "${transferenciaBien.clienteOrigen.oid}" />
		</@s.if>

		<#assign transferenciaBienClienteDestino = "0" />
		<@s.if test="transferenciaBien.clienteDestino != null && transferenciaBien.clienteDestino.oid != null">
			<#assign transferenciaBienClienteDestino = "${transferenciaBien.clienteDestino.oid}" />
		</@s.if>

		<#assign transferenciaBienFechaDesde = "" />
		<@s.if test="fechaDesde != null">
			<#assign transferenciaBienFechaDesde = "${fechaDesde?string('dd/MM/yyyy')}" />
		</@s.if>

		<#assign transferenciaBienFechaHasta = "" />
		<@s.if test="fechaHasta != null">
			<#assign transferenciaBienFechaHasta = "${fechaHasta?string('dd/MM/yyyy')}" />
		</@s.if>

		<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
				<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Transferencia de Bienes encontrados</td>
					<td>
						<div class="alineacionDerecha">
							<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0266" 
								cssClass="item" 
								href="${request.contextPath}/bien/transferenciaBien/printXLS.action?transferenciaBien.numero=${transferenciaBienNumero}&transferenciaBien.estado=${transferenciaBienEstado}&transferenciaBien.clienteOrigen.oid=${transferenciaBienClienteOrigen}&transferenciaBien.clienteOrigen.descripcion=${transferenciaBien.clienteOrigen.descripcion}&transferenciaBien.clienteDestino.oid=${transferenciaBienClienteDestino}&transferenciaBien.clienteDestino.descripcion=${transferenciaBien.clienteDestino.descripcion}&fechaDesde=${transferenciaBienFechaDesde}&fechaHasta=${transferenciaBienFechaHasta}">
								<img src="${request.contextPath}/common/images/imprimirExcel.gif" title="Exportar a Excel" align="absmiddle" border="0" hspace="3" >
							</@security.a>
						</div>
						<div class="alineacionDerecha">
							<b>Imprimir</b>
							<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0266" 
								cssClass="item" 
								href="${request.contextPath}/bien/transferenciaBien/printPDF.action?transferenciaBien.numero=${transferenciaBienNumero}&transferenciaBien.estado=${transferenciaBienEstado}&transferenciaBien.clienteOrigen.oid=${transferenciaBienClienteOrigen}&transferenciaBien.clienteOrigen.descripcion=${transferenciaBien.clienteOrigen.descripcion}&transferenciaBien.clienteDestino.oid=${transferenciaBienClienteDestino}&transferenciaBien.clienteDestino.descripcion=${transferenciaBien.clienteDestino.descripcion}&fechaDesde=${transferenciaBienFechaDesde}&fechaHasta=${transferenciaBienFechaHasta}">
								<img src="${request.contextPath}/common/images/imprimirPdf.gif" title="Exportar a PDF" align="absmiddle" border="0" hspace="3">
							</@security.a>
						</div>	
					</td>
				</tr>
			</table>
		
			<@vc.anchors target="contentTrx">
	          		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="transferenciaBienList" id="transferenciaBien" pagesize=15 defaultsort=2 partialList=true size="recordSize" keepStatus=true excludedParams="resetFlag">
						<@display.column headerClass="tbl-contract-service-select" class="botoneraAnchoCon4" title="Acciones">
							<div class="alineacion">
								<@security.a 
									templateDir="custontemplates" 
									securityCode="CUF0262" 
									enabled="transferenciaBien.readable" 
									cssClass="item" 
									href="${request.contextPath}/bien/transferenciaBien/readTransferenciaBienView.action?transferenciaBien.oid=${transferenciaBien.oid?c}&navigationId=verTransferenciaBien&flowControl=regis">
									<img src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver" border="0">
								</@security.a>
							</div>
							<div class="alineacion">	
								<@security.a 
									templateDir="custontemplates" 
									securityCode="CUF0263" 
									enabled="transferenciaBien.updatable"
									cssClass="item"  
									href="${request.contextPath}/bien/transferenciaBien/administrarTransferenciaBienView.action?transferenciaBien.oid=${transferenciaBien.oid?c}&navigationId=administrarTransferenciaBien&flowControl=regis">
									<img src="${request.contextPath}/common/images/administrar.gif" alt="Administrar" title="Administrar" border="0">
								</@security.a>
							</div>
							<div class="alineacion">
								<@security.a 
									templateDir="custontemplates" 
									securityCode="CUF0264" 
									enabled="transferenciaBien.eraseable"
									cssClass="item"  
									href="${request.contextPath}/bien/transferenciaBien/deleteTransferenciaBienView.action?transferenciaBien.oid=${transferenciaBien.oid?c}&navigationId=transferenciaBien&flowControl=regis">
									<img src="${request.contextPath}/common/images/eliminar.gif" alt="Eliminar" title="Eliminar" border="0">
								</@security.a>				
							</div>
							<div class="alineacion">
								<@security.a 
									templateDir="custontemplates" 
									securityCode="CUF0265" 
									enabled="transferenciaBien.readable" 
									cssClass="no-rewrite" 
									href="${request.contextPath}/bien/transferenciaBien/printTransferenciaBienPDF.action?transferenciaBien.oid=${transferenciaBien.oid?c}">
									<img src="${request.contextPath}/common/images/imprimir.gif" alt="Imprimir" title="Imprimir" border="0">
								</@security.a>		
							</div>
						</@display.column>

						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="numero" title="N&uacute;mero" />				

						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="fecha" format="{0,date,dd/MM/yyyy}" title="Fecha Transferencia" />

						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="clienteOrigen.descripcion" title="Cliente Origen" />

						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="clienteDestino.descripcion" title="Cliente Destino" />

						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="estado" title="Estado" />		

					</@display.table>
				</@vc.anchors>
				</div>
			</@s.if>
		   

<@vc.htmlContent 
  baseUrl="${request.contextPath}/bien/transferenciaBien/search.action"
  source="buscar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="resetFlag=true,navigationId={navigationId},flowControl=regis,transferenciaBien.numero={transferenciaBien.numero},transferenciaBien.estado={estado},transferenciaBien.clienteOrigen.oid={transferenciaBien.clienteOrigen.oid},transferenciaBien.clienteOrigen.descripcion={transferenciaBien.clienteOrigen.descripcion},transferenciaBien.clienteDestino.oid={transferenciaBien.clienteDestino.oid},transferenciaBien.clienteDestino.descripcion={transferenciaBien.clienteDestino.descripcion},fechaDesde={fechaDesde},fechaHasta={fechaHasta}"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/bien/transferenciaBien/view.action"
  source="limpiar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=regis"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/bien/transferenciaBien/createTransferenciaBienView.action" 
  source="crear" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=abm-transferenciaBien-create,flowControl=regis"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/bien/transferenciaBien/searchSelectClienteOrigen.action" 
  source="seleccionarClienteOrigen" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=change,navegacionIdBack={navigationId},transferenciaBien.numero={transferenciaBien.numero},transferenciaBien.estado={estado},transferenciaBien.clienteOrigen.oid={transferenciaBien.clienteOrigen.oid},transferenciaBien.clienteOrigen.descripcion={transferenciaBien.clienteOrigen.descripcion},transferenciaBien.clienteDestino.oid={transferenciaBien.clienteDestino.oid},transferenciaBien.clienteDestino.descripcion={transferenciaBien.clienteDestino.descripcion},fechaDesde={fechaDesde},fechaHasta={fechaHasta}"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/bien/transferenciaBien/searchSelectClienteDestino.action" 
  source="seleccionarClienteDestino" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=change,navegacionIdBack={navigationId},transferenciaBien.numero={transferenciaBien.numero},transferenciaBien.estado={estado},transferenciaBien.clienteOrigen.oid={transferenciaBien.clienteOrigen.oid},transferenciaBien.clienteOrigen.descripcion={transferenciaBien.clienteOrigen.descripcion},transferenciaBien.clienteDestino.oid={transferenciaBien.clienteDestino.oid},transferenciaBien.clienteDestino.descripcion={transferenciaBien.clienteDestino.descripcion},fechaDesde={fechaDesde},fechaHasta={fechaHasta}"/>
