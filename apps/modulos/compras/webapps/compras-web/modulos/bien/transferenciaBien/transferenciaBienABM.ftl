<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>


<@s.hidden id="webFlow" name="webFlow"/>
<@s.hidden id="viewState" name="viewState"/>

<@s.hidden id="transferenciaBien.oid" name="transferenciaBien.oid"/>
<@s.hidden id="transferenciaBien.estado" name="transferenciaBien.estado.ordinal()"/>
<@s.hidden id="transferenciaBien.versionNumber" name="transferenciaBien.versionNumber"/>
<@s.hidden id="transferenciaBien.clienteOrigen.oid" name="transferenciaBien.clienteOrigen.oid"/>
<@s.hidden id="transferenciaBien.clienteOrigen.descripcion" name="transferenciaBien.clienteOrigen.descripcion"/>
<@s.hidden id="transferenciaBien.clienteDestino.oid" name="transferenciaBien.clienteDestino.oid"/>
<@s.hidden id="transferenciaBien.clienteDestino.descripcion" name="transferenciaBien.clienteDestino.descripcion"/>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
			<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos de la Transferencia de Bienes</b></td>
				<td>
					<div align="right">
						<@s.a templateDir="custontemplates" id="modificarTransferenciaBien" name="modificarTransferenciaBien" href="javascript://nop/" cssClass="ocultarIcono">
							<b>Modificar</b><img src="${request.contextPath}/common/images/modificar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
						</@s.a>
					</div>
				</td>
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>		
			<tr>
      			<td class="textoCampo">N&uacute;mero:</td>
      			<td class="textoDato"><@s.property default="&nbsp;" escape=false value="transferenciaBien.numero"/></td>
      			<td  class="textoCampo">Fecha:</td>
				<td class="textoDato">
					<@s.if test="transferenciaBien.fecha != null">
						<#assign fechaTransferencia = transferenciaBien.fecha>
						${fechaTransferencia?string("dd/MM/yyyy")}
					</@s.if>&nbsp;
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Cliente Origen:</td>
      			<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="transferenciaBien.clienteOrigen.descripcion"/>
					<@s.a templateDir="custontemplates" id="seleccionarClienteOrigen" name="seleccionarClienteOrigen" href="javascript://nop/" cssClass="ocultarIcono">
						<img src="${request.contextPath}/common/images/buscar.gif" border="0" align="absmiddle" hspace="3" >
					</@s.a>
				</td>
				<td class="textoCampo">Cliente Destino:</td>
      			<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="transferenciaBien.clienteDestino.descripcion"/>
					<@s.a templateDir="custontemplates" id="seleccionarClienteDestino" name="seleccionarClienteDestino" href="javascript://nop/" cssClass="ocultarIcono">
						<img src="${request.contextPath}/common/images/buscar.gif"  border="0" align="absmiddle" hspace="3" >
					</@s.a>
				</td>
			</tr>
    		<tr>
				<td class="textoCampo">Observaciones:</td>
      			<td class="textoDato" colspan="3">
					<@s.textarea
						templateDir="custontemplates"
						cols="89" rows="2"
						cssClass="textarea"
						id="transferenciaBien.observaciones"
						name="transferenciaBien.observaciones"
						label="Observaciones"/>
				</td>
    		</tr>
    		<tr>
				<td class="textoCampo">Estado:</td>
      			<td  class="textoDato" colspan="3">
					<@s.property default="&nbsp;" escape=false value="transferenciaBien.estado"/></td>
				</td>
	 		</tr>
	 		<tr><td colspan="4" class="textoCampo">&nbsp;</td>
		</table>		
	</div>	
<@vc.htmlContent 
  baseUrl="${request.contextPath}/bien/transferenciaBien/updateTransferenciaBienView.action" 
  source="modificarTransferenciaBien" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId=administrar-updateDetalle,flowControl=regis,transferenciaBien.oid={transferenciaBien.oid},transferenciaBien.versionNumber={transferenciaBien.versionNumber}"/>

