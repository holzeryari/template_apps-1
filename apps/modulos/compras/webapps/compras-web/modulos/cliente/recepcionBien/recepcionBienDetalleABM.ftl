<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<@s.hidden id="recepcionProductoBienDetalle.oid" name="recepcionProductoBienDetalle.oid"/>
<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
			<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos de la Recepci&oacute;n del Bien</b></td>
			</tr>
		</table>
	
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
			<tr>
      			<td class="textoCampo">N&uacute;mero:</td>
      			<td class="textoDato">
      			<@s.property default="&nbsp;" escape=false value="recepcionProductoBienDetalle.recepcionProductoBien.numero"/></td>
      			<td  class="textoCampo">Fecha: </td>
				<td class="textoDato">
				<@s.if test="recepcionProductoBienDetalle.recepcionProductoBien.fechaComprobante != null">
				<#assign fecha = recepcionProductoBienDetalle.recepcionProductoBien.fechaComprobante> 
				${fecha?string("dd/MM/yyyy")}	
				</@s.if>				&nbsp;					
				</td>
    	    </tr>	

			<tr>
	      		<td class="textoCampo">Proveedor</td>
				<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="recepcionProductoBienDetalle.recepcionProductoBien.proveedor.detalleDePersona.razonSocial" />
				<@s.property default="&nbsp;" escape=false value="recepcionProductoBienDetalle.recepcionProductoBien.proveedor.detalleDePersona.nombre" />
				</td>
				<td  class="textoCampo">Fecha Recepci&oacute;n: </td>
				<td class="textoDato">
				<@s.if test="recepcionProductoBienDetalle.recepcionProductoBien.fechaRecepcion != null">
				<#assign fechaRecepcion = recepcionProductoBienDetalle.recepcionProductoBien.fechaRecepcion> 
				${fechaRecepcion?string("dd/MM/yyyy")}	
				</@s.if>				&nbsp;					
				</td>	
    		</tr>
	    		
    		<tr>
      			<td class="textoCampo">Dep&oacute;sito</td>
      			<td class="textoDato">
      				<@s.property default="&nbsp;" escape=false value="recepcionProductoBienDetalle.recepcionProductoBien.deposito.descripcion" />
				</td>
               <td class="textoCampo">Cliente:</td>
				<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="recepcionProductoBienDetalle.recepcionProductoBien.cliente.descripcion" />				
				</td>	
			</tr>
				
			<tr>
  				<td class="textoCampo">Nro. Factura:</td>
      			<td class="textoDato">
      			<@s.property default="&nbsp;" escape=false value="recepcionProductoBienDetalle.recepcionProductoBien.numeroFactura" />
      			
				</td>	
				<td class="textoCampo">Nro. Remito:</td>
      			<td class="textoDato">
      			<@s.property default="&nbsp;" escape=false value="recepcionProductoBienDetalle.recepcionProductoBien.numeroRemito" />
      			</td>
			</tr>		

    		<tr>
				<td class="textoCampo">Observaciones:</td>
      			<td  class="textoDato"colspan="3">
      			<@s.property default="&nbsp;" escape=false value="recepcionProductoBienDetalle.recepcionProductoBien.observaciones" />
				</td>					
    		</tr>
    		
    		<tr>
				<td class="textoCampo">Estado:</td>
      			<td  class="textoDato" colspan="3">
					<@s.property default="&nbsp;" escape=false value="recepcionProductoBienDetalle.recepcionProductoBien.estado"/></td>
				</td>
			</tr>	    	
    		<tr><td class="textoCampo" colspan="4">&nbsp;</td></tr>
		</table>		    			
	</div>
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos del Producto/Bien</b></td>
			</tr>
		</table>
	
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
	
			<tr>
      			<td class="textoCampo">C&oacute;digo:</td>
      			<td class="textoDato">
      			<@s.property default="&nbsp;" escape=false value="recepcionProductoBienDetalle.productoBien.oid"/>
      			</td>
      			<td  class="textoCampo">Descripci&oacute;n: </td>
				<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="recepcionProductoBienDetalle.productoBien.descripcion"/>		
				</td>	      			
			</tr>	
			<tr>
      			<td class="textoCampo">Tipo:</td>
	      		<td class="textoDato">
	      			<@s.property default="&nbsp;" escape=false value="recepcionProductoBienDetalle.productoBien.tipo"/>						      			
	      		</td>		      		
										
			<td class="textoCampo">Rubro:</td>
  			<td class="textoDato">
  				<@s.property default="&nbsp;" escape=false value="recepcionProductoBienDetalle.productoBien.rubro.descripcion"/>
			</td>
    	</tr>
		<tr>
			<td class="textoCampo">Marca: </td>
			<td  class="textoDato">
				<@s.property default="&nbsp;" escape=false value="recepcionProductoBienDetalle.productoBien.marca"/>										
			</td>
			<td class="textoCampo">Modelo: </td>
			<td  class="textoDato">
				<@s.property default="&nbsp;" escape=false value="recepcionProductoBienDetalle.productoBien.modelo"/>										
			</td>
		</tr>
		<tr>
			<td class="textoCampo">Valor Mercado: </td>
			<td  class="textoDato">
				<@s.property default="&nbsp;" escape=false value="bien.valorMercado"/>
			</td>
			<td class="textoCampo">Fecha Valor Mercado: </td>
			<td  class="textoDato">
				<@s.property default="&nbsp;" escape=false value="bien.fechaValorMercado"/>
			</td>
		</tr>
		<tr>
			<td class="textoCampo">C&oacute;digo de Barra: </td>
			<td  class="textoDato">
				<@s.property default="&nbsp;" escape=false value="recepcionProductoBienDetalle.productoBien.codigoBarra"/>										
			</td>
			<td class="textoCampo">Es Cr&iacute;tico: </td>
			<td  class="textoDato">
				<@s.property default="&nbsp;" escape=false value="recepcionProductoBienDetalle.productoBien.critico"/>						      			
			</td>
		</tr>
		<tr>
			<td class="textoCampo">Reposici&oacute;n Autm&aacute;tica: </td>
			<td  class="textoDato">	
				<@s.property default="&nbsp;" escape=false value="producto.reposicionAutomatica"/>						      											
			</td>
			<td class="textoCampo">Existencia M&iacute;nima: </td>
			<td  class="textoDato">
				<@s.property default="&nbsp;" escape=false value="producto.existenciaMinima"/>
			</td>
		</tr>
		<tr>
			<td class="textoCampo">Tipo Unidad:</td>
			<td  class="textoDato" colspan="3">
				<@s.property default="&nbsp;" escape=false value="producto.tipoUnidad.codigo"/>
			</td>
		</tr>
		<tr>
			<td class="textoCampo">Es Registrable: </td>
			<td  class="textoDato" colspan="3">
			<@s.property default="&nbsp;" escape=false value="bien.registrable"/>						      			
			</td>
			
		</tr>
		<tr>
			<td class="textoCampo">Es Veh&iacute;culo: </td>
			<td  class="textoDato"colspan="3">
				<@s.property default="&nbsp;" escape=false value="bien.vehiculo"/>
			</td>
		</tr>							
		<tr>
			<td class="textoCampo">Estado: </td>
  			<td class="textoDato" colspan="3">
  			<@s.property default="&nbsp;" escape=false value="productoBien.estado"/></td>
		</tr>
		<tr>
			<td class="textoCampo">Cantidad Pedida: </td>
			<td  class="textoDato" colspan="3">
				<@s.property default="&nbsp;" escape=false value="recepcionProductoBienDetalle.cantidadPedida"/>
			</td>
		</tr>							
		<tr>
			<td class="textoCampo">Cantidad Pendiente: </td>
  			<td class="textoDato" colspan="3">
  			<@s.property default="&nbsp;" escape=false value="recepcionProductoBienDetalle.cantidadPendiente"/></td>
		</tr>
		<tr>
			<td class="textoCampo">Cantidad a Recibir: </td>
			<td  class="textoDato" colspan="3">
				<@s.textfield									
					templateDir="custontemplates"
					template="textMoney"  
					id="recepcionProductoBienDetalle.cantidadRecibida" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="recepcionProductoBienDetalle.cantidadRecibida" 
					title="Cantidad Recibida" />							      			
			</td>
		</tr>
		<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>
	</table>
	</div>
			
					
			
		</td>
	</tr>
</table> 
