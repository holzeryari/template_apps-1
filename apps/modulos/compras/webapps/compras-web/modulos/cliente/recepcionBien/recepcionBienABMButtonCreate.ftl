<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancel" type="button" name="btnVolver" value="Cancelar" class="boton"/>
				<input id="store" type="button" name="btnAgregar" value="Agregar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>
<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=buscar-recepcionBien,flowControl=back"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/cliente/recepcionBien/create.action" 
  source="store" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="recepcionProductoBien.deposito.oid={recepcionProductoBien.deposito.oid},recepcionProductoBien.proveedor.nroProveedor={recepcionProductoBien.proveedor.nroProveedor},recepcionProductoBien.proveedor.razonSocial={recepcionProductoBien.proveedor.razonSocial},recepcionProductoBien.proveedor.nombre={recepcionProductoBien.proveedor.nombre},recepcionProductoBien.numeroFactura={recepcionProductoBien.numeroFactura},recepcionProductoBien.numeroRemito={recepcionProductoBien.numeroRemito},recepcionProductoBien.observaciones={recepcionProductoBien.observaciones},recepcionProductoBien.fechaRecepcion={recepcionProductoBien.fechaRecepcion},recepcionProductoBien.cliente.oid={recepcionProductoBien.cliente.oid}"/>
  
  
  <@vc.htmlContent 
  baseUrl="${request.contextPath}/cliente/recepcionBien/selectProveedorABM.action" 
  source="seleccionarProveedor" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=change,recepcionProductoBien.cliente.oid={recepcionProductoBien.cliente.oid},recepcionProductoBien.cliente.descripcion={recepcionProductoBien.cliente.descripcion},recepcionProductoBien.deposito.oid={recepcionProductoBien.deposito.oid},recepcionProductoBien.proveedor.nroProveedor={recepcionProductoBien.proveedor.nroProveedor},recepcionProductoBien.proveedor.razonSocial={recepcionProductoBien.proveedor.razonSocial},recepcionProductoBien.proveedor.nombre={recepcionProductoBien.proveedor.nombre},recepcionProductoBien.numeroFactura={recepcionProductoBien.numeroFactura},recepcionProductoBien.numeroRemito={recepcionProductoBien.numeroRemito},recepcionProductoBien.observaciones={recepcionProductoBien.observaciones},recepcionProductoBien.fechaRecepcion={recepcionProductoBien.fechaRecepcion}"/>
  
  <@vc.htmlContent 
  baseUrl="${request.contextPath}/cliente/recepcionBien/selectClienteABM.action" 
  source="seleccionarCliente" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=change,recepcionProductoBien.cliente.oid={recepcionProductoBien.cliente.oid},recepcionProductoBien.cliente.descripcion={recepcionProductoBien.cliente.descripcion},recepcionProductoBien.deposito.oid={recepcionProductoBien.deposito.oid},recepcionProductoBien.proveedor.nroProveedor={recepcionProductoBien.proveedor.nroProveedor},recepcionProductoBien.proveedor.razonSocial={recepcionProductoBien.proveedor.razonSocial},recepcionProductoBien.proveedor.nombre={recepcionProductoBien.proveedor.nombre},recepcionProductoBien.numeroFactura={recepcionProductoBien.numeroFactura},recepcionProductoBien.numeroRemito={recepcionProductoBien.numeroRemito},recepcionProductoBien.observaciones={recepcionProductoBien.observaciones},recepcionProductoBien.fechaRecepcion={recepcionProductoBien.fechaRecepcion}"/>
  

  