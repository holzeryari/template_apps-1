<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<@s.hidden id="fsc_fssDetalle.oid" name="fsc_fssDetalle.oid"/>
<@s.hidden id="fsc_fssDetalle.servicio.oid" name="fsc_fssDetalle.servicio.oid"/>
<@s.hidden id="fsc_fssDetalle.productoBien.oid" name="fsc_fssDetalle.productoBien.oid"/>
<@s.hidden id="fsc_fssDetalle.fsc_fss.oid" name="fsc_fssDetalle.fsc_fss.oid"/>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
			<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos del FSC/FSS</b></td>
			</tr>
		</table>
	
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
			<tr>
      			<td class="textoCampo">N&uacute;mero:</td>
      			<td class="textoDato"><@s.property default="&nbsp;" escape=false value="fsc_fssDetalle.fsc_fss.numero"/></td>
      			<td  class="textoCampo" >Fecha Solicitud: </td>
				<td class="textoDato">
					<@s.if test="fsc_fssDetalle.fsc_fss.fechaSolicitud != null">
					<#assign fechaSolicitud = fsc_fssDetalle.fsc_fss.fechaSolicitud> 
					${fechaSolicitud?string("dd/MM/yyyy")}	
					</@s.if>									
				</td>	      			
			</tr>	
			<tr>
				<td class="textoCampo">Cliente:</td>
      			<td class="textoDato">
      				<@s.property default="&nbsp;" escape=false value="fsc_fssDetalle.fsc_fss.cliente.descripcion"/>	      				
				</td>
				<td class="textoCampo">Tipo:</td>
      			<td class="textoDato">
      				<@s.property default="&nbsp;" escape=false value="fsc_fssDetalle.fsc_fss.tipoFormulario"/>	      	
      			</td>
    		</tr>
	    	<tr>
				<td class="textoCampo">Fecha Entrega Estimada:</td>
      			<td class="textoDato">
      				<@s.if test="fsc_fssDetalle.fsc_fss.fechaEntregaEstimada != null">
						<#assign fechaEntregaEstimada = fsc_fssDetalle.fsc_fss.fechaEntregaEstimada> 
						${fechaEntregaEstimada?string("dd/MM/yyyy")}	
					</@s.if>
				</td>						
      			<td class="textoCampo">Monto Estimado:</td>
      			<td class="textoDato">
      				<@s.property default="&nbsp;" escape=false value="fsc_fssDetalle.fsc_fss.montoEstimado"/>
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Insumo, equipamiento y/o servicio solicitado:</td>
      			<td  class="textoDato" colspan="3">
      				<@s.property default="&nbsp;" escape=false value="fsc_fssDetalle.fsc_fss.descripcionSolicitud"/>
					&nbsp;	
				</td>					
    		</tr>
	    	<tr>
				<td class="textoCampo">Normas y/o especificaciones:</td>
      			<td  class="textoDato"colspan="3">
      				<@s.property default="&nbsp;" escape=false value="fsc_fssDetalle.fsc_fss.normaEspecificacion"/>
      				&nbsp;	
				</td>					
    		</tr>
	    	<tr>
				<td class="textoCampo">Requisitos de la calidad del proveedor:</td>
      			<td  class="textoDato" colspan="3">
      				<@s.property default="&nbsp;" escape=false value="fsc_fssDetalle.fsc_fss.requisitosCalidadProveedor"/>
      				&nbsp;
				</td>					
    		</tr>
			<tr>
				<td class="textoCampo" >Generado Automaticamente:</td>
      			<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="fsc_fssDetalle.fsc_fss.generadoAutomaticamente"/></td>
				</td>
				<td class="textoCampo">Intervenido Automaticamente:</td>
      			<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="fsc_fssDetalle.fsc_fss.intervenidoAutomaticamente"/></td>
				</td>
    		</tr>
	    	<tr>
				<td class="textoCampo">Estado:</td>
	      		<td class="textoDato" colspan="3">
					<@s.property default="&nbsp;" escape=false value="fsc_fssDetalle.fsc_fss.estado"/></td>
				</td>
			</tr>
	    	<tr><td class="textoCampo" colspan="4">&nbsp;</td></tr>
		</table>	
	</div>	
	<@s.if test="fsc_fssDetalle.productoBien != null">
			<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
				<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            		<tr>
						<td><b>Datos del Producto/Bien</b></td>
					</tr>
				</table>
	
				<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
					<tr>
        				<td class="textoCampo" colspan="4">&nbsp;</td>
        			</tr>
					<tr>
		      			<td class="textoCampo">C&oacute;digo:</td>
		      			<td class="textoDato">
		      				<@s.property default="&nbsp;" escape=false value="fsc_fssDetalle.productoBien.oid"/>
		      			</td>
		      			<td  class="textoCampo">Descripci&oacute;n: </td>
						<td class="textoDato">
							<@s.property default="&nbsp;" escape=false value="fsc_fssDetalle.productoBien.descripcion"/>		
						</td>	      			
					</tr>	
					<tr>
		      			<td class="textoCampo">Tipo:</td>
			      		<td class="textoDato">
			      			<@s.property default="&nbsp;" escape=false value="fsc_fssDetalle.productoBien.tipo"/>						      			
			      		</td>		      		
												
						<td class="textoCampo">Rubro:</td>
	      				<td class="textoDato">
	      					<@s.property default="&nbsp;" escape=false value="fsc_fssDetalle.productoBien.rubro.descripcion"/>
						</td>
		    		</tr>
					<tr>
						<td class="textoCampo">Marca: </td>
						<td  class="textoDato">
							<@s.property default="&nbsp;" escape=false value="fsc_fssDetalle.productoBien.marca"/>&nbsp;										
						</td>
						<td class="textoCampo">Modelo: </td>
						<td  class="textoDato">
							<@s.property default="&nbsp;" escape=false value="fsc_fssDetalle.productoBien.modelo"/>&nbsp;										
						</td>
		    		</tr>
					<tr>
						<td class="textoCampo">Valor Mercado: </td>
						<td  class="textoDato">
							<@s.property default="&nbsp;" escape=false value="fsc_fssDetalle.productoBien.valorMercado"/>
						</td>
						<td class="textoCampo">Fecha Valor Mercado: </td>
						<td  class="textoDato">
							<@s.if test="fsc_fssDetalle.productoBien.fechaValorMercado != null">
								<#assign fechaValorMercado = fsc_fssDetalle.productoBien.fechaValorMercado> 
								${fechaValorMercado?string("dd/MM/yyyy")}	
							</@s.if>
							<@s.else>
								&nbsp;
							</@s.else>			
						</td>
		    		</tr>
					<tr>
						<td class="textoCampo">C&oacute;digo de Barra: </td>
						<td  class="textoDato">
							<@s.property default="&nbsp;" escape=false value="fsc_fssDetalle.productoBien.codigoBarra"/>										
						</td>
						<td class="textoCampo">Es Cr&iacute;tico: </td>
						<td  class="textoDato">
							<@s.property default="&nbsp;" escape=false value="fsc_fssDetalle.productoBien.critico"/>						      			
						</td>
		    		</tr>
					<tr>
						<td class="textoCampo" >Reposici&oacute;n Autm&aacute;tica: </td>
						<td  class="textoDato">	
							<@s.property default="&nbsp;" escape=false value="producto.reposicionAutomatica"/>						      											
						</td>
						<td class="textoCampo">Existencia M&iacute;nima: </td>
						<td  class="textoDato" align="left">
							<@s.property default="&nbsp;" escape=false value="producto.existenciaMinima"/>
						</td>
		    		</tr>
					<tr>
			    		<td class="textoCampo">Tipo Unidad:</td>
			    		<td  class="textoDato"colspan="3">
								<@s.property default="&nbsp;" escape=false value="producto.tipoUnidad.codigo"/>
						</td>
			    	</tr>
					<tr>
						<td class="textoCampo">Es Registrable: </td>
						<td  class="textoDato" colspan="3">
							<@s.property default="&nbsp;" escape=false value="bien.registrable"/>						      			
						</td>
					</tr>
					<tr>
						<td class="textoCampo">Es Veh&iacute;culo: </td>
						<td  class="textoDato">
							<@s.property default="&nbsp;" escape=false value="bien.vehiculo"/>
						</td>
						<td class="textoCampo">Amortizaci&oacute;n (%): </td>
						<td  class="textoDato">
							<@s.property default="&nbsp;" escape=false value="bien.amortizacion"/>										
						</td>
		    		</tr>							
					<tr>
						<td class="textoCampo">Estado: </td>
					    <td class="textoDato" colspan="3">
					    	<@s.property default="&nbsp;" escape=false value="fsc_fssDetalle.productoBien.estado"/></td>
					</tr>
					<tr>
						<td class="textoCampo">Cantidad Solicitada: </td>
						<td  class="textoDato" colspan="3">
							<@s.textfield									
								templateDir="custontemplates"
								template="textMoney"  
								id="fsc_fssDetalle.cantidadSolicitada" 
								cssClass="textarea"
								cssStyle="width:160px" 
								name="fsc_fssDetalle.cantidadSolicitada" 
								title="Cantidad Solicitada" />							      			
						</td>
					</tr>
					<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>
				</table>
				</div>
			</@s.if>
			
			<@s.if test="fsc_fssDetalle.servicio != null">
				<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
					<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            			<tr>
							<td><b>Datos del Servicio</b></td>
						</tr>
					</table>
	
					<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
						<tr>
        					<td class="textoCampo" colspan="4">&nbsp;</td>
        				</tr>
						<tr>
			      			<td class="textoCampo">C&oacute;digo:</td>
			      			<td class="textoDato">
				      			<@s.property default="&nbsp;" escape=false value="fsc_fssDetalle.servicio.oid"/>			      			
			      			<td class="textoCampo">Descripci&oacute;n</td>
							<td class="textoDato">
								<@s.property default="&nbsp;" escape=false value="fsc_fssDetalle.servicio.descripcion"/>			
						</tr>	
						<tr>
							<td class="textoCampo">Rubro:</td>
				      		<td class="textoDato">
				      			<@s.property default="&nbsp;" escape=false value="fsc_fssDetalle.servicio.rubro.descripcion"/>
							</td>
							<td class="textoCampo">Cr&iacute;tico: </td>
							<td class="textoDato">
								<@s.property default="&nbsp;" escape=false value="fsc_fssDetalle.servicio.critico"/>								
							</td>
						</tr>
						<tr>
			      			<td class="textoCampo">Proveedor &Uacute;nico:</td>
							<td class="textoDato">
								<@s.property default="&nbsp;" escape=false value="fsc_fssDetalle.servicio.proveedorUnico"/>
							</td>
							<td class="textoCampo">Proveedor:</td>
							<td class="textoDato">
								<@s.property default="&nbsp;" escape=false value="fsc_fssDetalle.servicio.proveedor.detalleDePersona.razonSocial" />
								<@s.property default="&nbsp;" escape=false value="pfsc_fssDetalle.servicio.proveedor.detalleDePersona.nombre" />
							</td>
			    		</tr>
			    		<tr>			
							<td class="textoCampo">Tipo:</td>
				      		<td class="textoDato">
				     			<@s.property default="&nbsp;" escape=false value="fsc_fssDetalle.servicio.tipo"/>
				      		</td>				      		
							<td class="textoCampo">Estado:</td>
							<td class="textoDato" colspan="3">
								<@s.property default="&nbsp;" escape=false value="fsc_fssDetalle.servicio.estado"/>
							</td>
						</tr>
						<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>	
					</table>
				</div>	
			</@s.if>	
		