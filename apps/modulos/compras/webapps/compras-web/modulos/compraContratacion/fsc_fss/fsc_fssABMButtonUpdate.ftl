<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancelar" type="button" name="btnCancelar" value="Cancelar" class="boton"/>
				<input id="update" type="button" name="btnModificar" value="Modificar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/compraContratacion/fsc_fss/updateFSC_FSS.action" 
  source="update" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="fsc_fss.versionNumber={fsc_fss.versionNumber},fsc_fss.oid={fsc_fss.oid},fsc_fss.fechaEntregaEstimada={fsc_fss.fechaEntregaEstimada},fsc_fss.montoEstimado={fsc_fss.montoEstimado},fsc_fss.descripcionSolicitud={fsc_fss.descripcionSolicitud},fsc_fss.normaEspecificacion={fsc_fss.normaEspecificacion},fsc_fss.requisitosCalidadProveedor={fsc_fss.requisitosCalidadProveedor}"/>
  

<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancelar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=administrar,flowControl=back"/>
  
  