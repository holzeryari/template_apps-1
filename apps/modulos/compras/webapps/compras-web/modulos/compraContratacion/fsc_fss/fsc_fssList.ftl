
<#setting locale="es_AR">

<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>

<@s.hidden id="navigationId" name="navigationId"/>
<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
        	<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Datos del FSC/FSS</b></td>
				<td>
					<div align="right">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0321" 
							enabled="true" 
							cssClass="item" 
							id="crear"										
							href="javascript://nop/">
							<b>Agregar</b>										
							<img src="${request.contextPath}/common/images/agregar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
						</@security.a>						
					</div>
				</td>
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
	    		<td class="textoCampo" colspan="4">&nbsp;</td>
	    	</tr>	
	    	<tr>
				<td class="textoCampo">Producto/Bien:</td>
				<td class="textoDato">
				<@s.hidden id="productoBien.oid" name="productoBien.oid"/>
				<@s.hidden id="productoBien.descripcion" name="productoBien.descripcion"/>
				<@s.property default="&nbsp;" escape=false value="productoBien.descripcion"/>					
					<@s.a href="javascript://nop/"
						templateDir="custontemplates" id="seleccionarProductoBien" name="seleccionarProductoBien" cssClass="ocultarIcono">
						<img src="${request.contextPath}/common/images/buscar.gif" border="0" align="absmiddle" hspace="3" >
					</@s.a>	
				</td>
				<td class="textoCampo" align="right">&nbsp;</td>
				<td class="textoDato" align="left">&nbsp;</td>
			</tr>
				
	    	<tr>
				<td class="textoCampo">N&uacute;mero:</td>
	      		<td class="textoDato">
	      			<@s.textfield 
      					templateDir="custontemplates" 
						id="fsc_fss.numero" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="fsc_fss.numero" 
						title="N&uacute;mero" />
					</td>							
	      		
					<@tiles.insertAttribute name="cliente"/>		
					
				</tr>
				<tr>
					<td class="textoCampo">Estado:</td>
	      			<td class="textoDato">
	      			<@s.select 
						templateDir="custontemplates" 
						id="fsc_fss.estado" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="fsc_fss.estado" 
						list="estadoList" 
						listKey="key" 
						listValue="description" 
						value="fsc_fss.estado.ordinal()"
						title="Estado"
						headerKey="0"
						headerValue="Todos"							
						/>
					</td>
					<td class="textoCampo">Tipo:</td>
	      			<td class="textoDato">
	      			<@s.select 
						templateDir="custontemplates" 
						id="fsc_fss.tipoFormulario" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="fsc_fss.tipoFormulario" 
						list="tipoFormularioList" 
						listKey="key" 
						listValue="description" 
						value="fsc_fss.tipoFormulario.ordinal()"
						title="Tipo Formulario"
						headerKey="0"
						headerValue="Todos"							
						/>
					</td>
				</tr>
				<tr>
					<td class="textoCampo">Intervenido Automaticamente:</td>
	      			<td class="textoDato">
	      				<@s.select 
								templateDir="custontemplates" 
								id="fsc_fss.intervenidoAutomaticamente" 
								cssClass="textarea"
								cssStyle="width:165px" 
								name="fsc_fss.intervenidoAutomaticamente" 
								list="intervenidoAutomaticamenteList" 
								listKey="key" 
								listValue="description" 
								value="fsc_fss.intervenidoAutomaticamente.ordinal()"
								title="Intervenido Automaticamente"
								headerKey="0"
								headerValue="Todos"							
								/>
					</td>
					<td class="textoCampo">Generado Automaticamente:</td>
	      			<td class="textoDato">
	      				<@s.select 
							templateDir="custontemplates" 
							id="fsc_fss.generadoAutomaticamente" 
							cssClass="textarea"
							cssStyle="width:165px" 
							name="fsc_fss.generadoAutomaticamente" 
							list="generadoAutomaticamenteList" 
							listKey="key" 
							listValue="description" 
							value="fsc_fss.generadoAutomaticamente.ordinal()"
							title="Generado Automaticamente"
							headerKey="0"
							headerValue="Todos"							
							/>
					</td>
				</tr>
				<tr>
					<td class="textoCampo">Monto Estimado Desde:</td>
	      			<td class="textoDato">
	      				<@s.textfield 
	      					templateDir="custontemplates"
	      					template="textMoney"  
							id="montoDesde" 
							cssClass="textarea"
							cssStyle="width:160px" 
							name="montoDesde" 
							title="Monto Desde" />
					</td>							
	      			<td class="textoCampo">Monto Estimado Hasta:</td>
	      			<td class="textoDato">
	      				<@s.textfield 
	      					templateDir="custontemplates"
	      					template="textMoney"  
							id="montoHasta" 
							cssClass="textarea"
							cssStyle="width:160px" 
							name="montoHasta" 
							title="Monto Hastae" />
					</td>
				</tr>
				<tr>
					<td class="textoCampo">Fecha Solicitud Desde:</td>
	      			<td class="textoDato">
						<@vc.rowCalendar 
						templateDir="custontemplates" 
						id="fechaDesde" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="fechaDesde" 
						title="Fecha Desde" />
					</td>
					<td class="textoCampo">Fecha Solicitud Hasta:</td>
	      			<td class="textoDato">
						<@vc.rowCalendar 
						templateDir="custontemplates" 
						id="fechaHasta" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="fechaHasta" 
						title="Fecha Hasta" />
					</td>
				</tr>
				<tr>
	    		<tr>
	    			<td colspan="4" class="lineaGris" align="right">
	      			<input id="limpiar" type="button" name="btnVolver" value="Limpiar" class="boton"/>		
	      			<input id="buscar" type="button" name="btnVolver" value="Buscar" class="boton"/>
	    			</td>
				</tr>	
			</table>
		</div>	
		
        <!-- Resultado Filtro -->
		<@s.if test="fsc_fssList!=null">
			<#assign fsc_fssNumero = "0" />
			<@s.if test="fsc_fss.numero != null">
				<#assign fsc_fssNumero = "${fsc_fss.numero}" />
			</@s.if>

			<#assign fsc_fssEstado = "0" />
			<@s.if test="fsc_fss.estado != null">
					<#assign fsc_fssEstado = "${fsc_fss.estado.ordinal()}" />
			</@s.if>

			<#assign fsc_fssFechaDesde = "" />
			<@s.if test="fechaDesde != null">
				<#assign fsc_fssFechaDesde = "${fechaDesde?string('dd/MM/yyyy')}" />
			</@s.if>

			<#assign fsc_fssFechaHasta = "" />
			<@s.if test="fechaHasta != null">
				<#assign fsc_fssFechaHasta = "${fechaHasta?string('dd/MM/yyyy')}" />
			</@s.if>
				
			<#assign fsc_fssMontoDesde = "" />
			<@s.if test="montoDesde != null">
				<#assign fsc_fssMontoDesde = "${montoDesde}" />
			</@s.if>

			<#assign fsc_fssMontoHasta = "" />
			<@s.if test="montoHasta != null">
				<#assign fsc_fssMontoHasta = "${montoHasta}" />
			</@s.if>
				
			<#assign fsc_fssClienteDescripcion = "" />
			<@s.if test=" fsc_fss.cliente!= null && fsc_fss.cliente.descripcion !=null && fsc_fss.cliente.descripcion!=''">
				<#assign fsc_fssClienteDescripcion = "${fsc_fss.cliente.descripcion}" />
			</@s.if>
			
			<#assign fsc_fssClienteOid = "0" />
			<@s.if test=" fsc_fss.cliente!= null && fsc_fss.cliente.oid !=null">
				<#assign fsc_fssClienteOid = "${fsc_fss.cliente.oid?c}" />
			</@s.if>
			
			<#assign productoBienDescripcion = "" />
			<@s.if test=" productoBien!= null && productoBien.descripcion !=null && productoBien.descripcion!=''">
				<#assign productoBienDescripcion = "${productoBien.descripcion}" />
			</@s.if>
			
			<#assign productoBienOid = "0" />
			<@s.if test=" productoBien!= null && productoBien.oid !=null">
				<#assign productoBienOid = "${productoBien.oid?c}" />
			</@s.if>
				
				<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
				<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>FSC/FSS encontrados</td>
					<td>
						<div class="alineacionDerecha">
							<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0322" 
								cssClass="item" 
								href="${request.contextPath}/compraContratacion/fsc_fss/printXLS.action?fsc_fss.numero=${fsc_fssNumero}&fsc_fss.estado=${fsc_fssEstado}&fechaDesde=${fsc_fssFechaDesde}&fechaHasta=${fsc_fssFechaHasta}&montoDesde=${fsc_fssMontoDesde}&montoHasta=${fsc_fssMontoHasta}&fsc_fss.tipoFormulario=${fsc_fss.tipoFormulario.ordinal()}&fsc_fss.cliente.descripcion=${fsc_fssClienteDescripcion}&fsc_fss.cliente.oid=${fsc_fssClienteOid}&productoBien.descripcion=${productoBienDescripcion}&productoBien.oid=${productoBienOid}">
								<img src="${request.contextPath}/common/images/imprimirExcel.gif" title="Exportar a Excel" align="absmiddle" border="0" hspace="3">
							</@security.a>
						</div>
						<div class="alineacionDerecha">
							<b>Imprimir</b>
							<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0322" 
								cssClass="item" 
								href="${request.contextPath}/compraContratacion/fsc_fss/printPDF.action?fsc_fss.numero=${fsc_fssNumero}&fsc_fss.estado=${fsc_fssEstado}&fechaDesde=${fsc_fssFechaDesde}&fechaHasta=${fsc_fssFechaHasta}&montoDesde=${fsc_fssMontoDesde}&montoHasta=${fsc_fssMontoHasta}&fsc_fss.tipoFormulario=${fsc_fss.tipoFormulario.ordinal()}&fsc_fss.cliente.descripcion=${fsc_fssClienteDescripcion}&fsc_fss.cliente.oid=${fsc_fssClienteOid}&productoBien.descripcion=${productoBienDescripcion}&productoBien.oid=${productoBienOid}">			
								<img src="${request.contextPath}/common/images/imprimirPdf.gif" title="Exportar a PDF" align="absmiddle" border="0" hspace="3">
							</@security.a>
						</div>	
					</td>
				</tr>
			</table>	

			<@vc.anchors target="contentTrx">			
          		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="fsc_fssList" id="fsc_fss" pagesize=15 decorator="ar.com.riouruguay.web.actions.compraContratacion.fsc_fss.decorators.FSC_FSSDecorator" partialList=true size="recordSize" keepStatus=true excludedParams="resetFlag">
          		
          		<#assign classBotonera = "botoneraAnchoCon5" />
          		<#assign classTexto = "estiloTexto" />
          		<#assign classNumero = "estiloNumero" />
          		<#if fsc_fss.intervenidoAutomaticamente.ordinal()==2>
	          		<#assign classBotonera = "botoneraAnchoCon5Color" />
	          		<#assign classTexto = "estiloTextoColor" />
	          		<#assign classNumero = "estiloNumeroColor" />
          		</#if>
  		
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" class="${classBotonera}" title="Acciones" >
						<div class="alineacion">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0323" 
							enabled="fsc_fss.readable" 
							cssClass="item" 
							href="${request.contextPath}/compraContratacion/fsc_fss/readFSC_FSSView.action?fsc_fss.oid=${fsc_fss.oid?c}&navigationId=visualizar-fsc_fss&flowControl=regis&navegacionIdBack=${navigationId}">
							<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver"  border="0">
						</@security.a>
						</div>
						
						<div class="alineacion">	
					
					<#-- Estado En Preparacion -->
					<#if fsc_fss.estado.ordinal() == 1>
				
								<@security.a 
									templateDir="custontemplates" 
									securityCode="CUF0324" 
									enabled="fsc_fss.updatable"
									cssClass="item"  
									href="${request.contextPath}/compraContratacion/fsc_fss/solicitarFSC_FSSView.action?fsc_fss.oid=${fsc_fss.oid?c}&navigationId=administrar&flowControl=regis">
									<img  src="${request.contextPath}/common/images/administrar.gif" alt="Administrar" title="Administrar"  border="0">
								</@security.a>
						
					<#-- Estado Pendiente autorizacion -->
					<#elseif fsc_fss.estado.ordinal() == 2>
				
								<@security.a 
									templateDir="custontemplates" 
									securityCode="CUF0325" 
									enabled="fsc_fss.updatable"
									cssClass="item"  
									href="${request.contextPath}/compraContratacion/fsc_fss/autorizarRechazarDerivarFSC_FSSView.action?fsc_fss.oid=${fsc_fss.oid?c}&navigationId=administrar&flowControl=regis">
									<img  src="${request.contextPath}/common/images/administrar.gif" alt="Administrar" title="Administrar"  border="0">
								</@security.a>
													
					<#-- Estado Derivado -->
					<#elseif fsc_fss.estado.ordinal() == 6>
				
								<@security.a 
									templateDir="custontemplates" 
									securityCode="CUF0326" 
									enabled="fsc_fss.updatable"
									cssClass="item"  
									href="${request.contextPath}/compraContratacion/fsc_fss/autorizarRechazarFSC_FSSView.action?fsc_fss.oid=${fsc_fss.oid?c}&navigationId=administrar&flowControl=regis">
									<img  src="${request.contextPath}/common/images/administrar.gif" alt="Administrar" title="Administrar"  border="0">
								</@security.a>
					
					<#-- Cualquier otro estado-->
					<#else>				
								<@security.a 
									templateDir="custontemplates" 
									securityCode="CUF0327" 
									enabled="false"
									cssClass="item"  
									href="">
									<img  src="${request.contextPath}/common/images/administrar.gif" alt="Administrar" title="Administrar"  border="0">
								</@security.a>												
					
					</#if>	
						
						</div>
						
						
						<div class="alineacion">		
						<#-- Estado En Preparacion -->
						<#if fsc_fss.estado.ordinal() == 1>
					
							<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0328" 
								enabled="fsc_fss.eraseable"
								cssClass="item"  
								href="${request.contextPath}/compraContratacion/fsc_fss/deleteFSC_FSSView.action?fsc_fss.oid=${fsc_fss.oid?c}&navigationId=delete-fsc_fss&flowControl=regis">
								<img  src="${request.contextPath}/common/images/eliminar.gif" alt="Eliminar" title="Eliminar"  border="0">
							</@security.a>
							
							<#-- Estado Pendiente autorizacion -->
						<#elseif fsc_fss.estado.ordinal() == 2>
							<@security.a 
									templateDir="custontemplates" 
									securityCode="CUF0328" 
									enabled="fsc_fss.eraseable"
									cssClass="item"  
									href="${request.contextPath}/compraContratacion/fsc_fss/anularFSC_FSSView.action?fsc_fss.oid=${fsc_fss.oid?c}&navigationId=delete-fsc_fss&flowControl=regis">
									<img  src="${request.contextPath}/common/images/eliminar.gif" alt="Anular" title="Anular"  border="0">
								</@security.a>
					<#-- Cualquier otro estado-->
					<#else>				
								<@security.a 
									templateDir="custontemplates" 
									securityCode="CUF0328" 
									enabled="false"
									cssClass="item"  
									href="">
									<img  src="${request.contextPath}/common/images/eliminar.gif" alt="Anular" title="Anular"  border="0">
								</@security.a>											
					
					</#if>	
								
					</div>
					<div class="alineacion">						
							<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0329" 
								enabled="fsc_fss.enable"
								cssClass="item"  
								href="${request.contextPath}/compraContratacion/fsc_fss/intervenirFSC_FSSView.action?fsc_fss.oid=${fsc_fss.oid?c}&navigationId=intervenir-fsc_fss&flowControl=regis">
								<img  src="${request.contextPath}/common/images/intervenir.gif" alt="Intervenir" title="Intervenir"  border="0">
							</@security.a>	
										
						</div>
						<div class="alineacion">						
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0323" 
							enabled="fsc_fss.readable" 
							cssClass="no-rewrite" 
							href="${request.contextPath}/compraContratacion/fsc_fss/printFSC_FSSPDF.action?fsc_fss.oid=${fsc_fss.oid?c}">
							<img  src="${request.contextPath}/common/images/imprimir.gif" alt="Imprimir" title="Imprimir"  border="0">
						</@security.a>							
						</div>

					</@display.column>
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="${classNumero}" property="numero" title="Nro." />
			
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="${classTexto}" property="tipoFormulario" title="Tipo" />
			 		
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="${classTexto}" property="cliente.descripcion" title="Cliente" />									
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="${classTexto}" property="fechaSolicitud" format="{0,date,dd/MM/yyyy}"  title="Fecha Sol." />
		
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="${classNumero}" format="{0,number,#,##0.00}" property="montoEstimado" title="Monto Estimado" />			
				
				
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="${classTexto}" property="estado" title="Estado" />
					
						
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="${classNumero}" property="repuestoParcialString" title="Rep. Par." />
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="${classNumero}" property="repuestoTotalString" title="Rep. Tot." />
											
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="${classNumero}" property="asignadoString" title="Asig." />
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="${classNumero}" property="cotizadoString" title="Cot." />
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="${classNumero}" property="pedidoParcialString" title="Ped. Par." />
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="${classNumero}" property="pedidoTotalString" title="Ped. Tot." />
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="${classNumero}" property="recibidoParcialString" title="Rec. Par." />
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="${classNumero}" property="recibidoTotalString" title="Rec. Tot." />
					
					
				</@display.table>
			</@vc.anchors>
			</div>
			
		</@s.if>
	
		
<@vc.htmlContent 
  baseUrl="${request.contextPath}/compraContratacion/fsc_fss/search.action"
  source="buscar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="resetFlag=true,navigationId={navigationId},flowControl=regis,fsc_fss.estado={fsc_fss.estado},fsc_fss.numero={fsc_fss.numero},fechaDesde={fechaDesde},fechaHasta={fechaHasta},montoDesde={montoDesde},montoHasta={montoHasta},productoBien.oid={productoBien.oid},productoBien.descripcion={productoBien.descripcion},fsc_fss.cliente.oid={fsc_fss.cliente.oid},fsc_fss.cliente.descripcion={fsc_fss.cliente.descripcion},fsc_fss.tipoFormulario={fsc_fss.tipoFormulario},fsc_fss.generadoAutomaticamente={fsc_fss.generadoAutomaticamente},fsc_fss.intervenidoAutomaticamente={fsc_fss.intervenidoAutomaticamente}"/>
  		      

<@vc.htmlContent 
  baseUrl="${request.contextPath}/compraContratacion/fsc_fss/view.action"
  source="limpiar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=regis"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/compraContratacion/fsc_fss/createFSC_FSSView.action" 
  source="crear" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=fsc_fss-create,flowControl=regis"/>
  
  <@vc.htmlContent 
  baseUrl="${request.contextPath}/compraContratacion/fsc_fss/selectClienteSearch.action" 
  source="seleccionarCliente" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=change,navegacionIdBack={navegacionIdBack},fsc_fss.estado={fsc_fss.estado},fsc_fss.numero={fsc_fss.numero},fechaDesde={fechaDesde},fechaHasta={fechaHasta},montoDesde={montoDesde},montoHasta={montoHasta},productoBien.oid={productoBien.oid},productoBien.descripcion={productoBien.descripcion},fsc_fss.cliente.oid={fsc_fss.cliente.oid},fsc_fss.cliente.descripcion={fsc_fss.cliente.descripcion},fsc_fss.tipoFormulario={fsc_fss.tipoFormulario},fsc_fss.generadoAutomaticamente={fsc_fss.generadoAutomaticamente},fsc_fss.intervenidoAutomaticamente={fsc_fss.intervenidoAutomaticamente}"/>
  

 <@vc.htmlContent 
  baseUrl="${request.contextPath}/compraContratacion/fsc_fss/selectProductoBienSearch.action" 
  source="seleccionarProductoBien" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=change,navegacionIdBack={navegacionIdBack},fsc_fss.estado={fsc_fss.estado},fsc_fss.numero={fsc_fss.numero},fechaDesde={fechaDesde},fechaHasta={fechaHasta},montoDesde={montoDesde},montoHasta={montoHasta},fsc_fss.cliente.oid={fsc_fss.cliente.oid},fsc_fss.cliente.descripcion={fsc_fss.cliente.descripcion},productoBien.oid={productoBien.oid},productoBien.descripcion={productoBien.descripcion},fsc_fss.tipoFormulario={fsc_fss.tipoFormulario},fsc_fss.generadoAutomaticamente={fsc_fss.generadoAutomaticamente},fsc_fss.intervenidoAutomaticamente={fsc_fss.intervenidoAutomaticamente}"/>
  