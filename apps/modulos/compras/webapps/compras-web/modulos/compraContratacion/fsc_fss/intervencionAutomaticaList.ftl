<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>


<@vc.anchors target="contentTrx" ajaxFlag="ajax">
	<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
		<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
			<tr>
				<td>Intervenciones Autom&aacute;ticas</td>
			</tr>
		</table>			

		<table class="tablaDetalleCuerpo" cellpadding="3">
			<tr>
				<th>Producto/Bien/Servicio</th>
				<th>OC_OS</th>
				<th>Cantidad Intervenida</th>
				<th>Tipo Intervencion</th>							
			</tr>
			
			<#assign listaDetalle = fsc_fss.fsc_fssDetalleList>
			<#list listaDetalle as fsc_fssDetalle>
				<#assign listaIntervenciones = fsc_fssDetalle.intervencionList>
				<#list listaIntervenciones as intervencion>							
				<tr>
					<td>
						<#if intervencion.fsc_fssDetalle.productoBien?exists>
						${intervencion.fsc_fssDetalle.productoBien.descripcion}
						<#else>
						${intervencion.fsc_fssDetalle.servicio.descripcion}
						</#if>
					</td>
					<td>
						${intervencion.oc_os.numero}
						-
						${intervencion.oc_os.fecha?string("dd/MM/yyyy")}
					</td>
					<td>
						${intervencion.cantidadIntervenida}
					</td>
					<td>
						${intervencion.tipoIntervencionAutomatica}
					</td>							
				</tr>
				</#list>								
			</#list>
			</table>
		</div>
</@vc.anchors>
<div id="capaSeparadora" class="capaSeparadora">&nbsp;</div>	

