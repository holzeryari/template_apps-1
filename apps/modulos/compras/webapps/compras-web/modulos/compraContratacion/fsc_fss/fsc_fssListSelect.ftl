<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>

<@s.hidden id="navigationId" name="navigationId"/>
<@s.hidden id="navegacionIdBack" name="navegacionIdBack"/>
<@s.hidden id="fsc_fss.tipoFormulario" name="fsc_fss.tipoFormulario.ordinal()"/>
<@s.hidden id="clienteDuro" name="clienteDuro"/>
<@s.hidden id="seleccionMultiple" name="seleccionMultiple"/>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
        	<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Datos del FSC/FSS</b></td>
				
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
	    		<td class="textoCampo" colspan="4">&nbsp;</td>
	    	</tr>	
			<tr>
				<td class="textoCampo">N&uacute;mero:</td>
	      		<td class="textoDato">
      			<@s.textfield 
      					templateDir="custontemplates" 
						id="fsc_fss.numero" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="fsc_fss.numero" 
						title="N&uacute;mero" />
				</td>							
	      		<td class="textoCampo">Tipo:</td>
	      		<td class="textoDato">
	      			<@s.property default="&nbsp;" escape=false value="fsc_fss.tipoFormulario"/>	      			
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Cliente:</td>
				<td class="textoDato">
					<@s.hidden id="fsc_fss.cliente.oid" name="fsc_fss.cliente.oid"/>
					<@s.hidden id="fsc_fss.cliente.descripcion" name="fsc_fss.cliente.descripcion"/>
					<@s.property default="&nbsp;" escape=false value="fsc_fss.cliente.descripcion"/>						
					<@s.a templateDir="custontemplates" id="seleccionarCliente" name="seleccionarCliente" href="javascript://nop/" cssClass="ocultarIcono">
						<img src="${request.contextPath}/common/images/buscar.gif"  border="0" align="absmiddle" hspace="3" >
					</@s.a>		
				</td>						
	
				<td class="textoCampo">Estado:</td>
	      		<td class="textoDato">
	      			Autorizado / En Curso (Asignado Parcial) 
				</td>
			</tr>
			<tr>
	  			<td class="textoCampo">Monto Estimado Desde:</td>
	  			<td class="textoDato">
	  			<@s.textfield 
	  					templateDir="custontemplates"
	  					template="textMoney"  
						id="montoDesde" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="montoDesde" 
						title="Monto Desde" />
				</td>							
	  			<td class="textoCampo">Monto Estimado Hasta:</td>
	  			<td class="textoDato">
	  				<@s.textfield 
	  					templateDir="custontemplates"
	  					template="textMoney"  
						id="montoHasta" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="montoHasta" 
						title="Monto Hastae" />
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Fecha Solicitud Desde:</td>
      			<td class="textoDato">
					<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="fechaDesde" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="fechaDesde" 
					title="Fecha Desde" />
				</td>
				<td class="textoCampo">Fecha Solicitud Hasta:</td>
      			<td class="textoDato">
				<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="fechaHasta" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="fechaHasta" 
					title="Fecha Hasta" />
				</td>
			</tr>
			<tr>
	    			<td colspan="4" class="lineaGris" align="right">
	      			<input id="limpiar" type="button" name="btnVolver" value="Limpiar" class="boton"/>		
	      			<input id="buscar" type="button" name="btnVolver" value="Buscar" class="boton"/>
	    			</td>
				</tr>	
			</table>
		</div>	
			
		<!-- Resultado Filtro -->
		<@s.if test="fsc_fssList!=null">
			<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
				<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
					<tr>
						<td>FSC/FSS encontrados</td>
					</tr>
				</table>
				<@vc.anchors target="contentTrx">			

          		<#if seleccionMultiple?exists && seleccionMultiple==true>          		
	          		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="fsc_fssList" id="fsc_fssSelect" pagesize=15 decorator="ar.com.riouruguay.web.actions.compraContratacion.fsc_fss.decorators.FSC_FSSDecorator" defaultsort=2>
						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" title="Acciones" class="botoneraAnchoCon2">
							<div class="alineacion">
								<@security.a 
									templateDir="custontemplates" 
									securityCode="CUF0323" 
									enabled="fsc_fss.readable" 
									cssClass="item" 
									href="${request.contextPath}/compraContratacion/fsc_fss/readFSC_FSSView.action?fsc_fss.oid=${fsc_fssSelect.oid?c}&navigationId=visualizar-fsc_fss&flowControl=regis&navegacionIdBack=${navigationId}">
									<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver"  border="0">
								</@security.a>
							</div>
	
							<@s.checkbox label="${fsc_fssSelect.oid?c}" fieldValue="${fsc_fssSelect.oid?c}" name="formularioList" />
						</@display.column>
						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="numero" title="Nro." />
						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="tipoFormulario" title="Tipo" />
						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="cliente.descripcion" title="Cliente" />									
						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="fechaSolicitud" format="{0,date,dd/MM/yyyy}"  title="Fecha Solicitud" />
						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" format="{0,number,#,##0.00}" property="montoEstimado" title="Monto Estimado" />			
						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="estado" title="Estado" />
						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="repuestoParcialString" title="Rep. Par." />
						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="repuestoTotalString" title="Rep. Tot." />
						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="asignadoString" title="Asig." />
						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="cotizadoString" title="Cot." />
						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="pedidoParcialString" title="Ped. Par." />
						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="pedidoTotalString" title="Ped. Tot." />
						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="recibidoParcialString" title="Rec. Par." />
						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="recibidoTotalString" title="Rec. Tot." />
					</@display.table>
				<#else>
	          		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="fsc_fssList" id="fsc_fssSelect" pagesize=15 decorator="ar.com.riouruguay.web.actions.compraContratacion.fsc_fss.decorators.FSC_FSSDecorator" defaultsort=2 partialList=true size="recordSize" keepStatus=true excludedParams="resetFlag">
						<#assign ref="${request.contextPath}/compraContratacion/fsc_fss/seleccionFSC_FSS.action?">										
						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" title="Acciones">
							<@s.a 
							templateDir="custontemplates" 
							href="${ref}fsc_fss.oid=${fsc_fssSelect.oid?c}&fsc_fss.numero=${fsc_fssSelect.numero?c}&navegacionIdBack=${navegacionIdBack}&navigationId=${navigationId}"
							id="seleccionIndividualFSC"
							name="seleccionIndividualFSC"
							cssClass="item">
								<img src="${request.contextPath}/common/images/seleccionar.gif" alt="Seleccionar" title="Seleccionar" width="16" height="16" border="0">
							</@s.a>
							&nbsp;
	
						</@display.column>
						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="numero" title="Nro." />
						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="tipoFormulario" title="Tipo" />
						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="cliente.descripcion" title="Cliente" />									
						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="fechaSolicitud" format="{0,date,dd/MM/yyyy}"  title="Fecha Solicitud" />
						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" format="{0,number,#,##0.00}" property="montoEstimado" title="Monto Estimado" />			
						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="estado" title="Estado" />
						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="repuestoParcialString" title="Rep. Par." />
						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="repuestoTotalString" title="Rep. Tot." />
						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="asignadoString" title="Asig." />
						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="cotizadoString" title="Cot." />
						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="pedidoParcialString" title="Ped. Par." />
						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="pedidoTotalString" title="Ped. Tot." />
						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="recibidoParcialString" title="Rec. Par." />
						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="recibidoTotalString" title="Rec. Tot." />
					</@display.table>
				</#if>
			</@vc.anchors>
			</div>
			</@s.if>
			<div id="capaBotonera" class="capaBotonera">
				<table id="tablaBotonera" class="tablaBotonera">
				<#if seleccionMultiple?exists && seleccionMultiple>
					<tr> 
						<td align="left">
							<input id="cancel" type="button" name="btnVolver" value="Volver" class="boton"/>
							<input id="seleccionar" type="button" name="btnSeleccionar" value="Seleccionar FSC/FSS" class="boton"/>
						</td>
					</tr>
				<#else>
					<tr> 
						<td align="left">
							<input id="cancel" type="button" name="btnVolver" value="Volver" class="boton"/>
						</td>
					</tr>
				</#if>
				</table>
			</div>		

<@vc.htmlContent 
  baseUrl="${request.contextPath}/compraContratacion/fsc_fss/selectFSC_FSSSearch.action" 
  source="buscar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="resetFlag=true,navigationId={navigationId},flowControl=regis,navegacionIdBack={navegacionIdBack},fsc_fss.estado={fsc_fss.estado},fsc_fss.numero={fsc_fss.numero},fechaDesde={fechaDesde},fechaHasta={fechaHasta},montoDesde={montoDesde},montoHasta={montoHasta},fsc_fss.cliente.oid={fsc_fss.cliente.oid},fsc_fss.cliente.descripcion={fsc_fss.cliente.descripcion},fsc_fss.tipoFormulario={fsc_fss.tipoFormulario},oidParameter=${oidParameter?c},clienteDuro={clienteDuro},seleccionMultiple={seleccionMultiple}"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/compraContratacion/fsc_fss/selectFSC_FSSView.action" 
  source="limpiar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=regis,navegacionIdBack={navegacionIdBack},oidParameter=${oidParameter?c},clienteDuro={clienteDuro},seleccionMultiple={seleccionMultiple}"/>
  
 <@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navegacionIdBack},flowControl=back"/>
 
  <@vc.htmlContent 
  baseUrl="${request.contextPath}/compraContratacion/fsc_fss/selecccionMultipleFSC_FSS.action" 
  source="seleccionar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="formularioList={formularioList},navegacionIdBack={navegacionIdBack},oidParameter=${oidParameter?c},clienteDuro={clienteDuro},seleccionMultiple={seleccionMultiple}"/>
 
 
  <@vc.htmlContent 
  baseUrl="${request.contextPath}/compraContratacion/fsc_fss/selectClienteSearch.action" 
  source="seleccionarCliente" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=change,navegacionIdBack={navegacionIdBack},fsc_fss.estado={fsc_fss.estado},fsc_fss.numero={fsc_fss.numero},fechaDesde={fechaDesde},fechaHasta={fechaHasta},montoDesde={montoDesde},montoHasta={montoHasta},fsc_fss.cliente.oid={fsc_fss.cliente.oid},fsc_fss.cliente.descripcion={fsc_fss.cliente.descripcion},fsc_fss.tipoFormulario={fsc_fss.tipoFormulario},oidParameter=${oidParameter?c},clienteDuro={clienteDuro},seleccionMultiple={seleccionMultiple}"/>
  
 
 