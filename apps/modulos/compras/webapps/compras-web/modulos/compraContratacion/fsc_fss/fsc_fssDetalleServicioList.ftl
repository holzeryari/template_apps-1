<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<@vc.anchors target="contentTrx" ajaxFlag="ajax">
	<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
		<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
			<tr>
				<td>Servicios solicitados</td>
				<td>
					<div align="right">
						<@s.a href="${request.contextPath}/compraContratacion/fsc_fss/selectServicio.action?fsc_fss.oid=${fsc_fss.oid?c}" templateDir="custontemplates" id="agregarProductoBien" name="agregarProductoBien" cssClass="ocultarIcono">
						<b>Agregar</b><img src="${request.contextPath}/common/images/agregar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
						</@s.a>		
					</div>
				</td>
			</tr>
		</table>	
	
		<!-- Resultado Filtro -->						
		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="fsc_fss.fsc_fssDetalleList" id="fsc_fssDetalle" defaultsort=2>	
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select"  class="botoneraAnchoCon3" title="Acciones">
				<a href="${request.contextPath}/compraContratacion/fsc_fss/readFSC_FSSDetalleView.action?fsc_fssDetalle.oid=${fsc_fssDetalle.oid?c}&navegacionIdBack=${navigationId}"><img  src="${request.contextPath}/common/images/ver.gif" alt="Ver Servicio" title="Ver Servicio" border="0"></a>
				&nbsp;		
				<a href="${request.contextPath}/compraContratacion/fsc_fss/deleteFSC_FSSDetalleView.action?fsc_fssDetalle.oid=${fsc_fssDetalle.oid?c}"><img  src="${request.contextPath}/common/images/eliminar.gif" alt="Eliminar" title="Eliminar" border="0"></a>
											
			</@display.column>
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="numero" title="N&uacute;mero Item" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="servicio.oid" title="C&oacute;digo" />	
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="servicio.descripcion" title="Descripci&oacute;n" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto"  property="servicio.rubro.descripcion" title="Rubro" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="servicio.tipo" title="Tipo" />		
		</@display.table>
	</div>	
</@vc.anchors>

