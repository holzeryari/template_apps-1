<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaSeparadora" class="capaSeparadora">&nbsp;</div>
<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancelar" type="button" name="btnCancelar" value="Cancelar" class="boton"/>
				<input id="autorizar" type="button" name="btnAutorizar" value="Autorizar" class="boton"/>
				<input id="rechazar" type="button" name="btnRechazar" value="Rechazar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

  
  
  <@vc.htmlContent 
   baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancelar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=buscar-fsc_fss,flowControl=back"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/compraContratacion/fsc_fss/rechazarFSC_FSS.action" 
  source="rechazar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="fsc_fss.oid={fsc_fss.oid},fsc_fss.comentario={fsc_fss.comentario}"/>
  
  <@vc.htmlContent 
  baseUrl="${request.contextPath}/compraContratacion/fsc_fss/autorizarFSC_FSS.action" 
  source="autorizar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="fsc_fss.oid={fsc_fss.oid},fsc_fss.comentario={fsc_fss.comentario}"/>
