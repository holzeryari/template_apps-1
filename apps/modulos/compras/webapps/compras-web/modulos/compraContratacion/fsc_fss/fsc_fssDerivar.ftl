<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos del Responsable de autorizaci&oacute;n del FSC/FSS</b></td>
			</tr>
		</table>
	
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
			<tr>
				<td class="textoCampo">Cliente:</td>
				<td class="textoDato">
					<@s.select 
						templateDir="custontemplates" 
						id="fsc_fss.clienteDerivado.oid" 
						cssClass="textarea"
						cssStyle="width:250px" 
						name="fsc_fss.clienteDerivado.oid"
						value="fsc_fss.clienteDerivado.oid"  
						list="clienteList" 
						listKey="oid" 
						listValue="descripcionConMontos"		
						title="Cliente"
						headerKey="" 
					    headerValue="Seleccionar" />
				</td>
				<td class="textoCampo">&nbsp;</td>
				<td class="textoCampo">&nbsp;</td>			
			</tr>
	  		<tr>	
				<td class="textoCampo">Observaciones:</td>
      			<td  class="textoDato">
				<@s.textarea	
						templateDir="custontemplates" 						  
						cols="80" rows="4"	      					
						cssClass="textarea"
						id="fsc_fss.observaciones"							 
						name="fsc_fss.observaciones"  
						label="Observaciones"														
						 />
				</td>
				<td class="textoCampo">&nbsp;</td>
				<td class="textoCampo">&nbsp;</td>					
    		</tr>
    		<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>
	    </table>
	    </div>		
	    		