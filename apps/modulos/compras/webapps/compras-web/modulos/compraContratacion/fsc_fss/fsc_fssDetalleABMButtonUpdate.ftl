<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancelar" type="button" name="btnCancelar" value="Cancelar" class="boton"/>
				<input id="modificar" type="button" name="btnMdoficar" value="Modificar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/compraContratacion/fsc_fss/updateFSC_FSSDetalle.action" 
  source="modificar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="fsc_fssDetalle.oid={fsc_fssDetalle.oid},fsc_fssDetalle.cantidadSolicitada={fsc_fssDetalle.cantidadSolicitada}"/>
  
 
 <@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancelar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=administrar,flowControl=back"/>
  
 
 <@vc.htmlContent 
  baseUrl="${request.contextPath}/bien/inventarioBien/selectCliente.action" 
  source="seleccionarCliente" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId=administrar-updateDetalle,flowControl=change,navegacionIdBack=administrar-updateDetalle,bienInventario.cliente.oid={bienInventariado.cliente.oid},bienInventariado.cliente.descripcion={bienInventariado.cliente.descripcion},bienInventariado.oid={bienInventariado.oid},bienInventariado.numeroInventario={bienInventariado.numeroInventario},bienInventariado.numeroFactura={bienInventariado.numeroFactura},bienInventariado.valorAdquisicion={bienInventariado.valorAdquisicion},bienInventariado.numeroSerie={bienInventariado.numeroSerie},bienInventariado.inventarioBien.tipoInventario={tipoI}"/>
  	