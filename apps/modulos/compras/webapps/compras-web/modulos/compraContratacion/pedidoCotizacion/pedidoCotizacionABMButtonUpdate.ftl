<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancelar" type="button" name="btnCancelar" value="Cancelar" class="boton"/>
				<input id="update" type="button" name="btnModificar" value="Modificar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/compraContratacion/pedidoCotizacion/updatePedidoCotizacion.action" 
  source="update" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="pedidoCotizacion.versionNumber={pedidoCotizacion.versionNumber},pedidoCotizacion.oid={pedidoCotizacion.oid},pedidoCotizacion.fechaRecepcion={pedidoCotizacion.fechaRecepcion},pedidoCotizacion.fechaLimiteCot={pedidoCotizacion.fechaLimiteCot},pedidoCotizacion.fechaValidezPrecio={pedidoCotizacion.fechaValidezPrecio},pedidoCotizacion.observaciones={pedidoCotizacion.observaciones}"/>
  

<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancelar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=administrar,flowControl=back"/>
  
  