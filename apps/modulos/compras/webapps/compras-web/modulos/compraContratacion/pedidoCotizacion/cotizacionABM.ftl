<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<@s.hidden id="cotizacion.oid" name="cotizacion.oid"/>
<@s.hidden id="cotizacion.pedidoCotizacionDetalle.oid" name="cotizacion.pedidoCotizacionDetalle.oid"/>
<#if existeCantidadMaxima?exists  && existeCantidadMaxima>
	<@s.hidden id="cotizacion.cantidadMaxima" name="cotizacion.cantidadMaxima"/>
</#if>
<#if existeAlicuota?exists  && existeAlicuota>
	<@s.hidden id="cotizacion.alicuotaIva" name="cotizacion.alicuotaIva"/>
</#if>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
			<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos del Pedido de Cotizaci&oacute;n</b></td>
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
			<tr>
      			<td class="textoCampo">N&uacute;mero Pedido Cotizaci&oacute;n:</td>
      			<td class="textoDato"><@s.property default="&nbsp;" escape=false value="cotizacion.pedidoCotizacionDetalle.pedidoCotizacion.numero"/></td>
      			<td  class="textoCampo">Fecha Pedido: </td>
				<td class="textoDato">
					<@s.if test="cotizacion.pedidoCotizacionDetalle.pedidoCotizacion.fechaPedido != null">
						<#assign fechaPedido = cotizacion.pedidoCotizacionDetalle.pedidoCotizacion.fechaPedido> ${fechaPedido?string("dd/MM/yyyy")}	
					</@s.if>									
				</td>	      			
			</tr>	

			<tr>
				<td class="textoCampo">Proveedor:</td>
      			<td class="textoDato">
      				<@s.property default="&nbsp;" escape=false value="cotizacion.pedidoCotizacionDetalle.pedidoCotizacion.proveedor.detalleDePersona.razonSocial" /><@s.property default="&nbsp;" escape=false value="pedidoCotizacion.proveedor.detalleDePersona.nombre" />							
				</td>
				<td class="textoCampo">Tipo Pedido:</td>
      			<td class="textoDato">
      			<@s.property default="&nbsp;" escape=false value="cotizacion.pedidoCotizacionDetalle.pedidoCotizacion.tipo"/>
      			</td>
      		</tr>
      		<tr>
				<td class="textoCampo">Nro Asignaci&oacute;n Proveedores:</td>
      			<td class="textoDato"><@s.property default="&nbsp;" escape=false value="cotizacion.pedidoCotizacionDetalle.pedidoCotizacion.asignacionProveedor.numero"/></td>
      			<td class="textoCampo">Fecha L&iacute;mite Recepci&oacute;n Cotizaciones:</td>
      			<td class="textoDato">
				
				<#if cotizacion.pedidoCotizacionDetalle.pedidoCotizacion.fechaLimiteCot?exists>	
				${cotizacion.pedidoCotizacionDetalle.pedidoCotizacion.fechaLimiteCot?string("dd/MM/yyyy")}
				</#if>						&nbsp;	
				</td>
				
			</tr>
			<tr>
				<td class="textoCampo">Fecha Recepci&oacute;n Cotizaciones:</td>
      			<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="cotizacion.pedidoCotizacionDetalle.pedidoCotizacion.fechaRecepcion" />							
				</td>
				<td class="textCampo">Fecha Hasta Validez Precios:</td>
      			<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="cotizacion.pedidoCotizacionDetalle.pedidoCotizacion.fechaValidezPrecio" />							
				</td>
			</tr>
			
    		<tr>
				<td class="textoCampo">Observaciones:</td>
      			<td  class="textoDato" colspan="3">
				<@s.property default="&nbsp;" escape=false value="cotizacion.pedidoCotizacionDetalle.pedidoCotizacion.observaciones"/>
      			</td>						
    		</tr>
	    		
    		<tr>
				<td class="textoCampo">Estado:</td>
      			<td  class="textoDato" colspan="3">
					<@s.property default="&nbsp;" escape=false value="cotizacion.pedidoCotizacionDetalle.pedidoCotizacion.estado"/></td>
				</td>
			</tr>
			<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>
		</table>	
	</div>			
	<@s.if test="cotizacion.pedidoCotizacionDetalle.productoBien != null">
		<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos del Producto/Bien</b></td>
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>	
							
			<tr>
      			<td class="textoCampo">C&oacute;digo:</td>
      			<td class="textoDato">
      			<@s.property default="&nbsp;" escape=false value="cotizacion.pedidoCotizacionDetalle.productoBien.oid"/>
      			</td>
      			<td  class="textoCampo">Descripci&oacute;n: </td>
				<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="cotizacion.pedidoCotizacionDetalle.productoBien.descripcion"/>		
				</td>	      			
			</tr>	
			<tr>
      			<td class="textoCampo">Tipo:</td>
	      		<td class="textoDato">
	      			<@s.property default="&nbsp;" escape=false value="cotizacion.pedidoCotizacionDetalle.productoBien.tipo"/>						      			
	      		</td>		      		
										
			<td class="textoCampo">Rubro:</td>
  			<td class="textoDato">
  				<@s.property default="&nbsp;" escape=false value="cotizacion.pedidoCotizacionDetalle.productoBien.rubro.descripcion"/>
			</td>
    		</tr>
			<tr>
				<td class="textoCampo" >Marca: </td>
				<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="pedidoCotizacionDetalle.productoBien.marca"/>										
				</td>
				<td class="textoCampo">Modelo: </td>
				<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="cotizacion.pedidoCotizacionDetalle.productoBien.modelo"/>										
				</td>
    		</tr>
    		<tr>
				<td class="textoCampo">Valor Mercado: </td>
				<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="bien.valorMercado"/>
				</td>
				<td class="textoCampo">Fecha Valor Mercado: </td>
				<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="bien.fechaValorMercado"/>
				</td>
    		</tr>
			<tr>
				<td class="textoCampo">C&oacute;digo de Barra: </td>
				<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="cotizacion.pedidoCotizacionDetalle.productoBien.codigoBarra"/>										
				</td>
				<td class="textoCampo">Es Cr&iacute;tico: </td>
				<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="cotizacion.pedidoCotizacionDetalle.productoBien.critico"/>						      			
				</td>
    		</tr>
    		<tr>
				<td class="textoCampo">Reposici&oacute;n Autm&aacute;tica: </td>
				<td  class="textoDato">	
					<@s.property default="&nbsp;" escape=false value="producto.reposicionAutomatica"/>						      											
				</td>
				<td class="textoCampo" Existencia M&iacute;nima: </td>
				<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="producto.existenciaMinima"/>
				</td>
    		</tr>
    		<tr>
    		<td class="textoCampo">Tipo Unidad:</td>
    		<td  class="textoDato" colspan="3">
					<@s.property default="&nbsp;" escape=false value="producto.tipoUnidad.codigo"/>
				</td>
    		</tr>
			<tr>
				<td class="textoCampo">Es Registrable: </td>
				<td  class="textoDato" colspan="3">
				<@s.property default="&nbsp;" escape=false value="bien.registrable"/>						      			
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Es Veh&iacute;culo: </td>
				<td  class="textoDato" colspan="3">
				<@s.property default="&nbsp;" escape=false value="bien.vehiculo"/>
				</td>
    		</tr>							
			<tr>
				<td class="textoCampo">Estado: </td>
      			<td class="textoDato" colspan="3"><@s.property default="&nbsp;" escape=false value="productoBien.estado"/></td>
    		</tr>
    		<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>
		</table>
		</div>
	</@s.if>
			
	<@s.if test="cotizacion.pedidoCotizacionDetalle.servicio != null">
		<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
			<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            	<tr>
					<td><b>Datos del Servicio</b></td>
				</tr>
			</table>

			<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
				<tr>
	        		<td class="textoCampo" colspan="4">&nbsp;</td>
	        	</tr>	
			
				<tr>
	      			<td class="textoCampo">C&oacute;digo:</td>
	      			<td class="textoDato">
		      			<@s.property default="&nbsp;" escape=false value="cotizacion.pedidoCotizacionDetalle.servicio.oid"/>			      			
	      			<td class="textoCampo">Descripci&oacute;n</td>
					<td class="textoDato">
						<@s.property default="&nbsp;" escape=false value="cotizacion.pedidoCotizacionDetalle.servicio.descripcion"/>			
				</tr>	
				<tr>
					<td class="textoCampo">Rubro:</td>
	      			<td class="textoDato">
	      				<@s.property default="&nbsp;" escape=false value="cotizacion.pedidoCotizacionDetalle.servicio.rubro.descripcion"/>
				     </td>
				
					<td class="textoCampo">Cr&iacute;tico: </td>
					<td class="textoDato">
						<@s.property default="&nbsp;" escape=false value="cotizacion.pedidoCotizacionDetalle.servicio.critico"/>								
					</td>
				</tr>

				<tr>
      				<td class="textoCampo">Proveedor &Uacute;nico:</td>
					<td class="textoDato">
						<@s.property default="&nbsp;" escape=false value="cotizacion.pedidoCotizacionDetalle.servicio.proveedorUnico"/>
					</td>
					<td class="textoCampo">Proveedor:</td>
					<td class="textoDato"><@s.property default="&nbsp;" escape=false value="cotizacion.pedidoCotizacionDetalle.servicio.proveedor.nombreFantasia"/>
					</td>
    			</tr>
    			<tr>			
					<td class="textoCampo">Tipo:</td>
	      			<td class="textoDato">
	      				<@s.property default="&nbsp;" escape=false value="cotizacion.pedidoCotizacionDetalle.servicio.tipo"/>
	      			</td>				      		
					<td class="textoCampo">Estado:</td>
					<td class="textoDato" colspan="3">
						<@s.property default="&nbsp;" escape=false value="cotizacion.pedidoCotizacionDetalle.servicio.estado"/>
					</td>
				</tr>
				<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>	
			</table>
		</div>	
	</@s.if>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
			<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            	<tr>
					<td><b>Datos de la Cotizaci&oacute;n</b></td>
				</tr>
			</table>

			<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
				<tr>
	        		<td class="textoCampo" colspan="4">&nbsp;</td>
	        	</tr>				
				<tr>
					<#-- Cotizaciones de Compra-->
					<#if cotizacion.pedidoCotizacionDetalle.pedidoCotizacion.tipo.ordinal()==1>								
							<td  class="textoCampo">Cantidad Desde: </td>
							<td class="textoDato">
							<@s.textfield 
  								templateDir="custontemplates" 
								id="cotizacion.cantidadMinima" 
								cssClass="textarea"
								cssStyle="width:160px" 
								name="cotizacion.cantidadMinima" 
								title="Cantidad M&iacute;nima" />
							</td>
							
					<#-- Cotizaciones de Servicio-->						
					<#else>
						  	<td  class="textoCampo">Cantidad Desde: </td>
							<td class="textoDato">1
							<@s.hidden id="cotizacion.cantidadMinima" name="cotizacion.cantidadMinima"/>									
							</td>
					</#if>
			  	 
							<td  class="textoCampo">Precio: </td>
							<td class="textoDato">
							<@s.textfield 
  								templateDir="custontemplates" 
								id="cotizacion.precio" 
								cssClass="textarea"
								cssStyle="width:160px" 
								name="cotizacion.precio" 
								title="Precio" />
							</td>     			
					</tr>	
					<tr>
						
						
					<#-- Cotizaciones de Compra-->
					<#if cotizacion.pedidoCotizacionDetalle.pedidoCotizacion.tipo.ordinal()==1>								
			     			<td  class="textoCampo">Cantidad M&aacute;xima a Proveer: </td>
							<td class="textoDato">
							<@s.textfield 
  								templateDir="custontemplates" 
								id="cotizacion.cantidadMaxima" 
								cssClass="textarea"
								cssStyle="width:160px" 
								name="cotizacion.cantidadMaxima" 
								title="Cantidad M&aacute;xima" />
							</td>	 
							
					<#-- Cotizaciones de Servicio-->						
					<#else>
			      			<td  class="textoCampo">Cantidad M&aacute;xima a Proveer: </td>
							<td class="textoDato">	&nbsp;						
							</td>	 
					</#if>
			  	 

							<td  class="textoCampo">Al&iacute;cuota IVA: </td>
							<td class="textoDato">
							<@s.textfield	      	
								templateDir="custontemplates" 							
  								template="textMoney"   
								id="cotizacion.alicuotaIva" 
								cssClass="textarea"
								cssStyle="width:160px" 
								name="cotizacion.alicuotaIva" 
								title="Al&iacute;cuota IVA" />
							</td>     			
						</tr>
						<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>		
			</table>
		</div>		
 
