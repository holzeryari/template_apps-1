
<#setting locale="es_AR">

<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>

<@s.hidden id="navigationId" name="navigationId"/>
<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
        	<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Datos del Pedido de Cotizaci&oacute;n</b></td>
			</tr>
		</table>
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
	    		<td class="textoCampo" colspan="4">&nbsp;</td>
	    	</tr>
	    	<tr>
				<td class="textoCampo">Producto/Bien:</td>
				<td class="textoDato">
				<@s.hidden id="productoBien.oid" name="productoBien.oid"/>
				<@s.hidden id="productoBien.descripcion" name="productoBien.descripcion"/>
				<@s.property default="&nbsp;" escape=false value="productoBien.descripcion"/>					
					<@s.a href="javascript://nop/"
						templateDir="custontemplates" id="seleccionarProductoBien" name="seleccionarProductoBien" cssClass="ocultarIcono">
						<img src="${request.contextPath}/common/images/buscar.gif" border="0" align="absmiddle" hspace="3" >
					</@s.a>	
				</td>
				<td class="textoCampo" align="right">&nbsp;</td>
				<td class="textoDato" align="left">&nbsp;</td>
			</tr>
			<tr>
	  			<td class="textoCampo">N&uacute;mero Pedido Cotizaci&oacute;n:</td>
      			<td class="textoDato">
      			<@s.textfield 
      				templateDir="custontemplates" 
					id="pedidoCotizacion.numero" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="pedidoCotizacion.numero" 
					title="Numero Pedido Cotizacion" />
				</td>							
	      		
      			<td class="textoCampo">Proveedor:</td>
      			<td class="textoDato">
      				<@s.property default="&nbsp;" escape=false value="pedidoCotizacion.proveedor.detalleDePersona.razonSocial" /><@s.property default="&nbsp;" escape=false value="pedidoCotizacion.proveedor.detalleDePersona.nombre" />							
					<@s.hidden id="nroProveedor" name="pedidoCotizacion.proveedor.nroProveedor"/>
					<@s.hidden id="razonSocial" name="pedidoCotizacion.proveedor.detalleDePersona.razonSocial"/>
					<@s.hidden id="nombre" name="pedidoCotizacion.proveedor.detalleDePersona.nombre"/>
					<@s.a templateDir="custontemplates" id="seleccionarProveedor" name="seleccionarProveedor" href="javascript://nop/" title="Buscar Proveedor">
						<img src="${request.contextPath}/common/images/buscar.gif" border="0" align="absmiddle" hspace="3" >
					</@s.a>		
				</td>	
			</tr>
			<tr>
      			<td class="textoCampo">Tipo:</td>
      			<td class="textoDato">
      			<@s.select 
						templateDir="custontemplates" 
						id="pedidoCotizacion.tipo" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="pedidoCotizacion.tipo" 
						list="tipoPedidoCotizacionList" 
						listKey="key" 
						listValue="description" 
						value="pedidoCotizacion.tipo.ordinal()"
						title="Tipo Pedido Cotizaci&oacute;n"
						headerKey="0"
						headerValue="Todos"							
				/>
				</td>
      			
      			<td class="textoCampo">Nro. Asignaci&oacute;n de Proveedores:</td>
      			<td class="textoDato">
	      			<@s.property default="&nbsp;" escape=false value="pedidoCotizacion.asignacionProveedor.numero" />
	      			<@s.hidden id="pedidoCotizacion.asignacionProveedor.numero" name="pedidoCotizacion.asignacionProveedor.numero"/>
	      			<@s.hidden id="pedidoCotizacion.asignacionProveedor.oid" name="pedidoCotizacion.asignacionProveedor.oid"/>	      			
	      			<@s.a templateDir="custontemplates" id="seleccionarAsignacionProveedor" name="seleccionarAsignacionProveedor" href="javascript://nop/">
						<img src="${request.contextPath}/common/images/buscar.gif" border="0" align="absmiddle" hspace="3" title="Buscar Asignaci&oacute;n de Proveedores">
					</@s.a>	
					<@vc.htmlContent 
						  baseUrl="${request.contextPath}/compraContratacion/pedidoCotizacion/selectAsignacionProveedor.action" 
						  source="seleccionarAsignacionProveedor" 
						  success="contentTrx" 
						  failure="errorTrx" 
						  parameters="navigationId={navigationId},flowControl=change,pedidoCotizacion.oid={pedidoCotizacion.oid},pedidoCotizacion.tipo={pedidoCotizacion.tipo},pedidoCotizacion.estado={pedidoCotizacion.estado},pedidoCotizacion.numero={pedidoCotizacion.numero},pedidoCotizacion.proveedor.nroProveedor={pedidoCotizacion.proveedor.nroProveedor},pedidoCotizacion.asignacionProveedor.numero={pedidoCotizacion.asignacionProveedor.numero},fechaPedidoDesde={fechaPedidoDesde},fechaPedidoHasta={fechaPedidoHasta},fechaLimiteRecepcionCotizacionDesde={fechaLimiteRecepcionCotizacionDesde},fechaLimiteRecepcionCotizacionHasta={fechaLimiteRecepcionCotizacionHasta}"/>
				</td>							
      		</tr>
	      	<tr>
				<td class="textoCampo">Estado:</td>
      			<td class="textoDato" colspan="3">
      			<@s.select 
						templateDir="custontemplates" 
						id="pedidoCotizacion.estado" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="pedidoCotizacion.estado" 
						list="estadoList" 
						listKey="key" 
						listValue="description" 
						value="pedidoCotizacion.estado.ordinal()"
						title="Estado"
						headerKey="0"
						headerValue="Todos"							
						/>
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Fecha Pedido Desde:</td>
      			<td class="textoDato">
      			<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="fechaPedidoDesde" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="fechaPedidoDesde" 
					title="Fecha Pedido Desde" />
				</td>							
      		
				<td class="textoCampo">Fecha Pedido Hasta:</td>
      			<td class="textoDato">
      			<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="fechaPedidoHasta" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="fechaPedidoHasta" 
					title="Fecha Pedido Hasta" />
			</tr>
				
			<tr>
				<td class="textoCampo">Fecha L&iacute;mite Recepci&oacute;n Cotizaciones Desde:</td>
      			<td class="textoDato">
      			<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="fechaLimiteRecepcionCotizacionDesde" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="fechaLimiteRecepcionCotizacionDesde" 
					title="Fecha L&iacute;mite Recepci&oacute;n Cotizaciones Desde" />
      			</td>
      			
      			<td class="textoCampo">Fecha L&iacute;mite Recepci&oacute;n Cotizaciones Hasta:</td>
      			<td class="textoDato">
      			<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="fechaLimiteRecepcionCotizacionHasta" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="fechaLimiteRecepcionCotizacionHasta" 
					title="Fecha L&iacute;mite Recepci&oacute;n Cotizaciones Hasta" />
			</tr>
			<tr>
	    			<td colspan="4" class="lineaGris" align="right">
	      			<input id="limpiar" type="button" name="btnVolver" value="Limpiar" class="boton"/>		
	      			<input id="buscar" type="button" name="btnVolver" value="Buscar" class="boton"/>
	    			</td>
				</tr>	
			</table>
		</div>	
			
		
        <!-- Resultado Filtro -->
			
			<@s.if test="pedidoCotizacionList!=null">
				<#assign pedidoCotizacionNumero = "0" />
				<@s.if test="pedidoCotizacion.numero != null">
					<#assign pedidoCotizacionNumero = "${pedidoCotizacion.numero?c}" />
				</@s.if>

				<#assign pedidoCotizacionEstado = "0" />
				<@s.if test="pedidoCotizacion.estado != null">
						<#assign pedidoCotizacionEstado = "${pedidoCotizacion.estado.ordinal()}" />
				</@s.if>
				
				<#assign pedidoCotizacionTipo = "0" />
				<@s.if test="pedidoCotizacion.tipo != null">
						<#assign pedidoCotizacionTipo = "${pedidoCotizacion.tipo.ordinal()}" />
				</@s.if>				
				
				<#assign pedidoCotizacionAsignacion = "0" />
				<@s.if test="pedidoCotizacion.asignacionProveedor.oid != null">
					<#assign pedidoCotizacionAsignacion = "${pedidoCotizacion.asignacionProveedor.oid?c}" />
				</@s.if>
				<#assign pedidoCotizacionAsignacionNumero = "0" />
				<@s.if test="pedidoCotizacion.asignacionProveedor.numero != null">
					<#assign pedidoCotizacionAsignacionNumero = "${pedidoCotizacion.asignacionProveedor.numero?c}" />
				</@s.if>
				
								
				<#assign pedidoCotizacionProveedor = "0" />
				<@s.if test="pedidoCotizacion.proveedor.nroProveedor != null">
					<#assign pedidoCotizacionProveedor = "${pedidoCotizacion.proveedor.nroProveedor?c}" />
				</@s.if>
				
				<#assign pedidoCotizacionFechaPedidoDesde = "" />
				<@s.if test="fechaPedidoDesde != null">
					<#assign pedidoCotizacionFechaPedidoDesde = "${fechaPedidoDesde?string('dd/MM/yyyy')}" />
				</@s.if>

				<#assign pedidoCotizacionFechaPedidoHasta = "" />
				<@s.if test="fechaPedidoHasta != null">
					<#assign pedidoCotizacionFechaPedidoHasta = "${fechaPedidoHasta?string('dd/MM/yyyy')}" />
				</@s.if>
				
				<#assign pedidoCotizacionFechaLimiteRecepcionCotizacionDesde = "" />
				<@s.if test="fechaLimiteRecepcionCotizacionDesde != null">
					<#assign pedidoCotizacionFechaLimiteRecepcionCotizacionDesde = "${fechaLimiteRecepcionCotizacionDesde?string('dd/MM/yyyy')}" />
				</@s.if>

				<#assign pedidoCotizacionFechaLimiteRecepcionCotizacionHasta = "" />
				<@s.if test="fechaLimiteRecepcionCotizacionHasta != null">
					<#assign pedidoCotizacionFechaLimiteRecepcionCotizacionHasta = "${fechaLimiteRecepcionCotizacionHasta?string('dd/MM/yyyy')}" />
				</@s.if>
				
				<#assign productoBienDescripcion = "" />
				<@s.if test=" productoBien!= null && productoBien.descripcion !=null && productoBien.descripcion!=''">
					<#assign productoBienDescripcion = "${productoBien.descripcion}" />
				</@s.if>
				
				<#assign productoBienOid = "0" />
				<@s.if test=" productoBien!= null && productoBien.oid !=null">
					<#assign productoBienOid = "${productoBien.oid?c}" />
				</@s.if>
				
				<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
				<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Pedidos de Cotizaci&oacute;n encontrados</td>
					<td> 
						<div class="alineacionDerecha">
							<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0361" 
								enabled="true" 
								cssClass="item" 
								id="enviarImprimir"
								name="enviarImprimir"												
								href="javascript://nop/">																				
								<img src="${request.contextPath}/common/images/enviarImprimir.gif" title="Imprimir/Enviar mail OC/OS" align="absmiddle" border="0" hspace="1" >
							</@security.a>
						</div>
						<div class="alineacionDerecha">
						<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0361" 
								cssClass="item" 
								href="${request.contextPath}/compraContratacion/pedidoCotizacion/printXLS.action?pedidoCotizacion.numero=${pedidoCotizacionNumero}&pedidoCotizacion.estado=${pedidoCotizacionEstado}&fechaPedidoDesde=${pedidoCotizacionFechaPedidoDesde}&fechaPedidoHasta=${pedidoCotizacionFechaPedidoHasta}&fechaLimiteRecepcionCotizacionDesde=${pedidoCotizacionFechaLimiteRecepcionCotizacionDesde}&fechaLimiteRecepcionCotizacionHasta=${pedidoCotizacionFechaLimiteRecepcionCotizacionHasta}&pedidoCotizacion.tipo=${pedidoCotizacionTipo}&pedidoCotizacion.proveedor.nroProveedor=${pedidoCotizacionProveedor}&pedidoCotizacion.asignacionProveedor.oid=${pedidoCotizacionAsignacion}&pedidoCotizacion.asignacionProveedor.numero=${pedidoCotizacionAsignacionNumero}&productoBien.descripcion=${productoBienDescripcion}&productoBien.oid=${productoBienOid}">
								<img src="${request.contextPath}/common/images/imprimirExcel.gif" title="Exportar a Excel" align="absmiddle" border="0" hspace="3">
							</@security.a>

						</div>
						<div class="alineacionDerecha">
							<b>Imprimir</b>
							<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0361" 
								cssClass="item" 
								href="${request.contextPath}/compraContratacion/pedidoCotizacion/printPDF.action?pedidoCotizacion.numero=${pedidoCotizacionNumero}&pedidoCotizacion.estado=${pedidoCotizacionEstado}&fechaPedidoDesde=${pedidoCotizacionFechaPedidoDesde}&fechaPedidoHasta=${pedidoCotizacionFechaPedidoHasta}&fechaLimiteRecepcionCotizacionDesde=${pedidoCotizacionFechaLimiteRecepcionCotizacionDesde}&fechaLimiteRecepcionCotizacionHasta=${pedidoCotizacionFechaLimiteRecepcionCotizacionHasta}&pedidoCotizacion.tipo=${pedidoCotizacionTipo}&pedidoCotizacion.proveedor.nroProveedor=${pedidoCotizacionProveedor}&pedidoCotizacion.asignacionProveedor.oid=${pedidoCotizacionAsignacion}&pedidoCotizacion.asignacionProveedor.numero=${pedidoCotizacionAsignacionNumero}&productoBien.descripcion=${productoBienDescripcion}&productoBien.oid=${productoBienOid}">
								<img src="${request.contextPath}/common/images/imprimirPdf.gif" title="Exportar a PDF" align="absmiddle" border="0"hspace="3">
							</@security.a>
						</div>	
					</td>
				</tr>
			</table>	
				
			

	  	  <@vc.htmlContent 
		  baseUrl="${request.contextPath}/compraContratacion/pedidoCotizacion/selectEnviarImprimir.action" 
		  source="enviarImprimir" 
		  success="contentTrx" 
		  failure="errorTrx"
		  parameters="navigationId=pedidoCotizacion-visualizarEnviarImprimir,flowControl=regis,pedidoCotizacion.numero={pedidoCotizacion.numero},pedidoCotizacion.proveedor.detalleDePersona.razonSocial={pedidoCotizacion.proveedor.detalleDePersona.razonSocial},pedidoCotizacion.proveedor.detalleDePersona.nombre={pedidoCotizacion.proveedor.detalleDePersona.nombre},pedidoCotizacion.estado={pedidoCotizacion.estado},pedidoCotizacion.asignacionProveedor.numero={pedidoCotizacion.asignacionProveedor.numero},pedidoCotizacion.proveedor.nroProveedor={pedidoCotizacion.proveedor.nroProveedor},pedidoCotizacion.tipo={pedidoCotizacion.tipo},fechaPedidoDesde={fechaPedidoDesde},fechaPedidoHasta={fechaPedidoHasta},fechaLimiteRecepcionCotizacionDesde={fechaLimiteRecepcionCotizacionDesde},fechaLimiteRecepcionCotizacionHasta={fechaLimiteRecepcionCotizacionHasta},pedidoCotizacion.asignacionProveedor.oid={pedidoCotizacion.asignacionProveedor.oid},navegacionIdBack=${navigationId}"/> 
										  
		<@vc.anchors target="contentTrx">			
          		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="pedidoCotizacionList" id="pedidoCotizacion" pagesize=15 defaultsort=2 partialList=true size="recordSize" keepStatus=true excludedParams="resetFlag">
        			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" class="botoneraAnchoCon4" title="Acciones">
						<div class="alineacion">
							<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0362" 
							enabled="pedidoCotizacion.readable" 
							cssClass="item" 
							href="${request.contextPath}/compraContratacion/pedidoCotizacion/readPedidoCotizacionView.action?pedidoCotizacion.oid=${pedidoCotizacion.oid?c}&navigationId=pedidoCotizacion-visualizar&flowControl=regis&navegacionIdBack=${navigationId}">							
							<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver"  border="0">
							</@security.a>
						</div>
						<div class="alineacion">	
							<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0363" 
							enabled="pedidoCotizacion.updatable"
							cssClass="item"  
							href="${request.contextPath}/compraContratacion/pedidoCotizacion/administrarPedidoCotizacionView.action?pedidoCotizacion.oid=${pedidoCotizacion.oid?c}&navigationId=administrar&flowControl=regis">
							<img  src="${request.contextPath}/common/images/administrar.gif" alt="Administrar" title="Administrar"  border="0">
							</@security.a>
						</div>
						<div class="alineacion">
							<@security.a  
							templateDir="custontemplates" 
							securityCode="CUF0364" 
							enabled="pedidoCotizacion.eraseable"
							cssClass="item"  
							href="${request.contextPath}/compraContratacion/pedidoCotizacion/anularPedidoCotizacionView.action?pedidoCotizacion.oid=${pedidoCotizacion.oid?c}&navigationId=pedidoCotizacion-anular&flowControl=regis">
							<img  src="${request.contextPath}/common/images/eliminar.gif" alt="Anular" title="Anular"  border="0">
							</@security.a>	
						</div>
						<#--
						<div class="alineacion">
							<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0365" 
							enabled="pedidoCotizacion.enable"
							cssClass="item"  
							href="${request.contextPath}/compraContratacion/pedidoCotizacion/intervenirPedidoCotizacionView.action?pedidoCotizacion.oid=${pedidoCotizacion.oid?c}&navigationId=pedidoCotizacion-intervenir&flowControl=regis">
							<img  src="${request.contextPath}/common/images/intervenir.gif" alt="Intervenir" title="Intervenir"  border="0">
							</@security.a>	
						</div>
						-->
						<div class="alineacion">						
							<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0366" 
							enabled="pedidoCotizacion.readable" 
							cssClass="no-rewrite" 
							href="${request.contextPath}/compraContratacion/pedidoCotizacion/printPedidoCotizacionPDF.action?pedidoCotizacion.oid=${pedidoCotizacion.oid?c}">
							<img  src="${request.contextPath}/common/images/imprimir.gif" alt="Imprimir" title="Imprimir"  border="0">
							</@security.a>							
						</div>
					</@display.column>
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="numero" title="Nro Pedido" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="fechaPedido" format="{0,date,dd/MM/yyyy}"  title="Fecha Pedido" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="tipo" title="Tipo" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" title="Proveedor">  
						${pedidoCotizacion.proveedor.detalleDePersona.razonSocial} 
						
					<#if pedidoCotizacion.proveedor.detalleDePersona.nombre?exists> 
							${pedidoCotizacion.proveedor.detalleDePersona.nombre}
						</#if>
		
					</@display.column> 
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="asignacionProveedor.numero" title="Nro Asignaci&oacute;n Prov." />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="fechaLimiteCot" format="{0,date,dd/MM/yyyy}"  title="Fecha Lim. Recep. Cot." />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="estado" title="Estado" />					
					
				</@display.table>
			</@vc.anchors>
			</div>
		</@s.if>
		
<@vc.htmlContent 
  baseUrl="${request.contextPath}/compraContratacion/pedidoCotizacion/selectProveedorSearch.action" 
  source="seleccionarProveedor" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=change,pedidoCotizacion.oid={pedidoCotizacion.oid},productoBien.oid={productoBien.oid},productoBien.descripcion={productoBien.descripcion},pedidoCotizacion.tipo={pedidoCotizacion.tipo},pedidoCotizacion.estado={pedidoCotizacion.estado},pedidoCotizacion.numero={pedidoCotizacion.numero},pedidoCotizacion.proveedor.nroProveedor={pedidoCotizacion.proveedor.nroProveedor},pedidoCotizacion.asignacionProveedor.numero={pedidoCotizacion.asignacionProveedor.numero},fechaPedidoDesde={fechaPedidoDesde},fechaPedidoHasta={fechaPedidoHasta},fechaLimiteRecepcionCotizacionDesde={fechaLimiteRecepcionCotizacionDesde},fechaLimiteRecepcionCotizacionHasta={fechaLimiteRecepcionCotizacionHasta},pedidoCotizacion.asignacionProveedor.oid={pedidoCotizacion.asignacionProveedor.oid},pedidoCotizacion.proveedor.detalleDePersona.razonSocial={pedidoCotizacion.proveedor.detalleDePersona.razonSocial}"/>


<@vc.htmlContent 
  baseUrl="${request.contextPath}/compraContratacion/pedidoCotizacion/search.action"
  source="buscar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="resetFlag=true,navigationId={navigationId},flowControl=regis,pedidoCotizacion.numero={pedidoCotizacion.numero},pedidoCotizacion.proveedor.detalleDePersona.razonSocial={pedidoCotizacion.proveedor.detalleDePersona.razonSocial},pedidoCotizacion.proveedor.detalleDePersona.nombre={pedidoCotizacion.proveedor.detalleDePersona.nombre},pedidoCotizacion.estado={pedidoCotizacion.estado},productoBien.oid={productoBien.oid},productoBien.descripcion={productoBien.descripcion},pedidoCotizacion.asignacionProveedor.numero={pedidoCotizacion.asignacionProveedor.numero},pedidoCotizacion.proveedor.nroProveedor={pedidoCotizacion.proveedor.nroProveedor},pedidoCotizacion.tipo={pedidoCotizacion.tipo},fechaPedidoDesde={fechaPedidoDesde},fechaPedidoHasta={fechaPedidoHasta},fechaLimiteRecepcionCotizacionDesde={fechaLimiteRecepcionCotizacionDesde},fechaLimiteRecepcionCotizacionHasta={fechaLimiteRecepcionCotizacionHasta},pedidoCotizacion.asignacionProveedor.oid={pedidoCotizacion.asignacionProveedor.oid}"/>
  		      

<@vc.htmlContent 
  baseUrl="${request.contextPath}/compraContratacion/pedidoCotizacion/view.action"
  source="limpiar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=regis"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/compraContratacion/pedidoCotizacion/createPedidoCotizacionView.action" 
  source="crear" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters=""/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/compraContratacion/pedidoCotizacion/selectProductoBienSearch.action" 
  source="seleccionarProductoBien" 
  success="contentTrx" 
  failure="errorTrx"
  parameters="navigationId={navigationId},flowControl=change,pedidoCotizacion.oid={pedidoCotizacion.oid},productoBien.oid={productoBien.oid},productoBien.descripcion={productoBien.descripcion},pedidoCotizacion.tipo={pedidoCotizacion.tipo},pedidoCotizacion.estado={pedidoCotizacion.estado},pedidoCotizacion.numero={pedidoCotizacion.numero},pedidoCotizacion.proveedor.nroProveedor={pedidoCotizacion.proveedor.nroProveedor},pedidoCotizacion.asignacionProveedor.numero={pedidoCotizacion.asignacionProveedor.numero},fechaPedidoDesde={fechaPedidoDesde},fechaPedidoHasta={fechaPedidoHasta},fechaLimiteRecepcionCotizacionDesde={fechaLimiteRecepcionCotizacionDesde},fechaLimiteRecepcionCotizacionHasta={fechaLimiteRecepcionCotizacionHasta},pedidoCotizacion.asignacionProveedor.oid={pedidoCotizacion.asignacionProveedor.oid},pedidoCotizacion.proveedor.detalleDePersona.razonSocial={pedidoCotizacion.proveedor.detalleDePersona.razonSocial}"/>

