<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<@vc.anchors target="contentTrx" ajaxFlag="ajax">
<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
			<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Servicios a cotizar</td>
				</tr>
			</table>		 

			<table class="tablaDetalleCuerpo" cellpadding="3" >	

						<#assign pedidoCotizacionDetalleLista = pedidoCotizacion.pedidoCotizacionDetalleList>
						<#list pedidoCotizacionDetalleLista as pedidoCotizacionDetalle>
							 <tr>
							<th align="center">Acciones</th>
							<th align="center">N&uacute;mero</th>
							<th align="center">C&oacute;digo</th>
							<th align="center">Descripci&oacute;n</th>
							<th align="center">Rubro</th>
							<th align="center">Tipo</th>
							
						</tr>
							
						</tr>
						<tr>
							<td> 
								<div align="center">
								<@s.a templateDir="custontemplates" id="verDetallePedidoCotizacion" name="verDetallePedidoCotizacion" href="${request.contextPath}/compraContratacion/pedidoCotizacion/readPedidoCotizacionDetalleView.action?pedidoCotizacionDetalle.oid=${pedidoCotizacionDetalle.oid?c}&navegacionIdBack=${navigationId}" cssClass="ocultarIcono">
									<img src="${request.contextPath}/common/images/ver.gif" border="0" align="absmiddle" hspace="3" >
								</@s.a>
							
								</div>
							</td>
							<td class="estiloNumero">${pedidoCotizacionDetalle.numero}</td>
							<td class="estiloNumero">${pedidoCotizacionDetalle.servicio.oid}</td>
							<td class="estiloTexto">${pedidoCotizacionDetalle.servicio.descripcion}</td>
							<td class="estiloTexto">${pedidoCotizacionDetalle.servicio.rubro.descripcion}</td>
							<td colspan="1" class="estiloTexto">${pedidoCotizacionDetalle.servicio.tipo}</td>
									   
						</tr>
						
						<tr>
							<td >&nbsp;</td>
							<th align="center" nowrap><b>N&uacute;mero Cotizaci&oacute;n</b></th>
							<th align="center">Precio</th>
							<th colspan="3" align="center">Al&iacute;cuota IVA</th>	
						</tr>
						
						<#assign cotizacionLista = pedidoCotizacionDetalle.cotizacionList>
						<#list cotizacionLista as cotizacion>
												
						<tr>
							<td>&nbsp;</td>
							<td class="estiloNumero">${cotizacion.numero}</td>			
							<td class="estiloNumero">${cotizacion.precio}</td>
							<td colspan="3" class="estiloNumero">
							<#if cotizacion.alicuotaIva?exists>
        						${cotizacion.alicuotaIva}%
        					<#else>
        						&nbsp;
        					</#if>
							</td>								
						</tr>						
						</#list>
						  
					</#list>						
			</table>
		</div>				

</@vc.anchors>
