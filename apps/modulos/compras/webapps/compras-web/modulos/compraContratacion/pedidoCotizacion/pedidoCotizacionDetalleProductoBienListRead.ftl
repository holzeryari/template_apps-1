<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<@vc.anchors target="contentTrx" ajaxFlag="ajax">
<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
			<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Productos/Bienes a cotizar</td>
				</tr>
			</table>		 

			<table class="tablaDetalleCuerpo" cellpadding="3" >
				
				<#assign pedidoCotizacionDetalleLista = pedidoCotizacion.pedidoCotizacionDetalleList>
					<#list pedidoCotizacionDetalleLista as pedidoCotizacionDetalle>
					     <tr>
							<th align="center">Acciones</th>
							<th align="center">N&uacute;mero</th>
							<th align="center">C&oacute;digo</th>
							<th align="center">Descripci&oacute;n</th>
							<th align="center">Marca</th>
							<th align="center">Modelo</th>
							<th align="center">Rubro</th>
							<th align="center">Cant. Solicitada</th>
						</tr>
						<tr>
							<td> 
								<div align="center">
								<@s.a templateDir="custontemplates" id="verDetallePedidoCotizacion" name="verDetallePedidoCotizacion" href="${request.contextPath}/compraContratacion/pedidoCotizacion/readPedidoCotizacionDetalleView.action?pedidoCotizacionDetalle.oid=${pedidoCotizacionDetalle.oid?c}&navegacionIdBack=${navigationId}" cssClass="ocultarIcono">
									<img src="${request.contextPath}/common/images/ver.gif" border="0" align="absmiddle" hspace="3" >
								</@s.a>
								<#if pedidoCotizacionDetalle.archivo?exists>
									<a href="javascript://nop/" class="item" >
									<img src="${request.contextPath}/common/images/bajar.png" alt="Bajar Archivo" title="Bajar Archivo" border="0"
									onclick="window.open('${request.contextPath}/compraContratacion/pedidoCotizacion/pedidoCotizacionRedirectArchivoView.action?pedidoCotizacionDetalle.oid=${pedidoCotizacionDetalle.oid?c}', '_blank','width=400,height=300,resizable=yes,scrollbars=yes,left=100,top=80,toolbar=no,menubar=no,location=no'); return false;">
									</a>
								</#if> 
							
								</div>
							</td>
							<td class="estiloNumero">${pedidoCotizacionDetalle.numero}</td>
							<td class="estiloNumero">${pedidoCotizacionDetalle.productoBien.oid}</td>
							<td class="estiloTexto">${pedidoCotizacionDetalle.productoBien.descripcion}</td>
							<td class="estiloTexto">${pedidoCotizacionDetalle.productoBien.marca}</td>
							<td class="estiloTexto">${pedidoCotizacionDetalle.productoBien.modelo}</td>
							<td class="estiloTexto">${pedidoCotizacionDetalle.productoBien.rubro.descripcion}</td>
							<td class="estiloNumero">${pedidoCotizacionDetalle.cantidadSolicitada}</td>  
						</tr>
						
						<tr>
							<td>&nbsp;</td>
							<th colspan="3" align="center" nowrap><b>N&uacute;mero Cotizaci&oacute;n</b></th>								 	
							<th align="center" >Cant. Desde</th>
							<th align="center" >Precio</th>
							<th align="center" >Al&iacute;cuota IVA</th>
							<th align="center">Cant. m&aacute;xima a proveer</th>
						</tr>
						
						<#assign cotizacionLista = pedidoCotizacionDetalle.cotizacionList>
						<#list cotizacionLista as cotizacion>
												
						<tr>
							<td >&nbsp;</td>
							<td colspan="3" class="estiloNumero">${cotizacion.numero}</td>
							<td class="estiloNumero">${cotizacion.cantidadMinima}</td>
							<td class="estiloNumero">${cotizacion.precio}</td>
							<td class="estiloNumero">
							<#if cotizacion.alicuotaIva?exists>
        						${cotizacion.alicuotaIva}%
        					<#else>
        						&nbsp;
        					</#if>
	
							
							</td>
							<td class="estiloNumero">
							<#if cotizacion.cantidadMaxima?exists>
        						${cotizacion.cantidadMaxima}
        					<#else>
        						&nbsp;
        					</#if>
							</td>
							
						</tr>						
						</#list>
						  
					</#list>						

				</table>
			</div>		

</@vc.anchors>

