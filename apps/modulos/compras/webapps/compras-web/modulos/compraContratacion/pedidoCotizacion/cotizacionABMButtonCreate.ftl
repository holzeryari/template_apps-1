<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaSeparadora" class="capaSeparadora">&nbsp;</div>
<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancel" type="button" name="btnCancelar" value="Cancelar" class="boton"/>
				<input id="store" type="button" name="btnAgregar" value="Agregar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/compraContratacion/pedidoCotizacion/createCotizacion.action" 
  source="store" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="cotizacion.pedidoCotizacionDetalle.oid={cotizacion.pedidoCotizacionDetalle.oid},cotizacion.cantidadMinima={cotizacion.cantidadMinima},cotizacion.precio={cotizacion.precio},cotizacion.cantidadMaxima={cotizacion.cantidadMaxima},cotizacion.alicuotaIva={cotizacion.alicuotaIva}"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=administrar,flowControl=back"/>
  
  