<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>
<#if asignacionProveedor.asignacionProveedorDetalleList.size()!=0>

<@vc.anchors target="contentTrx" ajaxFlag="ajax">
	<div id="capaSeparadora" class="capaSeparadora">&nbsp;</div>
	<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
		<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
			<tr>
				<td>Servicios solicitados</td>
				<td>
					<div align="right">
						<@s.a href="${request.contextPath}/compraContratacion/asignacionProveedor/selectServicio.action?asignacionProveedor.oid=${asignacionProveedor.oid?c}" templateDir="custontemplates" id="agregarProductoBien" name="agregarProductoBien" cssClass="ocultarIcono">
						<b>Agregar</b><img src="${request.contextPath}/common/images/agregar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
						</@s.a>		
					</div>
				</td>
			</tr>
		</table>
	
		<table class="tablaDetalleCuerpo" cellpadding="3" >
			<#assign asignacionProveedorDetalleLista = asignacionProveedor.asignacionProveedorDetalleList>
			<#list asignacionProveedorDetalleLista as asignacionProveedorDetalle>
			 <tr>	      					
				<th align="center">Acciones</th>
				<th align="center">N&uacute;mero</th>
				<th align="center">C&oacute;digo</th>
				<th align="center" colspan="2">Descripci&oacute;n</th>
				<th align="center" colspan="2">Rubro</th>
				<th align="center" colspan="2">Tipo</th>
			</tr>
			
			<tr>
				<td class="botoneraAnchoCon2"> 
					<div class="alineacion">
					<@s.a templateDir="custontemplates" id="verAsignacionProveedorDetalle" name="verAsignacionProveedorDetalle" 
					href="${request.contextPath}/compraContratacion/asignacionProveedor/readAsignacionProveedorDetalleView.action?asignacionProveedorDetalle.oid=${asignacionProveedorDetalle.oid?c}&navegacionIdBack=${navigationId}&navigationId=asignacionProveedorDetalle-visualizar&flowControl=regis" >
						<img src="${request.contextPath}/common/images/ver.gif" border="0" align="absmiddle" hspace="3" alt="Ver" title="Ver">
					</@s.a>
					</div>
					<div class="alineacion">
						<#if !asignacionProveedor.desahibilitarBotonesAsignacion>
							<@s.a templateDir="custontemplates" id="eliminarAsignacionProveedorDetalle" name="eliminarAsignacionProveedorDetalle" href="${request.contextPath}/compraContratacion/asignacionProveedor/deleteAsignacionProveedorDetalleView.action?asignacionProveedorDetalle.oid=${asignacionProveedorDetalle.oid?c}&navegacionIdBack=${navigationId}&navigationId=asignacionProveedorDetalle-eliminar&flowControl=regis" >
								<img src="${request.contextPath}/common/images/eliminar.gif" border="0" align="absmiddle" hspace="3" alt="Eliminar" title="Eliminar" >
							</@s.a>
						</#if>	
					</div>
					
				</td>
				<td class="estiloNumero">${asignacionProveedorDetalle.numero}</td>
				<td class="estiloNumero">${asignacionProveedorDetalle.servicio.oid}</td>
				<td class="estiloTexto" colspan="2">${asignacionProveedorDetalle.servicio.descripcion}</td>
				<td class="estiloTexto" colspan="2">${asignacionProveedorDetalle.servicio.rubro.descripcion}</td>
				<td class="estiloTexto" colspan="2">${asignacionProveedorDetalle.servicio.tipo}</td>			   
			</tr>
			
			<tr>
				<td colspan="1">&nbsp;</td>
				<th colspan="1" nowrap>
					<div align="center">
					<#if asignacionProveedorDetalle.servicio.proveedorUnico.ordinal()==2>
						<b>Proveedor</b>								
					<#else>
						<b>Proveedor</b>
						<@s.a templateDir="custontemplates" id="agregarProveedor" name="agregarProveedor" 
						href="${request.contextPath}/compraContratacion/asignacionProveedor/selectProveedor.action?navegacionIdBack=${navigationId}&asignacionProveedorDetalle.oid=${asignacionProveedorDetalle.oid?c}" cssClass="ocultarIcono">						
							<img src="${request.contextPath}/common/images/agregar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
						</@s.a>								
					</#if>
				</div>
				</th>	
				<th align="center">Condici&oacute;n</th>									 	
				<th align="center">Nombre Fantas&iacute;a</th>
				<th align="center">Raz&oacute;n Social</th>
				<th align="center">Nombre</th>
				<th align="center">CUIT/CUIL</th>
				<th align="center">ICS</th>
				<th align="center">Situaci&oacute;n</th>
			</tr>
			
			<#assign proveedorLista = asignacionProveedorDetalle.proveedorList>
			<#list proveedorLista as proveedor>
									
			<tr>
				<td>&nbsp;</td>
				<td class="botoneraAnchoCon2">
					<div class="alineacion">								
					<@s.a templateDir="custontemplates" id="verProveedor" name="verProveedor"					
					href="${request.contextPath}/maestro/proveedor/readView.action?proveedor.nroProveedor=${proveedor.nroProveedor?c}&navigationId=read-proveedor&navegacionIdBack=${navigationId}&flowControl=regis">
						<img src="${request.contextPath}/common/images/ver.gif" border="0" align="absmiddle" hspace="3" alt="Ver" title="Ver">
					</@s.a>
					</div>								
					<#if asignacionProveedorDetalle.servicio.proveedorUnico.ordinal()!=2>
						<div class="alineacion">
							<#if !asignacionProveedor.desahibilitarBotonesAsignacion>
								<@s.a templateDir="custontemplates" id="eliminarProveedor" name="eliminarProveedor" href="${request.contextPath}/compraContratacion/asignacionProveedor/deleteProveedorAsignado.action?proveedor.nroProveedor=${proveedor.nroProveedor?c}&asignacionProveedorDetalle.oid=${asignacionProveedorDetalle.oid?c}&navegacionIdBack=${navigationId}&navigationId=proveedor-visualizar&flowControl=regis" cssClass="texto1">
								<img src="${request.contextPath}/common/images/eliminar.gif" border="0" align="absmiddle" hspace="3" >
								</@s.a>		
							</#if>					
						</div>								
					</#if>
	
				</td>
				<td  class="botoneraAnchoCon3">	
				<div class="alineacionMenor" >				
					<#if proveedor.esCooperativa?exists && proveedor.esCooperativa.ordinal()==2>
					<img src="${request.contextPath}/common/images/pino.gif" border="0" align="center" alt="Es cooperativa" title="Es cooperativa" >								
					<#else>
					<div class="item">
					<img src="${request.contextPath}/common/images/pino.gif" border="0" align="center" alt="No es cooperativa" title="No es cooperativa">
					</div>
					</#if>
				</div>
					
				<div class="alineacionMenor">							
					<#if proveedor.adtividadRegion?exists && proveedor.adtividadRegion.ordinal()==2>
					<img src="${request.contextPath}/common/images/sailboat.gif" border="0" align="center" alt="Tiene actividad en la region" title="Tiene actividad en la region" >								
					<#else>
					<div class="item">
					<img src="${request.contextPath}/common/images/sailboat.gif" border="0" align="center"  alt="No tiene actividad en la region" title="No tiene actividad en la region">
					</div>
					</#if>	
				</div>
				<div class="alineacionMenor">
					<#if proveedor.detalleDePersona.socio?exists>
					<img src="${request.contextPath}/common/images/manosAgarradas.gif" border="0" align="center" alt="Es socio" title="Es socio" >								
					<#else>
					<div class="item">
					<img src="${request.contextPath}/common/images/manosAgarradas.gif" border="0" align="center" alt="No es socio" title="No es socio" >
					</div>
					</#if>
				</div>
					
				</td>
				<td class="estiloTexto">
					<#if proveedor.nombreFantasia?exists>
						${proveedor.nombreFantasia}
					</#if>&nbsp;
				</td>
				<td class="estiloTexto">
					<#if proveedor.detalleDePersona.razonSocial?exists>
						${proveedor.detalleDePersona.razonSocial}
					</#if>&nbsp;
				</td>
				<td class="estiloTexto">
					<#if proveedor.detalleDePersona.nombre?exists>
						${proveedor.detalleDePersona.nombre}
					</#if>&nbsp;
				</td>
				<td class="estiloNumero">
					<#if proveedor.detalleDePersona.persona.docFiscal?exists>
						${proveedor.detalleDePersona.persona.docFiscal?c}
					</#if>&nbsp;
				</td>
				<td class="estiloNumero">
					<#if proveedor.ultimoICS?exists>
						${proveedor.ultimoICS}
					</#if>&nbsp;
				</td>
				<td class="estiloTexto">
					<#if proveedor.situacionICS?exists>
						${proveedor.situacionICS}
					</#if>&nbsp;
				</td>	
				
			</tr>						
			</#list>
			  		<tr><td colspan="9">&nbsp;</td></tr>
		</#list>						
	
	</tbody>
	
	</table>	



</@vc.anchors>
</div>
</#if>
