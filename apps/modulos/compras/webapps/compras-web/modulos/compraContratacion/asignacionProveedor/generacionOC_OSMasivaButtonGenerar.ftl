<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaSeparadora" class="capaSeparadora">&nbsp;</div>
<div id="capaTexto" class="capaTexto">
	<table>
		<tr>
			<td align="left">
				<input id="proponer" type="button" name="btnProponer" value="Sugerir" class="boton"/>
				La acci&oacute;n de "Sugerir" mostrar&aacute; las mejores cotizaciones y eliminar&aacute; los datos selecciones de las cotizaciones existentes.
			</td>
		</tr>
	</table>			
</div>
<div id="capaSeparadora" class="capaSeparadora">&nbsp;</div>
<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancelar" type="button" name="btnCancelar" value="Cancelar" class="boton"/>
				<input id="store" type="button" name="btnActivar" value="Generar OC/OS" class="boton"/>
			</td>
		</tr>	
	</table>
</div>


<@vc.htmlContent 
  baseUrl="${request.contextPath}/compraContratacion/asignacionProveedor/proponerCotizaciones.action" 
  source="proponer" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="asignacionProveedor.oid={asignacionProveedor.oid}"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/compraContratacion/asignacionProveedor/generarOCOS.action" 
  source="store" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="asignacionProveedor.oid={asignacionProveedor.oid},navigationId=asignacionProveedor-generarOCMasiva,flowControl=regis"/>
  
  <@vc.htmlContent 
   baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancelar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=buscar-asignacionProveedor,flowControl=back"/>