<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<@s.hidden id="asignacionProveedorDetalle.oid" name="asignacionProveedorDetalle.oid"/>
<@s.hidden id="asignacionProveedorDetalle.versionNumber" name="asignacionProveedorDetalle.versionNumber"/>
<@s.hidden id="proveedor.nroProveedor" name="proveedor.nroProveedor"/>
<@s.hidden id="asignacionProveedorDetalleCotizacion.oid" name="asignacionProveedorDetalleCotizacion.oid"/>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
			<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos de la Asignaci&oacute;n de Proveedor</b></td>
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>	
			<tr>
      			<td class="textoCampo">N&uacute;mero:</td>
      			<td class="textoDato">
      				<@s.property default="&nbsp;" escape=false value="asignacionProveedorDetalle.asignacionProveedor.numero"/>
      			</td>
      			<td  class="textoCampo">Fecha Asignaci&oacute;n: </td>
				<td class="textoDato">
				<@s.if test="asignacionProveedorDetalle.asignacionProveedor.fechaAsignacion != null">
				<#assign fechaAsignacion = asignacionProveedorDetalle.asignacionProveedor.fechaAsignacion> 
				${fechaAsignacion?string("dd/MM/yyyy")}	
				</@s.if>									
				</td>	      			
			</tr>	
			<tr>
				<td class="textoCampo">Tipo:</td>
	      		<td class="textoDato">
      			<@s.property default="&nbsp;" escape=false value="asignacionProveedorDetalle.asignacionProveedor.tipo"/>
      			</td>
				<td class="textoCampo">Fecha L&iacute;mite Recepci&oacute;n de Cotizaciones:</td>
      			<td class="textoDato">	
      			<@s.if test="asignacionProveedorDetalle.asignacionProveedor.fechaLimiteCotizacion != null">
				<#assign fechaLimiteCotizacion = asignacionProveedorDetalle.asignacionProveedor.fechaLimiteCotizacion> 
				${fechaLimiteCotizacion?string("dd/MM/yyyy")}	
				</@s.if>	
				</td>	
    		</tr>
    		
    		<tr>
				<td class="textoCampo">Fecha de Entrega/Prestaci&oacute;n:</td>
      			<td class="textoDato" colspan="3">
      			<@s.if test="asignacionProveedorDetalle.asignacionProveedor.fechaEntrega != null">
				<#assign fechaEntrega = asignacionProveedorDetalle.asignacionProveedor.fechaEntrega> 
				${fechaEntrega?string("dd/MM/yyyy")}
				</@s.if>	
				</td>	
			</tr>
			<tr>
				<td class="textoCampo">Lugar de Entrega/Prestaci&oacute;n:</td>
      			<td  class="textoDato" colspan="3">
      			<@s.property default="&nbsp;" escape=false value="asignacionProveedorDetalle.asignacionProveedor.lugarEntrega"/>							 
				</td>					
    		</tr>
    		
    		<tr>
				<td class="textoCampo">Observaciones:</td>
      			<td  class="textoDato" colspan="3">
      			<@s.property default="&nbsp;" escape=false value="asignacionProveedorDetalle.asignacionProveedor.observaciones"/>
      			&nbsp;	
				</td>					
    		</tr>
	    		
    		<tr>
				<td class="textoCampo">Estado:</td>
      			<td  class="textoDato" colspan="3">
					<@s.property default="&nbsp;" escape=false value="asignacionProveedorDetalle.asignacionProveedor.estado"/>
				</td>	      		
    		</tr>
    		
		</table>		
	</div>

	<@s.if test="asignacionProveedorDetalle.productoBien != null">
		<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos del Producto/Bien Cotizaci&oacute;n</b></td>
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
									
			<tr>
      			<td class="textoCampo">C&oacute;digo:</td>
      			<td class="textoDato">
      			<@s.property default="&nbsp;" escape=false value="asignacionProveedorDetalle.productoBien.oid"/>
      			</td>
      			<td  class="textoCampo">Descripci&oacute;n: </td>
				<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="asignacionProveedorDetalle.productoBien.descripcion"/>		
				</td>	      			
			</tr>	
			<tr>
      			<td class="textoCampo">Tipo:</td>
	      		<td class="textoDato">
	      			<@s.property default="&nbsp;" escape=false value="asignacionProveedorDetalle.productoBien.tipo"/>						      			
	      		</td>		      		
										
			<td class="textoCampo">Rubro:</td>
  			<td class="textoDato">
  				<@s.property default="&nbsp;" escape=false value="asignacionProveedorDetalle.productoBien.rubro.descripcion"/>
			</td>
    		</tr>
			<tr>
				<td class="textoCampo">Marca: </td>
				<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="asignacionProveedorDetalle.productoBien.marca"/>										
				</td>
				<td class="textoCampo">Modelo: </td>
				<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="asignacionProveedorDetalle.productoBien.modelo"/>										
				</td>
    		</tr>
    		<tr>
				<td class="textoCampo">Valor Mercado: </td>
				<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="asignacionProveedorDetalle.bien.valorMercado"/>									
				</td>
				<td class="textoCampo">Fecha Valor Mercado: </td>
				<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="asignacionProveedorDetalle.bien.fechaValorMercado"/>									
				</td>
    		</tr>
			<tr>
				<td class="textoCampo">C&oacute;digo de Barra: </td>
				<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="asignacionProveedorDetalle.productoBien.codigoBarra"/>										
				</td>
				<td class="textoCampo">Es Cr&iacute;tico: </td>
				<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="asignacionProveedorDetalle.productoBien.critico"/>						      			
				</td>
    		</tr>
    		<tr>
				<td class="textoCampo">Reposici&oacute;n Autm&aacute;tica: </td>
				<td  class="textoDato">	
					<@s.property default="&nbsp;" escape=false value="asignacionProveedorDetalle.producto.reposicionAutomatica"/>						      											
				</td>
				<td class="textoCampo">Existencia M&iacute;nima: </td>
				<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="asignacionProveedorDetalle.producto.existenciaMinima"/>
				</td>
    		</tr>
    		<tr>
    		<td class="textoCampo">Tipo Unidad:</td>
    		<td  class="textoDato" colspan="3">
					<@s.property default="&nbsp;" escape=false value="asignacionProveedorDetalle.producto.tipoUnidad.codigo"/>
				</td>
    		</tr>
			<tr>
				<td class="textoCampo">Es Registrable: </td>
				<td  class="textoDato" colspan="3">
				<@s.property default="&nbsp;" escape=false value="asignacionProveedorDetalle.bien.registrable"/>						      			
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Es Veh&iacute;culo: </td>
				<td  class="textoDato" colspan="3"><@s.property default="&nbsp;" escape=false value="asignacionProveedorDetalle.bien.vehiculo"/></td>
			</tr>							
			<tr>
				<td class="textoCampo">Estado: </td>
	  			<td class="textoDato" colspan="3"><@s.property default="&nbsp;" escape=false value="asignacionProveedorDetalle.productoBien.estado"/></td>
			</tr>
    		<tr>					    		
				<td class="textoCampo">Cantidad Solicitada Total en FSC/FSS: </td>
				<td  class="textoDato">
				<@s.property default="&nbsp;" escape=false value="asignacionProveedorDetalle.cantidadSolicitadaTotal"/>
				<td class="textoCampo">Existencia: </td>
				<td  class="textoDato">
				<@s.property default="&nbsp;" escape=false value="asignacionProveedorDetalle.producto.cantidadTotalExistencia"/>							      			
			</td>
			</tr>
								
    		<tr>					    		
				<td class="textoCampo">Cantidad a Pedir: </td>
				<td  class="textoDato" colspan="3">																	
				<@s.textfield									
						templateDir="custontemplates"
						template="textMoney"  
						id="cantidadPedir" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="cantidadPedir" 
						title="Cantidad a Pedir" />				      			
			</td>
			</tr>
		</table>
	</div>	
	<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
				<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>OC con entregas pendientes del producto seleccionado</td>
				</tr>
		</table>
		
		<@vc.anchors target="contentTrx">
			
	        <@display.table class="tablaDetalleCuerpo" cellpadding="3" name="listaOC" id="oc_osDetalle"  defaultsort=2 >          		
  		
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" class="botoneraAncho1" title="Acciones">
					<div align="center">
						<@s.a 
							templateDir="custontemplates"	 
							 name="ver"
							 id="ver"
							cssClass="item" 
							href="${request.contextPath}/compraContratacion/oc_os/readOC_OSView.action?oc_os.oid=${oc_osDetalle.oc_os.oid?c}&navigationId=oc_os-visualizar&flowControl=regis&navegacionIdBack=${navigationId}">
							<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver"  border="0">
						</@s.a>
						</div>
	
					</@display.column>
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service"  property="oc_os.numero" title="N&uacute;mero" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service"  property="oc_os.fecha" format="{0,date,dd/MM/yyyy}"  title="Fecha"/>					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" title="Proveedor">  
						${oc_osDetalle.oc_os.proveedor.detalleDePersona.razonSocial} 
						
							<#if oc_osDetalle.oc_os.proveedor.detalleDePersona.nombre?exists> 
							 ${oc_osDetalle.oc_os.proveedor.detalleDePersona.nombre}
							</#if>
						
						
					</@display.column> 
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" property="oc_os.importe" title="Importe" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service"  title="Cant. Pendiente Entrega">
						<#assign resta=(oc_osDetalle.cantidadPedida - oc_osDetalle.cantidadRecibida) >
						${resta}
					</@display.column>
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" title="Ultima Fecha Entrega">
					<#if oc_osDetalle.oc_os.ultimaFechaEngrega?exists>
						${oc_osDetalle.oc_os.ultimaFechaEngrega?string("dd/MM/yyyy")}
					</#if>
	
					</@display.column>
				</@display.table>
				</@vc.anchors>			
			</div>
			</@s.if>
			
			<@s.if test="asignacionProveedorDetalle.servicio != null">
				<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
					<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            			<tr>
							<td><b>Datos del Servicio</b></td>
						</tr>
					</table>

					<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
						<tr>
        					<td class="textoCampo" colspan="4">&nbsp;</td>
        				</tr>
					
						<tr>
			      			<td class="textoCampo">C&oacute;digo:</td>
			      			<td class="textoDato">
				      			<@s.property default="&nbsp;" escape=false value="asignacionProveedorDetalle.servicio.oid"/>			      			
			      			<td class="textoCampo">Descripci&oacute;n</td>
							<td class="textoDato">
								<@s.property default="&nbsp;" escape=false value="asignacionProveedorDetalle.servicio.descripcion"/>			
						</tr>	
						<tr>
							<td class="textoCampo">Rubro:</td>
			      			<td class="textoDato">
			      				<@s.property default="&nbsp;" escape=false value="asignacionProveedorDetalle.servicio.rubro.descripcion"/>
						     </td>
							
							<td class="textoCampo">Cr&iacute;tico: </td>
							<td class="textoDato">
								<@s.property default="&nbsp;" escape=false value="asignacionProveedorDetalle.servicio.critico"/>								
							</td>
						</tr>
		
						<tr>
			      			<td class="textoCampo">Proveedor &Uacute;nico:</td>
							<td class="textoDato">
								<@s.property default="&nbsp;" escape=false value="asignacionProveedorDetalle.servicio.proveedorUnico"/>
							</td>
							<td class="textoCampo">Proveedor:</td>
							<td class="textoDato">							
							<@s.property default="&nbsp;" escape=false value="asignacionProveedorDetalle.servicio.proveedor.nombreFantasia"/>
							</td>
			    		</tr>
			    		<tr>			
							<td class="textoCampo">Tipo:</td>
				      		<td class="textoDato">
				     			<@s.property default="&nbsp;" escape=false value="asignacionProveedorDetalle.servicio.tipo"/>
				      		</td>				      		
							<td class="textoCampo">Estado:</td>
							<td class="textoDato" colspan="3">
								<@s.property default="&nbsp;" escape=false value="asignacionProveedorDetalle.servicio.estado"/>
							</td>
						</tr>	
				</table>
			</div>	
		</@s.if>
			
		<@vc.anchors target="contentTrx" ajaxFlag="ajax">		
			<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
					<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
            			<tr>
							<td><b>Cotizaciones Recibidas</b></td>
						</tr>
					</table>

					<table class="tablaDetalleCuerpo" cellpadding="3" >
						<tr>
							<th align="center">Raz&oacute;n Social</th>
							<th align="center">Nombre</th>
							<th align="center">Cant. m&iacute;nima</th>
							<th align="center">Precio</th>
							<th align="center">Fecha Hasta Validez Precios</th>
							<th colspan="1" align="center">Cant. M&aacute;xima a Proveer</th>
						</tr>
						
						<#assign cotizacionLista =asignacionProveedorDetalle.proveedorCotizacionList>
						<#list cotizacionLista as asignacionProveedorDetalleCotizacion>
							<#if proveedor.nroProveedor==asignacionProveedorDetalleCotizacion.proveedor.nroProveedor>
								<tr>
									<td class="estiloTexto">			
										<#if asignacionProveedorDetalleCotizacion.proveedor.detalleDePersona.razonSocial?exists>
											${asignacionProveedorDetalleCotizacion.proveedor.detalleDePersona.razonSocial}&nbsp;
										</#if>
									</td>
									<td  class="estiloTexto">
										<#if asignacionProveedorDetalleCotizacion.proveedor.detalleDePersona.nombre?exists>
											${asignacionProveedorDetalleCotizacion.proveedor.detalleDePersona.nombre}&nbsp;
										</#if>									
									</td>
									<td class="estiloNumero">
										${asignacionProveedorDetalleCotizacion.cotizacion.cantidadMinima}&nbsp;
									</td>
									<td class="estiloNumero">												
										${asignacionProveedorDetalleCotizacion.cotizacion.precio}				&nbsp;					
									</td>
									<td class="estiloNumero">
										<#if asignacionProveedorDetalleCotizacion.cotizacion.pedidoCotizacionDetalle.pedidoCotizacion.fechaValidezPrecio?exists>
											${asignacionProveedorDetalleCotizacion.cotizacion.pedidoCotizacionDetalle.pedidoCotizacion.fechaValidezPrecio?string("dd/MM/yyyy")}											
										</#if>
										&nbsp;
									</td>
									<td  class="estiloNumero">
										<#if asignacionProveedorDetalleCotizacion.cotizacion.cantidadMaxima?exists>
											${asignacionProveedorDetalleCotizacion.cotizacion.cantidadMaxima}
										</#if>										
										&nbsp;
									</td>

								</tr>
							
							</#if>			
						</#list>
					</table>	
				</div>	
			</@vc.anchors>


