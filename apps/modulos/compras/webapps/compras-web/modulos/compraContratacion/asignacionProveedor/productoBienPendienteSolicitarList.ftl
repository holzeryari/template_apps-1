<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>

<@vc.anchors target="contentTrx" ajaxFlag="ajax">
<div id="capaSeparadora" class="capaSeparadora">&nbsp;</div>
	<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
		<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
			<tr>
				<td>Productos/Bienes con cant. pedidas mayores o menores a las correspondientes cant. solicitadas</td>
			</tr>
		</table>

		<table class="tablaDetalleCuerpo" cellpadding="3" >
			<#assign asignacionProveedorDetalleLista = asignacionProveedor.asignacionProveedorDetalleList>
			<#list asignacionProveedorDetalleLista as asignacionProveedorDetalle>
			
				<#if (asignacionProveedorDetalle.cantidad > asignacionProveedorDetalle.cantidadSolicitadaTotal) || (asignacionProveedorDetalle.cantidad < asignacionProveedorDetalle.cantidadSolicitadaTotal) >
				<tr>
					<th  align="center" width="1">Acciones</th>
					<th  align="center" width="1">C&oacute;digo</th>
					<th  align="center" >Descripci&oacute;n</th>
					<th  align="center">Marca</th>
					<th  align="center">Modelo</th>
					<th  align="center">Rubro</th>
					<th  align="center">Cantidad Solicitada</th>
					<th  align="center">Cantidad Pedida </th>
				</tr>
						
				<tr>
					<td>							
						<div align="center">
						<@s.a templateDir="custontemplates" id="verProductoBien" name="verProductoBien" 
						href="${request.contextPath}/maestro/productoBien/readView.action?productoBien.oid=${asignacionProveedorDetalle.productoBien.oid?c}&productoBien.tipo=${asignacionProveedorDetalle.productoBien.tipo.ordinal()?c}&navigationId=productobien-ver&navegacionIdBack=${navigationId}&flowControl=regis" 
						cssClass="ocultarIcono">
							<img src="${request.contextPath}/common/images/ver.gif" border="0" align="absmiddle" hspace="3">								
						</@s.a>						
						</div>								
					</td>
					<td class="estiloNumero">${asignacionProveedorDetalle.productoBien.oid}</td>
					<td class="estiloTexto"  >${asignacionProveedorDetalle.productoBien.descripcion}</td>
					<td class="estiloTexto">${asignacionProveedorDetalle.productoBien.marca}</td>
					<td class="estiloTexto">${asignacionProveedorDetalle.productoBien.modelo}</td>
					<td class="estiloTexto">${asignacionProveedorDetalle.productoBien.rubro.descripcion}</td>
					<td class="estiloNumero">${asignacionProveedorDetalle.cantidadSolicitadaTotal}</td>
					<td class="estiloNumero">${asignacionProveedorDetalle.cantidad}</td>				   
				</tr>
				</#if>
			</#list>						
		</table>	
	</div>

</@vc.anchors>



