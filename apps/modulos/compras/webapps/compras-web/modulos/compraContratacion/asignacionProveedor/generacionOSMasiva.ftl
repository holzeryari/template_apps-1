<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>

<@vc.anchors target="contentTrx" ajaxFlag="ajax">
<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
		<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
			<tr>
				<td>Servicios solicitados en los FSC/FSS</td>
			</tr>
		</table>

		<table class="tablaDetalleCuerpo" cellpadding="3" >
			<#assign asignacionProveedorDetalleLista = asignacionProveedor.asignacionProveedorDetalleList>
			<#list asignacionProveedorDetalleLista as asignacionProveedorDetalle>
				<tr>
					<th align="center">Servicio</th>
					<th align="center">C&oacute;digo</th>
					<th align="center" colspan="2">Descripci&oacute;n</th>
					<th align="center">Rubro</th>
					<th align="center">Tipo</th>
					<th colspan="2" align="center">Proveedor &Uacute;nico</th>
				</tr>
						
				<tr>
					<td class="botoneraAnchoCon2"> 
						<div align="center">
							<@s.a templateDir="custontemplates" id="verAsignacionProveedorDetalle" name="verAsignacionProveedorDetalle" href="${request.contextPath}/compraContratacion/asignacionProveedor/readAsignacionProveedorDetalleView.action?asignacionProveedorDetalle.oid=${asignacionProveedorDetalle.oid?c}&navegacionIdBack=${navigationId}&navigationId=asignacionProveedorDetalle-visualizar&flowControl=regis" cssClass="ocultarIcono">
								<img src="${request.contextPath}/common/images/ver.gif" border="0" align="absmiddle" hspace="3" >
							</@s.a>
						</div>
					</td>	
					<td align="center">${asignacionProveedorDetalle.servicio.oid}</td>
					<td align="center" colspan="2">${asignacionProveedorDetalle.servicio.descripcion}</td>
					<td align="center">${asignacionProveedorDetalle.servicio.rubro.descripcion}</td>
					<td align="center">${asignacionProveedorDetalle.servicio.tipo}</td>
					<td colspan="2"  align="center">${asignacionProveedorDetalle.servicio.proveedorUnico}</td>
				</tr>
						
				<tr>
					<td>&nbsp;</td>
					<th align="center">								
							<b>Cotizaciones</b>									
					</th>
					<th align="center">Raz&oacute;n Social</th>
					<th align="center">Nombre</th>
					<th align="center">ICS</th>
					<th align="center">Situacion</th>																		
					<th align="center">Precio</th>
					<th align="center">Fecha Validez Precio</th>
				</tr>
						
				<#assign cotizacionLista =asignacionProveedorDetalle.proveedorCotizacionList>				
					<#list cotizacionLista as asignacionProveedorDetalleCotizacion>
						<#if asignacionProveedorDetalleCotizacion.cantidadPedida?exists>
							<#assign color ="celdaHabilitada">											
								<#else>
							<#assign color ="celdaDeshabilitada">	
						</#if>
							
						<tr>						
							<td>&nbsp;</td>
							<td colspan="1" class="${color}">	
								<div align="center">
									<div class="alineacionMenor" >	
										<#if asignacionProveedorDetalleCotizacion.cantidadPedida?exists>
											<@s.a templateDir="custontemplates" id="modificarCotizacionCantidaPedida" name="modificarCotizacionCantidaPedida" href="${request.contextPath}/compraContratacion/asignacionProveedor/eliminarSeleccionCotizacionView.action?asignacionProveedorDetalle.oid=${asignacionProveedorDetalle.oid?c}&proveedor.nroProveedor=${asignacionProveedorDetalleCotizacion.proveedor.nroProveedor?c}&asignacionProveedorDetalleCotizacion.oid=${asignacionProveedorDetalleCotizacion.oid?c}&navegacionIdBack=${navigationId}&navigationId=asigancionProveedor-updateCantidadPedir&flowControl=regis" cssClass="ocultarIcono">
											<img src="${request.contextPath}/common/images/eliminar.gif" border="0" align="absmiddle" hspace="3" >
											</@s.a>										
										<#else>
											<@s.a templateDir="custontemplates" id="modificarCotizacionCantidaPedida" name="modificarCotizacionCantidaPedida" href="${request.contextPath}/compraContratacion/asignacionProveedor/updateCantidadPedirView.action?asignacionProveedorDetalle.oid=${asignacionProveedorDetalle.oid?c}&proveedor.nroProveedor=${asignacionProveedorDetalleCotizacion.proveedor.nroProveedor?c}&asignacionProveedorDetalleCotizacion.oid=${asignacionProveedorDetalleCotizacion.oid?c}&navegacionIdBack=${navigationId}&navigationId=asigancionProveedor-updateCantidadPedir&flowControl=regis" cssClass="ocultarIcono">
												<img src="${request.contextPath}/common/images/seleccionar.gif" border="0" align="absmiddle" hspace="3" >
											</@s.a>
										</#if>
									</div>
									<div class="alineacionMenor" >				
										<#if asignacionProveedorDetalleCotizacion.proveedor.esCooperativa?exists && asignacionProveedorDetalleCotizacion.proveedor.esCooperativa.ordinal()==2>
										<img src="${request.contextPath}/common/images/pino.gif" border="0" align="center" >								
										<#else>
										<div class="item">
										<img src="${request.contextPath}/common/images/pino.gif" border="0" align="center" >
										</div>
										</#if>
									</div>
											
									<div class="alineacionMenor">							
											<#if asignacionProveedorDetalleCotizacion.proveedor.adtividadRegion?exists && asignacionProveedorDetalleCotizacion.proveedor.adtividadRegion.ordinal()==2>
											<img src="${request.contextPath}/common/images/sailboat.gif" border="0" align="center" >								
											<#else>
											<div class="item">
											<img src="${request.contextPath}/common/images/sailboat.gif" border="0" align="center" >
											</div>
											</#if>	
									</div>
									<div class="alineacionMenor">
											<#if asignacionProveedorDetalleCotizacion.proveedor.detalleDePersona.socio?exists>
											<img src="${request.contextPath}/common/images/manosAgarradas.gif" border="0" align="center" >								
											<#else>
											<div class="item">
											<img src="${request.contextPath}/common/images/manosAgarradas.gif" border="0" align="center" >
											</div>	
											</#if>
									</div>
								</div>	
								</td>
									<td class="${color}" align="left">			
										<#if asignacionProveedorDetalleCotizacion.proveedor.detalleDePersona.razonSocial?exists>
											${asignacionProveedorDetalleCotizacion.proveedor.detalleDePersona.razonSocial}
										</#if>&nbsp;
									</td>
									<td class="${color}" align="left">
										<#if asignacionProveedorDetalleCotizacion.proveedor.detalleDePersona.nombre?exists>
											${asignacionProveedorDetalleCotizacion.proveedor.detalleDePersona.nombre}
										</#if>		&nbsp;							
									</td>
									<td align="right" class="${color}">
										<#if asignacionProveedorDetalleCotizacion.proveedor.ultimoICS?exists>
	        								${asignacionProveedorDetalleCotizacion.proveedor.ultimoICS}
	        							</#if>&nbsp;
									</td>	
									<td align="left" class="${color}">
										<#if asignacionProveedorDetalleCotizacion.proveedor.situacionICS?exists>
	        								${asignacionProveedorDetalleCotizacion.proveedor.situacionICS}
	        							</#if>&nbsp;
									</td>	
									<td align="right" class="${color}">												
										${asignacionProveedorDetalleCotizacion.cotizacion.precio}				&nbsp;					
									</td>
									<td align="right" class="${color}">
										<#if asignacionProveedorDetalleCotizacion.cotizacion.pedidoCotizacionDetalle.pedidoCotizacion.fechaValidezPrecio?exists>
											${asignacionProveedorDetalleCotizacion.cotizacion.pedidoCotizacionDetalle.pedidoCotizacion.fechaValidezPrecio?string("dd/MM/yyyy")}
										</#if>&nbsp;
									</td>
								</tr>						
																			
						</#list>
						<tr><td colspan="7"></td></tr>
					</#list>						

				</table>	
			</div>
		</@vc.anchors>



