<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancel" type="button" name="btnCancelar" value="Cancelar" class="boton"/>
				<input id="store" type="button" name="btnAgregar" value="Agregar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/compraContratacion/asignacionProveedor/createAsignacionProveedor.action" 
  source="store" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="asignacionProveedor.fechaLimiteCotizacion={asignacionProveedor.fechaLimiteCotizacion},asignacionProveedor.fechaEntrega={asignacionProveedor.fechaEntrega},asignacionProveedor.tipo={asignacionProveedor.tipo},asignacionProveedor.lugarEntrega={asignacionProveedor.lugarEntrega},asignacionProveedor.observaciones={asignacionProveedor.observaciones}"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=buscar-asignacionProveedor,flowControl=back"/>
  