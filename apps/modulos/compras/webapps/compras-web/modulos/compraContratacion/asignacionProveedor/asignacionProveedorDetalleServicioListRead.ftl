<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>
<#if asignacionProveedor.asignacionProveedorDetalleList.size()!=0>

<@vc.anchors target="contentTrx" ajaxFlag="ajax">
<div id="capaSeparadora" class="capaSeparadora">&nbsp;</div>
<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
		<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
			<tr>
				<td>Servicios solicitados</td>
			</tr>
		</table>

		<table class="tablaDetalleCuerpo" cellpadding="3" >
			<#assign asignacionProveedorDetalleLista = asignacionProveedor.asignacionProveedorDetalleList>
			<#list asignacionProveedorDetalleLista as asignacionProveedorDetalle>
				 <tr>	      					
					<th colspan="1" align="center">Acciones</th>
					<th colspan="1" align="center">N&uacute;mero</th>
					<th colspan="1" align="center">C&oacute;digo</th>
					<th colspan="1" align="center">Descripci&oacute;n</th>
					<th colspan="1" align="center">Rubro</th>
					<th colspan="1" align="center">Tipo</th>
					<th colspan="1" align="center">Cr&iacute;tico</th>
					
			
				</tr>
						
				<tr>
					<td class="botoneraAnchoCon1" 
						<div align="center">
							<@s.a templateDir="custontemplates" id="verAsignacionProveedorDetalle" name="verAsignacionProveedorDetalle" href="${request.contextPath}/compraContratacion/asignacionProveedor/readAsignacionProveedorDetalleView.action?asignacionProveedorDetalle.oid=${asignacionProveedorDetalle.oid?c}&navegacionIdBack=${navigationId}&navigationId=asignacionProveedorDetalle-visualizar&flowControl=regis" cssClass="ocultarIcono">
								<img src="${request.contextPath}/common/images/ver.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
							</@s.a>
	
						</div>
					</td>
					<td class="estiloNumero">${asignacionProveedorDetalle.numero}</td>
					<td class="estiloNumero">${asignacionProveedorDetalle.servicio.oid}</td>
					<td class="estiloTexto">${asignacionProveedorDetalle.servicio.descripcion}</td>
					<td class="estiloTexto">${asignacionProveedorDetalle.servicio.rubro.descripcion}</td>
					<td class="estiloTexto">${asignacionProveedorDetalle.servicio.tipo}</td>
					<td class="estiloTexto">${asignacionProveedorDetalle.servicio.critico}</td>
					
		   
				</tr>
						
				<tr>
					<td colspan="1">&nbsp;</td>
					<th colspan="1" nowrap align="center">
						<b>Proveedor</b>
 					</th>	
					<th colspan="1" align="center">Condici&oacute;n</th>									 	
					<th colspan="1" align="center">Nombre Fantas&iacute;a</th>
					<th colspan="1" align="center">Raz&oacute;n Social</th>
					<th colspan="1" align="center">Nombre</th>
					<th colspan="1" align="center">CUIT/CUIL</th>
				</tr>
						
				<#assign proveedorLista = asignacionProveedorDetalle.proveedorList>
				<#list proveedorLista as proveedor>
						<tr>
							<td colspan="1">&nbsp;</td>
							<td colspan="1">								
								<div align="center">
									<@s.a templateDir="custontemplates" id="verProveedor" name="verProveedor"									
									href="${request.contextPath}/maestro/proveedor/readView.action?proveedor.nroProveedor=${proveedor.nroProveedor?c}&navigationId=read-proveedor&navegacionIdBack=${navigationId}&flowControl=regis">
									<img src="${request.contextPath}/common/images/ver.gif" border="0" align="absmiddle" hspace="3" >
									</@s.a>
								</div>		
							</td>
							<td colspan="1" class="botoneraAnchoCon3">	
								<div class="alineacionMenor" >				
								<#if proveedor.esCooperativa?exists && proveedor.esCooperativa.ordinal()==2>
									<img src="${request.contextPath}/common/images/pino.gif" border="0" align="absmiddle" title="Es Cooperativa" >								
								<#else>
								<div class="item">
									<img src="${request.contextPath}/common/images/pino.gif" border="0" align="absmiddle" title="NO es Cooperativa">
								</div>
								</#if>
							</div>
								
							<div class="alineacionMenor">							
								<#if proveedor.adtividadRegion?exists && proveedor.adtividadRegion.ordinal()==2>
									<img src="${request.contextPath}/common/images/sailboat.gif"  border="0" align="absmiddle"  title="Actividad en Region">								
								<#else>
								<div class="item">
								<img src="${request.contextPath}/common/images/sailboat.gif" border="0" align="absmiddle" title="NO tiene Actividad en Region">
								</div>
								</#if>	
							</div>
							<div class="alineacionMenor">
								<#if proveedor.detalleDePersona.socio?exists>
								<img src="${request.contextPath}/common/images/manosAgarradas.gif" border="0" align="absmiddle"  title="Es Socio">								
								<#else>
								<div class="item">
								<img src="${request.contextPath}/common/images/manosAgarradas.gif"  border="0" align="absmiddle"  title="NO es Socio" >
								</div>
								</#if>
							</div>

							</td>
							<td colspan="1" class="estiloTexto">
								<#if proveedor.nombreFantasia?exists>
									${proveedor.nombreFantasia}
								</#if>&nbsp;
							</td>
							<td colspan="1" class="estiloTexto">
								<#if proveedor.detalleDePersona.razonSocial?exists>
									${proveedor.detalleDePersona.razonSocial}
								</#if>&nbsp;
							</td>
							<td colspan="1" class="estiloTexto">
								<#if proveedor.detalleDePersona.nombre?exists>
									${proveedor.detalleDePersona.nombre}
								</#if>&nbsp;
							</td>
							<td colspan="1" class="estiloNumero">
								<#if proveedor.detalleDePersona.persona.docFiscal?exists>
	        						${proveedor.detalleDePersona.persona.docFiscal?c}
	        					</#if>&nbsp;
							</td>
							
						</tr>						
						</#list>
						  		<tr><td colspan="7"></td></tr>
					</#list>						

			</table>	

	</@vc.anchors>
	</div>
</#if>