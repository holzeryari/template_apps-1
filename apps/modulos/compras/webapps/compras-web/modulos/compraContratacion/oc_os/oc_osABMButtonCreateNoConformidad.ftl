<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>


<div id="capaSeparadora" class="capaSeparadora">&nbsp;</div>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancel" type="button" name="btnCancelar" value="Cancelar" class="boton"/>
				<input id="store" type="button" name="btnAgregar" value="Agregar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/compraContratacion/oc_os/createOC_OSNoConformidad.action" 
  source="store" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="oc_osNoConformidad.oc_os.oid={oc_osNoConformidad.oc_os.oid},oc_osNoConformidad.observacionManual={oc_osNoConformidad.observacionManual},oc_osNoConformidad.fechaUltMod={oc_osNoConformidad.fechaUltMod}"/>
  
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=buscar-oc_os,flowControl=back"/>
  