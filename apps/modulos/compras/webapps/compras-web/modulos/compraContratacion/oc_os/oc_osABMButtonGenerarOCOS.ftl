<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaSeparadora" class="capaSeparadora">&nbsp;</div>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancelar" type="button" name="btnCancelar" value="Cancelar" class="boton"/>
				<input id="store" type="button" name="btnGenerarOCOS" value="Generar OC/OS" class="boton"/>
			</td>
		</tr>	
	</table>
</div>


<@vc.htmlContent 
  baseUrl="${request.contextPath}/compraContratacion/oc_os/generarOC_OS.action" 
  source="store" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="oc_os.oid={oc_os.oid}"/>
  
  <@vc.htmlContent 
   baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancelar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=buscar-oc_os,flowControl=back"/>