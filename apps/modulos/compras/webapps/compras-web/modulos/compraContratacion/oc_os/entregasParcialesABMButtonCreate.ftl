<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaSeparadora" class="capaSeparadora">&nbsp;</div>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancelar" type="button" name="btnVolver" value="Cancelar" class="boton"/>
				<input id="agregar" type="button" name="btnAgregar" value="Agregar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>



<@vc.htmlContent 
  baseUrl="${request.contextPath}/compraContratacion/oc_os/createEntregaParcial.action" 
  source="agregar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navegacionIdBack=${navegacionIdBack},oc_osEntregaParcial.oc_os.oid={oc_osEntregaParcial.oc_os.oid},oc_osEntregaParcial.fecha={oc_osEntregaParcial.fecha},oc_osEntregaParcial.observaciones={oc_osEntregaParcial.observaciones}"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancelar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=${navegacionIdBack},flowControl=back"/>
 