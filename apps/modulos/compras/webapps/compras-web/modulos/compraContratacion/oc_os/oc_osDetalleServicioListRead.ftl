<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>


<@vc.anchors target="contentTrx" ajaxFlag="ajax">
<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
		<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
			<tr>
				<td>Servicios solicitados</td>
			</tr>
		</table>		
			

		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="oc_os.oc_osDetalleList" id="oc_osDetalle" defaultsort=2>	
							R
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="numero" title="N&uacute;mero" />
		
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="servicio.oid" title="C&oacute;digo" />
		 	
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="servicio.descripcion" title="Descripci&oacute;n" />									
			
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="servicio.rubro.descripcion" title="Rubro" />
		
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="servicio.tipo" title="Tipo" />
		
		<#--<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="cantidadPedida" title="Cantidad" />-->			
		
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="precio" format="{0,number,###0.00;-###0.00}"  title="Precio" />
		
		<#--<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="subtotal" title="Subtotal" />-->
		
		
		</@display.table>
	</div>	
	
	
	
	<@s.if test="oc_os.asignacionProveedor != null">
		<div id="capaSeparadora" class="capaSeparadora">&nbsp;</td></tr></div>
		
		
		<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
			<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>FSS incluidos en la Asignaci&oacute;n de Proveedores</td>
				</tr>
			</table>	
			<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="oc_os.asignacionProveedor.fsc_fssList" id="fsc_fss" defaultsort=2>	
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="numero" title="N&uacute;mero" />
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="tipoFormulario" title="Tipo" />
		 		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="cliente.descripcion" title="Cliente" />									
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="fechaSolicitud" format="{0,date,dd/MM/yyyy}"  title="Fecha Solicitud" />
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="montoEstimado" title="Monto Estimado" />			
				<#if oc_os.asignacionProveedor.tipo.ordinal()==2>
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="estado" title="Estado" />
				</#if>
			</@display.table>
		</div>
		
	<div id="capaSeparadora" class="capaSeparadora">&nbsp;</td></tr></div>
			
		<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
				<tr><td><b>Asignaci&oacute;n de Proveedores relacionada</b></td></tr>
			</table>	
			<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
				<tr>
		      		<td class="textoCampo">N&uacute;mero:</td>
		      		<td class="textoDato"><@s.property default="&nbsp;" escape=false value="oc_os.asignacionProveedor.numero"/></td>
		      		<td  class="textoCampo">Fecha Asignaci&oacute;n: </td>
					<td class="textoDato">
						<@s.if test="oc_os.asignacionProveedor.fechaAsignacion != null">
						<#assign fechaAsignacion = oc_os.asignacionProveedor.fechaAsignacion> 
						${fechaAsignacion?string("dd/MM/yyyy")}	
						</@s.if>
						<@s.else>
							&nbsp;
						</@s.else>								
					</td>	      			
				</tr>
				<tr>
					<td class="textoCampo">Tipo:</td>
	      			<td class="textoDato"><@s.property default="&nbsp;" escape=false value="oc_os.asignacionProveedor.tipo"/></td>
	      			<td class="textoCampo">Fecha L&iacute;mite Recepci&oacute;n de Cotizaciones:</td>
	      			<td class="textoDato">
	      				<@s.if test="oc_os.asignacionProveedor.fechaLimiteCotizacion != null">
						<#assign fechaLimiteCotizacion = oc_os.asignacionProveedor.fechaLimiteCotizacion> 
						${fechaLimiteCotizacion?string("dd/MM/yyyy")}	
						</@s.if>
						<@s.else>
							&nbsp;
						</@s.else>	
	      		</tr>
		    		
	    		<tr>
					<td class="textoCampo">Fecha de Entrega/Prestaci&oacute;n:</td>
	      			<td class="textoDato">
	      				<@s.if test="oc_os.asignacionProveedor.fechaEntrega != null">
						<#assign fechaEntrega = oc_os.asignacionProveedor.fechaEntrega> 
						${fechaEntrega?string("dd/MM/yyyy")}	
						</@s.if>
						<@s.else>
							&nbsp;
						</@s.else>	
	      			<td class="textoCampo">Lugar de Entrega/Prestaci&oacute;n:</td>
	      			<td  class="textoDato"><@s.property default="&nbsp;" escape=false value="oc_os.asignacionProveedor.lugarEntrega"/></td>
				</tr>
			</table>		
		</div>
		
	</@s.if>	

	<div id="capaSeparadora" class="capaSeparadora">&nbsp;</td></tr></div>

	<div id="capaSeparadora" class="capaSeparadora">&nbsp;</td></tr></div>

	<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
		<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
			<tr><td>Prestaciones de Servicio relacionadas</td></tr>
		</table>	
		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="prestacionServicioList" id="recepcion" defaultsort=1>
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="numero" title="Nro. Recepci&oacute;n" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="fechaPrestacion" format="{0,date,dd/MM/yyyy}" title="Fecha Prestaci&oacute;n" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="estado" title="Estado" />
		</@display.table>
	</div>

	<div id="capaSeparadora" class="capaSeparadora">&nbsp;</td></tr></div>

	<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
		<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
			<tr><td>Comprobantes relacionados</td></tr>
		</table>	
		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="oc_os.faturaOCOSList" id="facturaOCOS" defaultsort=1>

			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="factura.fechaIngreso" format="{0,date,dd/MM/yy}"  title="F. Ingreso" />

			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero"  property="factura.numeroString" class="botoneraAnchoCon4" title="Suc - Nro."/>

			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="factura.fechaEmision" format="{0,date,dd/MM/yy}"  title="F. Emisi&oacute;n" />

			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTextoAnchoFijo" title="Proveedor">  
				${facturaOCOS.factura.proveedor.detalleDePersona.razonSocial}
				<#if facturaOCOS.factura.proveedor.detalleDePersona.nombre?exists>
					 ${facturaOCOS.factura.proveedor.detalleDePersona.nombre}
				 </#if>
			</@display.column>

			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" title="Total">
				<#if facturaOCOS.factura.totalFactura?exists>
					<#assign totalFactura = facturaOCOS.factura.totalFactura> 
					${totalFactura?string(",##0.00")}
				<#else>	
					&nbsp;
				</#if>	
			</@display.column>

			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" style="width:80px;"  property="factura.origenComprobante" title="Origen" />

			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="factura.claseComprobante" title="Clase" />

			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="factura.estado" title="Estado" />		

		</@display.table>
	</div>

</@vc.anchors>
