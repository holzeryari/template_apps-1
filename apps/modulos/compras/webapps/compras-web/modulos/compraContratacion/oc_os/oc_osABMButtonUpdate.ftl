<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>



<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancelar" type="button" name="btnCancelar" value="Cancelar" class="boton"/>
				<input id="update" type="button" name="btnModificar" value="Modificar" class="boton"/>

			</td>
		</tr>	
	</table>
</div>


<@vc.htmlContent 
  baseUrl="${request.contextPath}/compraContratacion/oc_os/updateOC_OS.action" 
  source="update" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="oc_os.versionNumber={oc_os.versionNumber},oc_os.oid={oc_os.oid},oc_os.lugarEntregaPrestacion={oc_os.lugarEntregaPrestacion},oc_os.observaciones={oc_os.observaciones},navegacionIdBack={navegacionIdBack},oc_os.cliente.oid={oc_os.cliente.oid}"/>
  

<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancelar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=${navegacionIdBack},flowControl=back"/>
  
  