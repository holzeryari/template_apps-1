<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<@s.hidden id="oc_os.oid" name="oc_os.oid"/>
<@s.hidden id="oc_os.versionNumber" name="oc_os.versionNumber"/>
<@s.hidden id="oc_os.cliente.oid" name="oc_os.cliente.oid"/>
<@s.hidden id="oc_os.cliente.descripcion" name="oc_os.cliente.descripcion"/>
<@s.hidden id="navegacionIdBack" name="navegacionIdBack"/>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
			<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos de la OC/OS</b></td>
				<td>
					<div align="right">
							<@s.a templateDir="custontemplates" id="modificarOC_OS" name="modificarOC_OS" href="javascript://nop/" cssClass="ocultarIcono">
								<b>Modificar</b><img src="${request.contextPath}/common/images/modificar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
							</@s.a>
					</div>
				</td>
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
			<tr>
      			<td class="textoCampo">N&uacute;mero:</td>
      			<td class="textoDato"><@s.property default="&nbsp;" escape=false value="oc_os.numero"/></td>
      			<td  class="textoCampo">Fecha: </td>
				<td class="textoDato">
				<@s.if test="oc_os.fecha != null">
				<#assign fecha = oc_os.fecha> 
				${fecha?string("dd/MM/yyyy")} 	
				</@s.if>
				<@s.else>
					&nbsp;
				</@s.else>									
				</td>	      			
			</tr>
			<tr>
				<td class="textoCampo">Tipo:</td>
      			<td class="textoDato">
      			<@s.select 
					templateDir="custontemplates" 
					id="oc_os.tipo" 
					cssClass="textarea"
					cssStyle="width:165px" 
					name="oc_os.tipo" 
					list="tipoList" 
					listKey="key" 
					listValue="description" 
					value="oc_os.tipo.ordinal()"
					title="Tipo"
					headerKey="0"
					headerValue="Todos"							
					/>
				</td>
				<td class="textoCampo">Proveedor:</td>
      			<td class="textoDato">
      				<@s.property default="&nbsp;" escape=false value="oc_os.proveedor.detalleDePersona.razonSocial" /><@s.property default="&nbsp;" escape=false value="oc_os.proveedor.detalleDePersona.nombre" />							
					<@s.hidden id="nroProveedor" name="oc_os.proveedor.nroProveedor"/>
					<@s.hidden id="razonSocial" name="oc_os.proveedor.detalleDePersona.razonSocial"/>
					<@s.hidden id="nombre" name="oc_os.proveedor.detalleDePersona.nombre"/>
					<@s.a templateDir="custontemplates" id="seleccionarProveedor" name="seleccionarProveedor" href="javascript://nop/"  cssClass="ocultarIcono">
						<img src="${request.contextPath}/common/images/buscar.gif"  border="0" align="absmiddle" hspace="3" >
					</@s.a>		
				</td>
    		</tr>
	    		
			<tr>
				<td class="textoCampo">Cliente:</td>
      			<td class="textoDato" colspan="3" >
      			<@s.if test="oc_os.cliente != null">
				<@s.property default="&nbsp;" escape=false value="oc_os.cliente.descripcion"/>		
				</@s.if>
				<@s.else>
				<@s.property default="&nbsp;" escape=false value="oc_os.getClientesEnString()"/>
				</@s.else>	
				<@s.a templateDir="custontemplates" id="seleccionarCliente" name="seleccionarCliente" href="javascript://nop/" cssClass="ocultarIcono">
										<img src="${request.contextPath}/common/images/buscar.gif" border="0" align="absmiddle" hspace="3" >
			</@s.a>		
					</td>	
			</tr>
	    	<tr>
				<td class="textoCampo">Lugar de Entrega/Prestaci&oacute;n:</td>
      			<td  class="textoDato" colspan="3">
				<@s.textarea	
						templateDir="custontemplates" 						  
						cols="89" rows="4"	      					
						cssClass="textarea"
						id="oc_os.lugarEntregaPrestacion"							 
						name="oc_os.lugarEntregaPrestacion"  
						label="Lugar de Entrega/Prestaci&oacute;n"														
						 />
				</td>					
    		</tr>
	    		
    		<tr>
				<td class="textoCampo">Observaciones:</td>
      			<td  class="textoDato" colspan="3">
				<@s.textarea	
						templateDir="custontemplates" 						  
						cols="89" rows="4"	      					
						cssClass="textarea"
						id="oc_os.observaciones"							 
						name="oc_os.observaciones"  
						label="Observaciones"														
						 />
				</td>					
    		</tr>
	    	<tr>
				<td class="textoCampo">Demorada:</td>
      			<td  class="textoDato" colspan="3">
					<@s.property default="&nbsp;" escape=false value="oc_os.demorada"/>
				</td>
			</tr>					
					
    		<tr>
				<td class="textoCampo">Estado:</td>
      			<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="oc_os.estado"/>
				</td>
				
				<#-- estado de la oc_os = "Intervenida"-->
    			<#if oc_os.estado.ordinal() == 7>
					<tr>
						<td class="textoCampo">Observaciones Intervenci&oacute;n:</td>
						<td  colspan="3" class="textoDato">
							<@s.property default="&nbsp;" escape=false value="oc_os.observacionTipoInt"/>
						</td>
					</tr>		
				<#else>
					<td class="textoCampo">&nbsp;</td>
      				<td class="textoDato">&nbsp;</td>
      			</#if>
    		</tr>
    		
    		<@s.if test="oc_os.comentario != null">	
    		<tr>
				<td class="textoCampo">Comentario Autorizaci&oacute;n/Rechazo:</td>
      			<td  class="textoDato" colspan="3">
				<@s.textarea	
						templateDir="custontemplates" 						  
						cols="89" rows="4"	      					
						cssClass="textarea"
						id="oc_os.comentario"							 
						name="oc_os.comentario"  
						label="Comentario Autorizacion/Rechazo"														
						 />
				</td>					
    		</tr>
    	</@s.if>
    		<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>
	    </table>		
	</div>				
			
	<@tiles.insertAttribute name="entregasParciales"/>
	
	<div id="capaSeparadora" class="capaSeparadora">&nbsp;</div>		
	<@tiles.insertAttribute name="oc_osDetalle"/>			
	<@tiles.insertAttribute name="intervencion"/>					
	
<@vc.htmlContent 
  baseUrl="${request.contextPath}/compraContratacion/oc_os/selectProveedorABM.action" 
  source="seleccionarProveedor" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId=${navigationId},flowControl=change,oc_os.oid={oc_os.oid},oc_os.tipo={oc_os.tipo},oc_os.lugarEntregaPrestacion={oc_os.lugarEntregaPrestacion},oc_os.observaciones={oc_os.observaciones},oc_os.cliente.oid={oc_os.cliente.oid},oc_os.cliente.descripcion={oc_os.cliente.descripcion}"/>


<@vc.htmlContent 
  baseUrl="${request.contextPath}/compraContratacion/oc_os/selectCliente.action" 
  source="seleccionarCliente" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId=${navigationId},flowControl=change,oc_os.oid={oc_os.oid},oc_os.tipo={oc_os.tipo},oc_os.lugarEntregaPrestacion={oc_os.lugarEntregaPrestacion},oc_os.observaciones={oc_os.observaciones},oc_os.cliente.oid={oc_os.cliente.oid},oc_os.cliente.descripcion={oc_os.cliente.descripcion},navegacionIdBack={navegacionIdBack}"/>


<@vc.htmlContent 
  baseUrl="${request.contextPath}/compraContratacion/oc_os/updateOC_OSView.action" 
  source="modificarOC_OS" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navegacionIdBack=${navigationId},navigationId=modificar-oc_os,flowControl=regis,oc_os.oid={oc_os.oid}"/>



