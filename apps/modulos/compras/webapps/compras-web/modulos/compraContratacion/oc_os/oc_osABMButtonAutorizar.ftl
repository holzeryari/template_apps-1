<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaSeparadora" class="capaSeparadora">&nbsp;</div>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancelar" type="button" name="btnCancelar" value="Cancelar" class="boton"/>
				<input id="autorizar" type="button" name="btnAutorizar" value="Autorizar" class="boton"/>
				<input id="rechazar" type="button" name="btnRechazar" value="Rechazar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

  
  
  <@vc.htmlContent 
   baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancelar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=${navegacionIdBack},flowControl=back"/>
  
  <@vc.htmlContent 
  baseUrl="${request.contextPath}/compraContratacion/oc_os/confirmarAutorizacionOC_OSView.action" 
  source="autorizar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=confirmarAutorizacion_ocos,flowControl=regis,navegacionIdBack=${navigationId},oc_os.oid={oc_os.oid}"/>
  
    <@vc.htmlContent 
  baseUrl="${request.contextPath}/compraContratacion/oc_os/confirmarRechazoOC_OSView.action" 
  source="rechazar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=confirmarRechazo_ocos,flowControl=regis,navegacionIdBack=${navigationId},oc_os.oid={oc_os.oid}"/>
 

