<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaSeparadora" class="capaSeparadora">&nbsp;</div>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancelar" type="button" name="btnCancelar" value="Cancelar" class="boton"/>
				<input id="imprimir" type="button" name="btnImprimir" value="Imprimir" class="boton"/>
				<input id="enviar" type="button" name="btnEnviar" value="Enviar Email" class="boton"/>
			</td>
		</tr>	
	</table>
</div>


<@vc.htmlContent 
  baseUrl="${request.contextPath}/compraContratacion/oc_os/imprimirOC_OS.action" 
  source="imprimir" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="oc_os.oid={oc_os.oid}"/>
  
  <@vc.htmlContent 
  baseUrl="${request.contextPath}/compraContratacion/oc_os/enviarEmailOC_OS.action" 
  source="enviar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="oc_os.oid={oc_os.oid}"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancelar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=buscar-oc_os,flowControl=back"/>
  