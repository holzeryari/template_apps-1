
<#setting locale="es_AR">

<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>

<@s.hidden id="navigationId" name="navigationId"/>
<@s.hidden id="navegacionIdBack" name="navegacionIdBack"/>


<@s.hidden id="seleccionMultiple" name="seleccionMultiple"/>
<@s.hidden id="modoBusqueda" name="modoBusqueda.ordinal()"/>
<@s.hidden id="oidParameter" name="oidParameter"/>
<@s.hidden id="cliente.oid" name="cliente.oid"/>

<@s.hidden id="proveedorDuro" name="proveedorDuro"/>


<@s.hidden id="tipoDuro" name="tipoDuro"/>
  <@s.if test="tipoDuro == true">
	<@s.hidden id="oc_os.tipo" name="oc_os.tipo.ordinal()"/>
  </@s.if>
  
  
 
<@s.hidden id="pendienteRecepcionDuro" name="pendienteRecepcionDuro"/>
  <@s.if test="pendienteRecepcionDuro == true">
	<@s.hidden id="pendienteRecepcion" name="pendienteRecepcion.ordinal()"/>
  </@s.if>


<@s.hidden id="pendienteFacturacionDuro" name="pendienteFacturacionDuro"/>
  <@s.if test="pendienteFacturacionDuro == true">
	<@s.hidden id="pendienteFacturacion" name="pendienteFacturacion.ordinal()"/>
  </@s.if>
    
  
<@s.hidden id="estadoDuro" name="estadoDuro"/>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
        	<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Datos de la OC/OS</b></td>
			</tr>
		</table>
		
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
	    		<td class="textoCampo" colspan="4">&nbsp;</td>
	    	</tr>	
			<tr>
	  			<td class="textoCampo">N&uacute;mero:</td>
      			<td class="textoDato">
      			<@s.textfield 
      				templateDir="custontemplates" 
					id="oc_os.numero" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="oc_os.numero" 
					title="Numero" />
				</td>							
      		
      			<td class="textoCampo">Tipo:</td>
      			<td class="textoDato">
      			<@s.select 
						templateDir="custontemplates" 
						id="oc_os.tipo" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="oc_os.tipo" 
						list="tipoList" 
						listKey="key" 
						listValue="description" 
						value="oc_os.tipo.ordinal()"
						title="Tipo"
						headerKey="0"
						headerValue="Todos"/>
				</td>
			</tr>
      		<tr>
      			<td class="textoCampo">Proveedor:</td>
      			<td class="textoDato">
      			<@s.property default="&nbsp;" escape=false value="oc_os.proveedor.detalleDePersona.razonSocial" />
      			<@s.property default="&nbsp;" escape=false value="oc_os.proveedor.detalleDePersona.nombre" />
      								
				<@s.hidden id="nroProveedor" name="oc_os.proveedor.nroProveedor"/>
				<@s.hidden id="razonSocial" name="oc_os.proveedor.detalleDePersona.razonSocial"/>
				<@s.hidden id="nombre" name="oc_os.proveedor.detalleDePersona.nombre"/>
				<@s.a templateDir="custontemplates" id="seleccionarProveedor"
				 name="seleccionarProveedor" href="javascript://nop/" title="Buscar Proveedor"
				 cssClass="ocultarIcono">
					<img src="${request.contextPath}/common/images/buscar.gif"  border="0" align="absmiddle" hspace="3" >
				</@s.a>		
				</td>	
				
				
				<td class="textoCampo">Estado:</td>
	      			<td class="textoDato" colspan="3">
	      				      			
					  <#if estadoDuro?exists>
						<@s.hidden id="oc_os.estado" name="oc_os.estado.ordinal()"/>
						<@s.property default="&nbsp;" escape=false value="estado" />
					  <#else>
							<@s.select 
								templateDir="custontemplates" 
								id="oc_os.estado" 
								cssClass="textarea"
								cssStyle="width:165px" 
								name="oc_os.estado" 
								list="estadoList" 
								listKey="key" 
								listValue="description" 
								value="oc_os.estado.ordinal()"
								title="Estado"
								headerKey="0"
								headerValue="Todos"							
								/>
										
 					</#if>
						
					</td>
			</tr>
				
			<tr>
				<td class="textoCampo">Importe Desde:</td>
      			<td class="textoDato" >
      			<@s.textfield 
					templateDir="custontemplates" 
					id="importeDesde" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="importeDesde" 
					title="Importe Desde" />
				</td>	
				
				<td class="textoCampo">Importe Hasta:</td>
      			<td class="textoDato">
      			<@s.textfield 
					templateDir="custontemplates" 
					id="importeHasta" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="importeHasta" 
					title="Importe Hasta" />
			</tr>
				
			<tr>
				<td class="textoCampo">Fecha Desde:</td>
      			<td class="textoDato">
      			<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="fechaDesde" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="fechaDesde" 
					title="Fecha Desde" />
				</td>							
      		
				<td class="textoCampo">Fecha Hasta:</td>
      			<td class="texto" align="left" width="30%">
      			<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="fechaHasta" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="fechaHasta" 
					title="Fecha Hasta" />
			</tr>
		
			<tr>
				<td class="textoCampo">Pendiente Recepci&oacute;n:</td>
      			<td class="textoDato">
      			<@s.select 
						templateDir="custontemplates" 
						id="pendienteRecepcion" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="pendienteRecepcion" 
						list="pendienteRecepcionList" 
						listKey="key" 
						listValue="description" 
						value="pendienteRecepcion.ordinal()"
						title="Pendiente Recepcion"
						onchange="javascript:pendienteSelectOCOS(this);"							
						headerKey="0"
						headerValue="Todos"							
						/>
				</td>
				
				<td class="textoCampo">Pendiente de Facturaci&oacute;n:</td>
      			<td class="textoDato">
      			<@s.select 
						templateDir="custontemplates" 
						id="pendienteFacturacion" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="pendienteFacturacion" 
						list="pendienteFacturacionList" 
						listKey="key" 
						listValue="description" 
						value="pendienteFacturacion.ordinal()"
						title="Pendiente Facturacion"
						onchange="javascript:pendienteSelectOCOS(this);"
						headerKey="0"
						headerValue="Todos"							
						/>
				</td>
				
			</tr>
	
			<tr>
	    			<td colspan="4" class="lineaGris" align="right">
	      			
	      			<#if !(tipoDuro?exists || estadoDuro?exists || proveedorDuro?exists)>
	      				<input id="limpiar" type="button" name="btnVolver" value="Limpiar" class="boton"/>						
					</#if>		
					<input id="buscar" type="button" name="btnVolver" value="Buscar" class="boton"/>
	      			
	    			</td>
				</tr>	
			</table>
		</div>	
		
		<!-- Resultado Filtro -->
		<@s.if test="oc_osList!=null">
				<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
				<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>OC/OS encontradas</td>
				<tr>
				</table>


			<@vc.anchors target="contentTrx" ajaxFlag="ajax">	
          		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="oc_osList" id="oc_os" pagesize=15 defaultsort=2 decorator="ar.com.riouruguay.web.actions.compraContratacion.oc_os.decorators.OC_OSDecorator" >					
				<#if seleccionMultiple?exists && seleccionMultiple==true>         		
  		
					<@display.column headerClass="tbl-contract-service-select" title="Acciones" style="text-align:center;">

						<@s.checkbox label="${oc_os.oid}" fieldValue="${oc_os.oid?c}" name="oc_osSelecccionList"/>

					</@display.column>
				<#else>
				
					<#assign ref="${request.contextPath}/compraContratacion/fsc_fss/seleccionFSC_FSS.action?">										
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" title="Acciones">

						<@s.a href="${ref}oc_os.oid=${oc_os.oid?c}&oc_os.numero=${oc_os.numero?c}&navegacionIdBack=${navegacionIdBack}&navigationId=${navigationId}"><img src="${request.contextPath}/common/images/seleccionar.gif" alt="Seleccionar" title="Seleccionar" width="16" height="16" border="0"></@s.a>
						&nbsp;

					</@display.column>
				</#if>
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="numero" title="N&uacute;mero" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="fecha" format="{0,date,dd/MM/yyyy}"  title="Fecha"/>					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" title="Proveedor">  
						${oc_os.proveedor.detalleDePersona.razonSocial} 
						
						<#if oc_os.proveedor.detalleDePersona.nombre?exists> 
				 			${oc_os.proveedor.detalleDePersona.nombre}
						</#if>
						
					</@display.column> 
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="importe" title="Importe" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="estado" title="Estado" />					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="recibidaParcialString" title="Recibida Parcial" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="recibidaTotalString" title="Recibida Total" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="facturadaParcialStringSeleccion" title="Facturada Parcial" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="facturadaTotalStringSeleccion" title="Facturada Total" />
					
					
				</@display.table>
			</@vc.anchors>
		</div>	
	</@s.if>

	<div id="capaBotonera" class="capaBotonera">
		<table id="tablaBotonera" class="tablaBotonera">
			<tr> 
				<#if seleccionMultiple?exists && seleccionMultiple==true> 
					<td align="left">
						<input id="cancel" type="button" name="btnVolver" value="Volver" class="boton"/>
						<input id="selecccionMultipleOCOS" type="button" name="selecccionMultipleOCOS" value="Seleccionar OC/OS" class="boton"/>
					</td>										
				<#else>
					<td align="left">
						<input id="cancel" type="button" name="btnVolver" value="Volver" class="boton"/>
					</td>
				</#if>
			</tr>	
		</table>
	</div>		
			
    

<@vc.htmlContent 
  baseUrl="${request.contextPath}/compraContratacion/oc_os/selectProveedorSearch.action" 
  source="seleccionarProveedor" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=change,oc_os.oid={oc_os.oid},oc_os.tipo={oc_os.tipo},oc_os.estado={oc_os.estado},oc_os.numero={oc_os.numero},oc_os.proveedor.nroProveedor={oc_os.proveedor.nroProveedor},fechaDesde={fechaDesde},fechaHasta={fechaHasta},importeDesde={importeDesde},importeHasta={importeHasta},navegacionIdBack={navegacionIdBack},oidParameter=${oidParameter},tipoDuro={tipoDuro},modoBusqueda={modoBusqueda},estadoDuro={estadoDuro},oc_os.proveedor.detalleDePersona.razonSocial={oc_os.proveedor.detalleDePersona.razonSocial},oc_os.proveedor.detalleDePersona.nombre={oc_os.proveedor.detalleDePersona.nombre},oc_os.estado={oc_os.estado},cliente.oid={cliente.oid},pendienteFacturacion={pendienteFacturacion},pendienteRecepcion={pendienteRecepcion},pendienteFacturacionDuro={pendienteFacturacionDuro},pendienteRecepcionDuro={pendienteRecepcionDuro}"/>


<@vc.htmlContent 
  baseUrl="${request.contextPath}/compraContratacion/oc_os/selectSearch.action"
  source="buscar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=regis,oc_os.numero={oc_os.numero},oc_os.proveedor.detalleDePersona.razonSocial={razonSocial},oc_os.proveedor.detalleDePersona.nombre={nombre},oc_os.estado={oc_os.estado},oc_os.proveedor.nroProveedor={oc_os.proveedor.nroProveedor},oc_os.tipo={oc_os.tipo},fechaDesde={fechaDesde},fechaHasta={fechaHasta},importeDesde={importeDesde},importeHasta={importeHasta},navegacionIdBack={navegacionIdBack},oidParameter=${oidParameter},tipoDuro={tipoDuro},seleccionMultiple={seleccionMultiple},modoBusqueda={modoBusqueda},estadoDuro={estadoDuro},proveedorDuro={proveedorDuro},cliente.oid={cliente.oid},pendienteFacturacion={pendienteFacturacion},pendienteRecepcion={pendienteRecepcion},pendienteFacturacionDuro={pendienteFacturacionDuro},pendienteRecepcionDuro={pendienteRecepcionDuro}"/>
  		      

<@vc.htmlContent 
  baseUrl="${request.contextPath}/compraContratacion/oc_os/selectView.action"
  source="limpiar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=regis,navegacionIdBack={navegacionIdBack},oidParameter=${oidParameter},tipoDuro={tipoDuro},seleccionMultiple={seleccionMultiple},modoBusqueda={modoBusqueda},estadoDuro={estadoDuro},oc_os.proveedor.detalleDePersona.razonSocial={razonSocial},oc_os.proveedor.detalleDePersona.nombre={nombre},oc_os.estado={oc_os.estado},oc_os.proveedor.nroProveedor={oc_os.proveedor.nroProveedor},proveedorDuro={proveedorDuro},oc_os.tipo={oc_os.tipo},cliente.oid={cliente.oid},pendienteFacturacionDuro={pendienteFacturacionDuro},pendienteRecepcionDuro={pendienteRecepcionDuro}"/>

 <@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navegacionIdBack},flowControl=back"/>
  
  <@vc.htmlContent 
  baseUrl="${request.contextPath}/compraContratacion/oc_os/selecccionMultipleOC_OS.action" 
  source="selecccionMultipleOCOS" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},navegacionIdBack={navegacionIdBack},oidParameter=${oidParameter},oc_osSelecccionList={oc_osSelecccionList},modoBusqueda={modoBusqueda}"/>
  
 
 