<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancel" type="button" name="btnCancelar" value="Cancelar" class="boton"/>
				<input id="store" type="button" name="btnAgregar" value="Agregar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>


<@vc.htmlContent 
  baseUrl="${request.contextPath}/compraContratacion/oc_os/createOC_OS.action" 
  source="store" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="oc_os.tipo={oc_os.tipo},oc_os.lugarEntregaPrestacion={oc_os.lugarEntregaPrestacion},oc_os.observaciones={oc_os.observaciones},oc_os.proveedor.detalleDePersona.razonSocial={oc_os.proveedor.detalleDePersona.razonSocial},oc_os.proveedor.detalleDePersona.nombre={oc_os.proveedor.detalleDePersona.nombre},oc_os.proveedor.nroProveedor={oc_os.proveedor.nroProveedor},oc_os.cliente.oid={oc_os.cliente.oid}"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=buscar-oc_os,flowControl=back"/>
  