<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<@s.hidden id="navigationId" name="navigationId"/>
<@s.hidden id="ordenPagoMedioPago.oid" name="ordenPagoMedioPago.oid"/>
<@s.hidden id="ordenPagoMedioPago.ordenPago.oid" name="ordenPagoMedioPago.ordenPago.oid"/>
<@s.hidden id="ordenPagoMedioPago.versionNumber" name="ordenPagoMedioPago.versionNumber"/>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
			<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos de la Orden de Pago</b></td>
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
			<tr>
      			<td class="textoCampo">N&uacute;mero:</td>
      			<td class="textoDato">
      			<@s.property default="&nbsp;" escape=false value="ordenPagoMedioPago.ordenPago.numero"/>
      			</td>
      			<td  class="textoCampo">Fecha: </td>
				<td class="textoDato">
				<@s.if test="ordenPagoMedioPago.ordenPago.fechaComprobante != null">
				<#assign fechaComprobante = ordenPagoMedioPago.ordenPago.fechaComprobante> 
				${fechaComprobante?string("dd/MM/yyyy")}	
				</@s.if>				&nbsp;					
				</td>	      			
			</tr>	
			<tr>
				<td class="textoCampo">Fecha Emisi&oacute;n:</td>
      			<td class="textoDato">
      			<@s.if test="ordenPagoMedioPago.ordenPago.fechaEmision != null">
				<#assign fechaEmision = ordenPagoMedioPago.ordenPago.fechaEmision> 
				${fechaEmision?string("dd/MM/yyyy")}	
				</@s.if>				&nbsp;
      		
				</td>
					
				<td class="textoCampo">Rubro:</td>
				<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="ordenPagoMedioPago.ordenPago.rubro.descripcion"/>
				</td>
      			
			</tr>
			<tr>
	      		<td class="textoCampo">Proveedor:</td>
				<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="ordenPagoMedioPago.ordenPago.proveedor.detalleDePersona.razonSocial" />
				<@s.property default="&nbsp;" escape=false value="ordenPagoMedioPago.ordenPago.proveedor.detalleDePersona.nombre" />
				</td>
			
				<td class="textoCampo">Beneficiario:</td>
				<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="ordenPagoMedioPago.ordenPago.beneficiario.razonSocial" />
				</td>						
			</tr>    		
	    	<tr>
   				<td  class="textoCampo">Origen:</td>
				<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="ordenPagoMedioPago.ordenPago.origen" />
				</td>
  				<td  class="textoCampo">Proceso:</td>
				<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="ordenPagoMedioPago.ordenPago.seccionEmisora" />
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Estado:</td>
      			<td  class="textoDato" colspan="3">
					<@s.property default="&nbsp;" escape=false value="ordenPagoMedioPago.ordenPago.estado"/></td>
				</td>
			</tr>	    	
	    	<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>	
		</table>		
	</div>		

	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos del Medio de Pago</b></td>
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
			
			<tr>
				<td class="textoCampo">Forma Pago:</td>
	      		<td class="textoDato">
			  		<@s.select 
						templateDir="custontemplates" 
						id="formaPago" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="ordenPagoMedioPago.formaPago.pk.secuencia" 
						list="formaDePagoList" 
						listKey="pk.secuencia" 
						listValue="descripcion" 
						title="Forma de Pago"
						headerKey="-1"
						headerValue="Seleccionar"  
						/>
				</td>
				<td class="textoCampo">Medio Pago:</td>
      			<td class="textoDato">
				  		<@s.select 
							templateDir="custontemplates" 
							id="medioPago" 
							cssClass="textarea"
							cssStyle="width:165px" 
							name="ordenPagoMedioPago.medioPago.pk.secuencia" 
							list="medioDePagoList" 
							listKey="pk.secuencia" 
							listValue="descripcion" 
							title="Medio de Pago"
							headerKey="-1"
							headerValue="Seleccionar"  
							/>
			
				</td>
			</tr>
			<tr>	
				<td class="textoCampo">Porcentaje:</td>
				<td  class="textoDato" colspan="3">
					<@s.textfield									
							templateDir="custontemplates"
							template="textMoney"  
							id="ordenPagoMedioPago.porcentaje" 
							cssClass="textarea"
							cssStyle="width:160px" 
							name="ordenPagoMedioPago.porcentaje" 
							title="Porcentaje" />
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Observaciones:</td>
      			<td  class="textoDato" colspan="3">
				<@s.textarea	
						templateDir="custontemplates" 						  
						cols="89" rows="4"	      					
						cssClass="textarea"
						id="ordenPagoMedioPago.observaciones"							 
						name="ordenPagoMedioPago.observaciones"  
						label="Observaciones"														
						 />
				</td>
			</tr>		
			<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>		
		</table>
	</div>	
