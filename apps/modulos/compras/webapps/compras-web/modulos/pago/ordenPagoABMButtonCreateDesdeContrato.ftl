<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>


<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancel" type="button" name="btnCancelar" value="Cancelar" class="boton"/>
				<input id="store" type="button" name="btnAgregar" value="Agregar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>
<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=${navegacionIdBack},flowControl=back"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/pago/generarOrdenPagoDesdeContrato.action" 
  source="store" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="ordenPago.proveedor.nroProveedor={ordenPago.proveedor.nroProveedor},ordenPago.proveedor.detalleDePersona.razonSocial={ordenPago.proveedor.detalleDePersona.razonSocial},ordenPago.proveedor.detalleDePersona.nombre={ordenPago.proveedor.detalleDePersona.nombre},ordenPago.numero={ordenPago.numero},ordenPago.fechaEmision={ordenPago.fechaEmision},contrato.oid=${contrato.oid},navegacionIdBack=${navegacionIdBack},ordenPago.formaEnvioCorrespondencia={ordenPago.formaEnvioCorrespondencia},ordenPago.bolsaCorreo.codigo={ordenPago.bolsaCorreo.codigo}"/>
