<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancelar" type="button" name="btnCancelar" value="Cancelar" class="boton"/>
				<input id="update" type="button" name="btnModificar" value="Modificar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/pago/update.action" 
  source="update" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="ordenPago.versionNumber={ordenPago.versionNumber},ordenPago.oid={ordenPago.oid},ordenPago.fechaEmision={ordenPago.fechaEmision},ordenPago.formaEnvioCorrespondencia={ordenPago.formaEnvioCorrespondencia},ordenPago.bolsaCorreo.codigo={ordenPago.bolsaCorreo.codigo}"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancelar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=ordenPago-administrar,flowControl=back"/>
  


    