<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>
<div id="capaSeparadora" class="capaSeparadora">&nbsp;</div>
<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancelar" type="button" name="btnCancelar" value="Cancelar" class="boton"/>
			
				
					<@security.button
						templateDir="custontemplates"
						securityCode="CUF0727" 
						enabled="true"
						name="btnActivar"
						id="store"
						value="Generar"
						cssClass="boton">
					</@security.button>
					
			</td>
		</tr>	
	</table>
</div>

  <@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancelar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=buscar-rendicionReintegroGastos,flowControl=back"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/comprobante/rendicionReintegroGastos/generarRendicionReintegroGastos.action" 
  source="store" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="rendicionReintegroGastos.oid={rendicionReintegroGastos.oid}"/>
  

