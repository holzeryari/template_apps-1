<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>
<div id="capaSeparadora" class="capaSeparadora">&nbsp;</div>
<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancelar" type="button" name="btnCancelar" value="Cancelar" class="boton"/>
				<@security.button
					templateDir="custontemplates"
					securityCode="CUF0707" 
					enabled="true"
					name="btnActivar"
					id="store"
					value="Ingresar Fondo Fijo"
					cssClass="boton">
				</@security.button>&nbsp;
			</td>
		</tr>	
	</table>
</div>

  <@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancelar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=buscar-fondoFijo,flowControl=back"/>
  

<@vc.htmlContent 
  baseUrl="${request.contextPath}/comprobante/fondoFijo/updateView.action" 
  source="modificarFondoFijo" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=fondoFijo-Update,flowControl=regis,fondoFijo.oid={fondoFijo.oid},navegacionIdBack=${navigationId}"/>


<@vc.htmlContent 
  baseUrl="${request.contextPath}/comprobante/fondoFijo/generarFondoFijo.action" 
  source="store" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="fondoFijo.oid={fondoFijo.oid}"/>
  
