
<#setting locale="es_AR">

<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>


<@s.hidden id="navigationId" name="navigationId"/>
<div id="capaSeparadora" class="capaSeparadora">&nbsp;</div>
<@vc.anchors target="contentTrx">	
	<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
		<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
			<tr>
				<td>Facturas</td>
				<td>
					<div align="right">
						<@s.a href="${request.contextPath}/comprobante/factura/createFacturaRubroCtaContableView.action?navigationId=factura-crear&facturaRubroCtaContable=true&factura.origenComprobante=4&oidParameter=${fondoFijo.oid?c}&factura.cliente.oid=${fondoFijo.cliente.oid?c}&factura.cliente.descripcion=${fondoFijo.cliente.descripcion}&flowControl=regis&navegacionIdBack=${navigationId}" templateDir="custontemplates" id="agregarFactura" name="agregarFactura" cssClass="ocultarIcono">
							<b>Agregar</b><img src="${request.contextPath}/common/images/agregar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
						</@s.a>	
					</div>
				</td>
			</tr>
		</table>	
	
		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="fondoFijo.facturaFondoFijoList" id="facturaFondoFijo" pagesize=15 defaultsort=2 >
        	<@display.column headerClass="tbl-contract-service-select" class="botoneraAnchoCon3" title="Acciones">
						<div class="alineacion">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0643" 
							enabled="factura.readable" 
							cssClass="item" 
							id="ver"
							name="ver"
							href="${request.contextPath}/comprobante/factura/readView.action?factura.pkFactura.nro_factura=${facturaFondoFijo.factura.pkFactura.nro_factura?c}&factura.pkFactura.nro_proveedor=${facturaFondoFijo.factura.pkFactura.nro_proveedor?c}&factura.pkFactura.sucursal=${facturaFondoFijo.factura.pkFactura.sucursal?c}&factura.pkFactura.tip_docum=${facturaFondoFijo.factura.pkFactura.tip_docum?c}&navigationId=factura-visualizar&flowControl=regis&navegacionIdBack=${navigationId}">
							<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver"  border="0">
						</@security.a>
						</div>
						<div class="alineacion">	
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0644" 
							enabled="factura.updatable"
							cssClass="item"
							id="admin"
							name="admin"  
							href="${request.contextPath}/comprobante/factura/administrarFacturaRubroCtaContableView.action?factura.pkFactura.sucursal=${facturaFondoFijo.factura.pkFactura.sucursal?c}&factura.pkFactura.nro_proveedor=${facturaFondoFijo.factura.pkFactura.nro_proveedor?c}&factura.pkFactura.nro_factura=${facturaFondoFijo.factura.pkFactura.nro_factura?c}&factura.pkFactura.tip_docum=${facturaFondoFijo.factura.pkFactura.tip_docum?c}&navegacionIdBack=${navigationId}&navigationId=factura-administrar&flowControl=regis">
							<img  src="${request.contextPath}/common/images/administrar.gif" alt="Administrar" title="Administrar"  border="0">
						</@security.a>
						</div>
						<div class="alineacion">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0645" 
							enabled="factura.eraseable"
							cssClass="item" 
							id="eliminar"
							name="eliminar" 
							href="${request.contextPath}/comprobante/fondoFijo/deleteFacturaFondoFijo.action?fondoFijo.oid=${facturaFondoFijo.fondoFijo.oid?c}&facturaFondoFijo.oid=${facturaFondoFijo.oid?c}&navigationId=factura-eliminar&flowControl=regis&navegacionIdBack=${navigationId}">
							<img  src="${request.contextPath}/common/images/eliminar.gif" alt="Eliminar" title="Eliminar"  border="0">
						</@security.a>				
						</div>
				
					</@display.column>


					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" title="Nro." class="botoneraAnchoCon4" property="factura.numeroString"/>
										
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="factura.fechaEmision" format="{0,date,dd/MM/yyyy}"  title="Fecha Emisi&oacute;n" />
									
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" title="Proveedor">  
						${facturaFondoFijo.factura.proveedor.detalleDePersona.razonSocial}
						<#if facturaFondoFijo.factura.proveedor.detalleDePersona.nombre?exists>
						 ${facturaFondoFijo.factura.proveedor.detalleDePersona.nombre}
						 </#if>
					</@display.column>
					
					 
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="factura.totalFactura" title="Total" />
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto"  title="Orden de Pago" />

					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="factura.estado" title="Estado" />		

				</@display.table>
			</div>	
		</@vc.anchors>
	