<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<#assign displayFecAsig="none">
<#if fondoFijoMedioPago.formaPago?exists && fondoFijoMedioPago.formaPago.conFechaAsignada?exists && fondoFijoMedioPago.formaPago.conFechaAsignada>
	<#assign displayFecAsig="">
<#else>
	<#assign displayFecAsig="none">				
</#if>

<@s.hidden id="navigationId" name="navigationId"/>
<@s.hidden id="fondoFijoMedioPago.oid" name="fondoFijoMedioPago.oid"/>
<@s.hidden id="fondoFijoMedioPago.fondoFijo.oid" name="fondoFijoMedioPago.fondoFijo.oid"/>
<@s.hidden id="fondoFijoMedioPago.versionNumber" name="fondoFijoMedioPago.versionNumber"/>

<@s.hidden id="fondoFijoMedioPago.formaPago.conFechaAsignada" name="fondoFijoMedioPago.formaPago.conFechaAsignada"/>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
			<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos del Fondo Fijo</b></td>
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>		
			<tr>
      			<td class="textoCampo">N&uacute;mero:</td>
      			<td class="textoDato">
      			<@s.property default="&nbsp;" escape=false value="fondoFijoMedioPago.fondoFijo.numero"/></td>
      			<td  class="textoCampo">Fecha Comprobante: </td>
				<td class="textoDato">
				<@s.if test="fondoFijoMedioPago.fondoFijo.fechaComprobante != null">
				<#assign fechaComprobante = fondoFijoMedioPago.fondoFijo.fechaComprobante> 
				${fechaComprobante?string("dd/MM/yyyy")}	
				</@s.if>	&nbsp;							
				</td>	      			
			</tr>		     
	    	<tr>      			      		
				<td class="textoCampo">Cliente:</td>
	  			<td class="textoDato">
				
				<@s.property default="&nbsp;" escape=false value="fondoFijoMedioPago.fondoFijo.cliente.descripcion"/>		
					</td>	
				</td>
				
				<td class="textoCampo">Fecha Rendici&oacute;n:</td>
	  			<td class="textoDato">
	
				<@s.if test="fondoFijoMedioPago.fondoFijo.fechaRendicion != null">
				<#assign fechaRendicion = fondoFijoMedioPago.fondoFijo.fechaRendicion> 
				${fechaRendicion?string("dd/MM/yyyy")}	
				</@s.if>	&nbsp;	
	
				</td>
			</tr>
			<tr>
				<td class="textoCampo" >Fecha Per&iacute;odo Desde:</td>
      			<td class="textoDato">

				<@s.if test="fondoFijoMedioPago.fondoFijo.fechaDesde != null">
				<#assign fechaDesde = fondoFijoMedioPago.fondoFijo.fechaDesde> 
				${fechaDesde?string("dd/MM/yyyy")}	
				</@s.if>	&nbsp;	
	
				</td>
				<td class="textoCampo">Fecha Per&iacute;odo Hasta:</td>
      			<td class="textoDato">
				
	
				<@s.if test="fondoFijoMedioPago.fondoFijo.fechaHasta != null">
				<#assign fechaHasta = fondoFijoMedioPago.fondoFijo.fechaHasta> 
				${fechaHasta?string("dd/MM/yyyy")}	
				</@s.if>	&nbsp;	
				</td>
			</tr>	
			<tr>
				<td class="textoCampo">Estado:</td>
      			<td  class="textoDato" colspan="3">
					<@s.property default="&nbsp;" escape=false value="fondoFijoMedioPago.fondoFijo.estado"/>
				</td>
			</tr>
			<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>			
		</table>
	</div>	
	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos del Medio de Pago</b></td>
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
			
			<tr>
				<td class="textoCampo">Forma Pago:</td>
	      		<td class="textoDato">
			  		<@s.select 
						templateDir="custontemplates" 
						id="formaPago" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="fondoFijoMedioPago.formaPago.pk.idSec" 
						list="formaDePagoList" 
						listKey="pk.idSec" 
						listValue="descripcion" 
						title="Forma de Pago"
						headerKey="-1"
						headerValue="Seleccionar"  
						/>
				</td>
				<td class="textoCampo">Medio Pago:</td>
      			<td class="textoDato">
				  		<@s.select 
							templateDir="custontemplates" 
							id="medioPago" 
							cssClass="textarea"
							cssStyle="width:165px" 
							name="fondoFijoMedioPago.medioPago.pk.idSec" 
							list="medioDePagoList" 
							listKey="pk.idSec" 
							listValue="descripcion" 
							title="Medio de Pago"
							headerKey="-1"
							headerValue="Seleccionar"  
							/>
			
				</td>
			</tr>
			<tr id ="fechaAsignadaTR" style="display:${displayFecAsig};" >	
				<td class="textoCampo">Fecha Asignada:</td>
				<td class="textoDato">
					<@vc.rowCalendar 
						templateDir="custontemplates" 
						id="fechaAsignada" 
						cssClass="textarea"
						cssStyle="width:160px;" 
						name="fondoFijoMedioPago.fechaAsignada" 
						title="Fecha Asignada" />
				</td>
				<td class="textoCampo">Cantidad de D&iacute;as:</td>
				<td class="textoDato">
	      			<@s.textfield 
	  					templateDir="custontemplates" 
						id="cantidadDias" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="fondoFijoMedioPago.cantidadDias" 
						title="Cantidad de Dias" />
				</td>
			</tr>
			<tr>	
				<td class="textoCampo">Porcentaje:</td>
				<td  class="textoDato" colspan="3">
					<@s.textfield									
							templateDir="custontemplates"
							template="textMoney"  
							id="fondoFijoMedioPago.porcentaje" 
							cssClass="textarea"
							cssStyle="width:160px" 
							name="fondoFijoMedioPago.porcentaje" 
							title="Porcentaje" />
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Observaciones:</td>
      			<td  class="textoDato" colspan="3">
				<@s.textarea	
						templateDir="custontemplates" 						  
						cols="89" rows="4"	      					
						cssClass="textarea"
						id="fondoFijoMedioPago.observaciones"							 
						name="fondoFijoMedioPago.observaciones"  
						label="Observaciones"														
						 />
				</td>
			</tr>		
			<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>		
		</table>
	</div>	
