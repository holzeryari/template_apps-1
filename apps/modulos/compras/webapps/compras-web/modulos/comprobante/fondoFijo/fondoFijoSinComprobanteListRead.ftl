
<#setting locale="es_AR">

<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>


<@s.hidden id="navigationId" name="navigationId"/>
<div id="capaSeparadora" class="capaSeparadora">&nbsp;</div>
<@vc.anchors target="contentTrx">	

	<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
		<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
			<tr>
				<td>Sin Comprobantes</td>
			</tr>
		</table>

		
  		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="fondoFijo.fondoFijoSinComprobanteList" id="fondoFijoSinComprobante" defaultsort=2 >
        	<@display.column headerClass="tbl-contract-service-select" class="botoneraAnchoCon1" title="Acciones">
				<div class="alineacion">
				<@s.a 
					templateDir="custontemplates"
					cssClass="item" 
					id="ver"
					name="ver"
					href="${request.contextPath}/comprobante/fondoFijo/readSinComprobanteView.action?fondoFijoSinComprobante.oid=${fondoFijoSinComprobante.oid?c}&navigationId=fondoFijoSinComprobante-visualizar&flowControl=regis&navegacionIdBack=${navigationId}">
					<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver"  border="0">
				</@s.a>
				</div>
		
			</@display.column>
			
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" title="Descripcion" property="descripcion"/>
							

			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" title="Rubro" property="rubro.descripcion"/>
								
			
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="cuentaContable.descripcion"  title="Cuenta Contable" />
			
			
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" title="Importe" property="importe"/>
							
		</@display.table>
		</div>
	</@vc.anchors>
