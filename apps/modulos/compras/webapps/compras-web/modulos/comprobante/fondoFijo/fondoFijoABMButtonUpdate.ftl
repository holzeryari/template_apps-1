<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancelar" type="button" name="btnCancelar" value="Cancelar" class="boton"/>
				<input id="update" type="button" name="btnModificar" value="Modificar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/comprobante/fondoFijo/update.action" 
  source="update" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="fondoFijo.oid={fondoFijo.oid},fondoFijo.fechaRendicion={fondoFijo.fechaRendicion},fondoFijo.fechaHasta={fondoFijo.fechaHasta},fondoFijo.fechaDesde={fondoFijo.fechaDesde}"/>
  

<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancelar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=fondoFijo-administrar,flowControl=back"/>
  
  
    <@vc.htmlContent 
  baseUrl="${request.contextPath}/comprobante/fondoFijo/selectPersona.action" 
  source="seleccionarPersona" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=change,fondoFijo.oid={fondoFijo.oid},fondoFijo.numero={fondoFijo.numero},fondoFijo.fechaRendicion={fondoFijo.fechaRendicion},fondoFijo.fechaHasta={fondoFijo.fechaHasta},fondoFijo.fechaDesde={fondoFijo.fechaDesde},fondoFijo.cliente.oid={fondoFijo.cliente.oid},fondoFijo.lineaPresupuestaria.oid={fondoFijo.lineaPresupuestaria.oid},fondoFijo.detallePersona.pk.secuencia={fondoFijo.detallePersona.pk.secuencia},fondoFijo.detallePersona.pk.identificador={fondoFijo.detallePersona.pk.identificador},fondoFijo.detallePersona.razonSocial={fondoFijo.detallePersona.razonSocial},fondoFijo.detallePersona.nombre={fondoFijo.detallePersona.nombre}"/>
  
  
