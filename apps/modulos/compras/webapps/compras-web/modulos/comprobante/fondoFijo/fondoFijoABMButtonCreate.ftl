<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancel" type="button" name="btnCancelar" value="Cancelar" class="boton"/>
				<input id="store" type="button" name="btnAgregar" value="Agregar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/comprobante/fondoFijo/create.action" 
  source="store" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="fondoFijo.numero={fondoFijo.numero},fondoFijo.fechaRendicion={fondoFijo.fechaRendicion},fondoFijo.fechaHasta={fondoFijo.fechaHasta},fondoFijo.fechaDesde={fondoFijo.fechaDesde},fondoFijo.cliente.oid={fondoFijo.cliente.oid},fondoFijo.lineaPresupuestaria.oid={fondoFijo.lineaPresupuestaria.oid},fondoFijo.detallePersona.pk.secuencia={fondoFijo.detallePersona.pk.secuencia},fondoFijo.detallePersona.pk.identificador={fondoFijo.detallePersona.pk.identificador},fondoFijo.subLineaPresupuestaria.oid={fondoFijo.subLineaPresupuestaria.oid}"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=buscar-fondoFijo,flowControl=back"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/comprobante/fondoFijo/selectClienteABM.action" 
  source="seleccionarCliente" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=change,fondoFijo.numero={fondoFijo.numero},fondoFijo.fechaRendicion={fondoFijo.fechaRendicion},fondoFijo.fechaHasta={fondoFijo.fechaHasta},fondoFijo.fechaDesde={fondoFijo.fechaDesde},fondoFijo.cliente.oid={fondoFijo.cliente.oid},fondoFijo.lineaPresupuestaria.oid={fondoFijo.lineaPresupuestaria.oid},fondoFijo.detallePersona.pk.secuencia={fondoFijo.detallePersona.pk.secuencia},fondoFijo.detallePersona.pk.identificador={fondoFijo.detallePersona.pk.identificador},fondoFijo.detallePersona.razonSocial={fondoFijo.detallePersona.razonSocial},fondoFijo.detallePersona.nombre={fondoFijo.detallePersona.nombre},fondoFijo.subLineaPresupuestaria.oid={fondoFijo.subLineaPresupuestaria.oid}"/>

  <@vc.htmlContent 
  baseUrl="${request.contextPath}/comprobante/fondoFijo/selectPersona.action" 
  source="seleccionarPersona" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=change,fondoFijo.numero={fondoFijo.numero},fondoFijo.fechaRendicion={fondoFijo.fechaRendicion},fondoFijo.fechaHasta={fondoFijo.fechaHasta},fondoFijo.fechaDesde={fondoFijo.fechaDesde},fondoFijo.cliente.oid={fondoFijo.cliente.oid},fondoFijo.lineaPresupuestaria.oid={fondoFijo.lineaPresupuestaria.oid},fondoFijo.detallePersona.pk.secuencia={fondoFijo.detallePersona.pk.secuencia},fondoFijo.detallePersona.pk.identificador={fondoFijo.detallePersona.pk.identificador},fondoFijo.detallePersona.razonSocial={fondoFijo.detallePersona.razonSocial},fondoFijo.detallePersona.nombre={fondoFijo.detallePersona.nombre},fondoFijo.subLineaPresupuestaria.oid={fondoFijo.subLineaPresupuestaria.oid}"/>
  

  <@ajax.select
  baseUrl="${request.contextPath}/comprobante/fondoFijo/refrescarSubLinea.action" 
  source="fondoFijo.lineaPresupuestaria.oid" 
  target="fondoFijo.subLineaPresupuestaria.oid"
  parameters="fondoFijo.lineaPresupuestaria.oid={fondoFijo.lineaPresupuestaria.oid}"
  parser="new ResponseXmlParser()"/>
