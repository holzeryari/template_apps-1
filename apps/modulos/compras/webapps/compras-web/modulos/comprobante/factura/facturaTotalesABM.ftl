<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Totales del Comprobante</b></td>
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
        								
			<tr>					      			
      			<td  class="textoCampo" >Total IVA:</td>
				<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="factura.montoIVA"/>		
				</td>	
				<td class="textoCampo">Total No Gravado:</td>
      			<td class="textoDato">
      				<@s.property default="&nbsp;" escape=false value="factura.noGravado"/>
				</td>
				      			
			</tr>	
			<tr>						
				<td class="textoCampo">Total Gravado:</td>
      			<td class="textoDato">
      				<@s.property default="&nbsp;" escape=false value="factura.netoGravado"/>
				</td>
				
				<td class="textoCampo">Percepciones de IVA:</td>
      			<td class="textoDato">
				<@s.textfield									
						templateDir="custontemplates"
						template="textMoney"  
						id="factura.percepciones" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="factura.percepciones" 
						title="Percepciones Nacionales" />
				</td>
			</tr>
			<tr>						
				<td class="textoCampo">Percepciones IB:</td>
      			<td class="textoDato">
				<@s.textfield									
						templateDir="custontemplates"
						template="textMoney"  
						id="factura.percIb" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="factura.percIb" 
						title="Percepciones IB" />
				</td>
				
				<td class="textoCampo">Percepciones SUSS:</td>
      			<td class="textoDato">
				<@s.textfield									
						templateDir="custontemplates"
						template="textMoney"  
						id="factura.percSUSS" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="factura.percSUSS" 
						title="Percepciones SUSS" />
				</td>
			</tr>	
	    		
			<tr>						
				<td class="textoCampo">Percepciones Municipales:</td>
      			<td class="textoDato">
				<@s.textfield									
						templateDir="custontemplates"
						template="textMoney"  
						id="factura.percMunic" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="factura.percMunic" 
						title="Percepciones Municipales" />
				</td>
				
				<td class="textoCampo">Impuestos Internos:</td>
      			<td class="textoDato">
				<@s.textfield									
						templateDir="custontemplates"
						template="textMoney"  
						id="factura.impInternos" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="factura.impInternos" 
						title="Impuestos Internos" />
				</td>
			</tr>	
			<tr>					      			
      			<td  class="textoCampo">Total Factura:</td>
				<td class="textoDato" colspan="3">
				<@s.property default="&nbsp;" escape=false value="factura.totalFactura"/>		
				</td> 			
			</tr>			

		</table>
	</div>
