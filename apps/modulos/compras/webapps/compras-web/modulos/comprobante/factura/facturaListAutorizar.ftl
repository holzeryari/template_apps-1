<#setting locale="es_AR">

<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>


<@s.hidden id="navigationId" name="navigationId"/>

<@s.hidden id="totalFacturas" name="totalFacturas"/>
<@s.hidden id="estado" name="factura.estado"/>
<@s.hidden id="factura.proveedor.nroProveedor" name="factura.proveedor.nroProveedor"/>
<@s.hidden id="factura.proveedor.detalleDePersona.razonSocial" name="factura.proveedor.detalleDePersona.razonSocial"/>
<@s.hidden id="factura.proveedor.detalleDePersona.nombre" name="factura.proveedor.detalleDePersona.nombre"/>
<@s.hidden id="factura.numeroFactura" name="factura.numeroFactura"/>
<@s.hidden id="fechaDesde" name="fechaDesdeString"/>
<@s.hidden id="fechaHasta" name="fechaHastaString"/>
<@s.hidden id="factura.origenComprobante" name="factura.origenComprobante"/>
<@s.hidden id="factura.claseComprobante" name="factura.claseComprobante"/>
<@s.hidden id="factura.origenComprobanteOrdinal" name="factura.origenComprobante.ordinal()"/>
<@s.hidden id="factura.claseComprobanteOrdinal" name="factura.claseComprobante.ordinal()"/>
<@s.hidden id="fechaIngresoDesde" name="fechaIngresoDesdeString"/>
<@s.hidden id="fechaIngresoHasta" name="fechaIngresoHastaString"/>
<@s.hidden id="factura.conOPAutorizada" name="factura.conOPAutorizada"/>
<@s.hidden id="factura.conOPAutorizadaOrdinal" name="factura.conOPAutorizada.ordinal()"/>
<@s.hidden id="selectedFacturasChecks" name="selectedFacturasChecks"/>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
	<table id="tablaTituloMenu" class="tablaTituloMenu">
		<tr>
			<td><@s.text name="${navegacion}"/></td>
		</tr>
	</table>
</div>
	
<div id="capaTituloAccion" class="capaTituloAccion">
	<table id="tablaTituloAccion" class="tablaTituloAccion">
		<#if mensajeAviso?exists && (mensajeAviso.length()>0)>
			<tr>
				<td class="estiloMensajeAviso"><@s.text name="${mensajeAviso}"/></td>
			</tr>
		</#if>
    	<tr>
        	<td><div id="errorTrx" align="left"></div></td>
       	</tr>
    	<tr>
			<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" /><@s.text name="${titulo}" /></td>
		</tr>
	</table>
</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Datos del Comprobante</b></td>
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
	    		<td class="textoCampo" colspan="4">&nbsp;</td>
	    	</tr>		
			<tr>
				<td class="textoCampo">N&uacute;mero:</td>
				<td class="textoDato">
	      			<@s.property default="&nbsp;" escape=false value="factura.numeroFactura" />
	      		</td>
				<td class="textoCampo">Proveedor:</td>
				<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="factura.proveedor.detalleDePersona.razonSocial" />
					<@s.property default="&nbsp;" escape=false value="factura.proveedor.detalleDePersona.nombre" />
				</td>						
			</tr>
			<tr>
				<td class="textoCampo">Origen Comprobante:</td>
	      		<td class="textoDato">
	      			<@s.property default="&nbsp;" escape=false value="factura.origenComprobante" />
				</td>
				<td class="textoCampo">Estado:</td>
      			<td class="textoDato">
      				<@s.property default="&nbsp;" escape=false value="factura.estado" />
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Fecha Ingreso Desde:</td>
      			<td class="textoDato">
      				<@s.if test="fechaIngresoDesde != null">					 
						${fechaIngresoDesde?string("dd/MM/yyyy")}	
					</@s.if>&nbsp;
				</td>
				<td class="textoCampo">Fecha Ingreso Hasta:</td>
      			<td class="textoDato">
	      			<@s.if test="fechaIngresoHasta != null">					 
						${fechaIngresoHasta?string("dd/MM/yyyy")}	
					</@s.if>&nbsp;
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Fecha Emisi&oacute;n Desde:</td>
      			<td class="textoDato">
	      			<@s.if test="fechaDesde != null">					 
						${fechaDesde?string("dd/MM/yyyy")}	
					</@s.if>&nbsp;
				</td>
				<td class="textoCampo">Fecha Emisi&oacute;n Hasta:</td>
      			<td class="textoDato">
	      			<@s.if test="fechaHasta != null">					 
						${fechaHasta?string("dd/MM/yyyy")}	
					</@s.if>&nbsp;
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Clase Comprobante:</td>
      			<td class="textoDato">
      				<@s.property default="&nbsp;" escape=false value="factura.claseComprobante" />
				</td>
				<td class="textoCampo">Con OP Emitida:</td>
      			<td class="textoDato">
      				<@s.property default="&nbsp;" escape=false value="factura.conOPAutorizada" />
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Ordenar por:</td>
	  			<td class="textoDato" colspan="3">
				<@s.select 
					templateDir="custontemplates" 
					id="keyOrdenamientoAutorizarID" 
					cssClass="textarea"
					cssStyle="width:165px" 
					name="keyOrdenamientoAutorizar" 
					list="ordenamientoAutorizarList" 
					listKey="key" 
					listValue="description" 
					value="keyOrdenamientoAutorizar"
					title="Ordenar por"  
					/>
	           </td>
			</tr>
			
			<tr>
    			<td colspan="4" class="lineaGris" align="right">
      				<input id="buscar" type="button" name="btnVolver" value="Buscar" class="boton"/>
    			</td>
			</tr>
		</table>
	</div>	
	
	<!-- Resultado Filtro -->
	<@s.if test="facturaList!=null">
					
		<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
			<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Comprobantes encontrados</td>
				</tr>
			</table>	
			
			<@vc.anchors target="contentTrx">			
          		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="facturaList" id="factura" partialList=false size="recordSize" keepStatus=true excludedParams="resetFlag">

					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select"  class="botoneraAnchoCon2" title="Acciones">		

						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0643" 
							enabled="factura.readable" 
							cssClass="item" 
							href="${request.contextPath}/comprobante/factura/readView.action?factura.pkFactura.nro_factura=${factura.pkFactura.nro_factura?c}&factura.pkFactura.nro_proveedor=${factura.pkFactura.nro_proveedor?c}&factura.pkFactura.sucursal=${factura.pkFactura.sucursal?c}&factura.pkFactura.tip_docum=${factura.pkFactura.tip_docum?c}&navigationId=factura-visualizar&flowControl=regis&navegacionIdBack=${navigationId}">
							<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver"  border="0">
						</@security.a>

						
						<@s.checkbox 
							label="${factura.pkFactura.nro_factura?c}"							
							fieldValue="${factura.pkFactura.nro_factura?c}__${factura.pkFactura.nro_proveedor?c}__${factura.pkFactura.sucursal?c}__${factura.pkFactura.tip_docum?c}" 
							name="selectedFacturas" value="true" onclick="javascript:calcularTotalFacturasAutorizar(this);"/>
						
						
					</@display.column>

					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTextoAnchoFijo" title="Proveedor">  
						${factura.proveedor.detalleDePersona.razonSocial}
						<#if factura.proveedor.detalleDePersona.nombre?exists>
						 ${factura.proveedor.detalleDePersona.nombre}
						 </#if>
					</@display.column>
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero"  property="numeroString" class="botoneraAnchoCon4" title="Suc - Nro. Factura"/>
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="fechaEmision" format="{0,date,dd/MM/yy}"  title="F. Emisi&oacute;n" />
									
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="fechaVencimiento" format="{0,date,dd/MM/yy}"  title="F. Sugerida Pago" />
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="mediosPagoPorcentajesString" title="Medio de pago" />
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" title="Total">
						<@s.hidden 
							id="${factura.pkFactura.nro_factura?c}__${factura.pkFactura.nro_proveedor?c}__${factura.pkFactura.sucursal?c}__${factura.pkFactura.tip_docum?c}" 
							name="${factura.pkFactura.nro_factura?c}__${factura.pkFactura.nro_proveedor?c}__${factura.pkFactura.sucursal?c}__${factura.pkFactura.tip_docum?c}"
							value="${factura.totalFactura?c}"/>
						
						<@s.hidden 
							id="${factura.pkFactura.nro_factura?c}__${factura.pkFactura.nro_proveedor?c}__${factura.pkFactura.sucursal?c}__${factura.pkFactura.tip_docum?c}_tipoFactura" 
							name="${factura.pkFactura.nro_factura?c}__${factura.pkFactura.nro_proveedor?c}__${factura.pkFactura.sucursal?c}__${factura.pkFactura.tip_docum?c}_tipoFactura"
							value="${factura.claseComprobanteOrdinal?c}"/>
						
						<#if factura.totalFactura?exists>
						<#assign totalFactura = factura.totalFactura> 
						${totalFactura?string(",##0.00")}
						<#else>	
						&nbsp;
						</#if>	
								
					</@display.column>
					
					<@display.footer>
						<tr>
							<td colspan=6 aling="right"><b>TOTAL SELECCIONADO:</b></td>							
							<td style="text-align: right; font:bold 13px Verdana,Tahoma,Arial,sans-serif; "  id="tdTotalFacturas">
							<b >
							<#assign totalFacturass = totalFacturas> 
								${totalFacturass?string(",##0.00")}
							  </b></td>
						<tr>
					</@display.footer>
					
				</@display.table>
			</@vc.anchors>
			
			<@s.if test="facturaList.size == 0">
				<span class="busquedaSinResultado">No hay elementos para mostrar</span><span class=""></span>
			</@s.if>	
			
		</div>
		
		<@s.if test="facturaList.size > 0">
		
		<div id="capaLink" class="capaLink">
			<div id="linkCheckUncheckAllComprobantes" class="alineacionIzquierda">					
				<@s.a 
					templateDir="custontemplates"	
					cssClass="no-rewrite" 
					id="checkAllOP"
					name="checkAllOP"	
					onclick="javascript:checkAllFacturas();"
					href="javascript://nop/"
					disabled="true">
					<b id="linkSeleccionarComprobantes" >Seleccionar</b>
					<img src="${request.contextPath}/common/images/seleccionar.gif" border="0" align="absmiddle" hspace="3" alt="Seleccionar todo" title="Seleccionar todo">
				</@s.a>	
				<b id="espacioIntermedio" >&nbsp;</b>
				<@s.a 
					templateDir="custontemplates"	
					cssClass="no-rewrite" 
					id="uncheckAllOP"
					name="uncheckAllOP"	
					onclick="javascript:uncheckAllFacturas();"
					href="javascript://nop/"
					disabled="true">
					<b id="linkDeseleccionarComprobantes" >Deseleccica	onar</b>
					<img src="${request.contextPath}/common/images/ico_titulos.gif" border="0" align="absmiddle" hspace="3" alt="Deseleccionar todo" title="Deseleccionar todo">
				</@s.a>	
			</div>
		</div>	
		
		</@s.if>
		
	</@s.if>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancelar" type="button" name="btnVolver" value="Volver" class="boton"/>
				<div id="linkAutorizarComprobantes" class="alineacionDerecha">
									
					<!-- input id="autorizar" type="button" name="btnAutorizarComprobantes" value="Autorizar" class="boton"/ -->
					
					<@security.button
						templateDir="custontemplates"
						securityCode="CUF0650" 
						enabled="true"
						name="btnAutorizarComprobantes"
						id="autorizar"
						value="Autorizar"
						cssClass="boton">
					</@security.button>&nbsp;
				</div>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/comprobante/factura/selectComprobantesAutorizar.action"
  source="buscar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId=comprobante-autorizar,flowControl=regis,navegacionIdBack=buscar-factura,factura.estado={estado},factura.proveedor.nroProveedor={factura.proveedor.nroProveedor},factura.proveedor.detalleDePersona.razonSocial={factura.proveedor.detalleDePersona.razonSocial},factura.proveedor.detalleDePersona.nombre={factura.proveedor.detalleDePersona.nombre},factura.numeroFactura={factura.numeroFactura},fechaDesde={fechaDesde},fechaHasta={fechaHasta},factura.origenComprobante={factura.origenComprobanteOrdinal},factura.claseComprobante={factura.claseComprobanteOrdinal},fechaIngresoDesde={fechaIngresoDesde},fechaIngresoHasta={fechaIngresoHasta},factura.conOPAutorizada={factura.conOPAutorizadaOrdinal},keyOrdenamientoAutorizar={keyOrdenamientoAutorizarID}"/>
  
<@vc.htmlContent 
   baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancelar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=${navegacionIdBack},flowControl=back"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/comprobante/factura/comprobantesAutorizar.action" 
  source="autorizar" 
  success="contentTrx" 
  failure="errorTrx"
  preFunction="seleccionarAutorizar"
  parameters="navegacionIdBack=${navegacionIdBack},selectedFacturasChecks={selectedFacturasChecks}"/>
  