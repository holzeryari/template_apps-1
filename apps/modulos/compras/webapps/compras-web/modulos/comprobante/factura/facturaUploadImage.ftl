<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
        	<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	

	<@tiles.insertAttribute name="factura"/>
	<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action"  
  source="cancelar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=buscar-carteleria,flowControl=back"/>
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Upload de Imagen</b></td>
			</tr>
		</table>
		<@s.form 
		   action="${request.contextPath}/imagenes/doUpload.action" 
		   method="post" 
		   enctype="multipart/form-data"  theme="ajax">
		   		<@s.hidden id="navegacionIdBack" name="navegacionIdBack"/>
		   		<@s.hidden id="navigationId" name="navigationId"/>
				<@s.hidden id="factura.pkFactura.nro_proveedor" name="factura.pkFactura.nro_proveedor"/>
				<@s.hidden id="factura.pkFactura.nro_factura" name="factura.pkFactura.nro_factura"/>
				<@s.hidden id="factura.pkFactura.sucursal" name="factura.pkFactura.sucursal"/>
				<@s.hidden id="factura.pkFactura.tip_docum" name="factura.pkFactura.tip_docum"/>					
	       		<@s.file name="upload" label="File"/> 
	       		<@s.submit  theme="ajax"/>		       
   	   </@s.form>	

	</div>	


  
   	<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="left">
				<input id="cancelar" type="button" name="btnVolver" value="Volver" class="boton"/>
			</td>
		</tr>	
	</table>
</div>
  
