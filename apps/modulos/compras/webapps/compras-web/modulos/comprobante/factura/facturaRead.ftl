<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos del Proveedor</b></td>
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>

			<tr>
      			<td class="textoCampo">Nombre Fantasia: </td>
					<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="factura.proveedor.nombreFantasia" />
				</td>
				<td class="textoCampo">Razon Social/Apellido:</td>
				<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="factura.proveedor.detalleDePersona.razonSocial" />						
				</td>				   			
			</tr>
					
			<tr>
	  			<td class="textoCampo">CUIT/CUIL:</td>
				<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="factura.proveedor.detalleDePersona.persona.docFiscal" />						
				</td>
				<td class="textoCampo">Documento:</td>
					<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="factura.proveedor.detalleDePersona.persona.docPersonal" />						
				</td>			
			</tr>
				
			<tr>
				<td class="textoCampo">Provincia:</td>
					<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="factura.proveedor.detalleDePersona.codigoPostal.provincia.pais.descripcion" />					
					
				</td>
				<td class="textoCampo">Localidad:</td>
					<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="factura.proveedor.detalleDePersona.codigoPostal.descripcion" />						
				</td>			
			</tr>
				
			<tr>
				<td class="textoCampo">Domicilio:</td>
				<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="factura.proveedor.detalleDePersona.calle" />						
					<@s.property default="&nbsp;" escape=false value="factura.proveedor.detalleDePersona.letrasCP" />
					<@s.property default="&nbsp;" escape=false value="factura.proveedor.detalleDePersona.numeroFinca" />
					<@s.property default="&nbsp;" escape=false value="factura.proveedor.detalleDePersona.aptoCasa" />
				</td>

				<td class="textoCampo">Tel&eacute;fono:</td>
				<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="factura.proveedor.detalleDePersona.primerTelLaboral" />
					&nbsp;						
				</td>			
			</tr>
			<tr>
      			<td class="textoCampo">Email:</td>
				<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="factura.proveedor.email" />
					&nbsp;					
					
				</td>
				<td class="textoCampo">Condici&oacute;n Fiscal IVA:</td>
				<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="factura.proveedor.detalleDePersona.persona.situacionFiscal.descripcion" />						
				</td>			
			</tr>		
			<tr>
      			<td class="textoCampo">CAI:</td>
      			<td class="textoDato">	     
      			<@s.property default="&nbsp;" escape=false value="factura.cai" />
      			&nbsp;
      			
      			</td>
 				<td class="textoCampo">Fecha Vencimiento CAI:</td>
      			<td class="textoDato">
      			<@s.if test="factura.fechaCai != null">
				<#assign fechaCai = factura.fechaCai> 
				${fechaCai?string("dd/MM/yyyy")}	
				</@s.if>&nbsp;	
				</td>	
			</tr>
			<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>
		</table>
	</div>
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos del Comprobante</b></td>
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>			
			<tr>
				<td class="textoCampo">Tipo Comprobante:</td>
	  			<td class="textoDato"colspan="3">
	  			<@s.property default="&nbsp;" escape=false value="factura.tipoComprobante.descripcion" />
				</td>
			</tr>
			<tr>
      			<td class="textoCampo">Punto de Venta:</td>
      			<td class="textoDato">	 
      			<@s.property default="&nbsp;" escape=false value="factura.sucursal" />
      			</td>

      			<td class="textoCampo">N&uacute;mero:</td>
      			<td class="textoDato">	
      			<@s.property default="&nbsp;" escape=false value="factura.numeroFactura" />      				      			
      			</td>	
			</tr>
			<tr>
      			<td class="textoCampo">Fecha Emisi&oacute;n:</td>
      			<td class="textoDato">
	      			<@s.if test="factura.fechaEmision != null">
					<#assign fechaEmision = factura.fechaEmision> 
					${fechaEmision?string("dd/MM/yyyy")}	
					</@s.if>&nbsp;	
				</td>
      			<td class="textoCampo">Origen Comprobante:</td>
      			<td class="textoDato">
      				<@s.property default="&nbsp;" escape=false value="factura.origenComprobante" />     
      			</td>
			</tr>
			<tr>
	  			<td class="textoCampo">Es Original:</td>
      			<td class="textoDato" colspan="3">
	      			<@s.property default="&nbsp;" escape=false value="factura.esOriginal" />
	 			</td>		
			</tr>
			<tr>
				<td class="textoCampo">Cliente:</td>
      			<td class="textoDato">
	      			<@s.property default="&nbsp;" escape=false value="factura.cliente.descripcion"/>	      									
				</td>				 	
				<td  class="textoCampo">Imagen Digital:</td>
				<td class="textoDato">
					<#if !(modoCrear?exists)>
						<a  
								id="imagenView" 
								name="imagenView"
								target="_blank" 
								href="${request.contextPath}/comprobante/factura/facturaRedirectImagenView.action?factura.pkFactura.nro_factura=${factura.pkFactura.nro_factura?c}&factura.pkFactura.nro_proveedor=${factura.pkFactura.nro_proveedor?c}&factura.pkFactura.sucursal=${factura.pkFactura.sucursal?c}&factura.pkFactura.tip_docum=${factura.pkFactura.tip_docum?c}&navigationId=factura-uploadImagen&flowControl=regis&navegacionIdBack=${navigationId}">
									<img src="${request.contextPath}/common/images/ver.gif" border="0" align="absmiddle" hspace="3" alt="Ver Imagen del Comprobante" title="Ver Imagen del Comprobante"  >
						</a>		
					</#if>
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Fecha de Ingreso:</td>
      			<td class="textoDato">
					<@s.if test="factura.fechaIngreso != null">
					<#assign fechaIngreso = factura.fechaIngreso> 
					${fechaIngreso?string("dd/MM/yyyy")}	
					</@s.if>&nbsp;	
				</td>	
				<td  class="textoCampo">Proceso:</td>
				<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="factura.seccion" />
				</td>
			</tr>
			<tr>
				<td  class="textoCampo">Clase Comprobante:</td>
				<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="factura.claseComprobante" />
				</td>
			 	<td  class="textoCampo">Estado:</td>
				<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="factura.estado" />
				</td>
			</tr>
			<tr>
				<td  class="textoCampo">Usuario Ingreso:</td>
				<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="factura.usuarioIngresa.apellido" />
				</td>
			 	<td  class="textoCampo">Usuario Autoriza:</td>
				<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="factura.usuarioAutoriza.apellido" />
				</td>
			</tr>
			
			
			<#if (factura.facturaFondoFijo.fondoFijo.numero)?exists>
				<tr>		
					<td  class="textoCampo">Nro.Fondo Fijo:</td>
					<td class="textoDato">
				    	<@s.property default="&nbsp;" escape=false value="factura.facturaFondoFijo.fondoFijo.numero" />
					</td>			 	
			    </tr>
			<#else>
			   &nbsp;
			</#if>

		</table>		
	</div>