<#setting locale="es_AR">

<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>

<@s.hidden id="navigationId" name="navigationId"/>

<div id="capaSeparadora" class="capaSeparadora">&nbsp;</div>
<@vc.anchors target="contentTrx">	
  <div id="capaDetalleCuerpo" class="capaDetalleCuerpo">

	<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
			<tr>
				<td>Medios de Pago</td>
			</tr>
	</table>
		
	<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
		<tr>
    		<td class="textoCampo" colspan="4">&nbsp;</td>
    	</tr>			
		<tr>
			<td class="textoCampo">Fecha Sugerida de Pago:</td>
  			<td class="textoDato">
      			<@s.if test="factura.fechaVencimiento != null">
				<#assign fechaVencimiento = factura.fechaVencimiento> 
				${fechaVencimiento?string("dd/MM/yyyy")}	
				</@s.if>&nbsp;					
			</td>
			<td class="textoCampo">&nbsp;</td>
  			<td class="textoDato">&nbsp;</td>
		</tr>
		<tr>
    		<td class="textoCampo" colspan="4">&nbsp;</td>
    	</tr>			
	</table>

	<#if factura.claseComprobante?exists && (factura.claseComprobante.ordinal()==1 || factura.claseComprobante.ordinal()==4 || factura.claseComprobante.ordinal()==3)>		
				<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="factura.facturaMedioPagoList" id="facturaMedioPago" pagesize=15 defaultsort=2 >
		          	<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="formaPago.descripcion" title="Forma Pago" />
		          	<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="medioPago.descripcion" title="Medio Pago" />
		          	<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="observaciones" title="Observaciones" />
		          	<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="fechaAsignada" format="{0,date,dd/MM/yyyy}" title="Fecha Asignada" />		
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="porcentaje" title="Porcentaje" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="montoAsignado" title="Monto Asignado" />
				</@display.table>
		
	</#if>
  </div>		
</@vc.anchors>