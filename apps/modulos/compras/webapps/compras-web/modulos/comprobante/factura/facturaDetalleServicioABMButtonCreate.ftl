<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancelar" type="button" name="btnCancelar" value="Cancelar" class="boton"/>
				<input id="agregar" type="button" name="btnAgregar" value="Agregar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/comprobante/factura/createDetalleProductoBienServicio.action" 
  source="agregar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="facturaDetalle.cuentaContable.codigo={facturaDetalle.cuentaContable.codigo},facturaDetalle.precioUnitario={facturaDetalle.precioUnitario},facturaDetalle.tipoImputacionIVA={facturaDetalle.tipoImputacionIVA},facturaDetalle.cantidadFacturada={facturaDetalle.cantidadFacturada},facturaDetalle.servicio.oid=${facturaDetalle.servicio.oid},facturaDetalle.rubro.oid=${facturaDetalle.rubro.oid},facturaDetalle.importeNoGravado={facturaDetalle.importeNoGravado},facturaDetalle.importeGravado={facturaDetalle.importeGravado},navegacionIdBack=factura-administrar"/>
  
 
 <@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancelar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=factura-administrar,flowControl=back"/>
  
 
 