<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancelar" type="button" name="btnCancelar" value="Cancelar" class="boton"/>
				<input id="update" type="button" name="btnModificar" value="Modificar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/comprobante/factura/update.action" 
  source="update" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="factura.pkFactura.nro_proveedor={factura.pkFactura.nro_proveedor},factura.pkFactura.nro_factura={factura.pkFactura.nro_factura},factura.pkFactura.sucursal={factura.pkFactura.sucursal},factura.pkFactura.tip_docum={factura.pkFactura.tip_docum},factura.numeroFactura={factura.numeroFactura},factura.cai={factura.cai},factura.fechaCai={factura.fechaCai},factura.sucursal={factura.sucursal},factura.fechaEmision={factura.fechaEmision},factura.fechaVencimiento={factura.fechaVencimiento},factura.tipoAdquisicion={factura.tipoAdquisicion},factura.esOriginal={factura.esOriginal},factura.formaPago={factura.formaPago},factura.tipoComprobante.codigo={factura.tipoComprobante.codigo},factura.origenComprobante={origenComprobanteID},factura.cliente.oid={factura.cliente.oid}"/>
  

<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancelar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=factura-administrar,flowControl=back"/>
  

  <@vc.htmlContent 
  baseUrl="${request.contextPath}/comprobante/factura/selectCliente.action" 
  source="seleccionarCliente" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=change,factura.pkFactura.nro_proveedor={factura.pkFactura.nro_proveedor},factura.pkFactura.nro_factura={factura.pkFactura.nro_factura},factura.pkFactura.sucursal={factura.pkFactura.sucursal},factura.pkFactura.tip_docum={factura.pkFactura.tip_docum},factura.numeroFactura={factura.numeroFactura},factura.cai={factura.cai},factura.fechaCai={factura.fechaCai},factura.sucursal={factura.sucursal},factura.fechaEmision={factura.fechaEmision},factura.fechaVencimiento={factura.fechaVencimiento},factura.tipoAdquisicion={factura.tipoAdquisicion},factura.esOriginal={factura.esOriginal},factura.formaPago={factura.formaPago},factura.tipoComprobante.codigo={factura.tipoComprobante.codigo},factura.origenComprobante={origenComprobanteID},factura.cliente.oid={factura.cliente.oid},factura.cliente.descripcion={factura.cliente.descripcion},factura.proveedor.nroProveedor={factura.proveedor.nroProveedor}"/>
  