<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancelar" type="button" name="btnCancelar" value="Cancelar" class="boton"/>
				<input id="agregar" type="button" name="btnAgregar" value="Agregar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>


<@vc.htmlContent 
  baseUrl="${request.contextPath}/comprobante/factura/createFacturaDetalleAlicuota.action" 
  source="agregar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="facturaDetalleAlicuotaIVA.oid={facturaDetalleAlicuotaIVA.oid},facturaDetalleAlicuotaIVA.alicuotaIVA.oid={facturaDetalleAlicuotaIVA.alicuotaIVA.oid},facturaDetalleAlicuotaIVA.facturaDetalle.oid={facturaDetalleAlicuotaIVA.facturaDetalle.oid},facturaDetalleAlicuotaIVA.tipoImputacionIVA={facturaDetalleAlicuotaIVA.tipoImputacionIVA},facturaDetalleAlicuotaIVA.montoIVA={facturaDetalleAlicuotaIVA.montoIVA},navegacionIdBack=${navegacionIdBack}"/>
  
 <@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancelar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=${navegacionIdBack},flowControl=back"/>