
<@s.hidden id="factura.proveedor.detalleDePersona.pk.secuencia" name="factura.proveedor.detalleDePersona.pk.secuencia"/>
<@s.hidden id="factura.proveedor.detalleDePersona.pk.identificador" name="factura.proveedor.detalleDePersona.pk.identificador"/>
<@s.hidden id="factura.proveedor.detalleDePersona.persona.oid" name="factura.proveedor.detalleDePersona.persona.oid"/>


<#if facturaRubroCtaContable?exists && facturaRubroCtaContable>
	<@s.hidden id="factura.origenComprobante" name="factura.origenComprobante.ordinal()"/>
</#if>

<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>				
<#assign claseCombo="textareagris">
<#assign claseTexto="textareagris">
<#assign habilitar="true">
				
<#if factura.proveedor?exists && factura.proveedor.nroProveedor?exists>
	<#assign claseCombo="textarea">
	<#assign claseTexto="textarea">
	<#assign habilitar="false">
<#else>
	<#assign claseCombo="textareagris">
	<#assign claseTexto="textareagris">
	<#assign habilitar="true">				
</#if>
<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos del Proveedor</b></td>
				<td>
					<div align="right">
						<@s.a templateDir="custontemplates" id="limpiarDatosProveedor" name="limpiarDatosProveedor" href="javascript://nop/" cssClass="ocultarIcono">
						<b>Limpiar</b>
						<img src="${request.contextPath}/common/images/limpiar.gif"  border="0" align="absmiddle" hspace="3" >
						</@s.a>
					</div>
				</td>
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>		
			<#if modoCrearFactura?exists && modoCrearFactura>
			<tr>
				<td class="textoCampo">Nro. Proveedor:</td>
	      		<td class="textoDato">	      				      			
	      			<@s.textfield 
	      					templateDir="custontemplates" 
							id="nroProveedor" 
							cssClass="textarea"
							cssStyle="width:160px" 
							name="nroProveedor" 
							title="Nro. Proveedor" />
							
					<@s.a templateDir="custontemplates" id="validarNroProveedor" name="validarNroProveedor" href="javascript://nop/" cssClass="ocultarIcono">
					<img src="${request.contextPath}/common/images/validar.gif"  border="0" align="absmiddle" hspace="3" >
					</@s.a>	
	      		</td>
	      		<td class="textoCampo">Cod. de Barras/CUIT:</td>
	      		<td class="textoDato">	      				      			
	      			<@s.textfield 
	      					templateDir="custontemplates" 
							id="codigoBarra" 
							cssClass="letraChica"
							cssStyle="width:196px" 
							name="codigoBarra" 
							title="Codigo de barras" />
							
						<@s.a templateDir="custontemplates" id="validarCodigoBarra" name="validarCodigoBarra" href="javascript://nop/" cssClass="ocultarIcono">
								<img src="${request.contextPath}/common/images/validar.gif"  border="0" align="absmiddle" hspace="3" >
						</@s.a>	
	      		</td>
	      	</tr>
		</#if>
									
		<tr>
	    	<td class="textoCampo">Nombre Fantasia: </td>
			<td class="textoDato">
				<@s.textfield 
  					templateDir="custontemplates" 
					id="factura.proveedor.nombreFantasia" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="factura.proveedor.nombreFantasia" 
					title="Nombre Fantasia" />
					
				<@s.a templateDir="custontemplates" id="seleccionarProveedor" name="seleccionarProveedor" href="javascript://nop/" cssClass="ocultarIcono">
						<img src="${request.contextPath}/common/images/buscar.gif"  border="0" align="absmiddle" hspace="3" >
				</@s.a>	

			</td>
			<td class="textoCampo">Razon Social/Apellido:</td>
			<td class="textoDato">
				<@s.textfield 
  					templateDir="custontemplates" 
					id="factura.proveedor.detalleDePersona.razonSocial" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="factura.proveedor.detalleDePersona.razonSocial" 
					title="Razon Social / Apellido" />
				
			</td>				   			
		</tr>
		<tr>
	    	<td class="textoCampo">CUIT:</td>
			<td class="textoDato">
				<@s.textfield 
  					templateDir="custontemplates" 
					id="factura.proveedor.detalleDePersona.persona.docFiscal" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="factura.proveedor.detalleDePersona.persona.docFiscal" 
					title="CUIT" />												
			</td>
			<td class="textoCampo">Documento:</td>
			<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="factura.proveedor.detalleDePersona.persona.docPersonal" />						
			</td>			
		</tr>
				
		<tr>
			<td class="textoCampo">Provincia:</td>
			<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="factura.proveedor.detalleDePersona.codigoPostal.provincia.pais.descripcion" />					
			</td>
			<td class="textoCampo">Localidad:</td>
			<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="factura.proveedor.detalleDePersona.codigoPostal.descripcion" />						
			</td>			
		</tr>
				
		<tr>
			<td class="textoCampo">Domicilio:</td>
			<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="factura.proveedor.detalleDePersona.calle" />						
				<@s.property default="&nbsp;" escape=false value="factura.proveedor.detalleDePersona.letrasCP" />
				<@s.property default="&nbsp;" escape=false value="factura.proveedor.detalleDePersona.numeroFinca" />
				<@s.property default="&nbsp;" escape=false value="factura.proveedor.detalleDePersona.aptoCasa" />
			</td>

			<td class="textoCampo">Tel&eacute;fono:</td>
			<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="factura.proveedor.detalleDePersona.primerTelLaboral" />
				&nbsp;						
			</td>			
		</tr>
		<tr>
  			<td class="textoCampo">Email:</td>
				<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="factura.proveedor.email" />
				&nbsp;					
				
			</td>
			<td class="textoCampo">Condici&oacute;n Fiscal IVA:</td>
				<td class="textoDato">
					<@s.select 
						templateDir="custontemplates" 
						id="factura.proveedor.detalleDePersona.persona.situacionFiscal.codigo" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="factura.proveedor.detalleDePersona.persona.situacionFiscal.codigo" 
						list="situacionFiscalList" 
						listKey="codigo" 
						listValue="descripcion" 
						title="Documento Fiscal V&aacute;lido"
						headerKey=""
						headerValue="Seleccionar"  
						/>												
			</td>			
		</tr>		
		<tr>
  			<td class="textoCampo">CAI:</td>
  			<td class="textoDato">	      				      			
  			<@s.textfield 
  					templateDir="custontemplates" 
					id="factura.cai" 
					cssClass="textarea"					
					cssStyle="width:160px" 
					name="factura.cai" 
					title="CAI"
					onfocus="true" />
  			</td>
 			<td class="textoCampo">Fecha Vencimiento CAI:</td>
  			<td class="textoDato">
				<@vc.rowCalendar 
				templateDir="custontemplates" 
				id="factura.fechaCai" 
				cssClass="textarea"						
				cssStyle="width:160px" 
				name="factura.fechaCai" 
				title="Fecha Cai" />
			</td>	
		</tr>
		<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>
	</table>
	</div>
	
	<div id="capaSeparadora" class="capaSeparadora">&nbsp;</div>			
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos del Comprobante</b></td>
				<td>
					<div align="right">
						<@s.a templateDir="custontemplates" id="modificarFactura" name="modificarFactura" href="javascript://nop/" cssClass="ocultarIcono">
						<b>Modificar</b><img src="${request.contextPath}/common/images/modificar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
						</@s.a>
					</div>
				</td>
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>		
			<tr>
				<td class="textoCampo">Tipo Comprobante:</td>
	      		<td class="textoDato" colspan="3">
					<@s.select 
						templateDir="custontemplates" 
						id="factura.tipoComprobante.codigo" 
						cssClass="textarea"						
						cssStyle="width:165px" 
						name="factura.tipoComprobante.codigo" 
						list="tipoComprobanteList" 
						listKey="codigo" 
						listValue="codigoDescripcionString" 
						title="Tipo Comprobante"
						headerKey="0"
						headerValue="Seleccionar"  
						/>
	               </td>
			</tr>
			<tr>
      			<td class="textoCampo">Punto de Venta:</td>
      			<td class="textoDato">	      				      			
      			<@s.textfield 
      					templateDir="custontemplates" 
						id="factura.sucursal" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="factura.sucursal" 
						title="Punto de Venta" />
      			</td>

      			<td class="textoCampo">N&uacute;mero:</td>
      			<td class="textoDato">	      				      			
      			<@s.textfield 
      					templateDir="custontemplates" 
						id="factura.numeroFactura" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="factura.numeroFactura" 
						title="N&uacute;mero Factura" />
      			</td>	
			</tr>
			<tr>
      			<td class="textoCampo">Fecha Emisi&oacute;n:</td>
      			<td class="textoDato">
					<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="factura.fechaEmision" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="factura.fechaEmision" 
					title="Fecha Emisi&oacute;n"
					onchange="javascript:actualizarFechaVencimFactura(this);" />
				</td>
      			<td class="textoCampo">Origen Comprobante:</td>
      			<td class="textoDato">
 			 	<@s.select 
							templateDir="custontemplates" 
							id="factura.origenComprobante" 
							cssClass="textarea"
							cssStyle="width:165px" 
							name="factura.origenComprobante" 
							list="origenComprobanteList" 
							listKey="key" 
							listValue="description" 
							value="factura.origenComprobante.ordinal()"
							title="Origen Comprobante"
							headerKey="0"
							headerValue="Seleccionar"							
							/>					
				</td>
			</tr>
			<tr>
	  			<td class="textoCampo">Es Original:</td>
      			<td class="textoDato" colspan="3">
	 				<@s.select 
							templateDir="custontemplates" 
							id="factura.esOriginal" 
							cssClass="textarea"
							cssStyle="width:165px" 
							name="factura.esOriginal" 
							list="esOriginalList" 
							listKey="key" 
							listValue="description" 
							value="factura.esOriginal.ordinal()"
							title="Es Original"
							headerKey="0"
							headerValue="Seleccionar"							
							/>	
				</td>		
			</tr>
			
				<tr>
					<td class="textoCampo">Cliente:</td>
	      			<td class="textoDato">
	      			
	      			<@s.property default="&nbsp;" escape=false value="factura.cliente.descripcion"/>	      									
						<@s.a templateDir="custontemplates" id="seleccionarCliente" name="seleccionarCliente" href="javascript://nop/" cssClass="ocultarIcono">
							<img src="${request.contextPath}/common/images/buscar.gif" border="0" align="absmiddle" hspace="3" >
						</@s.a>		
						</td>				 	
										
	      			<td  class="textoCampo">Imagen Digital:</td>
					<td class="textoDato">
					<#if !(modoCrearNota?exists || modoCrearFactura?exists)>	
						<a  
								id="imagenView" 
								name="imagenView"
								target="_blank" 
								href="${request.contextPath}/comprobante/factura/facturaRedirectImagenView.action?factura.pkFactura.nro_factura=${factura.pkFactura.nro_factura?c}&factura.pkFactura.nro_proveedor=${factura.pkFactura.nro_proveedor?c}&factura.pkFactura.sucursal=${factura.pkFactura.sucursal?c}&factura.pkFactura.tip_docum=${factura.pkFactura.tip_docum?c}&navigationId=factura-uploadImagen&flowControl=regis&navegacionIdBack=${navigationId}">
									<img src="${request.contextPath}/common/images/ver.gif" border="0" align="absmiddle" hspace="3" alt="Ver Imagen del Comprobante" title="Ver Imagen del Comprobante">
						</a>		
					</#if>
						&nbsp;
					<#if modoModificar?exists && modoModificar>

								<a templateDir="custontemplates" 
							id="seleccionarFoto" 
							name="seleccionarFoto" 
							target="_blank"
							href="${request.contextPath}/imagenes/uploadImagenView.action?factura.pkFactura.nro_factura=${factura.pkFactura.nro_factura?c}&factura.pkFactura.nro_proveedor=${factura.pkFactura.nro_proveedor?c}&factura.pkFactura.sucursal=${factura.pkFactura.sucursal?c}&factura.pkFactura.tip_docum=${factura.pkFactura.tip_docum?c}&navigationId=factura-uploadImagen&flowControl=regis&navegacionIdBack=${navigationId}">
								<img src="${request.contextPath}/common/images/imagenUP.gif" border="0" align="absmiddle" hspace="3" alt="Upload Imagen del Comprobante" title="Upload Imagen del Comprobante">
						</a>						
					</#if>			
					</td>		      			

				</tr>
				<tr>
					<td class="textoCampo">Fecha de Ingreso:</td>
	      			<td class="textoDato">
					<@s.if test="factura.fechaIngreso != null">
					<#assign fechaIngreso = factura.fechaIngreso> 
					${fechaIngreso?string("dd/MM/yyyy")}	
					</@s.if>&nbsp;	
					</td>	
					<td  class="textoCampo">Proceso:</td>
					<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="factura.seccion" />
					</td>
				</tr>
				<tr>
					<td  class="textoCampo">Clase Comprobante:</td>
					<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="factura.claseComprobante" />
					</td>
				 	<td  class="textoCampo">Estado:</td>
					<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="factura.estado" />
					</td>
				</tr>
				<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>
			</table>		
		</div>	

