<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaSeparadora" class="capaSeparadora">&nbsp;</div>
<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="left">
				<input id="cancelar" type="button" name="btnVolver" value="Volver" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

  <@vc.htmlContent 
   baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancelar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=${navegacionIdBack},flowControl=back"/>
  
  
  <@vc.htmlContent 
  baseUrl="${request.contextPath}/comprobante/factura/updateView.action" 
  source="modificarFactura" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="factura.pkFactura.nro_proveedor={factura.pkFactura.nro_proveedor},factura.pkFactura.nro_factura={factura.pkFactura.nro_factura},factura.pkFactura.sucursal={factura.pkFactura.sucursal},factura.pkFactura.tip_docum={factura.pkFactura.tip_docum},navigationId=factura-actualizar,flowControl=regis"/>

  