<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancel" type="button" name="btnCancelar" value="Cancelar" class="boton"/>
				<input id="store" type="button" name="btnAgregar" value="Agregar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=${navegacionIdBack},flowControl=back"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/comprobante/factura/create.action" 
  source="store" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="factura.proveedor.nroProveedor={factura.proveedor.nroProveedor},factura.proveedor.razonSocial={factura.proveedor.razonSocial},factura.proveedor.nombre={factura.proveedor.nombre},factura.numeroFactura={factura.numeroFactura},factura.cai={factura.cai},factura.fechaCai={factura.fechaCai},factura.sucursal={factura.sucursal},factura.fechaEmision={factura.fechaEmision},factura.fechaVencimiento={factura.fechaVencimiento},factura.tipoAdquisicion={factura.tipoAdquisicion},factura.esOriginal={factura.esOriginal},factura.formaPago={factura.formaPago},factura.tipoComprobante.codigo={factura.tipoComprobante.codigo},factura.origenComprobante={origenComprobanteID},navegacionIdBack=${navegacionIdBack},oidParameter=${oidParameter},facturaRubroCtaContable={facturaRubroCtaContable},factura.cliente.oid={factura.cliente.oid}"/>
  
  
  
  <@vc.htmlContent 
  baseUrl="${request.contextPath}/comprobante/factura/selectProveedorABM.action" 
  source="seleccionarProveedor" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId=${navigationId},flowControl=change,factura.proveedor.nroProveedor={factura.proveedor.nroProveedor},factura.proveedor.razonSocial={factura.proveedor.razonSocial},factura.proveedor.nombre={factura.proveedor.nombre},factura.numeroFactura={factura.numeroFactura},factura.cai={factura.cai},factura.fechaCai={factura.fechaCai},factura.sucursal={factura.sucursal},factura.fechaEmision={factura.fechaEmision},factura.fechaVencimiento={factura.fechaVencimiento},factura.tipoAdquisicion={factura.tipoAdquisicion},factura.esOriginal={factura.esOriginal},factura.formaPago={factura.formaPago},factura.tipoComprobante.codigo={factura.tipoComprobante.codigo},factura.origenComprobante={origenComprobanteID},navegacionIdBack=${navegacionIdBack},oidParameter=${oidParameter},facturaRubroCtaContable={facturaRubroCtaContable},factura.cliente.oid={factura.cliente.oid},factura.cliente.descripcion={factura.cliente.descripcion}"/>

  <@vc.htmlContent 
  baseUrl="${request.contextPath}/comprobante/factura/validarCodigoBarra.action" 
  source="validarCodigoBarra" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId=${navigationId},flowControl=change,factura.proveedor.nroProveedor={factura.proveedor.nroProveedor},factura.proveedor.razonSocial={factura.proveedor.razonSocial},factura.proveedor.nombre={factura.proveedor.nombre},factura.numeroFactura={factura.numeroFactura},factura.cai={factura.cai},factura.fechaCai={factura.fechaCai},factura.sucursal={factura.sucursal},factura.fechaEmision={factura.fechaEmision},factura.fechaVencimiento={factura.fechaVencimiento},factura.tipoAdquisicion={factura.tipoAdquisicion},factura.esOriginal={factura.esOriginal},factura.formaPago={factura.formaPago},factura.tipoComprobante.codigo={factura.tipoComprobante.codigo},factura.origenComprobante={origenComprobanteID},navegacionIdBack=${navegacionIdBack},oidParameter=${oidParameter},facturaRubroCtaContable={facturaRubroCtaContable},factura.cliente.oid={factura.cliente.oid},factura.cliente.descripcion={factura.cliente.descripcion},codigoBarra={codigoBarra}"/>



  <@vc.htmlContent 
  baseUrl="${request.contextPath}/comprobante/factura/selectCliente.action" 
  source="seleccionarCliente" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId=${navigationId},flowControl=change,factura.proveedor.nroProveedor={factura.proveedor.nroProveedor},factura.proveedor.razonSocial={factura.proveedor.razonSocial},factura.proveedor.nombre={factura.proveedor.nombre},factura.numeroFactura={factura.numeroFactura},factura.cai={factura.cai},factura.fechaCai={factura.fechaCai},factura.sucursal={factura.sucursal},factura.fechaEmision={factura.fechaEmision},factura.fechaVencimiento={factura.fechaVencimiento},factura.tipoAdquisicion={factura.tipoAdquisicion},factura.esOriginal={factura.esOriginal},factura.formaPago={factura.formaPago},factura.tipoComprobante.codigo={factura.tipoComprobante.codigo},factura.origenComprobante={origenComprobanteID},navegacionIdBack=${navegacionIdBack},oidParameter=${oidParameter},facturaRubroCtaContable={facturaRubroCtaContable},factura.cliente.oid={factura.cliente.oid},factura.cliente.descripcion={factura.cliente.descripcion}"/>
  
  
  
  
  <@vc.htmlContent 
  baseUrl="${request.contextPath}/comprobante/factura/limpiarFactura.action" 
  source="limpiarDatosProveedor" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId=${navigationId},flowControl=change,factura.proveedor.nroProveedor={factura.proveedor.nroProveedor},factura.proveedor.razonSocial={factura.proveedor.razonSocial},factura.proveedor.nombre={factura.proveedor.nombre},factura.numeroFactura={factura.numeroFactura},factura.cai={factura.cai},factura.fechaCai={factura.fechaCai},factura.sucursal={factura.sucursal},factura.fechaEmision={factura.fechaEmision},factura.fechaVencimiento={factura.fechaVencimiento},factura.tipoAdquisicion={factura.tipoAdquisicion},factura.esOriginal={factura.esOriginal},factura.formaPago={factura.formaPago},factura.tipoComprobante.codigo={factura.tipoComprobante.codigo},factura.origenComprobante={origenComprobanteID},navegacionIdBack=${navegacionIdBack},oidParameter=${oidParameter},facturaRubroCtaContable={facturaRubroCtaContable},factura.cliente.oid={factura.cliente.oid},factura.cliente.descripcion={factura.cliente.descripcion}"/>


  <@vc.htmlContent 
  baseUrl="${request.contextPath}/comprobante/factura/validarNroProveedor.action" 
  source="validarNroProveedor" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId=${navigationId},flowControl=change,factura.proveedor.nroProveedor={factura.proveedor.nroProveedor},factura.proveedor.razonSocial={factura.proveedor.razonSocial},factura.proveedor.nombre={factura.proveedor.nombre},factura.numeroFactura={factura.numeroFactura},factura.cai={factura.cai},factura.fechaCai={factura.fechaCai},factura.sucursal={factura.sucursal},factura.fechaEmision={factura.fechaEmision},factura.fechaVencimiento={factura.fechaVencimiento},factura.tipoAdquisicion={factura.tipoAdquisicion},factura.esOriginal={factura.esOriginal},factura.formaPago={factura.formaPago},factura.tipoComprobante.codigo={factura.tipoComprobante.codigo},factura.origenComprobante={origenComprobanteID},navegacionIdBack=${navegacionIdBack},oidParameter=${oidParameter},facturaRubroCtaContable={facturaRubroCtaContable},factura.cliente.oid={factura.cliente.oid},factura.cliente.descripcion={factura.cliente.descripcion},nroProveedor={nroProveedor}"/>  
  
 
  
  
  