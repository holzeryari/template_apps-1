<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>


<#if factura.ordenPagoList?exists && (factura.ordenPagoList.size()>0)>

<div id="capaSeparadora" class="capaSeparadora">&nbsp;</div>

<@vc.anchors target="contentTrx" ajaxFlag="ajax">
	<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
		<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
			<tr><td>Ordenes de Pago relacionadas</td></tr>
		</table>	
		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="factura.ordenPagoList" id="ordenPago" defaultsort=1>

			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="numero" title="Nro." />				

			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" title="Proveedor">
				<#if ordenPago.proveedor?exists>  
					${ordenPago.proveedor.detalleDePersona.razonSocial}
					<#if ordenPago.proveedor.detalleDePersona.nombre?exists> 
						 ${ordenPago.proveedor.detalleDePersona.nombre}
					</#if>
				</#if>
			</@display.column>

			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" title="Beneficiario">  
				<#if ordenPago.beneficiario?exists>
					${ordenPago.beneficiario.razonSocial}
					<#if ordenPago.beneficiario.nombre?exists> 
					 	${ordenPago.beneficiario.nombre}
					</#if>
				</#if>
			</@display.column>
			
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="fechaEmision" format="{0,date,dd/MM/yyyy}"  title="Fecha Emisi&oacute;n" /> 

			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="estado" title="Estado" />		

		</@display.table>
	</div>
</@vc.anchors>

</#if>
