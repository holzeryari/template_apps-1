<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<@s.hidden id="entregaSeleccion" name="entregaSeleccion"/>

<#if factura.estado.ordinal()==1>
    <div id="capaSeparadora" class="capaSeparadora">&nbsp;</div>
	<@vc.anchors target="contentTrx" ajaxFlag="ajax">
		<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
			<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<#if factura.origenComprobante.ordinal()==1>
					<tr>
						<td>Ordenes de Compra</td>
					</tr>
				<#else>
					<tr>
						<td>Ordenes de Servicio</td>
					</tr>
				</#if>
			</table>			
	   		
	   		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="factura.facturaOCOSList" id="facturaOCOS"  defaultsort=2 decorator="ar.com.riouruguay.web.actions.compraContratacion.oc_os.decorators.OC_OSDecorator" >
	        	<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" class="botoneraAnchoCon1" title="Acciones">
							<div class="alineacion">
							<@s.a 				
								templateDir="custontemplates"
								cssClass="item" 
								id="ver"
								name="ver"
								href="${request.contextPath}/comprobante/factura/readOCOSView.action?facturaOCOS.oid=${facturaOCOS.oid?c}&navigationId=factura-verOCOS&flowControl=regis&navegacionIdBack=${navigationId}">
								<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver"  border="0">
							</@s.a>
							</div>
	
						</@display.column>
						
						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="ocos.numero" title="N&uacute;mero" />
						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="ocos.fecha" format="{0,date,dd/MM/yyyy}"  title="Fecha"/>					
						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="ocos.importe" title="Importe" />
						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="ocos.estado" title="Estado" />				
						
			</@display.table>	
		</div>
		</@vc.anchors>
</#if>