<#setting locale="es_AR">

<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>



<@s.hidden id="navigationId" name="navigationId"/>
<div id="capaSeparadora" class="capaSeparadora">&nbsp;</div>

<@vc.anchors target="contentTrx">	

	<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
		<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Medios de Pago</td>
					<td>	
					  <#if factura.claseComprobante?exists && (factura.claseComprobante.ordinal()==1 || factura.claseComprobante.ordinal()==4 || factura.claseComprobante.ordinal()==3)>
						<div align="right">
							<@s.a href="${request.contextPath}/comprobante/factura/createMedioPagoView.action?navegacionIdBack=${navigationId}&facturaMedioPago.factura.pkFactura.nro_proveedor=${factura.pkFactura.nro_proveedor?c}&facturaMedioPago.factura.pkFactura.nro_factura=${factura.pkFactura.nro_factura?c}&facturaMedioPago.factura.pkFactura.sucursal=${factura.pkFactura.sucursal?c}&facturaMedioPago.factura.pkFactura.tip_docum=${factura.pkFactura.tip_docum?c}&facturaMedioPago.factura.proveedor.nroProveedor=${factura.proveedor.nroProveedor?c}" templateDir="custontemplates" id="agregarComprobante" name="agregarComprobante" cssClass="ocultarIcono">
								<b>Agregar</b><img src="${request.contextPath}/common/images/agregar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
							</@s.a>
						</div>
					  </#if>						
					</td>
				</tr>
		</table>
		
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
				<td class="textoCampo">Fecha Sugerida de Pago:</td>
	  			<td class="textoDato">
	      			<@s.if test="factura.fechaVencimiento != null">
					<#assign fechaVencimiento = factura.fechaVencimiento> 
					${fechaVencimiento?string("dd/MM/yyyy")}	
					</@s.if>&nbsp;
					<@s.a templateDir="custontemplates" id="modificarDatosPago" name="modificarDatosPago" href="javascript://nop/" cssClass="ocultarIcono">
						<img src="${request.contextPath}/common/images/modificar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
					</@s.a>
				</td>
				<td class="textoCampo">&nbsp;</td>
	  			<td class="textoDato">&nbsp;</td>
			</tr>
		</table>

	<#if factura.claseComprobante?exists && (factura.claseComprobante.ordinal()==1 || factura.claseComprobante.ordinal()==4 || factura.claseComprobante.ordinal()==3)>		
        <@display.table class="tablaDetalleCuerpo" cellpadding="3" name="factura.facturaMedioPagoList" id="facturaMedioPago" pagesize=15 defaultsort=2 >
        	<@display.column headerClass="tbl-contract-service-select" class="botoneraAnchoCon3" title="Acciones">
				<div class="alineacion">				
					<@s.a 
						templateDir="custontemplates" 
						cssClass="item" 
						id="ver"
						name="ver"
						href="${request.contextPath}/comprobante/factura/readFormaMedioPagoView.action?facturaMedioPago.oid=${facturaMedioPago.oid?c}&navegacionIdBack=${navigationId}">
						<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver" border="0">
					</@s.a>
				</div>
	        	<div class="alineacion">
					<@s.a 
						templateDir="custontemplates" 
						cssClass="item"
						id="modificar"
						name="modificar"  
						href="${request.contextPath}/comprobante/factura/updateFormaMedioPagoView.action?facturaMedioPago.oid=${facturaMedioPago.oid?c}&navegacionIdBack=${navigationId}">
						<img  src="${request.contextPath}/common/images/modificar.gif" alt="Modificar" title="Modificar" border="0">
					</@s.a>									
				</div>
				<div class="alineacion"> 
					<@s.a 
						templateDir="custontemplates"  
						cssClass="item" 
						id="eliminar"
						name="eliminar" 
						href="${request.contextPath}/comprobante/factura/deleteFormaMedioPagoView.action?facturaMedioPago.oid=${facturaMedioPago.oid?c}&navegacionIdBack=${navigationId}">
						<img  src="${request.contextPath}/common/images/eliminar.gif" alt="Eliminar" title="Eliminar"  border="0">
					</@s.a>
				</div>
			</@display.column>
						
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="formaPago.descripcion" title="Forma Pago" />
					
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="medioPago.descripcion" title="Medio Pago" />
							
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="observaciones" title="Observaciones" />
			
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="fechaAsignada" format="{0,date,dd/MM/yyyy}" title="Fecha Asignada" />
					
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="porcentaje" title="Porcentaje" />
			
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="montoAsignado" title="Monto Asignado" />
			
		</@display.table>
	</#if>					
	</div>	
</@vc.anchors>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/comprobante/factura/updatePagoView.action" 
  source="modificarDatosPago" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="factura.pkFactura.nro_proveedor={factura.pkFactura.nro_proveedor},factura.pkFactura.nro_factura={factura.pkFactura.nro_factura},factura.pkFactura.sucursal={factura.pkFactura.sucursal},factura.pkFactura.tip_docum={factura.pkFactura.tip_docum}"/>
