<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<#assign displayFecAsig="none">
<#if facturaMedioPago.formaPago?exists && facturaMedioPago.formaPago.conFechaAsignada?exists && facturaMedioPago.formaPago.conFechaAsignada>
	<#assign displayFecAsig="">
<#else>
	<#assign displayFecAsig="none">				
</#if>

<@s.hidden id="navigationId" name="navigationId"/>
<@s.hidden id="facturaMedioPago.oid" name="facturaMedioPago.oid"/>
<@s.hidden id="facturaMedioPago.factura.oid" name="facturaMedioPago.factura.oid"/>
<@s.hidden id="facturaMedioPago.versionNumber" name="facturaMedioPago.versionNumber"/>

<@s.hidden id="facturaMedioPago.formaPago.conFechaAsignada" name="facturaMedioPago.formaPago.conFechaAsignada"/>

<@s.hidden id="facturaMedioPago.factura.pkFactura.nro_factura" name="facturaMedioPago.factura.pkFactura.nro_factura"/>
<@s.hidden id="facturaMedioPago.factura.pkFactura.nro_proveedor" name="facturaMedioPago.factura.pkFactura.nro_proveedor"/>
<@s.hidden id="facturaMedioPago.factura.pkFactura.sucursal" name="facturaMedioPago.factura.pkFactura.sucursal"/>
<@s.hidden id="facturaMedioPago.factura.pkFactura.tip_docum" name="facturaMedioPago.factura.pkFactura.tip_docum"/>
<@s.hidden id="facturaMedioPago.factura.totalFactura" name="facturaMedioPago.factura.totalFactura"/>


<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
</div>
<div id="capaTituloAccion" class="capaTituloAccion">
	<table id="tablaTituloAccion" class="tablaTituloAccion">
		<tr>
        	<td><div id="errorTrx" align="left"></div></td>
       	</tr>
    	<tr>
			<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" /><@s.text name="${titulo}" /></td>
		</tr>
	</table>
</div>	

<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos del Proveedor</b></td>
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>

			<tr>
      			<td class="textoCampo">Nombre Fantasia: </td>
					<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="facturaMedioPago.factura.proveedor.nombreFantasia" />
				</td>
				<td class="textoCampo">Razon Social/Apellido:</td>
				<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="facturaMedioPago.factura.proveedor.detalleDePersona.razonSocial" />						
				</td>				   			
			</tr>
					
			<tr>
	  			<td class="textoCampo">CUIT/CUIL:</td>
				<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="facturaMedioPago.factura.proveedor.detalleDePersona.persona.docFiscal" />						
				</td>
				<td class="textoCampo">Documento:</td>
					<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="facturaMedioPago.factura.proveedor.detalleDePersona.persona.docPersonal" />						
				</td>			
			</tr>
				
			<tr>
				<td class="textoCampo">Provincia:</td>
					<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="facturaMedioPago.factura.proveedor.detalleDePersona.codigoPostal.provincia.pais.descripcion" />					
				</td>
				<td class="textoCampo">Localidad:</td>
					<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="facturaMedioPago.factura.proveedor.detalleDePersona.codigoPostal.descripcion" />						
				</td>			
			</tr>
				
			<tr>
				<td class="textoCampo">Domicilio:</td>
				<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="facturaMedioPago.factura.proveedor.detalleDePersona.calle" />						
					<@s.property default="&nbsp;" escape=false value="facturaMedioPago.factura.proveedor.detalleDePersona.letrasCP" />
					<@s.property default="&nbsp;" escape=false value="facturaMedioPago.factura.proveedor.detalleDePersona.numeroFinca" />
					<@s.property default="&nbsp;" escape=false value="facturaMedioPago.factura.proveedor.detalleDePersona.aptoCasa" />
				</td>

				<td class="textoCampo">Tel&eacute;fono:</td>
				<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="facturaMedioPago.factura.proveedor.detalleDePersona.primerTelLaboral" />
					&nbsp;						
				</td>			
			</tr>
			<tr>
      			<td class="textoCampo">Email:</td>
				<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="facturaMedioPago.factura.proveedor.email" />
					&nbsp;					
					
				</td>
				<td class="textoCampo">Condici&oacute;n Fiscal IVA:</td>
				<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="facturaMedioPago.factura.proveedor.detalleDePersona.persona.situacionFiscal.descripcion" />						
				</td>			
			</tr>		
			<tr>
      			<td class="textoCampo">CAI:</td>
      			<td class="textoDato">	     
      			<@s.property default="&nbsp;" escape=false value="facturaMedioPago.factura.cai" />
      			&nbsp;
      			
      			</td>
 				<td class="textoCampo">Fecha Vencimiento CAI:</td>
      			<td class="textoDato">
      			<@s.if test="facturaMedioPago.factura.fechaCai != null">
				<#assign fechaCai = facturaMedioPago.factura.fechaCai> 
				${fechaCai?string("dd/MM/yyyy")}	
				</@s.if>&nbsp;	
				</td>	
			</tr>
			<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>
		</table>
	</div>
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos del Comprobante</b></td>
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>			
			<tr>
				<td class="textoCampo">Tipo Comprobante:</td>
	  			<td class="textoDato"colspan="3">
		  			<@s.property default="&nbsp;" escape=false value="facturaMedioPago.factura.tipoComprobante.descripcion" />
				</td>
			</tr>
			<tr>
      			<td class="textoCampo">Sucursal:</td>
      			<td class="textoDato">	 
      				<@s.property default="&nbsp;" escape=false value="facturaMedioPago.factura.sucursal" />
      			</td>

      			<td class="textoCampo">N&uacute;mero:</td>
      			<td class="textoDato">	
      				<@s.property default="&nbsp;" escape=false value="facturaMedioPago.factura.numeroFactura" />      				      			
      			</td>	
			</tr>
			<tr>
      			<td class="textoCampo">Fecha Emisi&oacute;n:</td>
      			<td class="textoDato">
	      			<@s.if test="facturaMedioPago.factura.fechaEmision != null">
					<#assign fechaEmision = facturaMedioPago.factura.fechaEmision> 
					${fechaEmision?string("dd/MM/yyyy")}	
					</@s.if>&nbsp;	
				</td>
	  			<td class="textoCampo">Fecha Sugerida de Pago:</td>
      			<td class="textoDato">
	      			<@s.if test="facturaMedioPago.factura.fechaVencimiento != null">
					<#assign fechaVencimiento = facturaMedioPago.factura.fechaVencimiento> 
					${fechaVencimiento?string("dd/MM/yyyy")}	
					</@s.if>&nbsp;					
				</td>		
			</tr>
			<tr>
      			<td class="textoCampo">Origen Comprobante:</td>
      			<td class="textoDato">
      				<@s.property default="&nbsp;" escape=false value="facturaMedioPago.factura.origenComprobante" />     
      			</td>
	  			<td class="textoCampo">Es Original:</td>
      			<td class="textoDato">
      			<@s.property default="&nbsp;" escape=false value="facturaMedioPago.factura.esOriginal" />
	 			</td>		
			</tr>


			<tr>
				<td class="textoCampo">Cliente:</td>
      			<td class="textoDato">
      			<@s.property default="&nbsp;" escape=false value="facturaMedioPago.factura.cliente.descripcion"/>	      									
				</td>				 	
				<td  class="textoCampo">Imagen Digital:</td>
				<td class="textoDato">
					<#if !(modoCrear?exists)>
						<a 	id="imagenView" 
							name="imagenView"
							target="_blank" 
							href="${request.contextPath}/comprobante/factura/facturaRedirectImagenView.action?factura.pkFactura.nro_factura=${facturaMedioPago.factura.pkFactura.nro_factura?c}&factura.pkFactura.nro_proveedor=${facturaMedioPago.factura.pkFactura.nro_proveedor?c}&factura.pkFactura.sucursal=${facturaMedioPago.factura.pkFactura.sucursal?c}&factura.pkFactura.tip_docum=${facturaMedioPago.factura.pkFactura.tip_docum?c}&navigationId=factura-uploadImagen&flowControl=regis&navegacionIdBack=${navigationId}">
									<img src="${request.contextPath}/common/images/ver.gif" border="0" align="absmiddle" hspace="3" alt="Ver Imagen del Comprobante" title="Ver Imagen del Comprobante"  />
						</a>		
					</#if>
					</td>						
				</td>		      			
			</tr>
			<tr>
				<td class="textoCampo">Fecha de Ingreso:</td>
      			<td class="textoDato">
					<@s.if test="facturaMedioPago.factura.fechaIngreso != null">
					<#assign fechaIngreso = facturaMedioPago.factura.fechaIngreso> 
					${fechaIngreso?string("dd/MM/yyyy")}	
					</@s.if>&nbsp;	
				</td>	
				<td  class="textoCampo">Proceso:</td>
				<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="facturaMedioPago.factura.seccion" />
				</td>
			</tr>
			<tr>
				<td  class="textoCampo">Clase Comprobante:</td>
				<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="facturaMedioPago.factura.claseComprobante" />
				</td>
			 	<td  class="textoCampo">Estado:</td>
				<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="facturaMedioPago.factura.estado" />
				</td>
			</tr>
	    	<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>	
		</table>		
	</div>		

	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos del Medio de Pago</b></td>
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
			
			<tr>
				<td class="textoCampo">Forma Pago:</td>
	      		<td class="textoDato">
			  		<@s.select 
						templateDir="custontemplates" 
						id="formaPago" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="facturaMedioPago.formaPago.pk.idSec" 
						list="formaDePagoList" 
						listKey="pk.idSec" 
						listValue="descripcion" 
						title="Forma de Pago"
						headerKey="-1"
						headerValue="Seleccionar"  
						/>
						
				</td>
				<td class="textoCampo">Medio Pago:</td>
      			<td class="textoDato">
				  		<@s.select 
							templateDir="custontemplates" 
							id="medioPago" 
							cssClass="textarea"
							cssStyle="width:165px" 
							name="facturaMedioPago.medioPago.pk.idSec" 
							list="medioDePagoList" 
							listKey="pk.idSec" 
							listValue="descripcion" 
							title="Medio de Pago"
							headerKey="-1"
							headerValue="Seleccionar"  
							/>
				</td>
			</tr>
			<tr id ="fechaAsignadaTR" style="display:${displayFecAsig};" >	
				<td class="textoCampo">Fecha Asignada:</td>
				<td class="textoDato">
					<@vc.rowCalendar 
						templateDir="custontemplates" 
						id="fechaAsignada" 
						cssClass="textarea"
						cssStyle="width:160px;" 
						name="facturaMedioPago.fechaAsignada" 
						title="Fecha Asignada" />
				</td>
				<td class="textoCampo">Cantidad de D&iacute;as:</td>
				<td class="textoDato">
	      			<@s.textfield 
      					templateDir="custontemplates" 
						id="cantidadDias" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="facturaMedioPago.cantidadDias" 
						title="Cantidad de Dias" />
				</td>
			</tr>
			<tr>	
				<td class="textoCampo">Porcentaje:</td>
				<td  class="textoDato">
					<@s.textfield									
							templateDir="custontemplates"
							template="textMoney"  
							id="facturaMedioPago.porcentaje" 
							cssClass="textarea"
							cssStyle="width:160px" 
							name="facturaMedioPago.porcentaje" 
							title="Porcentaje" />
				</td>
				<td class="textoCampo">Importe:</td>
				<td  class="textoDato">
					<@s.textfield									
							templateDir="custontemplates"
							template="textMoney"  
							id="facturaMedioPago.montoAsignado" 
							cssClass="textarea"
							cssStyle="width:160px" 
							name="facturaMedioPago.montoAsignado" 
							title="Importe" 
							onchange="javascript:calcularPorcentajeMedioCobroFactura(this);"/>

				</td>
			</tr>
			<tr>
				<td class="textoCampo">Observaciones:</td>
      			<td  class="textoDato" colspan="3">
				<@s.textarea	
						templateDir="custontemplates" 						  
						cols="89" rows="4"	      					
						cssClass="textarea"
						id="facturaMedioPago.observaciones"							 
						name="facturaMedioPago.observaciones"  
						label="Observaciones"														
						 />
				</td>
			</tr>		
			<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>		
		</table>
	</div>	
