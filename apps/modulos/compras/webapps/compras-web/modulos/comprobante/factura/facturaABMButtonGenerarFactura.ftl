<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaSeparadora" class="capaSeparadora">&nbsp;</div>
<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancelar" type="button" name="btnCancelar" value="Cancelar" class="boton"/>
				<input id="store" type="button" name="btnGenerarFactura" value="Generar Comprobante" class="boton"/>
			</td>
		</tr>	
	</table>
</div>
  
  
  <@vc.htmlContent 
   baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancelar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=buscar-factura,flowControl=back"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/comprobante/factura/generarFactura.action" 
  source="store" 
  success="contentTrx" 
  failure="errorTrx"
  parameters="factura.pkFactura.nro_proveedor={factura.pkFactura.nro_proveedor},factura.pkFactura.nro_factura={factura.pkFactura.nro_factura},factura.pkFactura.sucursal={factura.pkFactura.sucursal},factura.pkFactura.tip_docum={factura.pkFactura.tip_docum}"/>
  

  <@vc.htmlContent 
  baseUrl="${request.contextPath}/comprobante/factura/updateView.action" 
  source="modificarFactura" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="factura.pkFactura.nro_proveedor={factura.pkFactura.nro_proveedor},factura.pkFactura.nro_factura={factura.pkFactura.nro_factura},factura.pkFactura.sucursal={factura.pkFactura.sucursal},factura.pkFactura.tip_docum={factura.pkFactura.tip_docum},navigationId=factura-actualizar,flowControl=regis"/>

