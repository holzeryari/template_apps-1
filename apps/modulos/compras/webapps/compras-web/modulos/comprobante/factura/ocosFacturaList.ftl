<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<@s.hidden id="entregaSeleccion" name="entregaSeleccion"/>

<div id="capaSeparadora" class="capaSeparadora">&nbsp;</td></tr></div>
<@vc.anchors target="contentTrx" ajaxFlag="ajax">

<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
	<#if factura.origenComprobante.ordinal()==1>
		<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
			<tr>
				<td>Ordenes de Compra</td>
				<td>	
					<div align="right">
						<@s.a href="${request.contextPath}/comprobante/factura/selectOC.action?factura.pkFactura.nro_factura=${factura.pkFactura.nro_factura?c}&factura.pkFactura.nro_proveedor=${factura.pkFactura.nro_proveedor?c}&factura.pkFactura.sucursal=${factura.pkFactura.sucursal?c}&factura.pkFactura.tip_docum=${factura.pkFactura.tip_docum?c}&factura.proveedor.nroProveedor=${factura.proveedor.nroProveedor?c}&factura.proveedor.detalleDePersona.razonSocial=${factura.proveedor.detalleDePersona.razonSocial}&navigationId=${navigationId}" templateDir="custontemplates" id="agregarIngresoProductoDetalle" name="agregarIngresoProductoDetalle" cssClass="ocultarIcono">
							<b>Agregar</b><img src="${request.contextPath}/common/images/agregar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
						</@s.a>		
					</div>
				</td>
			</tr>
		</table>
	<#else>
		<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
			<tr>
				<td>Ordenes de Servicio</td>
				<td>	
					<div align="right">
						<@s.a href="${request.contextPath}/comprobante/factura/selectOS.action?factura.pkFactura.nro_factura=${factura.pkFactura.nro_factura?c}&factura.pkFactura.nro_proveedor=${factura.pkFactura.nro_proveedor?c}&factura.pkFactura.sucursal=${factura.pkFactura.sucursal?c}&factura.pkFactura.tip_docum=${factura.pkFactura.tip_docum?c}&factura.proveedor.nroProveedor=${factura.proveedor.nroProveedor?c}&factura.proveedor.detalleDePersona.razonSocial=${factura.proveedor.detalleDePersona.razonSocial}&navigationId=${navigationId}" templateDir="custontemplates" id="agregarIngresoProductoDetalle" name="agregarIngresoProductoDetalle" cssClass="ocultarIcono">
						<b>Agregar</b><img src="${request.contextPath}/common/images/agregar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
						</@s.a>		
					</div>
				</td>
			</tr>
		</table>
	</#if>
			
	<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="factura.facturaOCOSList" id="facturaOCOS"  defaultsort=2 decorator="ar.com.riouruguay.web.actions.compraContratacion.oc_os.decorators.OC_OSDecorator" >
        <@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" class="botoneraAnchoCon2" title="Acciones">
						<div class="alineacion">
						<@s.a 				
							templateDir="custontemplates"
							cssClass="item" 
							id="ver"
							name="ver"
							href="${request.contextPath}/comprobante/factura/readOCOSView.action?facturaOCOS.oid=${facturaOCOS.oid}&navigationId=factura-verOCOS&flowControl=regis&navegacionIdBack=${navigationId}">
							<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver"  border="0">
						</@s.a>
						</div>
						<div class="alineacion">
							<@s.a  
								templateDir="custontemplates"	
								cssClass="item"  
								id="eliminar"
								name="eliminar"
								href="${request.contextPath}/comprobante/factura/deleteOCOSView.action?facturaOCOS.oid=${facturaOCOS.oid?c}&navigationId=factura-eliminarOCOS&flowControl=regis&navegacionIdBack=${navigationId}">
								<img  src="${request.contextPath}/common/images/eliminar.gif" alt="Eliminar" title="Eliminar"  border="0">
							</@s.a>	
						</div>
					</@display.column>
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="ocos.numero" title="N&uacute;mero" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="ocos.fecha" format="{0,date,dd/MM/yyyy}"  title="Fecha"/>					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="ocos.importe" title="Importe" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="ocos.estado" title="Estado" />				
					
		</@display.table>	
		
	</div>
</@vc.anchors>

