<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<@s.hidden id="navigationId" name="navigationId"/>
<@s.hidden id="factura.pkFactura.nro_proveedor" name="factura.pkFactura.nro_proveedor"/>
<@s.hidden id="factura.pkFactura.nro_factura" name="factura.pkFactura.nro_factura"/>
<@s.hidden id="factura.pkFactura.sucursal" name="factura.pkFactura.sucursal"/>
<@s.hidden id="factura.pkFactura.tip_docum" name="factura.pkFactura.tip_docum"/>
<@s.hidden id="factura.versionNumber" name="factura.versionNumber"/>
<@s.hidden id="factura.proveedor.nroProveedor" name="factura.proveedor.nroProveedor"/>
<@s.hidden id="facturaRubroCtaContable" name="facturaRubroCtaContable"/>
<@s.hidden id="factura.cliente.oid" name="factura.cliente.oid"/>
<@s.hidden id="factura.cliente.descripcion" name="factura.cliente.descripcion"/>
<@s.hidden id="facturaDetalleLista" name="facturaDetalleLista"/>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
			<#if mensajeAviso?exists && (mensajeAviso.length()>0)>
				<tr>
					<td class="estiloMensajeAviso"><@s.text name="${mensajeAviso}"/></td>
				</tr>
			</#if>
			<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<@tiles.insertAttribute name="factura"/>
	<@tiles.insertAttribute name="mediosPagos"/>
	<@tiles.insertAttribute name="ocosContrato"/>	
	<@tiles.insertAttribute name="detalle"/>
	<@tiles.insertAttribute name="detalleConDiferencia"/>						
	<@tiles.insertAttribute name="detalleOrdenPago"/>
	<@tiles.insertAttribute name="detalleObligacionPago"/>
	<@tiles.insertAttribute name="ocosPendienteFacturacion"/>						
	<@tiles.insertAttribute name="contratosProveedor"/>
	<@tiles.insertAttribute name="bienInventariado"/>
			
		

