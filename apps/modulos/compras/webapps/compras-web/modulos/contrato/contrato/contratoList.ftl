<#setting locale="es_AR">

<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>

<@s.hidden id="navigationId" name="navigationId"/>
<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
        	<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Datos del Contrato, Acuerdo o Convenio</b></td>
				<td>
					<div align="right">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0521" 
							enabled="true" 
							cssClass="item" 
							id="crear"										
							href="javascript://nop/">
							<b >Agregar</b>										
							<img src="${request.contextPath}/common/images/agregar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
						</@security.a>		
					</div>
				</td>
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
	    		<td class="textoCampo" colspan="4">&nbsp;</td>
	    	</tr>
			<tr>
	  			<td class="textoCampo">N&uacute;mero:</td>
	  			<td class="textoDato">
	  			<@s.textfield 
	  					templateDir="custontemplates" 
						id="contrato.oid" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="contrato.oid" 
						title="N&uacute;mero" />
				</td>
					
				<td class="textoCampo">Descripci&oacute;n:</td>
      			<td class="textoDato">
      			<@s.textfield 
      					templateDir="custontemplates" 
						id="contrato.descripcion" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="contrato.descripcion" 
						title="Descripci&oacute;n" />
				</td>	
			</tr>
			<tr>					
				<td class="textoCampo">Tipo Documento:</td>
      			<td class="textoDato">
	      			<@s.select 
						templateDir="custontemplates" 
						id="contrato.tipoDocumento" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="contrato.tipoDocumento" 
						list="tipoDocumentoList" 
						listKey="key" 
						listValue="description" 
						value="contrato.tipoDocumento.ordinal()"
						title="Tipo Documento"
						headerKey="0"
						headerValue="Todos"							
						/>
				</td>
				
				<td class="textoCampo">Tipo Contrato:</td>
      			<td class="textoDato">
	      			<@s.select 
						templateDir="custontemplates" 
						id="contrato.tipoContrato" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="contrato.tipoContrato" 
						list="tipoContratoList" 
						listKey="key" 
						listValue="description" 
						value="contrato.tipoContrato.ordinal()"
						title="Tipo Contrato"
						headerKey="0"
						headerValue="Todos"							
						/>
				</td>					
			</tr>

			<tr>
				<td class="textoCampo">Estado:</td>
      			<td class="textoDato" align="left">
	      			<@s.select 
						templateDir="custontemplates" 
						id="contrato.estado" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="contrato.estado" 
						list="estadoList" 
						listKey="key" 
						listValue="description" 
						value="contrato.estado.ordinal()"
						title="Estado"
						headerKey="0"
						headerValue="Todos"							
						/>
				</td>
				<td class="textoCampo">Comprobante de pago requerido:</td>
      			<td class="textoDato" align="left">
			    	<@s.select 
						templateDir="custontemplates" 
						id="contrato.comprobanteReqPag" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="contrato.comprobanteReqPag" 
						list="comprobanteReqPagList" 
						listKey="key" 
						listValue="description" 
						value="contrato.comprobanteReqPag.ordinal()"
						title="Comprobante de pago requerido"
						headerKey="0"
						headerValue="Todos"			
						/>
				</td>
			</tr>
			
			<tr>
    			<td colspan="4" class="lineaGris" align="right">
      			<input id="limpiar" type="button" name="btnVolver" value="Limpiar" class="boton"/>		
      			<input id="buscar" type="button" name="btnVolver" value="Buscar" class="boton"/>
    			</td>
			</tr>	
			</table>
		</div>	
		<!-- Resultado Filtro -->
		<@s.if test="contratoList!=null">
			<#assign contratoOid = "0" />
			<@s.if test="contrato.oid != null">
				<#assign contratoOid = "${contrato.oid}" />
			</@s.if>

			<#assign contratoDescripcion = "" />
			<@s.if test="contrato.descripcion != null">
				<#assign contratoDescripcion = "${contrato.descripcion}" />
			</@s.if>

			<#assign contratoTipoDocumento = "0" />
			<@s.if test="contrato.tipoDocumento != null">
				<#assign contratoTipoDocumento = "${contrato.tipoDocumento.ordinal()}" />
			</@s.if>

			<#assign contratoTipoContrato = "0" />
			<@s.if test="contrato.tipoContrato != null">
				<#assign contratoTipoContrato = "${contrato.tipoContrato.ordinal()}" />
			</@s.if>

			<#assign contratoEstado = "0" />
			<@s.if test="contrato.estado != null">
				<#assign contratoEstado = "${contrato.estado.ordinal()}" />
			</@s.if>
			
			<#assign comprobanteReqPag = "0" />
			<@s.if test="contrato.comprobanteReqPag != null">
				<#assign comprobanteReqPag = "${contrato.comprobanteReqPag.ordinal()}" />
			</@s.if>
			
			
			
			<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">		
			
			<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr class="alturaFila">
					<td width="40%">Contratos, Acuerdos o Convenios encontrados</td>
					<td width="60%">
					<div class="alineacionDerechaBig">						
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0522" 
							enabled="true" 
							cssClass="item" 
							href="${request.contextPath}/contrato/printPendientePagoXLS.action?contrato.oid=${contratoOid}&contrato.descripcion=${contratoDescripcion}&contrato.tipoDocumento=${contratoTipoDocumento}&contrato.tipoContrato=${contratoTipoContrato}&contrato.estado=${contratoEstado}">
							<img src="${request.contextPath}/common/images/imprimirExcel.gif" title="Exportar a Excel el listado de Contrato proximos a vencer" align="absmiddle" border="0" hspace="3">
						</@security.a>
								
						</div>
						<div class="alineacionDerechaBig">
							<b>Pendiente de Pago</b>
								<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0522" 
								enabled="true" 
								cssClass="item" 
								href="${request.contextPath}/contrato/printPendientePagoPDF.action?contrato.oid=${contratoOid}&contrato.descripcion=${contratoDescripcion}&contrato.tipoDocumento=${contratoTipoDocumento}&contrato.tipoContrato=${contratoTipoContrato}&contrato.estado=${contratoEstado}">
								<img src="${request.contextPath}/common/images/imprimirPdf.gif" title="Exportar a PDF el listado de Contrato proximos a vencer" align="absmiddle" border="0" hspace="3" >
							</@security.a>
						</div>	
						<div class="alineacionDerechaBig">						
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0522" 
							enabled="true" 
							cssClass="item" 
							href="${request.contextPath}/contrato/printPorVencerXLS.action?contrato.oid=${contratoOid}&contrato.descripcion=${contratoDescripcion}&contrato.tipoDocumento=${contratoTipoDocumento}&contrato.tipoContrato=${contratoTipoContrato}&contrato.estado=${contratoEstado}">
							<img src="${request.contextPath}/common/images/imprimirExcel.gif" title="Exportar a Excel el listado de Contrato proximos a vencer" align="absmiddle" border="0" hspace="3">
						</@security.a>
								
						</div>
						<div class="alineacionDerechaBig">
							<b>Por Vencer</b>
								<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0522" 
								enabled="true" 
								cssClass="item" 
								href="${request.contextPath}/contrato/printPorVencerPDF.action?contrato.oid=${contratoOid}&contrato.descripcion=${contratoDescripcion}&contrato.tipoDocumento=${contratoTipoDocumento}&contrato.tipoContrato=${contratoTipoContrato}&contrato.estado=${contratoEstado}">
								<img src="${request.contextPath}/common/images/imprimirPdf.gif" title="Exportar a PDF el listado de Contrato proximos a vencer" align="absmiddle" border="0" hspace="3" >
							</@security.a>
						</div>	
						<div class="alineacionDerechaBig">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0522" 
							enabled="true" 
							cssClass="item" 
							href="${request.contextPath}/contrato/printXLS.action?contrato.oid=${contratoOid}&contrato.descripcion=${contratoDescripcion}&contrato.tipoDocumento=${contratoTipoDocumento}&contrato.tipoContrato=${contratoTipoContrato}&contrato.estado=${contratoEstado}">
							<img src="${request.contextPath}/common/images/imprimirExcel.gif" title="Exportar a Excel" align="absmiddle" border="0" hspace="3">
						</@security.a>
						</div>
						<div class="alineacionDerechaBig">
						<b>Imprimir</b>
							<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0522" 
								enabled="true" 
								cssClass="item" 
								href="${request.contextPath}/contrato/printPDF.action?contrato.oid=${contratoOid}&contrato.descripcion=${contratoDescripcion}&contrato.tipoDocumento=${contratoTipoDocumento}&contrato.tipoContrato=${contratoTipoContrato}&contrato.estado=${contratoEstado}">
								<img src="${request.contextPath}/common/images/imprimirPdf.gif" title="Exportar a PDF" align="absmiddle" border="0" hspace="3" >
							</@security.a>
						</div>
	
					</td>
				</tr>
			</table>	
			
			

			<@ajax.anchors target="contentTrx">			
          		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="contratoList" id="contrato" pagesize=15  defaultsort=2 partialList=true size="recordSize" keepStatus=true excludedParams="resetFlag">
          		
  		
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" class="botoneraAnchoCon5" title="Acciones">
						<div class="alineacion">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0523" 
							enabled="contrato.readable" 
							cssClass="item" 
							href="${request.contextPath}/contrato/readContratoView.action?contrato.oid=${contrato.oid?c}&navigationId=contrato-visualizar&flowControl=regis&navegacionIdBack=${navigationId}">
							<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver"  border="0">
						</@security.a>
						</div>
						<div class="alineacion">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0524" 
							enabled="contrato.updatable"
							cssClass="item"  
							href="${request.contextPath}/contrato/updateContratoView.action?contrato.oid=${contrato.oid?c}&navigationId=contrato-update&flowControl=regis">
							<img  src="${request.contextPath}/common/images/modificar.gif" alt="Modificar" title="Modificar"  border="0">
						</@security.a>
						</div>
						<div class="alineacion">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0525" 
							enabled="contrato.eraseable"
							cssClass="item"  
							href="${request.contextPath}/contrato/deleteContratoView.action?contrato.oid=${contrato.oid?c}&navigationId=contrato-delete&flowControl=regis">
							<img  src="${request.contextPath}/common/images/eliminar.gif" alt="Eliminar" title="Eliminar"  border="0">
						</@security.a>				
						</div>
						<div class="alineacion">
							<#if contrato.estado.ordinal() == 1> <#-- Hacer Vigente -->
								<@security.a 
									templateDir="custontemplates" 
									securityCode="CUF0526" 
									enabled="contrato.enable"
									cssClass="item"  
									href="${request.contextPath}/contrato/hacerVigenteContratoView.action?contrato.oid=${contrato.oid?c}&navigationId=contrato-hacerVigente&navegacionIdBack=${navigationId}&flowControl=regis">
									<img src="${request.contextPath}/common/images/activar.gif" alt="Hacer Vigente" title="Hacer Vigente" border="0">
								</@security.a>				
							<#elseif contrato.estado.ordinal() == 2> <#-- Cancelar -->
								<@security.a 
									templateDir="custontemplates" 
									securityCode="CUF0526" 
									enabled="contrato.enable"
									cssClass="item"  
									href="${request.contextPath}/contrato/cancelarContratoView.action?contrato.oid=${contrato.oid?c}&navigationId=contrato-cancelar&navegacionIdBack=${navigationId}&flowControl=regis">
									<img src="${request.contextPath}/common/images/anular.gif" alt="Cancelar" title="Cancelar" border="0">
								</@security.a>				
							<#else>
								<@security.a 
									templateDir="custontemplates" 
									securityCode="CUF0526" 
									enabled="contrato.enable"
									cssClass="item"  
									href="">
									<img src="${request.contextPath}/common/images/activar.gif" alt="Hacer Vigente" title="Hacer Vigente" border="0">
								</@security.a>				
							</#if>						
						</div>
						
						<div class="alineacion">		
							<#if contrato.estado.ordinal()==2 && contrato.comprobanteReqPag.ordinal()==1>
								<@security.a 
									templateDir="custontemplates" 
									securityCode="CUF0527" 
									enabled="true"
									cssClass="item"  
									href="${request.contextPath}/contrato/authorizationContratoView.action?contrato.oid=${contrato.oid?c}&navegacionIdBack=${navigationId}">
									<img  src="${request.contextPath}/common/images/autorizar.png" alt="Autorizar Pago de Contrato" title="Autorizar Pago de Contrato"  border="0">
								</@security.a>
							<#else>
								<@security.a 
									templateDir="custontemplates" 
									securityCode="CUF0527" 
									enabled="false"
									cssClass="item"  
									href="${request.contextPath}/contrato/authorizationContratoView.action?contrato.oid=${contrato.oid?c}&navegacionIdBack=${navigationId}">
									<img  src="${request.contextPath}/common/images/autorizar.png" alt="Autorizar Pago de Contrato" title="Autorizar Pago de Contrato"  border="0">
								</@security.a>
							</#if>
						</div>
					</@display.column>

					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="oid" title="N&uacute;mero" />

					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="descripcion" title="Descripci&oacute;n" />				

					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="tipoDocumento" title="Tipo Documento" />

					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="tipoContrato" title="Tipo Contrato" />
<#--
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="cliente.descripcion" title="Cliente" />
-->
					<#-- muestra RazonSocial + Nombre de la Persona, independiente del TipoBeneficiario, debido a que un Proveedor es una Persona -->
<#--
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="nombreBeneficiario" title="Beneficiario" />
-->
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="estado" title="Estado" />		

				</@display.table>
			</@ajax.anchors>
			</div>
			</@s.if>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/contrato/search.action"
  source="buscar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="resetFlag=true,navigationId={navigationId},flowControl=regis,contrato.oid={contrato.oid},contrato.descripcion={contrato.descripcion},contrato.tipoDocumento={contrato.tipoDocumento},contrato.tipoContrato={contrato.tipoContrato},contrato.estado={contrato.estado}, contrato.comprobanteReqPag={contrato.comprobanteReqPag}"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/contrato/view.action"
  source="limpiar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=regis"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/contrato/createContratoView.action" 
  source="crear" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=abm-contrato-create,flowControl=regis"/>

<#--
<@vc.htmlContent 
  baseUrl="${request.contextPath}/contrato/selectClienteSearch.action" 
  source="seleccionarCliente" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=change,navegacionIdBack={navegacionIdBack},contrato.oid={contrato.oid}, contrato.descripcion={contrato.descripcion}, contrato.tipoDocumento={contrato.tipoDocumento}, contrato.tipoContrato={contrato.tipoContrato}, contrato.tipoAlquiler={contrato.tipoAlquiler}, contrato.tipoPublicidad={contrato.tipoPublicidad},contrato.tipoDonacion={contrato.tipoDonacion}, contrato.cartel.oid={contrato.cartel.oid},contrato.cartel.descripcion={contrato.cartel.descripcion}, contrato.inmueble={contrato.inmueble}, contrato.tipoBeneficiario={contrato.tipoBeneficiario}, contrato.personaSecuencia.pk.identificador={contrato.personaSecuencia.pk.identificador}, contrato.personaSecuencia.pk.secuencia={contrato.personaSecuencia.pk.secuencia}, contrato.proveedor.nro_proveed={contrato.proveedor.nro_proveed},contrato.cliente.oid={contrato.cliente.oid}, contrato.cliente.descripcion={contrato.cliente.descripcion}, contrato.rubro.oid={contrato.rubro.oid}, contrato.rubro.descripcion={contrato.rubro.descripcion}, contrato.estado={contrato.estado}"/>  
-->