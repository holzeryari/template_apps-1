
<#setting locale="es_AR">

<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>


<@s.hidden id="navigationId" name="navigationId"/>
<div id="capaSeparadora" class="capaSeparadora">&nbsp;</div>
<@vc.anchors target="contentTrx">	
	<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
		<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
			<tr>
				<td>Facturas</td>
			</tr>
		</table>	
	
		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="contrato.facturaList" id="facturaContrato" pagesize=15 defaultsort=2 >
        	<@display.column headerClass="tbl-contract-service-select" class="botoneraAnchoCon1" title="Acciones">
						<div class="alineacion">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0643" 
							enabled="factura.readable" 
							cssClass="item" 
							id="ver"
							name="ver"
							href="${request.contextPath}/comprobante/factura/readView.action?factura.pkFactura.nro_factura=${facturaContrato.pkFactura.nro_factura?c}&factura.pkFactura.nro_proveedor=${facturaContrato.pkFactura.nro_proveedor?c}&factura.pkFactura.sucursal=${facturaContrato.pkFactura.sucursal?c}&factura.pkFactura.tip_docum=${facturaContrato.pkFactura.tip_docum?c}&navigationId=factura-visualizar&flowControl=regis&navegacionIdBack=${navigationId}">
							<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver"  border="0">
						</@security.a>
						</div>

					</@display.column>


					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" title="Nro." class="botoneraAnchoCon4" property="numeroString"/>
										
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="fechaEmision" format="{0,date,dd/MM/yyyy}"  title="Fecha Emisi&oacute;n" />
									
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" title="Proveedor">  
						${facturaContrato.proveedor.detalleDePersona.razonSocial}
						<#if facturaContrato.proveedor.detalleDePersona.nombre?exists>
						 ${facturaContrato.proveedor.detalleDePersona.nombre}
						 </#if>
					</@display.column>
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="totalFactura" title="Total" />
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto"  title="Orden de Pago" />

					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="estado" title="Estado" />		

				</@display.table>
			</div>	
		</@vc.anchors>
	