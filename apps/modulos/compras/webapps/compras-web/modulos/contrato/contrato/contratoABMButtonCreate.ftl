<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancel" type="button" name="btnCancelar" value="Cancelar" class="boton"/>
				<input id="store" type="button" name="btnAgregar" value="Agregar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/contrato/createContrato.action" 
  source="store" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="contrato.oid={contrato.oid},contrato.versionNumber={contrato.versionNumber},contrato.estado={contrato.estado},contrato.descripcion={descripcion},contrato.tipoDocumento={tipoDocumento},contrato.tipoContrato={tipoContrato},contrato.tipoAlquiler={tipoAlquiler},contrato.inmueble.oid={contrato.inmueble.oid},contrato.cartel.oid={contrato.cartel.oid},contrato.tipoPublicidad={tipoPublicidad},contrato.tipoDonacion={tipoDonacion},contrato.cliente.oid={contrato.cliente.oid},contrato.contactoRU.pk.identificador={contrato.contactoRU.pk.identificador},contrato.contactoRU.pk.secuencia={contrato.contactoRU.pk.secuencia},contrato.tipoBeneficiario={tipoBeneficiario},contrato.proveedor.nroProveedor={contrato.proveedor.nroProveedor},contrato.personaSecuencia.pk.identificador={contrato.personaSecuencia.pk.identificador},contrato.personaSecuencia.pk.secuencia={contrato.personaSecuencia.pk.secuencia},contrato.categoriaBeneficiario={categoriaBeneficiario},contrato.fechaVigenciaDesde={fechaDesde},contrato.fechaVigenciaHasta={fechaHasta},contrato.frecuenciaPago={frecuenciaPago},contrato.diaPago={diaPago},contrato.rubro.oid={contrato.rubro.oid},contrato.cuentaContable.codigo={cuentaContable},contrato.comprobanteReqPag={comprobanteReqPag},contrato.medioPago={medioPago},contrato.importePeriodo={importePeriodo},contrato.formaPago={formaPago}"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action"  
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=buscar-contrato,flowControl=back"/>
