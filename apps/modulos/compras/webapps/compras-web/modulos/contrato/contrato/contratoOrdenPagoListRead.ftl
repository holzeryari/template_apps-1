
<#setting locale="es_AR">

<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>


<@s.hidden id="navigationId" name="navigationId"/>
<@vc.anchors target="contentTrx">	
	<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
		<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
			<tr>
				<td><b>Ordenes de Pago</b></td>
			</tr>
		</table>
        
        <@display.table class="tablaDetalleCuerpo" cellpadding="3" name="contrato.ordenPagoList" id="ordenPago" pagesize=15 defaultsort=2 >
          		
  		
					<@display.column headerClass="tbl-contract-service-select" class="botoneraAnchoCon1" title="Acciones">
						<div class="alineacion">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0443" 
							enabled="ordenPago.readable" 
							cssClass="item" 
							id="ver"
							name="ver"
							href="${request.contextPath}/pago/readView.action?ordenPago.oid=${ordenPago.oid?c}&navigationId=ordenPago-visualizar&flowControl=regis&navegacionIdBack=${navigationId}">
							<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver"  border="0">
						</@security.a>
						</div>
					</@display.column>


					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="numero" title="Nro." />				
										
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" title="Proveedor">
					<#if ordenPago.proveedor?exists>  
						${ordenPago.proveedor.detalleDePersona.razonSocial}
						<#if ordenPago.proveedor.detalleDePersona.nombre?exists> 
							 ${ordenPago.proveedor.detalleDePersona.nombre}
						</#if>
					</#if>
						 
					</@display.column>
					
															
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" title="Beneficiario">  
						<#if ordenPago.beneficiario?exists>
							${ordenPago.beneficiario.razonSocial}
							<#if ordenPago.beneficiario.nombre?exists> 
							 	${ordenPago.beneficiario.nombre}
							</#if>
						</#if>
						 
					</@display.column>
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="fechaEmision" format="{0,date,dd/MM/yyyy}"  title="Fecha Emisi&oacute;n" /> 

					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="estado" title="Estado" />		

				</@display.table>
			</div>	
		</@vc.anchors>
	    