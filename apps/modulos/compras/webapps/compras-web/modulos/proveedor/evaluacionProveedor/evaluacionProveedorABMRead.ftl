<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<@s.hidden id="evaluacionProveedor.oid" name="evaluacionProveedor.oid"/>
<@s.hidden id="evaluacionProveedor.versionNumber" name="evaluacionProveedor.versionNumber"/>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
			
			<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Datos de la Evaluaci&oacute;n del Proveedor</b></td>
			</tr>
		</table>
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
			<tr>
  				<td class="textoCampo">N&uacute;mero:</td>
  				<td class="textoDato">
  					<@s.property default="&nbsp;" escape=false value="evaluacionProveedor.numero"/>
  				</td>
  				<td  class="textoCampo">Fecha Evaluaci&oacute;n: </td>
  				<td class="textoDato">
  					<#assign fechaEvaluacion = evaluacionProveedor.fechaEvaluacion> 
					${fechaEvaluacion?string("dd/MM/yyyy")} 	
  				</td>
			</tr>	
			<tr>
				<td class="textoCampo">Es Carga Inicial:</td>
  				<td class="textoDato">
  					<@s.property default="&nbsp;" escape=false value="evaluacionProveedor.esCargaInicial"/>
  				</td>
				<td class="textoCampo">Tipo Evaluaci&oacute;n:</td>
  				<td class="textoDato">
  					<@s.property default="&nbsp;" escape=false value="evaluacionProveedor.tipoEvaluacion"/>
  				</td>
  			</tr>
  			<tr>	
				<td class="textoCampo">Proveedor:</td>
  				<td class="textoDato">
  				<@s.property default="&nbsp;" escape=false value="evaluacionProveedor.proveedor.detalleDePersona.razonSocial" /><@s.property default="&nbsp;" escape=false value="evaluacionProveedor.proveedor.detalleDePersona.nombre" />							
				<@s.hidden id="nroProveedor" name="fechaEvaluacion.proveedor.nroProveedor"/>
				<@s.hidden id="razonSocial" name="fechaEvaluacion.proveedor.detalleDePersona.razonSocial"/>
				<@s.hidden id="nombre" name="fechaEvaluacion.proveedor.detalleDePersona.nombre"/>
				<@s.a templateDir="custontemplates" id="seleccionarProveedor" name="seleccionarProveedor" href="javascript://nop/"  cssClass="ocultarIcono">
					<img src="${request.contextPath}/common/images/buscar.gif"  border="0" align="absmiddle" hspace="3" >
				</@s.a>		
				</td>
				<td class="textoCampo">Indice Evaluador:</td>
  				<td class="textoDato">
  				<@s.property default="&nbsp;" escape=false value="evaluacionProveedor.indiceEvaluador"/>%
				</td>
			</tr>
			
			<#if evaluacionProveedor.esCargaInicial.ordinal() == 1>
				<tr>
					<td  class="textoCampo">Fecha Inicio Per&iacute;odo: </td>
					<td class="textoDato">
						<@s.property default="&nbsp;" escape=false value="evaluacionProveedor.periodoEvaluacion.fechaInicioStr"/>
					</td>	
  				
					<td  class="textoCampo">Fecha Fin Per&iacute;odo: </td>
					<td class="textoDato">
  					<@s.property default="&nbsp;" escape=false value="evaluacionProveedor.periodoEvaluacion.fechaFinStr"/>
  					</td>
  				</tr>
  			</#if>
																      		      	
			<tr>
				<td class="textoCampo">Situaci&oacute;n Proveedor Anterior a Evaluaci&oacute;n:</td>
  				<td class="textoDato">
  				<@s.property default="&nbsp;" escape=false value="evaluacionProveedor.situacionProveedorAnterior"/>
				</td>
				<td class="textoCampo">Situaci&oacute;n Proveedor Posterior a Evaluaci&oacute;n:</td>
  				<td class="textoDato">
  				<@s.property default="&nbsp;" escape=false value="evaluacionProveedor.situacionProveedorPosterior"/>
				</td>
			</tr>
				
			<tr>
			<td colspan="4" class="textoCampo">&nbsp;</td>
			</tr>
		</table>
	</div>
		
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Par&aacute;metros de la Evaluaci&oacute;n</b></td>
			</tr>
		</table>
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
			<#if evaluacionProveedor.tipoEvaluacion.ordinal()==1>
				<#if evaluacionProveedor.esCargaInicial.ordinal() == 1>
				<tr>
					<td class="textoCampo">Flexibilidad y Atenci&oacute;n(FM):</td>
  					<td class="textoDato">
  					<@s.property default="&nbsp;" escape=false value="evaluacionProveedor.FA"/>%
					</td>
					<td class="textoCampo">Cumplimiento de Entrega en T&eacute;rmino(ET):</td>
  					<td class="textoDato">
  					<@s.property default="&nbsp;" escape=false value="evaluacionProveedor.ET"/>%
					</td>
				</tr>
				<tr>
					<td class="textoCampo">Cumplimiento de Cantidades Recibidas(CR):</td>
  					<td class="textoDato">
  					<@s.property default="&nbsp;" escape=false value="evaluacionProveedor.CR"/>%
					</td>
					<td class="textoCampo">Cumplimiento de Calidad del Producto(CP):</td>
  					<td class="textoDato">
  					<@s.property default="&nbsp;" escape=false value="evaluacionProveedor.CP"/>%
					</td>
				</tr>
				<tr>
					<td class="textoCampo">Indice de Calidad de Productos(ICP):</td>
  					<td class="textoDato" colspan="3">
  					<@s.property default="&nbsp;" escape=false value="evaluacionProveedor.ICP"/>%
					</td>
				</tr>
				<#else>
					<tr>
						<td class="textoCampo">Indice de Calidad de Productos(ICP):</td>
      					<td class="textoDato" colspan="3">
      					<@s.property default="&nbsp;" escape=false value="evaluacionProveedor.ICP"/>%
						</td>
					</tr>
				</#if>	
			</#if>	
			<#if evaluacionProveedor.tipoEvaluacion.ordinal()==2>
				<#if evaluacionProveedor.esCargaInicial.ordinal() == 1>
					<tr>
						<td class="textoCampo">Desempe&ntilde;o de Proveedor(DP):</td>
	      					<td class="textoDato">
	      					<@s.property default="&nbsp;" escape=false value="evaluacionProveedor.DP"/>%
							</td>
							<td class="textoCampo">Indice de Calidad Durante Servicio(ICDS):</td>
	      					<td class="textoDato">
	      					<@s.property default="&nbsp;" escape=false value="evaluacionProveedor.ICDS"/>%
							</td>
						</tr>
						<tr>
							<td class="textoCampo">Indice de Calidad Post Servicio(ICPS):</td>
	      					<td class="textoDato">
	      					<@s.property default="&nbsp;" escape=false value="evaluacionProveedor.ICPS"/>%
							</td>
							<td class="textoCampo">Indice de Calidad de Servicio(ICS):</td>
	      					<td class="textoDato">
	      					<@s.property default="&nbsp;" escape=false value="evaluacionProveedor.ICS"/>%
							</td>
						</tr>
						<#else>
							<tr>
								<td class="textoCampo">Indice de Calidad de Servicio(ICS):</td>
		      					<td class="textoDato" colspan="3">
		      					<@s.property default="&nbsp;" escape=false value="evaluacionProveedor.ICS"/>%
								</td>
							</tr>
						</#if>
					</#if>	
					<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>
	    		</table>		
	    	</div>
	    	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
				<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
					<tr>
						<td><b>Advertencia para el Proveedor evaluado</b></td>
					</tr>
				</table>
				<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
					<tr>
		        		<td class="textoCampo" colspan="4">&nbsp;</td>
		        	</tr>	
					<tr>
						<td class="textoCampo">Descripci&oacute;n:</td>
	    				<td class="textoDato">
	    					<@s.property default="&nbsp;" escape=false value="evaluacionProveedor.advertencia.descripcion"/>
	    				</td>
	    				<td  class="textoCampo">Estado: </td>
	    				<td class="textoDato">
	      				<@s.property default="&nbsp;" escape=false value="evaluacionProveedor.advertencia.estado"/>
	    				</td>
					</tr>
					<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>
				</table>
			</div>	
			<#if evaluacionProveedor.esCargaInicial.ordinal() == 1>
				<@tiles.insertAttribute name="oc_osEvaluacion"/>
			</#if>	
		
<@vc.htmlContent 
  baseUrl="${request.contextPath}/proveedor/evaluacionProveedor/seleccionarProveedorView.action" 
  source="seleccionarProveedor" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId=${navigationId},flowControl=change,evaluacionProveedor.fechaEvaluacion={evaluacionProveedor.fechaEvaluacion}, evaluacionProveedor.tipoEvaluacion={evaluacionProveedor.tipoEvaluacion}"/>



