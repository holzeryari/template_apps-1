<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>

<@s.hidden id="navigationId" name="navigationId"/>
<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
        	<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Datos de la Evaluaci&oacute;n del Proveedor</b></td>
				<td>
					<div align="right">
						<@security.a 
									templateDir="custontemplates" 
									securityCode="CUF0421" 
									enabled="true" 
									cssClass="item" 
									id="crear"										
									href="javascript://nop/">
									<b>Agregar</b>										
									<img src="${request.contextPath}/common/images/agregar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" title="Evaluaci&oacute;n en Per&iacute;odo Activo">
							</@security.a>	
					</div>
				</td>
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
	    		<td class="textoCampo" colspan="4">&nbsp;</td>
	    	</tr>	
			<tr>
	  			<td class="textoCampo">N&uacute;mero:</td>
      			<td class="textoDato">
      			<@s.textfield 
      				templateDir="custontemplates" 
					id="evaluacionProveedor.numero" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="evaluacionProveedor.numero" 
					title="Numero" />
				</td>							
      		
      			<td class="textoCampo">Tipo:</td>
      			<td class="textoDato">
      			<@s.select 
						templateDir="custontemplates" 
						id="evaluacionProveedor.tipoEvaluacion" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="evaluacionProveedor.tipoEvaluacion" 
						list="tipoEvaluacionList" 
						listKey="key" 
						listValue="description" 
						value="evaluacionProveedor.tipoEvaluacion.ordinal()"
						title="Tipo"
						headerKey="0"
						headerValue="Todos"/>
				</td>
			</tr>
			<tr>
      			<td class="textoCampo">Proveedor:</td>
      			<td class="textoDato">
      				<@s.property default="&nbsp;" escape=false value="evaluacionProveedor.proveedor.detalleDePersona.razonSocial" /><@s.property default="&nbsp;" escape=false value="evaluacionProveedor.proveedor.detalleDePersona.nombre" />							
					<@s.hidden id="nroProveedor" name="evaluacionProveedor.proveedor.nroProveedor"/>
					<@s.hidden id="razonSocial" name="evaluacionProveedor.proveedor.detalleDePersona.razonSocial"/>
					<@s.hidden id="nombre" name="evaluacionProveedor.proveedor.detalleDePersona.nombre"/>
					<@s.a templateDir="custontemplates" id="seleccionarProveedor" name="seleccionarProveedor" href="javascript://nop/" title="Buscar Proveedor">
						<img src="${request.contextPath}/common/images/buscar.gif"  border="0" align="absmiddle" hspace="3" >
					</@s.a>		
				</td>	
				<td class="textoCampo">Es Carga Inicial:</td>
      			<td class="textoDato">
      			<@s.select 
						templateDir="custontemplates" 
						id="evaluacionProveedor.esCargaInicial" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="evaluacionProveedor.esCargaInicial" 
						list="esCargaInicialList" 
						listKey="key" 
						listValue="description" 
						value="evaluacionProveedor.esCargaInicial.ordinal()"
						title="Carga Inicial"
						headerKey="0"
						headerValue="Todos"/>
				</td>
			</tr>
      		<tr>
				<td class="textoCampo">Fecha Per&iacute;odo Desde:</td>
      			<td class="textoDato">
      			<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="fechaDesde" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="fechaDesde" 
					title="Fecha Desde" />
				</td>							
      		
				<td class="textoCampo">Fecha Per&iacute;odo Hasta:</td>
      			<td class="textoDato">
      			<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="fechaHasta" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="fechaHasta" 
					title="Fecha Hasta" />
			</tr>
			<tr>
				<td class="textoCampo">Proveedor Servicio/Producto/Bien Cr&iacute;tico:</td>
				<td class="textoDato" colspan="3">
					<@s.select 
						templateDir="custontemplates" 
						id="estado" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="proveedorCritico" 
						list="proveedorCriticoList" 
						listKey="key" 
						listValue="description" 
						value="proveedorCritico.ordinal()"
						title="Proveedor Cr&iacute;tico"
						headerKey="0"
						headerValue="Todos"/>
				</td>
			</tr>
				
			<tr>
    			<td colspan="4" class="lineaGris" align="right">
      			<input id="limpiar" type="button" name="btnVolver" value="Limpiar" class="boton"/>		
      			<input id="buscar" type="button" name="btnVolver" value="Buscar" class="boton"/>
    			</td>
			</tr>	
			</table>
		</div>	
			
		
        <!-- Resultado Filtro -->		
			
			
		<@s.if test="evaluacionProveedorList!=null">
		
			<#assign numeroProveedor=0>
			<#if evaluacionProveedor?exists && evaluacionProveedor.proveedor?exists && evaluacionProveedor.proveedor.nroProveedor?exists>
				<#assign numeroProveedor =evaluacionProveedor.proveedor.nroProveedor>
			</#if>			
		
			<#assign evaluacionProveedorNumero = "0" /> 
			<@s.if test="evaluacionProveedor.numero != null">
				<#assign evaluacionProveedorNumero = "${evaluacionProveedor.numero}" />
			</@s.if>

			<#assign evaluacionProveedorEstado = "0" />
			<@s.if test="evaluacionProveedor.estado != null">
					<#assign evaluacionProveedorEstado = "${evaluacionProveedor.estado.ordinal()}" />
			</@s.if>
			
			<#assign evaluacionProveedorCritico = "0" />
			<@s.if test="proveedorCritico != null">
					<#assign evaluacionProveedorCritico = "${proveedorCritico.ordinal()}" />
			</@s.if>
			
			<#assign evaluacionProveedorTipo = "0" />
			<@s.if test="evaluacionProveedor.tipoEvaluacion != null">
					<#assign evaluacionProveedorTipo = "${evaluacionProveedor.tipoEvaluacion.ordinal()}" />
			</@s.if>

			<#assign evaluacionProveedorFechaDesde = "" />
			<@s.if test="fechaDesde != null">
				<#assign evaluacionProveedorFechaDesde = "${fechaDesde?string('dd/MM/yyyy')}" />
			</@s.if>

			<#assign evaluacionProveedorFechaHasta = "" />
			<@s.if test="fechaHasta != null">
				<#assign evaluacionProveedorFechaHasta = "${fechaHasta?string('dd/MM/yyyy')}" />
			</@s.if>
			
			<#assign evaluacionProveedorRazonSocialProveedor = "" />
			<@s.if test="evaluacionProveedor.proveedor != null && evaluacionProveedor.proveedor.detalleDePersona != null && evaluacionProveedor.proveedor.detalleDePersona.razonSocial != null">
				<#assign evaluacionProveedorRazonSocialProveedor = "${evaluacionProveedor.proveedor.detalleDePersona.razonSocial}" />
			</@s.if>
			
			<#assign evaluacionProveedorNombreProveedor = "" />
			<@s.if test="evaluacionProveedor.proveedor != null && evaluacionProveedor.proveedor.detalleDePersona != null && evaluacionProveedor.proveedor.detalleDePersona.nombre != null">
				<#assign evaluacionProveedorNombreProveedor = "${evaluacionProveedor.proveedor.detalleDePersona.nombre}" />
			</@s.if>
				
			<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
				<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
	
				
				<tr>
					<td>Evaluaciones de Proveedores encontradas</td>
					<td>
						<div class="alineacionDerechaBig">
								<@security.a 
									templateDir="custontemplates" 
									securityCode="CUF0422" 
									cssClass="item" 
									href="${request.contextPath}/proveedor/evaluacionProveedor/printOCOSNoConformidadesXLS.action?evaluacionProveedor.numero=${evaluacionProveedorNumero}&evaluacionProveedor.tipoEvaluacion=${evaluacionProveedorTipo}&evaluacionProveedor.proveedor.nroProveedor=${numeroProveedor?c}&evaluacionProveedor.proveedor.detalleDePersona.razonSocial=${evaluacionProveedorRazonSocialProveedor}&evaluacionProveedor.proveedor.detalleDePersona.nombre=${evaluacionProveedorNombreProveedor}&fechaDesde=${evaluacionProveedorFechaDesde}&fechaHasta=${evaluacionProveedorFechaHasta}&proveedorCritico=${evaluacionProveedorCritico}">
									<img src="${request.contextPath}/common/images/imprimirExcel.gif" title="Exportar a Excel" align="absmiddle" border="0" hspace="1">
								</@security.a>
						</div>
						<div class="alineacionDerechaBig">
						<b>No Conformidades</b>
							<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0422" 
								cssClass="item" 
								href="${request.contextPath}/proveedor/evaluacionProveedor/printOCOSNoConformidadesPDF.action?evaluacionProveedor.numero=${evaluacionProveedorNumero}&evaluacionProveedor.tipoEvaluacion=${evaluacionProveedorTipo}&evaluacionProveedor.proveedor.nroProveedor=${numeroProveedor?c}&evaluacionProveedor.proveedor.detalleDePersona.razonSocial=${evaluacionProveedorRazonSocialProveedor}&evaluacionProveedor.proveedor.detalleDePersona.nombre=${evaluacionProveedorNombreProveedor}&fechaDesde=${evaluacionProveedorFechaDesde}&fechaHasta=${evaluacionProveedorFechaHasta}&proveedorCritico=${evaluacionProveedorCritico}">
								<img src="${request.contextPath}/common/images/imprimirPdf.gif" title="Exportar a PDF" align="absmiddle" border="0" hspace="1" >
							</@security.a>
						</div>
						<div class="alineacionDerechaBig">
								<@security.a 
									templateDir="custontemplates" 
									securityCode="CUF0422" 
									cssClass="item" 
									href="${request.contextPath}/proveedor/evaluacionProveedor/printXLS.action?evaluacionProveedor.numero=${evaluacionProveedorNumero}&evaluacionProveedor.tipoEvaluacion=${evaluacionProveedorTipo}&evaluacionProveedor.proveedor.nroProveedor=${numeroProveedor?c}&evaluacionProveedor.proveedor.detalleDePersona.razonSocial=${evaluacionProveedorRazonSocialProveedor}&evaluacionProveedor.proveedor.detalleDePersona.nombre=${evaluacionProveedorNombreProveedor}&fechaDesde=${evaluacionProveedorFechaDesde}&fechaHasta=${evaluacionProveedorFechaHasta}&proveedorCritico=${evaluacionProveedorCritico}">
									<img src="${request.contextPath}/common/images/imprimirExcel.gif" title="Exportar a Excel" align="absmiddle" border="0" hspace="1">
								</@security.a>
								
						</div>
						<div class="alineacionDerechaBig">
							<b>Evaluaciones</b>
							<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0422" 
								cssClass="item" 
								href="${request.contextPath}/proveedor/evaluacionProveedor/printPDF.action?evaluacionProveedor.numero=${evaluacionProveedorNumero}&evaluacionProveedor.tipoEvaluacion=${evaluacionProveedorTipo}&evaluacionProveedor.proveedor.nroProveedor=${numeroProveedor?c}&evaluacionProveedor.proveedor.detalleDePersona.razonSocial=${evaluacionProveedorRazonSocialProveedor}&evaluacionProveedor.proveedor.detalleDePersona.nombre=${evaluacionProveedorNombreProveedor}&fechaDesde=${evaluacionProveedorFechaDesde}&fechaHasta=${evaluacionProveedorFechaHasta}&proveedorCritico=${evaluacionProveedorCritico}">
								<img src="${request.contextPath}/common/images/imprimirPdf.gif" title="Exportar a PDF" align="absmiddle" border="0" hspace="1" >
							</@security.a>
						</div>	
					</td>
				</tr>
			</table>	
			
			

		<@vc.anchors target="contentTrx">	
  		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="evaluacionProveedorList" id="evaluacionProveedor" pagesize=15 defaultsort=2 partialList=true size="recordSize" keepStatus=true excludedParams="resetFlag">
  		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" class="botoneraAnchoCon2" title="Acciones">
				<div class="alineacion">
				<@security.a 
					templateDir="custontemplates" 
					securityCode="CUF0423" 
					enabled="evaluacionProveedor.readable" 
					cssClass="item" 
					href="${request.contextPath}/proveedor/evaluacionProveedor/readEvaluacionProveedorView.action?evaluacionProveedor.oid=${evaluacionProveedor.oid?c}&navigationId=evaluacionProveedor-visualizar&flowControl=regis&navegacionIdBack=${navigationId}">
					<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver"  border="0">
				</@security.a>
				</div>
				
				<div class="alineacion">
				<@security.a  
						templateDir="custontemplates" 
						securityCode="CUF0425" 
						enabled="evaluacionProveedor.eraseable"
						cssClass="item"  
						href="${request.contextPath}/proveedor/evaluacionProveedor/deleteEvaluacionProveedorView.action?evaluacionProveedor.oid=${evaluacionProveedor.oid?c}&navigationId=evaluacionProveedor-eliminar&flowControl=regis">
						<img  src="${request.contextPath}/common/images/eliminar.gif" alt="Eliminar" title="Eliminar"  border="0">
					</@security.a>	
				</div>	
					
				 
			</@display.column>
			
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="numero" title="N&uacute;mero" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" title="Proveedor">  
				${evaluacionProveedor.proveedor.detalleDePersona.razonSocial} 
				
				<#if evaluacionProveedor.proveedor.detalleDePersona.nombre?exists> 
					 ${evaluacionProveedor.proveedor.detalleDePersona.nombre}
				</#if>
				
				
			</@display.column> 
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="tipoEvaluacion" title="Tipo" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="fechaEvaluacion" format="{0,date,dd/MM/yyyy}"  title="F. Eval."/>
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="esCargaInicial" title="Carga Inicial" /> 
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" title="Per&iacute;odo" property="periodoEvaluacion.fechasPeriodoEvaluacion"/>
			<#--<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="periodoEvaluacion.fechaFin" format="{0,date,dd/MM/yyyy}"  title="F. Fin Per&iacute;odo"/>-->
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="ICP" title="ICP[%]" /> 
			<#--<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="ET" title="ET" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="CR" title="CR" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="CP" title="CP" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="FA" title="FA" />-->
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="ICS" title="ICS[%]" /> 
			<#-- <@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="ICDS" title="ICDS" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="ICPS" title="ICPS" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="DP" title="DP" />-->
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="indiceEvaluador" title="Ind. Eval.[%]" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="situacionProveedorAnterior" title="Sit. Ant. Prov." />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="situacionProveedorPosterior" title="Sit. Post. Prov." />
										
		</@display.table>
	</@vc.anchors>
	</div>
</@s.if>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/proveedor/evaluacionProveedor/selectProveedorSearch.action" 
  source="seleccionarProveedor" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=change,evaluacionProveedor.oid={evaluacionProveedor.oid},evaluacionProveedor.tipoEvaluacion={evaluacionProveedor.tipoEvaluacion},evaluacionProveedor.numero={evaluacionProveedor.numero},evaluacionProveedor.proveedor.nroProveedor={evaluacionProveedor.proveedor.nroProveedor},fechaDesde={fechaDesde},fechaHasta={fechaHasta},proveedorCritico={proveedorCritico}"/>


<@vc.htmlContent 
  baseUrl="${request.contextPath}/proveedor/evaluacionProveedor/search.action"
  source="buscar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="resetFlag=true,navigationId={navigationId},flowControl=regis,evaluacionProveedor.numero={evaluacionProveedor.numero},evaluacionProveedor.tipoEvaluacion={evaluacionProveedor.tipoEvaluacion},fechaDesde={fechaDesde},fechaHasta={fechaHasta}, evaluacionProveedor.proveedor.nroProveedor={evaluacionProveedor.proveedor.nroProveedor}, evaluacionProveedor.proveedor.detalleDePersona.razonSocial={evaluacionProveedor.proveedor.detalleDePersona.razonSocial},evaluacionProveedor.proveedor.detalleDePersona.nombre={evaluacionProveedor.proveedor.detalleDePersona.nombre}, evaluacionProveedor.esCargaInicial={evaluacionProveedor.esCargaInicial},proveedorCritico={proveedorCritico}"/>
  		      

<@vc.htmlContent 
  baseUrl="${request.contextPath}/proveedor/evaluacionProveedor/view.action"
  source="limpiar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=regis"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/proveedor/evaluacionProveedor/createEvaluacionProveedorView.action" 
  source="crear" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=evaluacionProveedor-create,flowControl=regis"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/proveedor/evaluacionProveedor/createEvaluacionProveedorInicialView.action" 
  source="crearInicial" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=evaluacionProveedor-createInicial,flowControl=regis"/> 
  