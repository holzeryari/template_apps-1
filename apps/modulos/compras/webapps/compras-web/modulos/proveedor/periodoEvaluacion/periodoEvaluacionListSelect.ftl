<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>

<@s.hidden id="navigationId" name="navigationId"/>
<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
        	<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Datos del Per&iacute;odo de Evaluaci&oacute;n</b></td>
				</tr>
			</table>
			
			<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
	    		<td class="textoCampo" colspan="4">&nbsp;</td>
	    	</tr>	
			<tr>
	  			<td class="textoCampo">N&uacute;mero:</td>
      			<td class="textoDato">
      			<@s.textfield 
      				templateDir="custontemplates" 
					id="periodoEvaluacion.numero" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="periodoEvaluacion.numero" 
					title="Numero" />
				</td>							
	      		
      			<td class="textoCampo">Situaci&oacute;n:</td>
      			<td class="textoDato">
      			<@s.select 
						templateDir="custontemplates" 
						id="periodoEvaluacion.situacion" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="oc_os.tipo" 
						list="situacionList" 
						listKey="key" 
						listValue="description" 
						value="periodoEvaluacion.situacion.ordinal()"
						title="Tipo"
						headerKey="0"
						headerValue="Todos"/>
				</td>
			</tr>
      		<tr>
				<td class="textoCampo">Estado:</td>
      			<td class="textoDato" colspan="3" >
      			<@s.select 
						templateDir="custontemplates" 
						id="periodoEvaluacion.estado" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="periodoEvaluacion.estado" 
						list="estadoList" 
						listKey="key" 
						listValue="description" 
						value="periodoEvaluacion.estado.ordinal()"
						title="Estado"
						headerKey="0"
						headerValue="Todos"							
						/>
				</td>
			</tr>
				
			<tr>
				<td class="textoCampo">Fecha Per&iacute;odo Desde:</td>
      			<td class="textoDato">
      			<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="fechaDesde" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="fechaDesde" 
					title="Fecha Desde" />
				</td>							
      		
				<td class="textoCampo">Fecha Per&iacute;odo Hasta:</td>
      			<td class="texto" align="left" width="30%">
      			<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="fechaHasta" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="fechaHasta" 
					title="Fecha Hasta" />
			</tr>
		
			<tr>
	    			<td colspan="4" class="lineaGris" align="right">
	      			<input id="limpiar" type="button" name="btnVolver" value="Limpiar" class="boton"/>		
	      			<input id="buscar" type="button" name="btnVolver" value="Buscar" class="boton"/>
	    			</td>
				</tr>	
			</table>
		</div>	
			
		
        <!-- Resultado Filtro -->
		<@s.if test="periodoEvaluacionList!=null">
		<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
			<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Per&iacute;odos de Evaluaci&oacute;n encontrados</td>	
				</tr>
			</table>
				
			<@vc.anchors target="contentTrx">	
      		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="periodoEvaluacionList" id="periodoEvaluacion" pagesize=15 defaultsort=2>
      		<#assign ref="${request.contextPath}/proveedor/periodoEvaluacion/selectPeriodoEvaluacionBack.action?">
      		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" class="botoneraAnchoCon5" title="Acciones">
					<@s.a href="${ref}periodoEvaluacion.oid=${periodoEvaluacion.oid?c}&navigationId=${navigationId}&periodoEvaluacion.fechaInicio=${periodoEvaluacion.fechaInicio}&periodoEvaluacion.fechaFin=${periodoEvaluacion.fechaFin}&navegacionIdBack=${navegacionIdBack}"><img src="${request.contextPath}/common/images/seleccionar.gif" alt="Seleccionar" title="Seleccionar" width="16" height="16" border="0"></@s.a>
					&nbsp;
					
				</@display.column>
				
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="numero" title="N&uacute;mero" />
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="fechaInicio" format="{0,date,dd/MM/yyyy}"  title="Fecha Inicio Per&iacute;odo"/>
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="fechaFin" format="{0,date,dd/MM/yyyy}"  title="Fecha Fin Per&iacute;odo"/>
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="estado" title="Estado" />
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="situacion" title="Situaci&oacute;n" />							
			</@display.table>
		</@vc.anchors>
		</div>	
	</@s.if>
      

<@vc.htmlContent 
  baseUrl="${request.contextPath}/proveedor/periodoEvaluacion/selectPeriodoEvaluacionSearch.action" 
  source="buscar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId=${navigationId},flowControl=regis,navegacionIdBack=${navegacionIdBack},periodoEvaluacion.numero={periodoEvaluacion.numero},periodoEvaluacion.estado={periodoEvaluacion.estado},periodoEvaluacion.situacion={periodoEvaluacion.situacion},fechaDesde={fechaDesde},fechaHasta={fechaHasta}"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/proveedor/periodoEvaluacion/selectPeriodoEvaluacionView.action" 
  source="limpiar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=regis,navegacionIdBack=${navegacionIdBack}"/>
  
 <@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId=${navegacionIdBack},flowControl=back"/>
  
 
  