<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>



<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancel" type="button" name="btnCancelar" value="Cancelar" class="boton"/>
				<input id="store" type="button" name="btnAgregar" value="Agregar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>



<@vc.htmlContent 
  baseUrl="${request.contextPath}/proveedor/periodoEvaluacion/createPeriodoEvaluacion.action" 
  source="store" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="periodoEvaluacion.fechaInicio={periodoEvaluacion.fechaInicioF}, periodoEvaluacion.fechaFin={periodoEvaluacion.fechaFin}, periodoEvaluacion.estado={periodoEvaluacion.estado}, periodoEvaluacion.situacion={periodoEvaluacion.situacion}"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=buscar-periodoEvaluacion,flowControl=back"/>
  