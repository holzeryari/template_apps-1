<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>

<@vc.anchors target="contentTrx" ajaxFlag="ajax">	
	<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
				<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
					<tr>
						<td>Proveedores pendientes de evaluaci&oacute;n de tipo "Compra de Productos/Bienes" en el Per&iacute;odo</td>	
					</tr>
				</table>
		
		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="proveedorPendEvaluacionOCList" id="proveedor" pagesize=15 defaultsort=2>
        	<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="nroProveedor" title="N&uacute;mero" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="detalleDePersona.persona.tipoPersona" title="Tipo De persona" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="nombreFantasia" title="Nombre de Fantasia" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="detalleDePersona.razonSocial" title="Raz&oacute;n Social" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="detalleDePersona.nombre" title="Nombre" /> 
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="detalleDePersona.persona.docPersonal" title="Documento" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="detalleDePersona.persona.docFiscal" title="CUIT/CUIL" />
		</@display.table>
	</div>
	<div id="capaSeparadora" class="capaSeparadora">&nbsp;</div>
	
	<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
				<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
					<tr>
						<td>Proveedores pendientes de evaluaci&oacute;n de tipo "Contrataci&oacute;n de Servicios" en el Per&iacute;odo</td>	
					</tr>
				</table>
		
		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="proveedorPendEvaluacionOSList" id="proveedor" pagesize=15 defaultsort=2>
        	<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="nroProveedor" title="N&uacute;mero" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="detalleDePersona.persona.tipoPersona" title="Tipo De persona" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="nombreFantasia" title="Nombre de Fantasia" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="detalleDePersona.razonSocial" title="Raz&oacute;n Social" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="detalleDePersona.nombre" title="Nombre" /> 
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="detalleDePersona.persona.docPersonal" title="Documento" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="detalleDePersona.persona.docFiscal" title="CUIT/CUIL" />
		</@display.table>
	</div>
	<div id="capaSeparadora" class="capaSeparadora">&nbsp;</div>
	
	<#--<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
		<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
			<tr>
				<td>Proveedores ya evaluados en el Per&iacute;odo</td>	
			</tr>
		</table>
	
		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="proveedorEvaluadoList" id="proveedor" pagesize=15 defaultsort=2>
        	<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="nroProveedor" title="N&uacute;mero" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="detalleDePersona.persona.tipoPersona" title="Tipo De persona" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="nombreFantasia" title="Nombre de Fantasia" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="detalleDePersona.razonSocial" title="Raz&oacute;n Social" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="detalleDePersona.nombre" title="Nombre" /> 
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="detalleDePersona.persona.docPersonal" title="Documento" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="detalleDePersona.persona.docFiscal" title="CUIT/CUIL" />
		</@display.table>
	</div>-->
</@vc.anchors>	
  