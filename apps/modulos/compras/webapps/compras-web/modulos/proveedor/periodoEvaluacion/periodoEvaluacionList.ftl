<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>

<@s.hidden id="navigationId" name="navigationId"/>
<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
        	<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Datos del Per&iacute;odo de Evaluaci&oacute;n</b></td>
				<td>
					<div align="right">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0401" 
							enabled="true" 
							cssClass="item" 
							id="crear"										
							href="javascript://nop/">
							<b>Agregar</b>										
							<img src="${request.contextPath}/common/images/agregar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
						</@security.a>		
					</div>
				</td>
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
	    		<td class="textoCampo" colspan="4">&nbsp;</td>
	    	</tr>	
			<tr>
	  			<td class="textoCampo">N&uacute;mero:</td>
      			<td class="textoDato">
      			<@s.textfield 
      				templateDir="custontemplates" 
					id="periodoEvaluacion.numero" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="periodoEvaluacion.numero" 
					title="Numero" />
				</td>							
	      		
      			<td class="textoCampo">Situaci&oacute;n:</td>
      			<td class="textoDato">
      			<@s.select 
						templateDir="custontemplates" 
						id="periodoEvaluacion.situacion" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="oc_os.tipo" 
						list="situacionList" 
						listKey="key" 
						listValue="description" 
						value="periodoEvaluacion.situacion.ordinal()"
						title="Tipo"
						headerKey="0"
						headerValue="Todos"/>
				</td>
			</tr>
      		<tr>
				<td class="textoCampo">Estado:</td>
      			<td class="textoDato" colspan="3" >
      			<@s.select 
						templateDir="custontemplates" 
						id="periodoEvaluacion.estado" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="periodoEvaluacion.estado" 
						list="estadoList" 
						listKey="key" 
						listValue="description" 
						value="periodoEvaluacion.estado.ordinal()"
						title="Estado"
						headerKey="0"
						headerValue="Todos"							
						/>
				</td>
			</tr>
				
			<tr>
				<td class="textoCampo">Fecha Per&iacute;odo Desde:</td>
      			<td class="textoDato">
      			<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="fechaDesde" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="fechaDesde" 
					title="Fecha Desde" />
				</td>							
      		
				<td class="textoCampo">Fecha Per&iacute;odo Hasta:</td>
      			<td class="texto" align="left" width="30%">
      			<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="fechaHasta" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="fechaHasta" 
					title="Fecha Hasta" />
			</tr>
		
			<tr>
	    			<td colspan="4" class="lineaGris" align="right">
	      			<input id="limpiar" type="button" name="btnVolver" value="Limpiar" class="boton"/>		
	      			<input id="buscar" type="button" name="btnVolver" value="Buscar" class="boton"/>
	    			</td>
				</tr>	
			</table>
		</div>	
		
        <!-- Resultado Filtro -->
			
		<@s.if test="periodoEvaluacionList!=null">
				
			<#assign periodoEvaluacionNumero = "0" /> 
			<@s.if test="periodoEvaluacion.numero != null">
				<#assign periodoEvaluacionNumero = "${periodoEvaluacion.numero}" />
			</@s.if>

			<#assign periodoEvaluacionEstado = "0" />
			<@s.if test="periodoEvaluacion.estado != null">
					<#assign periodoEvaluacionEstado = "${periodoEvaluacion.estado.ordinal()}" />
			</@s.if>
			
			<#assign periodoEvaluacionSituacion = "0" />
			<@s.if test="periodoEvaluacion.situacion != null">
					<#assign periodoEvaluacionSituacion = "${periodoEvaluacion.situacion.ordinal()}" />
			</@s.if>

			<#assign periodoEvaluacionFechaDesde = "" />
			<@s.if test="fechaDesde != null">
				<#assign periodoEvaluacionFechaDesde = "${fechaDesde?string('dd/MM/yyyy')}" />
			</@s.if>

			<#assign periodoEvaluacionFechaHasta = "" />
			<@s.if test="fechaHasta != null">
				<#assign periodoEvaluacionFechaHasta = "${fechaHasta?string('dd/MM/yyyy')}" />
			</@s.if>
				
			<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
				<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Per&iacute;odos de Evaluaci&oacute;n encontrados</td>
					<td>
						<div class="alineacionDerecha">
						<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0402" 
								cssClass="item" 
								href="${request.contextPath}/proveedor/periodoEvaluacion/printXLS.action?periodoEvaluacion.numero=${periodoEvaluacionNumero}&fechaDesde=${periodoEvaluacionFechaDesde}&fechaHasta=${periodoEvaluacionFechaHasta}&periodoEvaluacion.situacion=${periodoEvaluacionSituacion}&periodoEvaluacion.estado=${periodoEvaluacionEstado}">
								<img src="${request.contextPath}/common/images/imprimirExcel.gif" title="Exportar a Excel" align="absmiddle" border="0" hspace="1">
							</@security.a>
								
						</div>
						<div class="alineacionDerecha">
							<b>Imprimir</b>
							<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0402" 
								cssClass="item" 
								href="${request.contextPath}/proveedor/periodoEvaluacion/printPDF.action?periodoEvaluacion.numero=${periodoEvaluacionNumero}&fechaDesde=${periodoEvaluacionFechaDesde}&fechaHasta=${periodoEvaluacionFechaHasta}&periodoEvaluacion.situacion=${periodoEvaluacionSituacion}&periodoEvaluacion.estado=${periodoEvaluacionEstado}">
								<img src="${request.contextPath}/common/images/imprimirPdf.gif" title="Exportar a PDF" align="absmiddle" border="0" hspace="1" >
							</@security.a>
						</div>	
					</td>
				</tr>
			</table>	
				
			
			<@vc.anchors target="contentTrx">	
      		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="periodoEvaluacionList" id="periodoEvaluacion" pagesize=15 defaultsort=2 partialList=true size="recordSize" keepStatus=true excludedParams="resetFlag">
      		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" class="botoneraAnchoCon5" title="Acciones">
					<div class="alineacion">
					<@security.a 
						templateDir="custontemplates" 
						securityCode="CUF0403" 
						enabled="periodoEvaluacion.readable" 
						cssClass="item" 
						href="${request.contextPath}/proveedor/periodoEvaluacion/readPeriodoEvaluacionView.action?periodoEvaluacion.oid=${periodoEvaluacion.oid?c}&navigationId=periodoEvaluacion-visualizar&flowControl=regis&navegacionIdBack=${navigationId}">
						<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver"  border="0">
					</@security.a>
					</div>
					
					<div class="alineacion">	
					<@security.a 
						templateDir="custontemplates" 
						securityCode="CUF0404" 
						enabled="periodoEvaluacion.updatable"
						cssClass="item"  
						href="${request.contextPath}/proveedor/periodoEvaluacion/updatePeriodoEvaluacionView.action?periodoEvaluacion.oid=${periodoEvaluacion.oid?c}&navigationId=periodoEvaluacion-modificar&flowControl=regis">
						<img  src="${request.contextPath}/common/images/modificar.gif" alt="Modificar" title="Modificar"  border="0">
					</@security.a>
					</div>
					
					<div class="alineacion">
					<@security.a  
							templateDir="custontemplates" 
							securityCode="CUF0405" 
							enabled="periodoEvaluacion.eraseable"
							cssClass="item"  
							href="${request.contextPath}/proveedor/periodoEvaluacion/deletePeriodoEvaluacionView.action?periodoEvaluacion.oid=${periodoEvaluacion.oid?c}&navigationId=periodoEvaluacion-eliminar&flowControl=regis">
							<img  src="${request.contextPath}/common/images/eliminar.gif" alt="Eliminar" title="Eliminar"  border="0">
						</@security.a>	
					</div>	
						
					 <div class="alineacion">
						<#if periodoEvaluacion.estado == "Activo">
							<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0406" 
								enabled="true"
								cssClass="item"  
								href="${request.contextPath}/proveedor/periodoEvaluacion/desactivarPeriodoEvaluacionView.action?periodoEvaluacion.oid=${periodoEvaluacion.oid?c}&navigationId=periodoEvaluacion-desactivar&flowControl=regis">
								<img src="${request.contextPath}/common/images/desactivar.gif" alt="Desactivar" title="Desactivar" border="0">
							</@security.a>			
						<#else>
							<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0406" 
								enabled="true"
								cssClass="item"  
								href="${request.contextPath}/proveedor/periodoEvaluacion/activarPeriodoEvaluacionView.action?periodoEvaluacion.oid=${periodoEvaluacion.oid?c}&navigationId=periodoEvaluacion-activar&flowControl=regis">
								<img src="${request.contextPath}/common/images/activar.gif" alt="Activar" title="Activar" border="0">
							</@security.a>								
						</#if>	
					</div>
					<div class="alineacion">
						<@security.a  
							templateDir="custontemplates" 
							securityCode="CUF0407" 
							enabled="periodoEvaluacion.enable"
							cssClass="item"  
							href="${request.contextPath}/proveedor/periodoEvaluacion/cerrarPeriodoEvaluacionView.action?periodoEvaluacion.oid=${periodoEvaluacion.oid?c}&navigationId=periodoEvaluacion-cerrar&flowControl=regis">
							<img  src="${request.contextPath}/common/images/cerrar.gif" alt="Cerrar" title="Cerrar"  border="0">
						</@security.a>	
					</div>
				</@display.column>
				
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="numero" title="N&uacute;mero" />
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="fechaInicio" format="{0,date,dd/MM/yyyy}"  title="Fecha Inicio Per&iacute;odo"/>
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="fechaFin" format="{0,date,dd/MM/yyyy}"  title="Fecha Fin Per&iacute;odo"/>
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="situacion" title="Situaci&oacute;n" />	
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="estado" title="Estado" />
										
			</@display.table>
		</@vc.anchors>
		</div>	
	</@s.if>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/proveedor/periodoEvaluacion/search.action"
  source="buscar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="resetFlag=true,navigationId={navigationId},flowControl=regis,periodoEvaluacion.numero={periodoEvaluacion.numero},periodoEvaluacion.estado={periodoEvaluacion.estado},periodoEvaluacion.situacion={periodoEvaluacion.situacion},fechaDesde={fechaDesde},fechaHasta={fechaHasta}"/>
  		      

<@vc.htmlContent 
  baseUrl="${request.contextPath}/proveedor/periodoEvaluacion/view.action"
  source="limpiar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=regis"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/proveedor/periodoEvaluacion/createPeriodoEvaluacionView.action" 
  source="crear" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=periodoEvaluacion-creacion,flowControl=regis"/>
  
 
  