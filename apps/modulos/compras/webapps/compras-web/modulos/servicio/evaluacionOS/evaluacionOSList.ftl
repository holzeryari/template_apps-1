
<#setting locale="es_AR">

<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>

<@s.hidden id="navigationId" name="navigationId"/>
<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
        	<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Datos de la OS</b></td>
				
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
	    		<td class="textoCampo" colspan="4">&nbsp;</td>
	    	</tr>	
			
			<tr>
	  			<td class="textoCampo">N&uacute;mero:</td>
      			<td class="textoDato">
      			<@s.textfield 
      				templateDir="custontemplates" 
					id="oc_os.numero" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="oc_os.numero" 
					title="Numero" />
				</td>							
      		
      			<td class="textoCampo">Tipo:</td>
      			<td class="textoDato">
      			<@s.property default="&nbsp;" escape=false value="oc_os.tipo" />
      			<@s.hidden id="oc_os.tipo" name="oc_os.tipo.ordinal()"/>
				</td>
			</tr>
      		<tr>
      			<td class="textoCampo">Proveedor:</td>
      			<td class="textoDato">
      				<@s.property default="&nbsp;" escape=false value="oc_os.proveedor.detalleDePersona.razonSocial" /><@s.property default="&nbsp;" escape=false value="oc_os.proveedor.detalleDePersona.nombre" />							
					<@s.hidden id="nroProveedor" name="oc_os.proveedor.nroProveedor"/>
					<@s.hidden id="razonSocial" name="oc_os.proveedor.detalleDePersona.razonSocial"/>
					<@s.hidden id="nombre" name="oc_os.proveedor.detalleDePersona.nombre"/>
					<@s.a templateDir="custontemplates" id="seleccionarProveedor" name="seleccionarProveedor" href="javascript://nop/" title="Buscar Proveedor">
						<img src="${request.contextPath}/common/images/buscar.gif"  border="0" align="absmiddle" hspace="3" >
					</@s.a>		
				</td>	
				
				<td class="textoCampo">Pendiente de Evaluaci&oacute;n:</td>
				<td class="textoDato">
				<@s.select 
						templateDir="custontemplates" 
						id="pendienteEvaluacionOS" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="pendienteEvaluacionOS" 
						list="pendienteEvaluacionOSList" 
						listKey="key" 
						listValue="description" 
						value="pendienteEvaluacionOS.ordinal()"
						title="Pendiente Evaluacion OS"
						headerKey="0"
						headerValue="Todos" 
						 />
				</td>
				<#--<td class="texto_datos"  align="right" width="20%">Estado:</td>
      			<td class="texto" align="left" colspan="3" width="30%">
      			<@s.select 
						templateDir="custontemplates" 
						id="oc_os.estado" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="oc_os.estado" 
						list="estadoList" 
						listKey="key" 
						listValue="description" 
						value="oc_os.estado.ordinal()"
						title="Estado"
						headerKey="0"
						headerValue="Todos"							
						/>
				</td>-->
				
			</tr>
				
			<tr>
				<td class="textoCampo">Importe Desde:</td>
      			<td class="textoDato">
      			<@s.textfield 
					templateDir="custontemplates" 
					id="importeDesde" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="importeDesde" 
					title="Importe Desde" />
				</td>	
				
				<td class="textoCampo">Importe Hasta:</td>
      			<td class="textoDato">
      			<@s.textfield 
					templateDir="custontemplates" 
					id="importeHasta" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="importeHasta" 
					title="Importe Hasta" />
			</tr>
				
			<tr>
				<td class="textoCampo">Fecha Desde:</td>
      			<td class="textoDato">
      			<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="fechaDesde" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="fechaDesde" 
					title="Fecha Desde" />
				</td>							
      		
				<td class="textoCampo">Fecha Hasta:</td>
      			<td class="texto" align="left" width="30%">
      			<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="fechaHasta" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="fechaHasta" 
					title="Fecha Hasta" />
			</tr>
			
			<tr>
    			<td colspan="4" class="lineaGris" align="right">
      			<input id="limpiar" type="button" name="btnVolver" value="Limpiar" class="boton"/>		
      			<input id="buscar" type="button" name="btnVolver" value="Buscar" class="boton"/>
    			</td>
			</tr>	
			</table>
		</div>	
			
		
        <!-- Resultado Filtro -->
		<@s.if test="oc_osList!=null">
				<#assign oc_osNumero = "0" />
				<@s.if test="oc_os.numero != null">
					<#assign oc_osNumero = "${oc_os.numero}" />
				</@s.if>
			
				<#assign oc_osFechaDesde = "" />
				<@s.if test="fechaDesde != null">
					<#assign oc_osFechaDesde = "${fechaDesde?string('dd/MM/yyyy')}" />
				</@s.if>

				<#assign oc_osFechaHasta = "" />
				<@s.if test="fechaHasta != null">
					<#assign oc_osFechaHasta = "${fechaHasta?string('dd/MM/yyyy')}" />
				</@s.if>
				
				<#assign oc_osImporteDesde = "" />
				<@s.if test="importeDesde != null">
					<#assign oc_osImporteDesde = "${importeDesde}" />
				</@s.if>

				<#assign oc_osImporteHasta = "" />
				<@s.if test="importeHasta != null">
					<#assign oc_osImporteHasta = "${importeHasta}" />
				</@s.if>
												
				<#assign oc_osRazonSocialProveedor = "" />
				<@s.if test=" oc_os.proveedor!= null">
					<#assign oc_osRazonSocialProveedor = "${oc_os.proveedor.detalleDePersona.razonSocial}" />
				</@s.if>
				
				<#assign oc_osNombreProveedor = "" />
				<@s.if test=" oc_os.proveedor!= null">
					<#assign oc_osNombreProveedor = "${oc_os.proveedor.detalleDePersona.nombre}" />
				</@s.if>
				
				<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
				<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Ordenes de Servicio encontradas</td>
					<td>
						<div class="alineacionDerecha">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0604" 
							cssClass="item" 
							href="${request.contextPath}/servicio/evaluacionOS/printXLS.action?oc_os.numero=${oc_osNumero}&fecha=${oc_osFechaDesde}&fechaHasta=${oc_osFechaHasta}&importeDesde=${oc_osImporteDesde}&importeHasta=${oc_osImporteHasta}&oc_os.tipo=${oc_os.tipo.ordinal()}&oc_os.proveedor.detalleDePersona.razonSocial=${oc_osRazonSocialProveedor}&oc_os.proveedor.detalleDePersona.nombre=${oc_osNombreProveedor},pendienteEvaluacionOS={pendienteEvaluacionOS}">
							<img src="${request.contextPath}/common/images/imprimirExcel.gif" title="Exportar a Excel" align="absmiddle" border="0" hspace="1">
						</@security.a>
						</div>
						<div class="alineacionDerecha">
							<b>Imprimir</b>
							<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0604" 
								cssClass="item" 
								href="${request.contextPath}/servicio/evaluacionOS/printPDF.action?oc_os.numero=${oc_osNumero}&fechaDesde=${oc_osFechaDesde}&fechaHasta=${oc_osFechaHasta}&importeDesde=${oc_osImporteDesde}&importeHasta=${oc_osImporteHasta}&oc_os.tipo=${oc_os.tipo.ordinal()}&oc_os.proveedor.detalleDePersona.razonSocial=${oc_osRazonSocialProveedor}&oc_os.proveedor.detalleDePersona.nombre=${oc_osNombreProveedor},pendienteEvaluacionOS={pendienteEvaluacionOS}">
								<img src="${request.contextPath}/common/images/imprimirPdf.gif" title="Exportar a PDF" align="absmiddle" border="0" hspace="1" >
							</@security.a>
						</div>	
					</td>
				</tr>
			</table>	
				
			<@vc.anchors target="contentTrx">	
          		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="oc_osList" id="oc_os" pagesize=15 defaultsort=2 decorator="ar.com.riouruguay.web.actions.compraContratacion.oc_os.decorators.OC_OSDecorator" partialList=true size="recordSize" keepStatus=true excludedParams="resetFlag">
          			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" class="botoneraAnchoCon3" title="Acciones">
						<div align="right">
						<#-- Ver OS -->
						<div class="alineacion">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0383" 
							enabled="oc_os.readable" 
							cssClass="item" 
							href="${request.contextPath}/compraContratacion/oc_os/readOC_OSView.action?oc_os.oid=${oc_os.oid?c}&navigationId=oc_os-visualizar&flowControl=regis&navegacionIdBack=${navigationId}">
							<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver"  border="0">
						</@security.a>
						</div>
						
						<#-- OS tiene Evaluacion -->
						<div class="alineacion">
						<#if oc_os.evaluacionOS?exists>
							<#-- estado os = En Curso -->
							<#if oc_os.estado.ordinal()== 4> 
								<#-- os fue recibida totalmente  -->
								<#if oc_os.recibidaTotal == oc_os.cantidadItemsDetalle>
									<@security.a 
										templateDir="custontemplates" 
										securityCode="CUF0602" 
										enabled="evaluacionOS.updatable"
										cssClass="item"  
										href="${request.contextPath}/servicio/evaluacionOS/updateEvaluacionOSView.action?evaluacionOS.oc_os.oid=${oc_os.oid?c}&evaluacionOS.oid=${oc_os.evaluacionOS.oid?c}&navigationId=evaluacionOS_modificar&flowControl=regis&navegacionIdBack=${navigationId}">
										<img  src="${request.contextPath}/common/images/modificar.gif" alt="Modificar Evaluaci&oacute;n OS" title="Modificar Evaluaci&oacute;n OS"  border="0">
									</@security.a>
								<#else>
									<@security.a 
										templateDir="custontemplates" 
										securityCode="CUF0602" 
										enabled="false"
										cssClass="item"  
										href="${request.contextPath}/servicio/evaluacionOS/updateEvaluacionOSView.action?evaluacionOS.oc_os.oid=${oc_os.oid?c}&evaluacionOS.oid=${oc_os.evaluacionOS.oid?c}&navigationId=evaluacionOS_modificar&flowControl=regis&navegacionIdBack=${navigationId}">
										<img  src="${request.contextPath}/common/images/modificar.gif" alt="Modificar Evaluaci&oacute;n OS" title="Modificar Evaluaci&oacute;n OS"  border="0">
									</@security.a>
								</#if>
							<#-- estado os = Intervenida -->
							<#elseif oc_os.estado.ordinal()== 7>
								<#-- os fue intervenida por recepcion  -->
								<#if oc_os.tipoIntervencionRecepcion.ordinal() == 2>
									<@security.a 
										templateDir="custontemplates" 
										securityCode="CUF0602" 
										enabled="evaluacionOS.updatable"
										cssClass="item"  
										href="${request.contextPath}/servicio/evaluacionOS/updateEvaluacionOSView.action?evaluacionOS.oc_os.oid=${oc_os.oid?c}&evaluacionOS.oid=${oc_os.evaluacionOS.oid?c}&navigationId=evaluacionOS_modificar&flowControl=regis&navegacionIdBack=${navigationId}">
										<img  src="${request.contextPath}/common/images/modificar.gif" alt="Modificar Evaluaci&oacute;n OS" title="Modificar Evaluaci&oacute;n OS"  border="0">
									</@security.a>
								<#else>
									<@security.a 
										templateDir="custontemplates" 
										securityCode="CUF0602" 
										enabled="false"
										cssClass="item"  
										href="${request.contextPath}/servicio/evaluacionOS/updateEvaluacionOSView.action?evaluacionOS.oc_os.oid=${oc_os.oid?c}&evaluacionOS.oid=${oc_os.evaluacionOS.oid?c}&navigationId=evaluacionOS_modificar&flowControl=regis&navegacionIdBack=${navigationId}">
										<img  src="${request.contextPath}/common/images/modificar.gif" alt="Modificar Evaluaci&oacute;n OS" title="Modificar Evaluaci&oacute;n OS"  border="0">
									</@security.a>
								</#if>							
							<#else>
							<#-- estado os = Finalizada-->
								<@security.a 
										templateDir="custontemplates" 
										securityCode="CUF0602" 
										enabled="evaluacionOS.updatable"
										cssClass="item"  
										href="${request.contextPath}/servicio/evaluacionOS/updateEvaluacionOSView.action?evaluacionOS.oc_os.oid=${oc_os.oid?c}&evaluacionOS.oid=${oc_os.evaluacionOS.oid?c}&navigationId=evaluacionOS_modificar&flowControl=regis&navegacionIdBack=${navigationId}">
										<img  src="${request.contextPath}/common/images/modificar.gif" alt="Modificar Evaluaci&oacute;n OS" title="Modificar Evaluaci&oacute;n OS"  border="0">
									</@security.a>
							</#if>									
						<#else>
							<#-- estado os = En Curso -->
							<#if oc_os.estado.ordinal()== 4> 
								<#-- os fue recibida totalmente  -->
								<#if oc_os.recibidaTotal == oc_os.cantidadItemsDetalle>
									<@security.a 
										templateDir="custontemplates" 
										securityCode="CUF0601" 
										enabled="evaluacionOS.readable" 
										cssClass="item" 
										href="${request.contextPath}/servicio/evaluacionOS/createEvaluacionOSView.action?evaluacionOS.oc_os.oid=${oc_os.oid?c}&navigationId=evaluacionOS_agregar&flowControl=regis&navegacionIdBack=${navigationId}">
										<img  src="${request.contextPath}/common/images/evaluarOS.gif" alt="Agregar Evaluaci&oacute;n OS" title="Agregar Evaluaci&oacute;n OS"  border="0">
									</@security.a>
								<#else>
									<@security.a 
										templateDir="custontemplates" 
										securityCode="CUF0601" 
										enabled="false" 
										cssClass="item" 
										href="${request.contextPath}/servicio/evaluacionOS/createEvaluacionOSView.action?evaluacionOS.oc_os.oid=${oc_os.oid?c}&navigationId=evaluacionOS_agregar&flowControl=regis&navegacionIdBack=${navigationId}">
										<img  src="${request.contextPath}/common/images/evaluarOS.gif" alt="Agregar Evaluaci&oacute;n OS" title="Agregar Evaluaci&oacute;n OS"  border="0">
									</@security.a>
								</#if>
							<#-- estado os = Intervenida -->
							<#elseif oc_os.estado.ordinal()== 7>
								<#-- os fue intervenida por recepcion  -->
								<#if oc_os.tipoIntervencionRecepcion.ordinal() == 2>
									<@security.a 
											templateDir="custontemplates" 
											securityCode="CUF0601" 
											enabled="evaluacionOS.readable" 
											cssClass="item" 
											href="${request.contextPath}/servicio/evaluacionOS/createEvaluacionOSView.action?evaluacionOS.oc_os.oid=${oc_os.oid?c}&navigationId=evaluacionOS_agregar&flowControl=regis&navegacionIdBack=${navigationId}">
											<img  src="${request.contextPath}/common/images/evaluarOS.gif" alt="Agregar Evaluaci&oacute;n OS" title="Agregar Evaluaci&oacute;n OS"  border="0">
										</@security.a>
									<#else>
										<@security.a 
											templateDir="custontemplates" 
											securityCode="CUF0601" 
											enabled="false" 
											cssClass="item" 
											href="${request.contextPath}/servicio/evaluacionOS/createEvaluacionOSView.action?evaluacionOS.oc_os.oid=${oc_os.oid?c}&navigationId=evaluacionOS_agregar&flowControl=regis&navegacionIdBack=${navigationId}">
											<img  src="${request.contextPath}/common/images/evaluarOS.gif" alt="Agregar Evaluaci&oacute;n OS" title="Agregar Evaluaci&oacute;n OS"  border="0">
										</@security.a>
									</#if>
							
							<#else>
							<#-- estado os = Finalizada-->
								<@security.a 
									templateDir="custontemplates" 
									securityCode="CUF0601" 
									enabled="evaluacionOS.readable" 
									cssClass="item" 
									href="${request.contextPath}/servicio/evaluacionOS/createEvaluacionOSView.action?evaluacionOS.oc_os.oid=${oc_os.oid?c}&navigationId=evaluacionOS_agregar&flowControl=regis&navegacionIdBack=${navigationId}">
									<img  src="${request.contextPath}/common/images/evaluarOS.gif" alt="Agregar Evaluaci&oacute;n OS" title="Agregar Evaluaci&oacute;n OS"  border="0">
								</@security.a>
							</#if>
						</#if>	
						</div>
						
						<div class="alineacion">
							<#--OS tiene evaluacion-->
							<#if oc_os.evaluacionOS?exists>
								<@security.a  
									templateDir="custontemplates" 
									securityCode="CUF0603" 
									enabled="oc_os.eraseable"
									cssClass="item"  
									href="${request.contextPath}/servicio/evaluacionOS/deleteEvaluacionOSView.action?evaluacionOS.oc_os.oid=${oc_os.oid?c}&evaluacionOS.oid=${oc_os.evaluacionOS.oid?c}&navigationId=evaluacionOS_eliminar&flowControl=regis&navegacionIdBack=${navigationId}">
									<img  src="${request.contextPath}/common/images/eliminar.gif" alt="Eliminar" title="Eliminar"  border="0">
								</@security.a>
							<#else>
								<@security.a  
									templateDir="custontemplates" 
									securityCode="CUF0603" 
									enabled="false"
									cssClass="item"  
									href="${request.contextPath}/servicio/evaluacionOS/deleteEvaluacionOSView.action?evaluacionOS.oc_os.oid=${oc_os.oid?c}&navigationId=evaluacionOS_eliminar&flowControl=regis&navegacionIdBack=${navigationId}">
									<img  src="${request.contextPath}/common/images/eliminar.gif" alt="Eliminar" title="Eliminar"  border="0">
								</@security.a>
							</#if>
						</div>	
					</div>
					</@display.column>
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="numero" title="N&uacute;mero" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="fecha" format="{0,date,dd/MM/yyyy}"  title="Fecha"/>
					<#--<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="tipo" title="Tipo" />-->
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" title="Proveedor">  
						${oc_os.proveedor.detalleDePersona.razonSocial} ${oc_os.proveedor.detalleDePersona.nombre}
					</@display.column> 
					<#--<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="importe" title="Importe" />-->
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="estado" title="Estado" />					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="osPrestadaTotal" title="Recibida Total" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="tipoIntervencionRecepcion" title="Recepci&oacute;n Intervenida" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="evaluada" title="Evaluada"/>
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="evaluacionOS.calificacionCC" title="CC"/>
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="evaluacionOS.calificacionPCa" title="PCa"/>
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="evaluacionOS.calificacionPE" title="PE"/>
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="evaluacionOS.FA" property="evaluacionOS.calificacionFA" title="FM"/>
					
					
					
				</@display.table>
				
			</@vc.anchors>
			</div>
		</@s.if>


<@vc.htmlContent 
  baseUrl="${request.contextPath}/compraContratacion/oc_os/selectProveedorSearch.action" 
  source="seleccionarProveedor" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=change,oc_os.oid={oc_os.oid},oc_os.tipo={oc_os.tipo},oc_os.numero={oc_os.numero},oc_os.proveedor.nroProveedor={oc_os.proveedor.nroProveedor},fechaDesde={fechaDesde},fechaHasta={fechaHasta},importeDesde={importeDesde},importeHasta={importeHasta},pendienteEvaluacionOS={pendienteEvaluacionOS}"/>


<@vc.htmlContent 
  baseUrl="${request.contextPath}/servicio/evaluacionOS/search.action"
  source="buscar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="resetFlag=true,navigationId={navigationId},flowControl=regis,oc_os.numero={oc_os.numero},oc_os.proveedor.detalleDePersona.razonSocial={oc_os.proveedor.detalleDePersona.razonSocial},oc_os.proveedor.detalleDePersona.nombre={oc_os.proveedor.detalleDePersona.nombre},oc_os.proveedor.nroProveedor={oc_os.proveedor.nroProveedor},oc_os.tipo={oc_os.tipo},fechaDesde={fechaDesde},fechaHasta={fechaHasta},importeDesde={importeDesde},importeHasta={importeHasta},pendienteEvaluacionOS={pendienteEvaluacionOS}"/>
  		      

<@vc.htmlContent 
  baseUrl="${request.contextPath}/servicio/evaluacionOS/view.action"
  source="limpiar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=regis"/>
  

  

  
  