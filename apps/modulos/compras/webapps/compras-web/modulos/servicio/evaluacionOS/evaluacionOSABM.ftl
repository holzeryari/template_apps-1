<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<@s.hidden id="evaluacionOS.oid" name="evaluacionOS.oid"/>
<@s.hidden id="evaluacionOS.oc_os.oid" name="evaluacionOS.oc_os.oid"/>
<@s.hidden id="evaluacionOS.versionNumber" name="evaluacionOS.versionNumber"/>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
			<#if mensajeAviso?exists && (mensajeAviso.length()>0)>
				<tr>
					<td class="estiloMensajeAviso"><@s.text name="${mensajeAviso}"/></td>
				</tr>
			</#if>
			<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos de la Orden de Servicio</b></td>
			</tr>
		</table>
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
			<tr>
  				<td class="textoCampo">N&uacute;mero:</td>
  				<td class="textoDato"><@s.property default="&nbsp;" escape=false value="evaluacionOS.oc_os.numero"/></td>
  				<td  class="textoCampo">Fecha: </td>
				<td class="textoDato">
					<#assign fecha = evaluacionOS.oc_os.fecha> 
					${fecha?string("dd/MM/yyyy")} 	
				</td>	      			
			</tr>	
			<tr>
				<td class="textoCampo">Tipo:</td>
  				<td class="textoDato"><@s.property default="&nbsp;" escape=false value="evaluacionOS.oc_os.tipo"/></td>
  				<td class="textoCampo">Proveedor:</td>
  				<td class="textoDato">
  					<@s.property default="&nbsp;" escape=false value="evaluacionOS.oc_os.proveedor.detalleDePersona.razonSocial" /><@s.property default="&nbsp;" escape=false value="evaluacionOS.oc_os.proveedor.detalleDePersona.nombre" />							
					<@s.hidden id="nroProveedor" name="evaluacionOS.oc_os.proveedor.nroProveedor"/>
					<@s.hidden id="razonSocial" name="evaluacionOS.oc_os.proveedor.detalleDePersona.razonSocial"/>
					<@s.hidden id="nombre" name="evaluacionOS.oc_os.proveedor.detalleDePersona.nombre"/>
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Lugar de Entrega/Prestaci&oacute;n:</td>
  				<td  class="textoDato" colspan="3">
					<@s.property default="&nbsp;" escape=false value="evaluacionOS.oc_os.lugarEntregaPrestacion" />
				</td>
			</tr>
		
			<tr>
				<td class="textoCampo">Observaciones:</td>
  				<td  class="textoDato" colspan="3">
				<@s.property default="&nbsp;" escape=false value="evaluacionOS.oc_os.observaciones" />
				</td>					
			</tr>
	   		<tr>
				<td class="textoCampo">Demorada:</td>
  					<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="evaluacionOS.oc_os.demorada"/>
				</td>
				<td class="textoCampo">Estado:</td>
  					<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="evaluacionOS.oc_os.estado"/>
				</td>			      		    
			</tr>
			<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>					
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos de la Evaluaci&oacute;n</b></td>
			</tr>
		</table>
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>	
			<tr>		
				<td class="textoCampo">Fecha Evaluaci&oacute;n:</td>
  				<td class="textoDato" colspan="3">
  					<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="evaluacionOS.fechaEvaluacion" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="evaluacionOS.fechaEvaluacion" 
					title="Fecha Evaluacion" />
				</td>	
			</tr>
			<tr>
  				<@s.hidden id="evaluacionOS.fechaEvaluacionStr" name="evaluacionOS.fechaEvaluacionStr"/>
  				<td class="textoCampo">Cumplimiento del Contrato [%]:</td>
  				<td class="textoDato">
  					<#-- <@s.textfield 
  						templateDir="custontemplates" 
						id="evaluacionOS.CC" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="evaluacionOS.CC" 
						title="CC" />-->
					<@s.select 
						templateDir="custontemplates" 
						id="evaluacionOS.calEvaProCC.oid" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="evaluacionOS.calEvaProCC.oid" 
						list="calificacionEvaluacionProveedorList" 
						listKey="oid" 
						listValue="calificacion" 
						value="evaluacionOS.calEvaProCC.oid"
						title="Calificacion Evaluacion Proveedor"
						headerKey="0"
						headerValue="Seleccionar"							
					/>
				</td>
	
				<td class="textoCampo">Personal Capacitado [%]:</td>
  				<td class="textoDato">
  					<#-- <@s.textfield 
  						templateDir="custontemplates" 
						id="evaluacionOS.PCa" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="evaluacionOS.PCa" 
						title="PCa" />-->
					<@s.select 
						templateDir="custontemplates" 
						id="evaluacionOS.calEvaProPCa.oid" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="evaluacionOS.calEvaProPCa.oid" 
						list="calificacionEvaluacionProveedorList" 
						listKey="oid" 
						listValue="calificacion" 
						value="evaluacionOS.calEvaProPCa.oid"
						title="Calificacion Evaluacion Proveedor"
						headerKey="0"
						headerValue="Seleccionar"							
					/>
				</td>
				</tr>
				<tr>
					<td class="textoCampo">Cumplimiento de Plazos Estipulados [%]:</td>
      				<td class="textoDato">
      					<#-- <@s.textfield 
      						templateDir="custontemplates" 
							id="evaluacionOS.PE" 
							cssClass="textarea"
							cssStyle="width:160px" 
							name="evaluacionOS.PE" 
							title="PE" />-->
						<@s.select 
							templateDir="custontemplates" 
							id="evaluacionOS.calEvaProPE.oid" 
							cssClass="textarea"
							cssStyle="width:165px" 
							name="evaluacionOS.calEvaProPE.oid" 
							list="calificacionEvaluacionProveedorList" 
							listKey="oid" 
							listValue="calificacion" 
							value="evaluacionOS.calEvaProPE.oid"
							title="Calificacion Evaluacion Proveedor"
							headerKey="0"
							headerValue="Seleccionar"							
						/>
					</td>
				
				<td class="textoCampo">Flexibilidad y Atenci&oacute;n [%]:</td>
      				<td class="textoDato">
      					<#-- <@s.textfield 
      						templateDir="custontemplates" 
							id="evaluacionOS.FA" 
							cssClass="textarea"
							cssStyle="width:160px" 
							name="evaluacionOS.FA" 
							title="FA" />-->
						<@s.select 
							templateDir="custontemplates" 
							id="evaluacionOS.calEvaProFA.oid" 
							cssClass="textarea"
							cssStyle="width:165px" 
							name="evaluacionOS.calEvaProFA.oid" 
							list="calificacionEvaluacionProveedorList" 
							listKey="oid" 
							listValue="calificacion" 
							value="evaluacionOS.calEvaProFA.oid"
							title="Calificacion Evaluacion Proveedor"
							headerKey="0"
							headerValue="Seleccionar"							
						/>
					</td>
				</tr>
				<tr><td class="textoCampo" colspan="4">&nbsp;</td></tr>
			</table>
		</div>				
		


