<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<@s.hidden id="prestacionServicioDetalle.oid" name="prestacionServicioDetalle.oid"/>
<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
			<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos de la Prestaci&oacute;n de Servicios</b></td>
			</tr>
		</table>
			
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
		<tr>
			<td class="textoCampo" colspan="4">&nbsp;</td>
		</tr>	
			
		<tr>
  			<td class="textoCampo">N&uacute;mero:</td>
  			<td class="textoDato">
  			<@s.property default="&nbsp;" escape=false value="prestacionServicioDetalle.prestacionServicio.numero"/></td>
  			<td  class="textoCampo">Fecha: </td>
			<td class="textoDato">
			<@s.if test="prestacionServicioDetalle.prestacionServicio.fechaComprobante != null">
			<#assign fecha = prestacionServicioDetalle.prestacionServicio.fechaComprobante> 
			${fecha?string("dd/MM/yyyy")}	
			</@s.if>				&nbsp;					
			</td>
	      			
		</tr>	

		<tr>
      		<td class="textoCampo">Proveedor</td>
			<td class="textoDato">
			<@s.property default="&nbsp;" escape=false value="prestacionServicioDetalle.prestacionServicio.proveedor.detalleDePersona.razonSocial" />
			<@s.property default="&nbsp;" escape=false value="prestacionServicioDetalle.prestacionServicio.proveedor.detalleDePersona.nombre" />
			</td>
			<td  class="textoCampo">Fecha Recepci&oacute;n: </td>
			<td class="textoDato">
			<@s.if test="prestacionServicioDetalle.prestacionServicio.fechaPrestacion != null">
			<#assign fechaPrestacion = prestacionServicioDetalle.prestacionServicio.fechaPrestacion> 
			${fechaPrestacion?string("dd/MM/yyyy")}	
			</@s.if>				&nbsp;					
			</td>	
		</tr>
		
		<tr>
  			<td class="textoCampo">Dep&oacute;sito</td>
  			<td class="textoDato">
  				<@s.property default="&nbsp;" escape=false value="prestacionServicioDetalle.prestacionServicio.servicio.descripcion" />
           </td>
           <td class="textoCampo">Cliente:</td>
				<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="prestacionServicioDetalle.prestacionServicio.cliente.descripcion" />				
			</td>	
		</tr>
				
		<tr>
  			<td class="textoCampo">Nro. Factura:</td>
  			<td class="textoDato">
  			<@s.property default="&nbsp;" escape=false value="prestacionServicioDetalle.prestacionServicio.numeroFactura" />
  			
			</td>	
			<td class="textoCampo">Nro. Remito:</td>
  			<td class="textoDato">
  			<@s.property default="&nbsp;" escape=false value="prestacionServicioDetalle.prestacionServicio.numeroRemito" />
  			
			</td>
		</tr>		

		<tr>
			<td class="textoCampo">Observaciones:</td>
  			<td  class="textoDato" colspan="3">
  			<@s.property default="&nbsp;" escape=false value="prestacionServicioDetalle.prestacionServicio.observaciones" />
			</td>					
		</tr>
		
		<tr>
			<td class="textoCampo">Estado:</td>
  			<td  class="textoDato" colspan="3">
				<@s.property default="&nbsp;" escape=false value="prestacionServicioDetalle.prestacionServicio.estado"/></td>
			</td>
		</tr>	    	
		<tr><td class="estadoCampo" colspan="4">&nbsp;</td></tr>
	</table>		    			
	</div>
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos del Servicio</b></td>
			</tr>
		</table>
			
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
		<tr>
			<td class="textoCampo" colspan="4">&nbsp;</td>
		</tr>	
		<tr>
  			<td class="textoCampo">C&oacute;digo:</td>
  			<td class="textoDato"><@s.property default="&nbsp;" escape=false value="servicio.oid"/></td>
			<td class="textoCampo">Tipo:</td>
  			<td class="textoDato"colspan="3">
      			<@s.property default="&nbsp;" escape=false value="prestacionServicioDetalle.servicio.tipo" />
      		</td>
      	</tr>		
		<tr>
  			<td class="textoCampo">Descripci&oacute;n</td>
			<td class="textoDato">							
					<@s.property default="&nbsp;" escape=false value="prestacionServicioDetalle.servicio.descripcion" />
			</td>	      			
			<td class="textoCampo">Rubro:</td>
  			<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="prestacionServicioDetalle.servicio.rubro.descripcion" />
			</td>
		</tr>	
		<tr>
			<td class="textoCampo">Cr&iacute;tico: </td>
			<td class="textoDato" colspan="3">
			<@s.property default="&nbsp;" escape=false value="prestacionServicioDetalle.servicio.critico"/>
			</td>
		</tr>

		<tr>
  			<td class="textoCampo">Proveedor &Uacute;nico:</td>
			<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="prestacionServicioDetalle.servicio.proveedorUnico"/>
			</td>
			<td class="textoCampo">Proveedor:</td>
			<td class="textoDato">
				<div id="provUnico">
					<@s.property default="&nbsp;" escape=false value="prestacionServicioDetalle.servicio.proveedor.detalleDePersona.razonSocial" />
					<@s.property default="&nbsp;" escape=false value="prestacionServicioDetalle.servicio.proveedor.detalleDePersona.nombre" />
				</div>
			</td>
		</tr>
		<tr>		
			<td class="textoCampo">Estado:</td>
			<td class="textoDato" colspan="3">
				<@s.property default="&nbsp;" escape=false value="prestacionServicioDetalle.servicio.estado"/>
			</td>
		</tr>					
		<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>	
	</table>
	</div>		
		