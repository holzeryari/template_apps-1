<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>


<@vc.anchors target="contentTrx" ajaxFlag="ajax">
<div id="capaSeparadora" class="capaSeparadora">&nbsp;</div>
<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
			<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Servicios</td>
				</tr>
			</table>	
			
			<!-- Resultado Filtro -->						
			<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="prestacionServicio.prestacionServicioDetalleList" id="prestacionServicioDetalle" defaultsort=2>	
									
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select"  class="botoneraAnchoCon3" title="Acciones">
					
							
					<a href="${request.contextPath}/servicio/prestacionServicio/readDetalleView.action?prestacionServicioDetalle.oid=${prestacionServicioDetalle.oid?c}&navegacionIdBack=${navigationId}&navegacionIdBack=${navigationId}"><img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver" border="0"></a>
					&nbsp;
					
										
				</@display.column>
				
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="numero" title="N&uacute;mero Item" />
				
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="servicio.oid" title="C&oacute;digo" />
				
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="servicio.tipo" title="Tipo" />	
					
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="servicio.descripcion" title="Descripci&oacute;n" />	
					
			</@display.table>
		</div>	
</@vc.anchors>

