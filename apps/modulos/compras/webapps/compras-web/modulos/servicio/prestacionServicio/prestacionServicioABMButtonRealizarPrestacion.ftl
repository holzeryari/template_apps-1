<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaSeparadora" class="capaSeparadora">&nbsp;</div>
<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancelar" type="button" name="btnCancelar" value="Cancelar" class="boton"/>
				<input id="prestacion" type="button" name="btnPrestacion" value="Realizar Prestaci&oacute;n" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

  
  
  <@vc.htmlContent 
   baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancelar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=buscar-prestacionServicio,flowControl=back"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/servicio/prestacionServicio/realizarPrestacion.action" 
  source="prestacion" 
  success="contentTrx" 
  failure="errorTrx"  
  preFunction="listaEntregasParciales"
  parameters="entregaSeleccion={entregaSeleccion},prestacionServicio.oid={prestacionServicio.oid}"/>
  

  <@vc.htmlContent 
  baseUrl="${request.contextPath}/servicio/prestacionServicio/updateView.action" 
  source="modificarPrestacionServicio" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="prestacionServicio.oid={prestacionServicio.oid},prestacionServicio.versionNumber={prestacionServicio.versionNumber},navigationId=prestacionServicio-actualizar"/>

  