<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancel" type="button" name="btnCancelar" value="Cancelar" class="boton"/>
				<input id="store" type="button" name="btnAgregar" value="Agregar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>
<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=buscar-prestacionServicio,flowControl=back"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/servicio/prestacionServicio/createPrestacionServicio.action" 
  source="store" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="prestacionServicio.servicio.oid={prestacionServicio.servicio.oid},prestacionServicio.proveedor.nroProveedor={prestacionServicio.proveedor.nroProveedor},prestacionServicio.proveedor.razonSocial={prestacionServicio.proveedor.razonSocial},prestacionServicio.proveedor.nombre={prestacionServicio.proveedor.nombre},prestacionServicio.numeroFactura={prestacionServicio.numeroFactura},prestacionServicio.numeroRemito={prestacionServicio.numeroRemito},prestacionServicio.observaciones={prestacionServicio.observaciones},prestacionServicio.fechaPrestacion={prestacionServicio.fechaPrestacion}"/>
  
  
  <@vc.htmlContent 
  baseUrl="${request.contextPath}/servicio/prestacionServicio/selectProveedorABM.action" 
  source="seleccionarProveedor" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=change,prestacionServicio.cliente.oid={prestacionServicio.cliente.oid},prestacionServicio.cliente.descripcion={prestacionServicio.cliente.descripcion},prestacionServicio.servicio.oid={prestacionServicio.servicio.oid},prestacionServicio.proveedor.nroProveedor={prestacionServicio.proveedor.nroProveedor},prestacionServicio.proveedor.razonSocial={prestacionServicio.proveedor.razonSocial},prestacionServicio.proveedor.nombre={prestacionServicio.proveedor.nombre},prestacionServicio.numeroFactura={prestacionServicio.numeroFactura},prestacionServicio.numeroRemito={prestacionServicio.numeroRemito},prestacionServicio.observaciones={prestacionServicio.observaciones},prestacionServicio.fechaPrestacion={prestacionServicio.fechaPrestacion}"/>
  
  <@vc.htmlContent 
  baseUrl="${request.contextPath}/servicio/prestacionServicio/selectClienteABM.action" 
  source="seleccionarCliente" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=change,prestacionServicio.servicio.oid={prestacionServicio.servicio.oid},prestacionServicio.proveedor.nroProveedor={prestacionServicio.proveedor.nroProveedor},prestacionServicio.proveedor.razonSocial={prestacionServicio.proveedor.razonSocial},prestacionServicio.proveedor.nombre={prestacionServicio.proveedor.nombre},prestacionServicio.numeroFactura={prestacionServicio.numeroFactura},prestacionServicio.numeroRemito={prestacionServicio.numeroRemito},prestacionServicio.observaciones={prestacionServicio.observaciones},prestacionServicio.fechaPrestacion={prestacionServicio.fechaPrestacion}"/>
  

  