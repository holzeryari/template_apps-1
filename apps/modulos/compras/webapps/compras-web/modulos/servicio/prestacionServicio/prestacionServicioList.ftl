
<#setting locale="es_AR">

<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>


<@s.hidden id="navigationId" name="navigationId"/>
<@s.hidden id="prestacionServicio.cliente.oid" name="prestacionServicio.cliente.oid"/>
<@s.hidden id="prestacionServicio.cliente.descripcion" name="prestacionServicio.cliente.descripcion"/>
<@s.hidden id="prestacionServicio.proveedor.nroProveedor" name="prestacionServicio.proveedor.nroProveedor"/>
<@s.hidden id="prestacionServicio.proveedor.detalleDePersona.razonSocial" name="prestacionServicio.proveedor.detalleDePersona.razonSocial"/>
<@s.hidden id="prestacionServicio.proveedor.detalleDePersona.nombre" name="prestacionServicio.proveedor.detalleDePersona.nombre"/>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
        	<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Datos de la Prestaci&oacute;n de Servicios</b></td>
				<td>
					<div align="right">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0621" 
							enabled="true" 
							cssClass="item" 
							id="crear"										
							href="javascript://nop/">
							<b class="texto1">Agregar</b>										
							<img src="${request.contextPath}/common/images/agregar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
						</@security.a>						
						
					</div>
				</td>
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
	    		<td class="textoCampo" colspan="4">&nbsp;</td>
	    	</tr>
			<tr>
				<td class="textoCampo">N&uacute;mero:</td>
      			<td class="textoDato">
      			<@s.textfield 
      					templateDir="custontemplates" 
						id="prestacionServicio.numero" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="prestacionServicio.numero" 
						title="N&uacute;mero" />
				</td>							
      			<td class="textoCampo">Proveedor</td>
					<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="prestacionServicio.proveedor.detalleDePersona.razonSocial" />
					<@s.property default="&nbsp;" escape=false value="prestacionServicio.proveedor.detalleDePersona.nombre" />
					<@s.a templateDir="custontemplates" id="seleccionarProveedor" name="seleccionarProveedor" href="javascript://nop/" cssClass="ocultarIcono">
							<img src="${request.contextPath}/common/images/buscar.gif"  border="0" align="absmiddle" hspace="3" >
					</@s.a>	
				</td>						
			</tr>
			<tr>
				<td class="textoCampo">Estado:</td>
      			<td class="textoDato" colspan="3">
      			<@s.select 
							templateDir="custontemplates" 
							id="estado" 
							cssClass="textarea"
							cssStyle="width:165px" 
							name="estado" 
							list="estadoList" 
							listKey="key" 
							listValue="description" 
							value="prestacionServicio.estado.ordinal()"
							title="Estado"
							headerKey="0"
							headerValue="Todos"							
							/>
				</td>
				
			</tr>
			<tr>
				<td class="textoCampo">Fecha Prestaci&oacute;n Desde:</td>
      			<td class="textoDato">
					<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="fechaDesde" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="fechaDesde" 
					title="Fecha Prestaci&oacute;n Desde" />
				</td>
				<td class="textoCampo">Fecha Prestaci&oacute;n Hasta:</td>
      			<td class="textoDato">
				<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="fechaHasta" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="fechaHasta" 
					title="Fecha Prestaci&oacute;n Hasta" />
				</td>
				
			</tr>
			<tr>
    			<td colspan="4" class="lineaGris" align="right">
      			<input id="limpiar" type="button" name="btnVolver" value="Limpiar" class="boton"/>		
      			<input id="buscar" type="button" name="btnVolver" value="Buscar" class="boton"/>
    			</td>
			</tr>	
			</table>
		</div>	
			
		<!-- Resultado Filtro -->
		<@s.if test="prestacionServicioList!=null">
		
			<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
				<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Prestaciones de Servicios encontradas</td>

				</tr>
			</table>

			<@vc.anchors target="contentTrx">			
          		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="prestacionServicioList" id="prestacionServicio" pagesize=15 defaultsort=2 partialList=true size="recordSize" keepStatus=true excludedParams="resetFlag">
          			<@display.column headerClass="tbl-contract-service-select" class="botoneraAnchoCon3" title="Acciones">
						<div class="alineacion">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0623" 
							enabled="prestacionServicio.readable" 
							cssClass="item" 
							href="${request.contextPath}/servicio/prestacionServicio/readView.action?prestacionServicio.oid=${prestacionServicio.oid?c}&navigationId=prestacionServicio-visualizar&flowControl=regis&navegacionIdBack=${navigationId}">
							<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver"  border="0">
						</@security.a>
						</div>
						<div class="alineacion">	
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0624" 
							enabled="prestacionServicio.updatable"
							cssClass="item"  
							href="${request.contextPath}/servicio/prestacionServicio/administrarView.action?prestacionServicio.oid=${prestacionServicio.oid?c}&navigationId=prestacionServicio-administrar&flowControl=regis">
							<img  src="${request.contextPath}/common/images/administrar.gif" alt="Administrar" title="Administrar"  border="0">
						</@security.a>
						</div>
						<div class="alineacion">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0625" 
							enabled="prestacionServicio.eraseable"
							cssClass="item"  
							href="${request.contextPath}/servicio/prestacionServicio/deleteView.action?prestacionServicio.oid=${prestacionServicio.oid?c}&navigationId=prestacionServicio-eliminar&flowControl=regis">
							<img  src="${request.contextPath}/common/images/eliminar.gif" alt="Eliminar" title="Eliminar"  border="0">
						</@security.a>				
						</div>

					</@display.column>


					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="numero" title="N&uacute;mero" />				
										
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" title="Proveedor">  
						${prestacionServicio.proveedor.detalleDePersona.razonSocial}
						<#if prestacionServicio.proveedor.detalleDePersona.nombre?exists> 
							 ${prestacionServicio.proveedor.detalleDePersona.nombre}
						</#if>


					</@display.column>
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="fechaPrestacion" format="{0,date,dd/MM/yyyy}"  title="Fecha Prestaci&oacute;n" /> 
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="estado" title="Estado" />		

				</@display.table>
			</@vc.anchors>
		</div>	
	</@s.if>
			
	      
<@vc.htmlContent 
  baseUrl="${request.contextPath}/servicio/prestacionServicio/selectProveedorSearch.action" 
  source="seleccionarProveedor" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=change,prestacionServicio.estado={estado},prestacionServicio.proveedor.nroProveedor={prestacionServicio.proveedor.nroProveedor},prestacionServicio.proveedor.detalleDePersona.razonSocial={prestacionServicio.proveedor.detalleDePersona.razonSocial},prestacionServicio.proveedor.detalleDePersona.nombre={prestacionServicio.proveedor.detalleDePersona.nombre},prestacionServicio.numero={prestacionServicio.numero},fechaDesde={fechaDesde},fechaHasta={fechaHasta}"/>


<@vc.htmlContent 
  baseUrl="${request.contextPath}/servicio/prestacionServicio/search.action"
  source="buscar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="resetFlag=true,navigationId={navigationId},flowControl=regis,prestacionServicio.estado={estado},prestacionServicio.proveedor.nroProveedor={prestacionServicio.proveedor.nroProveedor},prestacionServicio.proveedor.detalleDePersona.razonSocial={prestacionServicio.proveedor.detalleDePersona.razonSocial},prestacionServicio.proveedor.detalleDePersona.nombre={prestacionServicio.proveedor.detalleDePersona.nombre},prestacionServicio.numero={prestacionServicio.numero},fechaDesde={fechaDesde},fechaHasta={fechaHasta}"/>
  		      

<@vc.htmlContent 
  baseUrl="${request.contextPath}/servicio/prestacionServicio/view.action"
  source="limpiar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=regis"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/servicio/prestacionServicio/createPrestacionServicioView.action" 
  source="crear" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=prestacionServicio-crear,flowControl=regis"/>
  
    
  
  
