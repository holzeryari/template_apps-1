<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<@s.hidden id="entregaSeleccion" name="entregaSeleccion"/>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>
<@vc.anchors target="contentTrx" ajaxFlag="ajax">
<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
	<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Ordenes de Compra</td>
					<td>
						<div align="right">
							<@s.a href="${request.contextPath}/deposito/recepcionProductoBien/selectOC.action?recepcionProductoBien.oid=${recepcionProductoBien.oid?c}&recepcionProductoBien.proveedor.nroProveedor=${recepcionProductoBien.proveedor.nroProveedor?c}&recepcionProductoBien.proveedor.detalleDePersona.nombre=${recepcionProductoBien.proveedor.detalleDePersona.nombre}&recepcionProductoBien.proveedor.detalleDePersona.razonSocial=${recepcionProductoBien.proveedor.detalleDePersona.razonSocial}&navigationId=${navigationId}" templateDir="custontemplates" id="agregarIngresoProductoDetalle" name="agregarIngresoProductoDetalle" cssClass="ocultarIcono">
								<b>Agregar</b><img src="${request.contextPath}/common/images/agregar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
							</@s.a>		
						</div>
					</td>
				</tr>
	</table>

	<table class="tablaDetalleCuerpo" cellpadding="3">
		<tr>
			<th>Acciones</th>
			<th>Numero</th>
			<th>Fecha</th>
			<th>Importe</th>
			<th>Estado</th>							
		</tr>
		<#assign mostro='true'>
		<#assign recepcionProductoBienOCEPlista = recepcionProductoBien.recepcionProductoBienOCEPList>
		<#list recepcionProductoBienOCEPlista as recepcionProductoBienOCEP>
			<#if mostro=='false'>									
				<tr>
					<th>Acciones</th>
					<th>Numero</th>
					<th>Fecha</th>
					<th>Importe</th>
					<th>Estado</th>							
				</tr>
			</#if>								
			<tr>
				<td class="botoneraAnchoCon2"> 
					<div align="center">
						<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0383" 
								enabled="oc_os.readable" 
								cssClass="item" 
								id="verOCOS"
								name="verOCOS"
								href="${request.contextPath}/compraContratacion/oc_os/readOC_OSView.action?oc_os.oid=${recepcionProductoBienOCEP.ocos.oid?c}&navigationId=oc_os-visualizar&flowControl=regis&navegacionIdBack=${navigationId}">
								<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver"  border="0">
							</@security.a>
							&nbsp;						
						<a href="${request.contextPath}/deposito/recepcionProductoBien/deleteOCView.action?recepcionProductoBien.oid=${recepcionProductoBien.oid?c}&oc.oid=${recepcionProductoBienOCEP.ocos.oid?c}&navigationId=ocRecepcion-eliminar&flowControl=regis&navegacionIdBack=${navigationId}">
							<img  src="${request.contextPath}/common/images/eliminar.gif" alt="Eliminar" title="Eliminar" border="0"></a>
					</div>
				</td>	
				<td class="estiloNumero">
					<#if recepcionProductoBienOCEP.ocos.numero?exists>${recepcionProductoBienOCEP.ocos.numero}<#else>&nbsp;</#if>
				</td>
				<td class="estiloNumero">
					<#if recepcionProductoBienOCEP.ocos.fecha?exists>${recepcionProductoBienOCEP.ocos.fecha?string("dd/MM/yyyy")}<#else>&nbsp;</#if>
				</td>
				<td class="estiloNumero">
					<#if recepcionProductoBienOCEP.ocos.importe?exists>${recepcionProductoBienOCEP.ocos.importe}<#else>&nbsp;</#if>
				</td>
				<td class="estiloTexto">
					<#if recepcionProductoBienOCEP.ocos.estado?exists>${recepcionProductoBienOCEP.ocos.estado}<#else>&nbsp;</#if>
				</td>														   
			</tr>
			<tr>
				<td>&nbsp;</td>
				<th class="botoneraAnchoCon1">								
					<b>Entregas Parciales</b>									
				</th>
				<th>Fecha</th>
				<th colspan="2">Observaciones</th>							
			</tr>
				<#assign entregaParcialLista =recepcionProductoBienOCEP.ocos.oc_osEntregaParcialList>
				<#list entregaParcialLista as entregaParcial>
				
					<#if !recepcionProductoBien.fechaRecepcion.after(entregaParcial.fecha)>					
					 	<#assign fechaValida='true'>
					 <#elseif entregaParcial.estaSeleccionada>
					 	<#assign fechaValida='false'>
					 <#else> 
					 	<#assign fechaValida='true'>
					 </#if>
				
					<#assign claseEntregaParcial="celdaHabilitada">							
					<#if fechaValida=='false'>
						<#assign claseEntregaParcial="celdaHabilitada">	
					</#if>		
					<tr>						
						<td>&nbsp;</td>
						<td class="${claseEntregaParcial}" align="center" width="60px">
							<#if fechaValida=='true'>
								<@s.checkbox label="${entregaParcial.oid}" fieldValue="${entregaParcial.oid?c}" name="checkEntregaSeleccion"/>
							<#else>
								&nbsp;	
							</#if>							
						</td>
						<td class="${claseEntregaParcial}" align="right">${entregaParcial.fecha?string("dd/MM/yyyy")}</td>
						<td colspan="2" class="${claseEntregaParcial}" align="left">
							<#if entregaParcial.observaciones?exists>${entregaParcial.observaciones}</#if>&nbsp;</td>
					</tr>	
				</#list>
				<tr><td  style="height:3px;" colspan="5"></td></tr>
						<#assign mostro='false'>
		</#list>						
	</table>
	</div>
</@vc.anchors>
<div id="capaSeparadora" class="capaSeparadora">&nbsp;</div>
