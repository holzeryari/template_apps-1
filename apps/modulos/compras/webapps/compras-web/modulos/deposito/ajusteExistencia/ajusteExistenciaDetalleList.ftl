<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>


<@ajax.anchors target="contentTrx">	
	<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
		<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Productos a Ajustar</td>
					<td>	
						<div align="right">
							<@s.a href="${request.contextPath}/deposito/ajusteExistencia/selectProducto.action?ajusteExistencia.oid=${ajusteExistencia.oid?c}&ajusteExistencia.deposito.oid=${ajusteExistencia.deposito.oid?c}&ajusteExistencia.deposito.descripcion=${ajusteExistencia.deposito.descripcion}&navigationId=${navigationId}" templateDir="custontemplates" id="agregarAjusteExistenciaDetalle" name="agregarAjusteExistencia" cssClass="ocultarIcono">
							<b>Agregar</b><img src="${request.contextPath}/common/images/agregar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
							</@s.a>		
						</div>
					</td>	
				</tr>
		</table>
	
						
		<!-- Resultado Filtro -->						
		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="ajusteExistencia.ajusteExistenciaDetalleList" id="ajusteExistenciaDetalle" defaultsort=2 decorator="ar.com.riouruguay.web.actions.deposito.ajusteExistencia.decorators.CantidadAjustadaDecorator">	
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select"  class="botoneraAnchoCon3" title="Acciones">
				<a href="${request.contextPath}/deposito/ajusteExistencia/readAjusteExistenciaDetalleView.action?ajusteExistenciaDetalle.oid=${ajusteExistenciaDetalle.oid?c}&navegacionIdBack=${navigationId}"><img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver" border="0"></a>
				&nbsp;
				<a href="${request.contextPath}/deposito/ajusteExistencia/updateAjusteExistenciaDetalleView.action?ajusteExistenciaDetalle.oid=${ajusteExistenciaDetalle.oid?c}"><img  src="${request.contextPath}/common/images/modificar.gif" alt="Modificar" title="Modificar" border="0"></a>
				&nbsp;
				<a href="${request.contextPath}/deposito/ajusteExistencia/deleteAjusteExistenciaDetalleView.action?ajusteExistenciaDetalle.oid=${ajusteExistenciaDetalle.oid?c}"><img  src="${request.contextPath}/common/images/eliminar.gif" alt="Eliminar" title="Eliminar" border="0"></a>
			</@display.column>
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="numero" title="N&uacute;mero Item" defaultorder="ascending"/>
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="producto.oid" title="C&oacute;digo" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="producto.descripcion" title="Descripci&oacute;n" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="producto.rubro.descripcion" title="Rubro" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="producto.marca" title="Marca" />					
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="producto.modelo" title="Modelo" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="cantidadAjustada" title="Cantidad a Ajustar" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="tipoAjuste" title="Tipo Ajuste" />
		</@display.table>
	</div>	
</@ajax.anchors>
