<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>


<table width="100%" border="0" cellspacing="0" cellpadding="3">
<tr><td>&nbsp;</td></tr>
	<tr>
		<td colspan="2" class="blue-linea-bot" align="left">
    		<input id="cancel" type="button" name="btnVolver" value="Cancelar" class="boton"/>	      		
		</td>
		<td colspan="2" class="blue-linea-bot" align="right">
			<input id="store" type="button" name="btnAgregar" value="Eliminar" class="boton"/>
		</td>
	</tr>	
</table>


<@vc.htmlContent 
  baseUrl="${request.contextPath}/deposito/ajusteExistencia/deleteAjusteExistencia.action" 
  source="store" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="ajusteExistencia.oid={oid}"/>
  
  <@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/deposito/view.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters=""/>