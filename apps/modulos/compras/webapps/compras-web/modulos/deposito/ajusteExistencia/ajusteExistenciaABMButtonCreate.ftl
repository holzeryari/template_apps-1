<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancel" type="button" name="btnVolver" value="Cancelar" class="boton"/>
				<input id="store" type="button" name="btnAgregar" value="Agregar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/deposito/ajusteExistencia/createAjusteExistencia.action" 
  source="store" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="ajusteExistencia.deposito.oid={ajusteExistencia.deposito.oid}, ajusteExistencia.observaciones={observaciones},ajusteExistencia.motivo={motivo}"/>
  
  <@vc.htmlContent 
    baseUrl="${request.contextPath}/compras/flowControl.action"  
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=buscar-ajusteExistencia,flowControl=back"/>