<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaSeparadora" class="capaSeparadora">&nbsp;</div>
<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancelar" type="button" name="btnVolver" value="Cancelar" class="boton"/>
				<input id="eliminar" type="button" name="btnAgregar" value="Eliminar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/deposito/ajusteExistencia/deleteAjusteExistenciaDetalle.action" 
  source="eliminar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="ajusteExistenciaDetalle.oid={ajusteExistenciaDetalle.oid},ajusteExistenciaDetalle.ajusteExistencia.oid={ajusteExistenciaDetalle.ajusteExistencia.oid}"/>
  
<@vc.htmlContent 
    baseUrl="${request.contextPath}/compras/flowControl.action"  
  source="cancelar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=ajusteExistencia-administrar,flowControl=back"/>
