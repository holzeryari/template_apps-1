<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancelar" type="button" name="btnVolver" value="Cancelar" class="boton"/>
				<input id="store" type="button" name="btnAgregar" value="Realizar Devoluci&oacute;n" class="boton"/>
			</td>
		</tr>	
	</table>
</div> 
    <@vc.htmlContent 
  baseUrl="${request.contextPath}/deposito/devolucionProductoBien/updateView.action" 
  source="modificardevolucionProductoBien" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="devolucionProductoBien.oid={devolucionProductoBien.oid},devolucionProductoBien.versionNumber={devolucionProductoBien.versionNumber},navigationId=devolucionProductoBien-actualizar"/>

  
  
  <@vc.htmlContent 
   baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancelar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=buscar-devolucionProductoBien,flowControl=back"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/deposito/devolucionProductoBien/realizarDevolucion.action" 
  source="store" 
  success="contentTrx" 
  failure="errorTrx"
  parameters="devolucionProductoBien.oid={devolucionProductoBien.oid}"/>
  

