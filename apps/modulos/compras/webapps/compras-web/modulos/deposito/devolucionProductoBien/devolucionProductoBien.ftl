	<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
		<tr>
        	<td class="textoCampo" colspan="4">&nbsp;</td>
        </tr>	
		<tr>
  			<td class="textoCampo">N&uacute;mero:</td>
  			<td class="textoDato">
  				<@s.property default="&nbsp;" escape=false value="devolucionProductoBien.numero"/></td>
  			<td class="textoCampo">Fecha Devoluci&oacute;n: </td>
			<td class="textoDato">
				<@s.if test="devolucionProductoBien.fechadevolucion != null">
					<#assign fechadevolucion = devolucionProductoBien.fechadevolucion> 
					${fechadevolucion?string("dd/MM/yyyy")}	
				</@s.if>				&nbsp;					
			</td>	      			
		</tr>	
		<tr>
      		<td class="textoCampo">Proveedor:</td>
			<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="devolucionProductoBien.proveedor.detalleDePersona.razonSocial" />
				<@s.property default="&nbsp;" escape=false value="devolucionProductoBien.proveedor.detalleDePersona.nombre" />
				<@s.a templateDir="custontemplates" id="seleccionarProveedor" name="seleccionarProveedor" href="javascript://nop/" cssClass="ocultarIcono">
					<img src="${request.contextPath}/common/images/buscar.gif"  border="0" align="absmiddle" hspace="3" >
				</@s.a>	
			</td>	
			<td class="textoCampo">Dep&oacute;sito:</td>
	      	<td class="textoDato">
				<@s.select 
					templateDir="custontemplates" 
					id="devolucionProductoBien.deposito.oid" 
					cssClass="textarea"
					cssStyle="width:165px" 
					name="devolucionProductoBien.deposito.oid" 
					list="depositoList" 
					listKey="oid" 
					listValue="descripcion" 
					value="devolucionProductoBien.deposito.oid"
					title="Dep&oacute;sito"
					headerKey="" 
	                headerValue="Todos" />
	      </td>
	   	</tr>
		<tr>
			<td class="textoCampo">Observaciones:</td>
	  		<td  class="textoDato" colspan="3">
			<@s.textarea	
					templateDir="custontemplates" 						  
					cols="89" rows="4"	      					
					cssClass="textarea"
					id="devolucionProductoBien.observaciones"							 
					name="devolucionProductoBien.observaciones"  
					label="Observaciones"														
					 />
			</td>					
		</tr>
		<tr>
			<td class="textoCampo">Estado:</td>
	  		<td  class="textoDato" colspan="3">
				<@s.property default="&nbsp;" escape=false value="devolucionProductoBien.estado"/></td>
			</td>
		</tr>	    	
		<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>	  
	</table>		
			

