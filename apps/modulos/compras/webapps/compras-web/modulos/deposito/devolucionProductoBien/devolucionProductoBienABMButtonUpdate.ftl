<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancelar" type="button" name="btnVolver" value="Cancelar" class="boton"/>
				<input id="update" type="button" name="btnModificar" value="Modificar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/deposito/devolucionProductoBien/update.action" 
  source="update" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="devolucionProductoBien.versionNumber={devolucionProductoBien.versionNumber},devolucionProductoBien.oid={devolucionProductoBien.oid},devolucionProductoBien.cliente.oid={devolucionProductoBien.cliente.oid},devolucionProductoBien.cliente.descripcion={devolucionProductoBien.cliente.descripcion},devolucionProductoBien.deposito.oid={devolucionProductoBien.deposito.oid},devolucionProductoBien.proveedor.nroProveedor={devolucionProductoBien.proveedor.nroProveedor},devolucionProductoBien.proveedor.razonSocial={devolucionProductoBien.proveedor.razonSocial},devolucionProductoBien.proveedor.nombre={devolucionProductoBien.proveedor.nombre},devolucionProductoBien.numeroFactura={devolucionProductoBien.numeroFactura},devolucionProductoBien.numeroRemito={devolucionProductoBien.numeroRemito},devolucionProductoBien.observaciones={devolucionProductoBien.observaciones}"/>
  

<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancelar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=devolucionProductoBien-administrar,flowControl=back"/>
  

  <@vc.htmlContent 
  baseUrl="${request.contextPath}/deposito/devolucionProductoBien/selectClienteABM.action" 
  source="seleccionarCliente" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=change,devolucionProductoBien.cliente.oid={devolucionProductoBien.cliente.oid},devolucionProductoBien.cliente.descripcion={devolucionProductoBien.cliente.descripcion},devolucionProductoBien.deposito.oid={devolucionProductoBien.deposito.oid},devolucionProductoBien.proveedor.nroProveedor={devolucionProductoBien.proveedor.nroProveedor},devolucionProductoBien.proveedor.razonSocial={devolucionProductoBien.proveedor.razonSocial},devolucionProductoBien.proveedor.nombre={devolucionProductoBien.proveedor.nombre},devolucionProductoBien.numeroFactura={devolucionProductoBien.numeroFactura},devolucionProductoBien.numeroRemito={devolucionProductoBien.numeroRemito},devolucionProductoBien.observaciones={devolucionProductoBien.observaciones}"/>
  

    