<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<@s.hidden id="navigationId" name="navigationId"/>
<@s.hidden id="devolucionProductoBien.oid" name="devolucionProductoBien.oid"/>
<@s.hidden id="devolucionProductoBien.versionNumber" name="devolucionProductoBien.versionNumber"/>
<@s.hidden id="devolucionProductoBien.cliente.oid" name="devolucionProductoBien.cliente.oid"/>
<@s.hidden id="devolucionProductoBien.cliente.descripcion" name="devolucionProductoBien.cliente.descripcion"/>
<@s.hidden id="devolucionProductoBien.proveedor.nroProveedor" name="devolucionProductoBien.proveedor.nroProveedor"/>
<@s.hidden id="devolucionProductoBien.proveedor.detalleDePersona.razonSocial" name="devolucionProductoBien.proveedor.detalleDePersona.razonSocial"/>
<@s.hidden id="devolucionProductoBien.proveedor.detalleDePersona.nombre" name="devolucionProductoBien.proveedor.detalleDePersona.nombre"/>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
			<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	

	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos de la Devoluci&oacute;n de Producto/Bien</b></td>
				<td>
					<div align="right">
						<@s.a templateDir="custontemplates" id="modificardevolucionProductoBien" name="modificardevolucionProductoBien" href="javascript://nop/" cssClass="ocultarIcono">
							<b>Modificar</b><img src="${request.contextPath}/common/images/modificar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
						</@s.a>
					</div>
				</td>
			</tr>
		</table>
				
		<@tiles.insertAttribute name="devolucion"/>
	</div>	
	<@tiles.insertAttribute name="oc"/>
	<@tiles.insertAttribute name="detalle"/>
			
		
