<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<@s.hidden id="devolucionProductoBienDetalle.oid" name="devolucionProductoBienDetalle.oid"/>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
			<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos de la Recepci&oacute;n del Producto/Bien</b></td>
			</tr>
		</table>
	
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
			<tr>
      			<td class="textoCampo">N&uacute;mero:</td>
      			<td class="textoDato">
      				<@s.property default="&nbsp;" escape=false value="devolucionProductoBienDetalle.devolucionProductoBien.numero"/></td>
      			<td  class="textoCampo">Fecha Devoluci&oacute;n: </td>
				<td class="textoDato">
					<@s.if test="devolucionProductoBienDetalle.devolucionProductoBien.fechadevolucion != null">
						<#assign fechadevolucion = devolucionProductoBienDetalle.devolucionProductoBien.fechadevolucion> 
						${fechadevolucion?string("dd/MM/yyyy")}	
					</@s.if>				&nbsp;					
				</td>	      			
			</tr>	
			<tr>
	      		<td class="textoCampo">Proveedor</td>
				<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="devolucionProductoBienDetalle.devolucionProductoBien.proveedor.detalleDePersona.razonSocial" />
					<@s.property default="&nbsp;" escape=false value="devolucionProductoBienDetalle.devolucionProductoBien.proveedor.detalleDePersona.nombre" />
				<td class="textoCampo">Dep&oacute;sito</td>
  				<td class="textoDato">
  					<@s.property default="&nbsp;" escape=false value="devolucionProductoBienDetalle.devolucionProductoBien.deposito.descripcion" />
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Observaciones:</td>
	      		<td  class="textoDato" colspan="3">
	      		<@s.property default="&nbsp;" escape=false value="devolucionProductoBienDetalle.devolucionProductoBien.observaciones" />
				</td>					
	    	</tr>
	    	<tr>
				<td class="textoCampo">Estado:</td>
      			<td  class="textoDato" colspan="3">
					<@s.property default="&nbsp;" escape=false value="devolucionProductoBienDetalle.devolucionProductoBien.estado"/></td>
				</td>
			</tr>	    	
    		<tr><td class="textoCampo" colspan="4">&nbsp;</td></tr>
		</table>		    			
	</div>
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos del Producto/Bien</b></td>
			</tr>
		</table>
	
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
			<tr>
      			<td class="textoCampo">C&oacute;digo:</td>
      			<td class="textoDato">
      				<@s.property default="&nbsp;" escape=false value="devolucionProductoBienDetalle.productoBien.oid"/>
      			</td>
      			<td  class="textoCampo">Descripci&oacute;n: </td>
				<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="devolucionProductoBienDetalle.productoBien.descripcion"/>		
				</td>	      			
			</tr>	
			<tr>
      			<td class="textoCampo">Tipo:</td>
	      		<td class="textoDato">
	      			<@s.property default="&nbsp;" escape=false value="devolucionProductoBienDetalle.productoBien.tipo"/>						      			
	      		</td>		      		
				<td class="textoCampo">Rubro:</td>
  				<td class="textoDato">
  					<@s.property default="&nbsp;" escape=false value="devolucionProductoBienDetalle.productoBien.rubro.descripcion"/>
				</td>
    		</tr>
			<tr>
				<td class="textoCampo">Marca: </td>
				<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="devolucionProductoBienDetalle.productoBien.marca"/>										
				</td>
				<td class="textoCampo">Modelo: </td>
				<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="devolucionProductoBienDetalle.productoBien.modelo"/>										
				</td>
    		</tr>
    		<tr>
				<td class="textoCampo">Valor Mercado: </td>
				<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="bien.valorMercado"/>
				</td>
				<td class="textoCampo">Fecha Valor Mercado: </td>
				<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="bien.fechaValorMercado"/>
				</td>
    		</tr>
			<tr>
				<td class="textoCampo">C&oacute;digo de Barra: </td>
				<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="devolucionProductoBienDetalle.productoBien.codigoBarra"/>										
				</td>
				<td class="textoCampo">Es Cr&iacute;tico: </td>
				<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="devolucionProductoBienDetalle.productoBien.critico"/>						      			
				</td>
    		</tr>
    		<tr>
				<td class="textoCampo">Reposici&oacute;n Autm&aacute;tica: </td>
				<td  class="textoDato">	
					<@s.property default="&nbsp;" escape=false value="producto.reposicionAutomatica"/>						      											
				</td>
				<td class="textoCampo">Existencia M&iacute;nima: </td>
				<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="producto.existenciaMinima"/>
				</td>
    		</tr>
			<tr>
    		<td class="textoCampo">Tipo Unidad:</td>
    		<td  class="textoDato" colspan="3">
					<@s.property default="&nbsp;" escape=false value="producto.tipoUnidad.codigo"/>
				</td>
    		</tr>
			<tr>
				<td class="textoCampo">Es Registrable: </td>
				<td  class="textoDato" colspan="3">
				<@s.property default="&nbsp;" escape=false value="bien.registrable"/>						      			
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Es Veh&iacute;culo: </td>
				<td  class="textoDato" colspan="3">
					<@s.property default="&nbsp;" escape=false value="bien.vehiculo"/>
				</td>
    		</tr>							
			<tr>
				<td class="textoCampo">Estado: </td>
      			<td class="textoDato" colspan="3">
      				<@s.property default="&nbsp;" escape=false value="productoBien.estado"/></td>
    		</tr>
    		<tr>
				<td class="textoCampo">Cantidad Recibida: </td>
				<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="devolucionProductoBienDetalle.cantidadRecibidaTotal"/>
				</td>
    		
				<td class="textoCampo">Cantidad a Devolver: </td>
				<td  class="textoDato">
					<@s.textfield									
						templateDir="custontemplates"
						template="textMoney"  
						id="devolucionProductoBienDetalle.cantidadDevuelta" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="devolucionProductoBienDetalle.cantidadDevuelta" 
						title="Cantidad Devuelta" />							      			
				</td>
			</tr>
			<tr><td class="textoCampo" colspan="4">&nbsp;</td></tr>
		</table>		    			
	</div>
	