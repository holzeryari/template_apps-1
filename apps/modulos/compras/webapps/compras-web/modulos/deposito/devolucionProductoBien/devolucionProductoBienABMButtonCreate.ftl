<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancel" type="button" name="btnVolver" value="Cancelar" class="boton"/>
				<input id="store" type="button" name="btnAgregar" value="Agregar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=buscar-devolucionProductoBien,flowControl=back"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/deposito/devolucionProductoBien/create.action" 
  source="store" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="devolucionProductoBien.deposito.oid={devolucionProductoBien.deposito.oid},devolucionProductoBien.proveedor.nroProveedor={devolucionProductoBien.proveedor.nroProveedor},devolucionProductoBien.proveedor.razonSocial={devolucionProductoBien.proveedor.razonSocial},devolucionProductoBien.proveedor.nombre={devolucionProductoBien.proveedor.nombre},devolucionProductoBien.observaciones={devolucionProductoBien.observaciones}"/>
  
  
  <@vc.htmlContent 
  baseUrl="${request.contextPath}/deposito/devolucionProductoBien/selectProveedorABM.action" 
  source="seleccionarProveedor" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=change,devolucionProductoBien.deposito.oid={devolucionProductoBien.deposito.oid},devolucionProductoBien.proveedor.nroProveedor={devolucionProductoBien.proveedor.nroProveedor},devolucionProductoBien.proveedor.razonSocial={devolucionProductoBien.proveedor.razonSocial},devolucionProductoBien.proveedor.nombre={devolucionProductoBien.proveedor.nombre},devolucionProductoBien.observaciones={devolucionProductoBien.observaciones}"/>
  
  <@vc.htmlContent 
  baseUrl="${request.contextPath}/deposito/devolucionProductoBien/selectClienteABM.action" 
  source="seleccionarCliente" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=change,devolucionProductoBien.deposito.oid={devolucionProductoBien.deposito.oid},devolucionProductoBien.proveedor.nroProveedor={devolucionProductoBien.proveedor.nroProveedor},devolucionProductoBien.proveedor.razonSocial={devolucionProductoBien.proveedor.razonSocial},devolucionProductoBien.proveedor.nombre={devolucionProductoBien.proveedor.nombre},devolucionProductoBien.observaciones={devolucionProductoBien.observaciones}"/>
  

  