
<#setting locale="es_AR">

<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>


<@s.hidden id="navigationId" name="navigationId"/>
<@s.hidden id="devolucionProductoBien.proveedor.nroProveedor" name="devolucionProductoBien.proveedor.nroProveedor"/>
<@s.hidden id="devolucionProductoBien.proveedor.detalleDePersona.razonSocial" name="devolucionProductoBien.proveedor.detalleDePersona.razonSocial"/>
<@s.hidden id="devolucionProductoBien.proveedor.detalleDePersona.nombre" name="devolucionProductoBien.proveedor.detalleDePersona.nombre"/>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
        	<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Datos de la Devoluci&oacute;n de Producto/Bien</b></td>
				<td>
					<div align="right">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0461" 
							enabled="true" 
							cssClass="item" 
							id="crear"										
							href="javascript://nop/">
							<b>Agregar</b>										
							<img src="${request.contextPath}/common/images/agregar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
						</@security.a>				
					</div>
				</td>
			</tr>
		</table>
		
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
	    		<td class="textoCampo" colspan="4">&nbsp;</td>
	    	</tr>
			<tr>
				<td class="textoCampo">N&uacute;mero:</td>
	      		<td class="textoDato">
	      			<@s.textfield 
	      					templateDir="custontemplates" 
							id="devolucionProductoBien.numero" 
							cssClass="textarea"
							cssStyle="width:160px" 
							name="devolucionProductoBien.numero" 
							title="N&uacute;mero" />
				</td>							
	      		<td class="textoCampo">Proveedor</td>
				<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="devolucionProductoBien.proveedor.detalleDePersona.razonSocial" />
					<@s.property default="&nbsp;" escape=false value="devolucionProductoBien.proveedor.detalleDePersona.nombre" />
					<@s.a templateDir="custontemplates" id="seleccionarProveedor" name="seleccionarProveedor" href="javascript://nop/" cssClass="ocultarIcono">
							<img src="${request.contextPath}/common/images/buscar.gif"  border="0" align="absmiddle" hspace="3" >
					</@s.a>	

				</td>						
			</tr>
			<tr>
				<td class="textoCampo">Dep&oacute;sito</td>
	      		<td class="textoDato">
					<@s.select 
						templateDir="custontemplates" 
						id="devolucionProductoBien.deposito.oid" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="devolucionProductoBien.deposito.oid" 
						list="depositoList" 
						listKey="oid" 
						listValue="descripcion" 
						value="devolucionProductoBien.deposito.oid"
						title="Dep&oacute;sito"
						headerKey="" 
	                    headerValue="Todos" />
				<td class="textoCampo">Estado:</td>
	      		<td class="textoDato">
	      			<@s.select 
						templateDir="custontemplates" 
						id="estado" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="estado" 
						list="estadoList" 
						listKey="key" 
						listValue="description" 
						value="devolucionProductoBien.estado.ordinal()"
						title="Estado"
						headerKey="0"
						headerValue="Todos"							
						/>
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Fecha Devoluci&oacute;n Desde:</td>
	      		<td class="textoDato">
						<@vc.rowCalendar 
						templateDir="custontemplates" 
						id="fechaDesde" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="fechaDesde" 
						title="Fecha Desde" />
				</td>
				<td class="textoCampo">Fecha Devoluci&oacute;n Hasta:</td>
	      		<td class="textoDato">
					<@vc.rowCalendar 
						templateDir="custontemplates" 
						id="fechaHasta" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="fechaHasta" 
						title="Fecha Hasta" />
					</td>
				</tr>
				<tr>
	    			<td colspan="4" class="lineaGris" align="right">
	      				<input id="limpiar" type="button" name="btnVolver" value="Limpiar" class="boton"/>		
	      				<input id="buscar" type="button" name="btnVolver" value="Buscar" class="boton"/>
	    			</td>
				</tr>	
			</table>
		</div>
		<!-- Resultado Filtro -->
		<@s.if test="devolucionProductoBienList!=null">
			<#assign devolucionNumero = 0 />
					<#if devolucionProductoBien.numero?exists && devolucionProductoBien.numero?exists>
					<#assign devolucionNumero =devolucionProductoBien.numero>
				</#if>

			
				<#assign devolucionProveedor =0>
				<#if devolucionProductoBien.proveedor?exists && devolucionProductoBien.proveedor.nroProveedor?exists>
					<#assign devolucionProveedor =devolucionProductoBien.proveedor.nroProveedor>
				</#if>
			
				<#assign devolucionDeposito = 0 />
				<@s.if test="devolucionProductoBien.deposito != null && devolucionProducto.deposito.oid != null">
					<#assign devolucionDeposito = "${devolucionProductoBien.deposito.oid}" />
				</@s.if>					

				<#assign devolucionEstado = "0" />
				<@s.if test="devolucionProductoBien.estado != null ">
					<#assign devolucionEstado = "${devolucionProductoBien.estado.ordinal()}" />
				</@s.if>
				

				<#assign devolucionFechaDesde = "" />
				<@s.if test="fechaDesde != null">
					<#assign devolucionFechaDesde = "${fechaDesde?string('dd/MM/yyyy')}" />
				</@s.if>

				<#assign devolucionFechaHasta = "" />
				<@s.if test="fechaHasta != null">
					<#assign devolucionFechaHasta = "${fechaHasta?string('dd/MM/yyyy')}" />
				</@s.if>
			<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
				<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Devoluciones de Productos/Bienes encontrados</td>
					<td>
						<div class="alineacionDerecha">
							<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0462" 
								cssClass="item" 
								href="${request.contextPath}/deposito/devolucionProductoBien/printXLS.action?devolucionProductoBien.estado=${devolucionEstado}&devolucionProductoBien.proveedor.nroProveedor=${devolucionProveedor}&devolucionProducto.deposito.oid=${devolucionDeposito}&devolucionProductoBien.numero=${devolucionNumero}&fechaDesde=${devolucionFechaDesde}&fechaHasta=${devolucionFechaHasta}">
								<img src="${request.contextPath}/common/images/imprimirExcel.gif" title="Exportar a Excel" align="absmiddle" border="0" hspace="3" >
							</@security.a>
						</div>
						<div class="alineacionDerecha">
							<b>Imprimir</b>
							<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0462" 
								cssClass="item" 
								href="${request.contextPath}/deposito/devolucionProductoBien/printPDF.action?devolucionProductoBien.estado=${devolucionEstado}&devolucionProductoBien.proveedor.nroProveedor=${devolucionProveedor}&devolucionProducto.deposito.oid=${devolucionDeposito}&devolucionProductoBien.numero=${devolucionNumero}&fechaDesde=${devolucionFechaDesde}&fechaHasta=${devolucionFechaHasta}">
								<img src="${request.contextPath}/common/images/imprimirPdf.gif" title="Exportar a PDF" align="absmiddle" border="0"  hspace="3" >
							</@security.a>
						</div>	
					</td>
				</tr>
			</table>
			<@vc.anchors target="contentTrx">			
          		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="devolucionProductoBienList" id="devolucionProductoBien" pagesize=15 defaultsort=2 partialList=true size="recordSize" keepStatus=true excludedParams="resetFlag">
          		
  		
					<@display.column headerClass="tbl-contract-service-select" class="botoneraAnchoCon4" title="Acciones">
						<div class="alineacion">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0463" 
							enabled="devolucionProductoBien.readable" 
							cssClass="item" 
							href="${request.contextPath}/deposito/devolucionProductoBien/readView.action?devolucionProductoBien.oid=${devolucionProductoBien.oid?c}&navigationId=devolucionProductoBien-visualizar&flowControl=regis&navegacionIdBack=${navigationId}">
							<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver"  border="0">
						</@security.a>
						</div>
						<div class="alineacion">	
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0464" 
							enabled="devolucionProductoBien.updatable"
							cssClass="item"  
							href="${request.contextPath}/deposito/devolucionProductoBien/administrarView.action?devolucionProductoBien.oid=${devolucionProductoBien.oid?c}&navigationId=devolucionProductoBien-administrar&flowControl=regis">
							<img  src="${request.contextPath}/common/images/administrar.gif" alt="Administrar" title="Administrar"  border="0">
						</@security.a>
						</div>
						<div class="alineacion">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0465" 
							enabled="devolucionProductoBien.eraseable"
							cssClass="item"  
							href="${request.contextPath}/deposito/devolucionProductoBien/deleteView.action?devolucionProductoBien.oid=${devolucionProductoBien.oid?c}&navigationId=devolucionProductoBien-eliminar&flowControl=regis">
							<img  src="${request.contextPath}/common/images/eliminar.gif" alt="Eliminar" title="Eliminar"  border="0">
						</@security.a>				
						</div>
						<div class="alineacion">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0463" 
							enabled="devolucionProductoBien.readable" 
							cssClass="no-rewrite" 
							href="${request.contextPath}/deposito/devolucionProductoBien/printDevolucionProductoBienPDF.action?devolucionProductoBien.oid=${devolucionProductoBien.oid?c}">
							<img  src="${request.contextPath}/common/images/imprimir.gif" alt="Imprimir" title="Imprimir"  border="0">
						</@security.a>		
									
						</div>
					</@display.column>


					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="numero" title="Nro." />				
										
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" title="Proveedor">  
						${devolucionProductoBien.proveedor.detalleDePersona.razonSocial}
						<#if devolucionProductoBien.proveedor.detalleDePersona.nombre?exists> 
							 ${devolucionProductoBien.proveedor.detalleDePersona.nombre}
						</#if>
						
						
						
					</@display.column>
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="fechaDevolucion" format="{0,date,dd/MM/yyyy}"  title="Fecha Devoluci&oacute;n" /> 
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="deposito.descripcion" title="Dep&oacute;sito" />
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="estado" title="Estado" />		

				</@display.table>
			</@vc.anchors>
		</div>
	</@s.if>
	  

<@vc.htmlContent 
  baseUrl="${request.contextPath}/deposito/devolucionProductoBien/search.action"
  source="buscar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="resetFlag=true,navigationId={navigationId},flowControl=regis,devolucionProductoBien.estado={estado},devolucionProductoBien.proveedor.nroProveedor={devolucionProductoBien.proveedor.nroProveedor},devolucionProductoBien.proveedor.detalleDePersona.razonSocial={devolucionProductoBien.proveedor.detalleDePersona.razonSocial},devolucionProductoBien.proveedor.detalleDePersona.nombre={devolucionProductoBien.proveedor.detalleDePersona.nombre},devolucionProductoBien.deposito.oid={devolucionProductoBien.deposito.oid},devolucionProductoBien.numero={devolucionProductoBien.numero},fechaDesde={fechaDesde},fechaHasta={fechaHasta}"/>
  		      

<@vc.htmlContent 
  baseUrl="${request.contextPath}/deposito/devolucionProductoBien/view.action"
  source="limpiar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=regis"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/deposito/devolucionProductoBien/createView.action" 
  source="crear" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=devolucionProductoBien-crear,flowControl=regis"/>
  
  <@vc.htmlContent 
  baseUrl="${request.contextPath}/deposito/devolucionProductoBien/selectProveedorSearch.action" 
  source="seleccionarProveedor" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=change,devolucionProductoBien.estado={estado},devolucionProductoBien.proveedor.nroProveedor={devolucionProductoBien.proveedor.nroProveedor},devolucionProductoBien.proveedor.detalleDePersona.razonSocial={devolucionProductoBien.proveedor.detalleDePersona.razonSocial},devolucionProductoBien.proveedor.detalleDePersona.nombre={devolucionProductoBien.proveedor.detalleDePersona.nombre},devolucionProductoBien.deposito.oid={devolucionProductoBien.deposito.oid},devolucionProductoBien.numero={devolucionProductoBien.numero},fechaDesde={fechaDesde},fechaHasta={fechaHasta}"/>
  
  <@vc.htmlContent 
  baseUrl="${request.contextPath}/deposito/devolucionProductoBien/selectClienteSearch.action" 
  source="seleccionarCliente" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=change,devolucionProductoBien.estado={estado},devolucionProductoBien.proveedor.nroProveedor={devolucionProductoBien.proveedor.nroProveedor},devolucionProductoBien.proveedor.detalleDePersona.razonSocial={devolucionProductoBien.proveedor.detalleDePersona.razonSocial},devolucionProductoBien.proveedor.detalleDePersona.nombre={devolucionProductoBien.proveedor.detalleDePersona.nombre},devolucionProductoBien.deposito.oid={devolucionProductoBien.deposito.oid},devolucionProductoBien.numero={devolucionProductoBien.numero},fechaDesde={fechaDesde},fechaHasta={fechaHasta}"/>
  
  
