
<#setting locale="es_AR">

<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>


<@s.hidden id="navigationId" name="navigationId"/>
<@s.hidden id="navegacionIdBack" name="navegacionIdBack"/>
<@s.hidden id="egresoProducto.clienteDestino.oid" name="egresoProducto.clienteDestino.oid"/>
<@s.hidden id="estadoDuro" name="estadoDuro"/>
<@s.hidden id="tipoDestinoDuro" name="tipoDestinoDuro"/>
<@s.hidden id="depositoDestinoDuro" name="depositoDestinoDuro"/>

  
  <@s.if test="estadoDuro == true">
		<@s.hidden id="egresoProducto.estado" name="egresoProducto.estado.ordinal()"/>
  </@s.if>
  
  <@s.if test="tipoDestinoDuro == true">
		<@s.hidden id="egresoProducto.tipoDestino" name="egresoProducto.tipoDestino.ordinal()"/>
  </@s.if> 
  
  <@s.if test="depositoDestinoDuro == true">
		<@s.hidden id="egresoProducto.depositoDestino.oid" name="egresoProducto.depositoDestino.oid"/>
  </@s.if>
  
  
<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
        	<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Datos del Egreso de Producto</b></td>
			</tr>
		</table>
		
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
	    		<td class="textoCampo" colspan="4">&nbsp;</td>
	    	</tr>
			<tr>
				<td class="textoCampo">N&uacute;mero:</td>
      			<td class="textoDato"colspan="3" >
      			<@s.textfield 
      					templateDir="custontemplates" 
						id="egresoProducto.numero" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="egresoProducto.numero" 
						title="N&uacute;mero" />
				</td>						
      		</tr>
			<tr>
				<td class="textoCampo">Dep&oacute;sito Origen:</td>
	      		<td class="textoDato">
					<@s.select 
							templateDir="custontemplates" 
							id="egresoProducto.depositoOrigen.oid" 
							cssClass="textarea"
							cssStyle="width:165px" 
							name="egresoProducto.depositoOrigen.oid" 
							list="depositoOrigenList" 
							listKey="oid" 
							listValue="descripcion" 
							value="egresoProducto.depositoOrigen.oid"
							title="Deposito Origen"
							headerKey="" 
                            headerValue="Todos" />
               </td>						
	      		<td class="textoCampo">Tipo Destino:</td>
	      		<td class="textoDato">
					<@s.select 
						templateDir="custontemplates" 
						id="egresoProducto.tipoDestino" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="egresoProducto.tipoDestino" 
						list="tipoDestinoList" 
						listKey="key" 
						listValue="description" 
						value="egresoProducto.tipoDestino.ordinal()"
						title="Motivo"
						headerKey="0" 
	                    headerValue="Todos"
						 />
	           </td>
			</tr>
			<tr>
				<td class="textoCampo">Dep&oacute;sito Destino:</td>
	      		<td class="textoDato">												
					<@s.select 
						templateDir="custontemplates" 
						id="egresoProducto.depositoDestino.oid" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="egresoProducto.depositoDestino.oid" 
						list="depositoDestinoList" 
						listKey="oid" 
						listValue="descripcion" 
						value="egresoProducto.depositoDestino.oid"
						title="Deposito Destino"
						headerKey="" 
                        headerValue="Todos" />
				</td>
				<td class="textoCampo">Cliente Destino:</td>
	      		<td class="textoDato">
	      			<@s.textfield 
      					templateDir="custontemplates" 
						id="egresoProducto.clienteDestino.descripcion" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="egresoProducto.clienteDestino.descripcion" 
						title="Cliente Destino" />								
					<@s.a templateDir="custontemplates" id="seleccionarCliente" name="seleccionarCliente" href="javascript://nop/" cssClass="ocultarIcono">
						<img src="${request.contextPath}/common/images/buscar.gif" border="0" align="absmiddle" hspace="3" >
					</@s.a>		
				</td>				
			</tr>
			<tr>
				<td class="textoCampo">Estado:</td>
	      		<td class="textoDato"colspan="3">
	      			<@s.select 
							templateDir="custontemplates" 
							id="egresoProducto.estado" 
							cssClass="textarea"
							cssStyle="width:165px" 
							name="egresoProducto.estado" 
							list="estadoList" 
							listKey="key" 
							listValue="description" 
							value="egresoProducto.estado.ordinal()"
							title="Estado"
							headerKey="0"
							headerValue="Todos"							
							/>
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Fecha Desde:</td>
      			<td class="textoDato">
    			<@s.textfield 
      					templateDir="custontemplates" 
						id="fechaDesde"							  
						cssClass="textarea"
						cssStyle="width:160px" 
						name="fechaDesde" 
						title="Fecha Desde"
						/>
				</td>
				<td class="textoCampo">Fecha Hasta:</td>
      			<td class="textoDato">
				<@s.textfield 
      					templateDir="custontemplates" 
						id="fechaHasta" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="fechaHasta" 
						title="Fecha Hasta"
						 />
				</td>
			</tr>
			<tr>
	    		<td colspan="4" class="lineaGris" align="right">
	      			<input id="limpiar" type="button" name="btnVolver" value="Limpiar" class="boton"/>		
	      			<input id="buscar" type="button" name="btnVolver" value="Buscar" class="boton"/>
	    		</td>
			</tr>	
		</table>
	</div>	
	<!-- Resultado Filtro -->
	<@s.if test="egresoProductoList!=null">
		<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">			
			<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Egresos de Productos encontrados</td>
				</tr>
			</table>

			<@vc.anchors target="contentTrx">			
          		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="egresoProductoList" id="egresoProductoSelect" pagesize=15  defaultsort=2 partialList=true size="recordSize" keepStatus=true excludedParams="resetFlag">	
          			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" style=" text-align:center; display:block" title="Acciones">
  						<@s.if test="navegacionIdBack == 'abm-ingresoProducto-create'">
						  	<#assign egresoProductoNumero = "${egresoProductoSelect.numero}" />
								<@s.if test="egresoProducto.numero != null">
								<#assign egresoProductoNumero = "${egresoProducto.numero}" />
							</@s.if>
							<@s.a id="select${egresoProductoSelect.oid}"href="javascript://nop/" name="select${egresoProductoSelect.oid}" cssClass="no-rewrite" >
								<img src="${request.contextPath}/common/images/seleccionar.gif" alt="Seleccionar" title="Seleccionar" width="16" height="16" border="0">
							</@s.a>
							<@vc.htmlContent 
							  baseUrl="${request.contextPath}/deposito/egresoProducto/selectEgresoProducto.action" 
							  source="select${egresoProductoSelect.oid}" 
							  success="contentTrx" 
							  failure="errorTrx" 
							  parameters="egresoProducto.oid=${egresoProductoSelect.oid},egresoProducto.numero=${egresoProductoSelect.numero},navegacionIdBack=${navegacionIdBack},navigationId=${navigationId}"/>
							</@s.if>
							<@s.else>
							  <#assign egresoProductoNumero = "0" />
									<@s.if test="egresoProducto.numero != null">
									<#assign egresoProductoNumero = "${egresoProductoSelect.numero}" />
								</@s.if>
								<@s.a id="select${egresoProductoSelect.oid}" href="javascript://nop/" name="select${egresoProductoSelect.oid}" cssClass="no-rewrite" >
									<img src="${request.contextPath}/common/images/seleccionar.gif" alt="Seleccionar" title="Seleccionar" width="16" height="16" border="0">
								</@s.a>
								<@vc.htmlContent 
								  baseUrl="${request.contextPath}/deposito/egresoProducto/selectEgresoProductoSinValidacion.action" 
								  source="select${egresoProductoSelect.oid}"  
								  success="contentTrx" 
								  failure="errorTrx" 
								  parameters="egresoProducto.oid=${egresoProductoSelect.oid},egresoProducto.numero=${egresoProductoSelect.numero},navegacionIdBack=${navegacionIdBack},navigationId=${navigationId}"/>
									
							</@s.else>
					</@display.column>
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="numero" title="N&uacute;mero" />				
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="fecha" format="{0,date,dd/MM/yyyy}" title="Fecha Egreso" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="depositoOrigen.descripcion" title="Dep&oacute;sito Origen" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="tipoDestino" title="Tipo Destino" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="depositoDestino.descripcion" title="Dep&oacute;sito Destino" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="clienteDestino.descripcion" title="Cliente Destino" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="estado" title="Estado" />		

				</@display.table>
			</@vc.anchors>
		</div>
	</@s.if>
			
	<div id="capaBotonera" class="capaBotonera">
		<table id="tablaBotonera" class="tablaBotonera">
			<tr> 
				<td align="left">
					<input id="cancel" type="button" name="btnVolver" value="Volver" class="boton"/>
				</td>
			</tr>	
		</table>
	</div>
	

<@vc.htmlContent 
  baseUrl="${request.contextPath}/deposito/egresoProducto/selectSearch.action"
  source="buscar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="resetFlag=true,navegacionIdBack={navegacionIdBack},navigationId={navigationId},flowControl=regis,egresoProducto.estado={egresoProducto.estado},egresoProducto.tipoDestino={egresoProducto.tipoDestino},egresoProducto.clienteDestino.oid={egresoProducto.clienteDestino.oid},egresoProducto.clienteDestino.descripcion={egresoProducto.clienteDestino.descripcion},egresoProducto.depositoOrigen.oid={egresoProducto.depositoOrigen.oid},egresoProducto.depositoDestino.oid={egresoProducto.depositoDestino.oid},egresoProducto.numero={egresoProducto.numero},estadoDuro={estadoDuro},tipoDestinoDuro={tipoDestinoDuro},depositoDestinoDuro={depositoDestinoDuro},fechaDesde={fechaDesde},fechaHasta={fechaHasta}"/>
  		      

<@vc.htmlContent 
  baseUrl="${request.contextPath}/deposito/egresoProducto/selectView.action"
  source="limpiar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navegacionIdBack={navegacionIdBack},navigationId={navigationId},flowControl=regis,egresoProducto.estado={egresoProducto.estado},estadoDuro={estadoDuro},tipoDestinoDuro={tipoDestinoDuro},egresoProducto.depositoDestino.oid={egresoProducto.depositoDestino.oid},depositoDestinoDuro={depositoDestinoDuro}"/>
  
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/deposito/egresoProducto/selectClienteSearch.action" 
  source="seleccionarCliente" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=change,navegacionIdBack={navegacionIdBack},egresoProducto.estado={egresoProducto.estado},egresoProducto.tipoDestino={egresoProducto.tipoDestino},egresoProducto.clienteDestino.oid={egresoProducto.clienteDestino.oid},egresoProducto.clienteDestino.descripcion={egresoProducto.clienteDestino.descripcion},egresoProducto.depositoDestino.oid={egresoProducto.depositoDestino.oid},egresoProducto.depositoOrigen.oid={egresoProducto.depositoOrigen.oid},egresoProducto.numero={egresoProducto.numero},estadoDuro={estadoDuro},fechaDesde={fechaDesde},fechaHasta={fechaHasta},tipoDestinoDuro={tipoDestinoDuro},depositoDestinoDuro={depositoDestinoDuro}"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navegacionIdBack},flowControl=back"/>
