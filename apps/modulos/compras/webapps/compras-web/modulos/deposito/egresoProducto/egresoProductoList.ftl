
<#setting locale="es_AR">

<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>

<@s.hidden id="navigationId" name="navigationId"/>
<@s.hidden id="egresoProducto.clienteDestino.oid" name="egresoProducto.clienteDestino.oid"/>
<@s.hidden id="egresoProducto.clienteDestino.descripcion" name="egresoProducto.clienteDestino.descripcion"/>
<@s.hidden id="productoBien.oid" name="productoBien.oid"/>
<@s.hidden id="productoBien.descripcion" name="productoBien.descripcion"/>
					
<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
        	<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Datos del Egreso de Producto</b></td>
				<td>
					<div align="right">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0166" 
							enabled="egresoProducto.readable" 
							cssClass="item" 
							id="crear"										
							href="javascript://nop/">
							<b>Agregar</b>										
							<img src="${request.contextPath}/common/images/agregar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
						</@security.a>						
					</div>
				</td>
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
	    		<td class="textoCampo" colspan="4">&nbsp;</td>
	    	</tr>		
	    	
	    	<tr>
				<td class="textoCampo">Producto/Bien:</td>
				<td class="textoDato">
				
					<@s.property default="&nbsp;" escape=false value="productoBien.descripcion"/>					
					<@s.a href="javascript://nop/"
						templateDir="custontemplates" id="seleccionarProductoBien" name="seleccionarProductoBien" cssClass="ocultarIcono">
						<img src="${request.contextPath}/common/images/buscar.gif" border="0" align="absmiddle" hspace="3" >
					</@s.a>	
				</td>
				<td class="textoCampo" align="right">&nbsp;</td>
				<td class="textoDato" align="left">&nbsp;</td>
			</tr>
	    	
			<tr>
				<td class="textoCampo">N&uacute;mero:</td>
	      		<td class="textoDato" colspan="3">
	      			<@s.textfield 
      					templateDir="custontemplates" 
						id="egresoProducto.numero" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="egresoProducto.numero" 
						title="N&uacute;mero" />
				</td>				
			</tr>
			<tr>
				<td class="textoCampo">Dep&oacute;sito Origen:</td>
	      		<td class="textoDato">
					<@s.select 
						templateDir="custontemplates" 
						id="egresoProducto.depositoOrigen.oid" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="egresoProducto.depositoOrigen.oid" 
						list="depositoOrigenList" 
						listKey="oid" 
						listValue="descripcion" 
						value="egresoProducto.depositoOrigen.oid"
						title="Deposito Origen"
						headerKey="" 
                        headerValue="Todos" />
	            </td>						
	      		<td class="textoCampo">Tipo Destino:</td>
	      		<td class="textoDato">
					<@s.select 
						templateDir="custontemplates" 
						id="egresoProducto.tipoDestino" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="egresoProducto.tipoDestino" 
						list="tipoDestinoList" 
						listKey="key" 
						listValue="description" 
						value="egresoProducto.tipoDestino.ordinal()"
						title="Motivo"
						headerKey="0" 
	                    headerValue="Todos"
						 />
	          </td>
			</tr>
			<tr>
				<td class="textoCampo">Dep&oacute;sito Destino:</td>
      			<td class="textoDato">												
				<@s.select 
							templateDir="custontemplates" 
							id="egresoProducto.depositoDestino.oid" 
							cssClass="textarea"
							cssStyle="width:165px" 
							name="egresoProducto.depositoDestino.oid" 
							list="depositoDestinoList" 
							listKey="oid" 
							listValue="descripcion" 
							value="egresoProducto.depositoDestino.oid"
							title="Deposito Destino"
							headerKey="" 
                            headerValue="Todos" />
				</td>
				<td class="textoCampo" >Cliente Destino:</td>
      			<td class="textoDato">
      				<@s.property default="&nbsp;" escape=false value="egresoProducto.clienteDestino.descripcion"/>
      				<@s.property default="&nbsp;" escape=false value="egresoProducto.fecha"/>							
					<@s.a templateDir="custontemplates" id="seleccionarCliente" name="seleccionarCliente" href="javascript://nop/">
						<img src="${request.contextPath}/common/images/buscar.gif" border="0" align="absmiddle" hspace="3" >
					</@s.a>		
				</td>				
			</tr>
			<tr>
				<td class="textoCampo">Estado:</td>
	  			<td class="textoDato" colspan="3">
	  			<@s.select 
							templateDir="custontemplates" 
							id="estado" 
							cssClass="textarea"
							cssStyle="width:165px" 
							name="estado" 
							list="estadoList" 
							listKey="key" 
							listValue="description" 
							value="egresoProducto.estado.ordinal()"
							title="Estado"
							headerKey="0"
							headerValue="Todos"							
							/>
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Fecha Desde:</td>
	  			<td class="textoDato" align="left" >
					<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="fechaDesde" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="fechaDesde" 
					title="Fecha Desde" />
				</td>
				<td class="textoCampo">Fecha Hasta:</td>
	  			<td class="textoDato">
				<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="fechaHasta" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="fechaHasta" 
					title="Fecha Hasta" />
				</td>
			</tr>
			<tr>
	    		<td colspan="4" class="lineaGris" align="right">
	      			<input id="limpiar" type="button" name="btnVolver" value="Limpiar" class="boton"/>		
	      			<input id="buscar" type="button" name="btnVolver" value="Buscar" class="boton"/>
	    		</td>
			</tr>	
		</table>
	</div>	
		
    <!-- Resultado Filtro -->
	<@s.if test="egresoProductoList!=null">
		<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
			<#assign egresoNumero = "0" />
			<@s.if test="egresoProducto.numero != null">
				<#assign egresoNumero = "${egresoProducto.numero}" />
			</@s.if>

			<#assign egresoClienteDestino = "0" />
			<@s.if test="egresoProducto.clienteDestino != null && egresoProducto.clienteDestino.oid != null">
				<#assign egresoClienteDestino = "${egresoProducto.clienteDestino.oid}" />
			</@s.if>
			
			<#assign egresoDepositoOrigen = "0" />
			<@s.if test="egresoProducto.depositoOrigen != null && egresoProducto.depositoOrigen.oid != null">
				<#assign egresoDepositoOrigen = "${egresoProducto.depositoOrigen.oid}" />
			</@s.if>			
			
			<#assign productoBienOid = "0" />
			<@s.if test=" productoBien!= null && productoBien.oid !=null">
				<#assign productoBienOid = "${productoBien.oid?c}" />
			</@s.if>
				
			<#assign productoBienDescripcion = "" />
			<@s.if test=" productoBien!= null && productoBien.descripcion !=null && productoBien.descripcion!=''">
				<#assign productoBienDescripcion = "${productoBien.descripcion}" />
			</@s.if>
			
			<#assign egresoDepositoDestino = "0" />
			<@s.if test="egresoProducto.depositoDestino != null && egresoProducto.depositoDestino.oid != null">
				<#assign egresoDepositoDestino = "${egresoProducto.depositoDestino.oid}" />
			</@s.if>
			

			<#assign egresoTipoDestino = "${egresoProducto.tipoDestino.ordinal()}" />
			<#assign egresoEstado = "${egresoProducto.estado.ordinal()}" />

			<#assign egresoFechaDesde = "" />
			<@s.if test="fechaDesde != null">
				<#assign egresoFechaDesde = "${fechaDesde?string('dd/MM/yyyy')}" />
			</@s.if>

			<#assign egresoFechaHasta = "" />
			<@s.if test="fechaHasta != null">
				<#assign egresoFechaHasta = "${fechaHasta?string('dd/MM/yyyy')}" />
			</@s.if>
			
				<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Egresos de Productos encontrados</td>
					<td>
						<div class="alineacionDerecha">
							<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0165" 
								cssClass="item" 
								href="${request.contextPath}/deposito/egresoProducto/printXLS.action?egresoProducto.estado=${egresoEstado}&productoBien.descripcion=${productoBienDescripcion}&productoBien.oid=${productoBienOid}&egresoProducto.tipoDestino=${egresoTipoDestino}&egresoProducto.depositoOrigen.oid=${egresoDepositoOrigen}&egresoProducto.clienteDestino.oid=${egresoClienteDestino}&egresoProducto.depositoDestino.oid=${egresoDepositoDestino}&egresoProducto.numero=${egresoNumero}&fechaDesde=${egresoFechaDesde}&fechaHasta=${egresoFechaHasta}">
								<img src="${request.contextPath}/common/images/imprimirExcel.gif" title="Exportar a Excel" align="absmiddle" border="0" hspace="3">
							</@security.a>
						</div>
						<div class="alineacionDerecha">
							<b>Imprimir</b>
							<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0165" 
								cssClass="item" 
								href="${request.contextPath}/deposito/egresoProducto/printPDF.action?egresoProducto.estado=${egresoEstado}&productoBien.descripcion=${productoBienDescripcion}&productoBien.oid=${productoBienOid}&egresoProducto.tipoDestino=${egresoTipoDestino}&egresoProducto.depositoOrigen.oid=${egresoDepositoOrigen}&egresoProducto.clienteDestino.oid=${egresoClienteDestino}&egresoProducto.depositoDestino.oid=${egresoDepositoDestino}&egresoProducto.numero=${egresoNumero}&fechaDesde=${egresoFechaDesde}&fechaHasta=${egresoFechaHasta}">
								<img src="${request.contextPath}/common/images/imprimirPdf.gif" title="Exportar a PDF" align="absmiddle" border="0" hspace="3">
							</@security.a>
						</div>	
					</td>
				</tr>
			</table>
			
			<@vc.anchors target="contentTrx">			
          		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="egresoProductoList" id="egresoProducto" pagesize=15  defaultsort=2 partialList=true size="recordSize" keepStatus=true excludedParams="resetFlag">
        			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" class="botoneraAnchoCon4" title="Acciones">
						<div class="alineacion">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0161" 
							enabled="egresoProducto.readable" 
							cssClass="item" 
							href="${request.contextPath}/deposito/egresoProducto/readEgresoProductoView.action?egresoProducto.oid=${egresoProducto.oid?c}&navegacionIdBack=${navigationId}&navigationId=egresoProducto-visualizar&flowControl=regis">
							<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver"  border="0">
						</@security.a>
						</div>
						<div class="alineacion">	
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0162" 
							enabled="egresoProducto.updatable"
							cssClass="item"  
							href="${request.contextPath}/deposito/egresoProducto/administrarEgresoProductoView.action?egresoProducto.oid=${egresoProducto.oid?c}&navigationId=egresoProducto-administrar&flowControl=regis">
							<img  src="${request.contextPath}/common/images/administrar.gif" alt="Administrar" title="Administrar"  border="0">
						</@security.a>
						</div>
						<div class="alineacion">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0163" 
							enabled="egresoProducto.eraseable"
							cssClass="item"  
							href="${request.contextPath}/deposito/egresoProducto/eliminarEgresoProductoView.action?egresoProducto.oid=${egresoProducto.oid?c}&navigationId=egresoProducto-eliminar&flowControl=regis">
							<img  src="${request.contextPath}/common/images/eliminar.gif" alt="Eliminar" title="Eliminar"  border="0">
						</@security.a>				
						</div>
						<div class="alineacion">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0164" 
							enabled="egresoProducto.readable" 
							cssClass="no-rewrite" 
							href="${request.contextPath}/deposito/egresoProducto/printEgresoProductoPDF.action?egresoProducto.oid=${egresoProducto.oid?c}">
							<img src="${request.contextPath}/common/images/imprimir.gif" alt="Imprimir" title="Imprimir"  border="0">
						</@security.a>	
									
						</div>
						
					</@display.column>


					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="numero" title="Nro." />				
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="fecha" format="{0,date,dd/MM/yyyy}" title="Fecha Egreso" />
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="productosYCantidad" title="Productos" />
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="depositoOrigen.descripcion" title="Dep&oacute;sito Origen" />
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="tipoDestino" title="Tipo Destino" />					
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" title="Destino">
						<#if egresoProducto.tipoDestino.ordinal() == 1>
							${egresoProducto.depositoDestino.descripcion}
						<#else>
							<#if egresoProducto.clienteDestino?exists>
							${egresoProducto.clienteDestino.descripcion}
							</#if>					
						</#if>
					</@display.column> 
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="fsc_fss.numero" title="FSC" />

				</@display.table>
			</@vc.anchors>
		</div>	
	</@s.if>
			
	
<@vc.htmlContent 
  baseUrl="${request.contextPath}/deposito/egresoProducto/search.action"
  source="buscar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="resetFlag=true,navigationId={navigationId},flowControl=regis,egresoProducto.estado={estado},productoBien.oid={productoBien.oid},productoBien.descripcion={productoBien.descripcion},egresoProducto.tipoDestino={egresoProducto.tipoDestino},egresoProducto.clienteDestino.oid={egresoProducto.clienteDestino.oid},egresoProducto.clienteDestino.descripcion={egresoProducto.clienteDestino.descripcion},egresoProducto.depositoDestino.oid={egresoProducto.depositoDestino.oid},egresoProducto.depositoOrigen.oid={egresoProducto.depositoOrigen.oid},egresoProducto.numero={egresoProducto.numero},fechaDesde={fechaDesde},fechaHasta={fechaHasta}"/>
  		      

<@vc.htmlContent 
  baseUrl="${request.contextPath}/deposito/egresoProducto/view.action"
  source="limpiar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=regis"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/deposito/egresoProducto/createEgresoProductoView.action" 
  source="crear" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=egresoProducto-create,flowControl=regis"/>
  
  
  <@vc.htmlContent 
  baseUrl="${request.contextPath}/deposito/egresoProducto/selectClienteSearch.action" 
  source="seleccionarCliente" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=change,navegacionIdBack={navegacionIdBack},egresoProducto.estado={estado},productoBien.oid={productoBien.oid},productoBien.descripcion={productoBien.descripcion},egresoProducto.tipoDestino={egresoProducto.tipoDestino},egresoProducto.clienteDestino.oid={egresoProducto.clienteDestino.oid},egresoProducto.clienteDestino.descripcion={egresoProducto.clienteDestino.descripcion},egresoProducto.depositoDestino.oid={egresoProducto.depositoDestino.oid},egresoProducto.depositoOrigen.oid={egresoProducto.depositoOrigen.oid},egresoProducto.numero={egresoProducto.numero},fechaDesde={fechaDesde},fechaHasta={fechaHasta}"/>
  
  
    <@vc.htmlContent 
  baseUrl="${request.contextPath}/deposito/egresoProducto/selectClienteSearch.action" 
  source="seleccionarFormulario" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=change,navegacionIdBack={navegacionIdBack},egresoProducto.estado={estado},productoBien.oid={productoBien.oid},productoBien.descripcion={productoBien.descripcion},egresoProducto.tipoDestino={egresoProducto.tipoDestino},egresoProducto.clienteDestino.oid={egresoProducto.clienteDestino.oid},egresoProducto.clienteDestino.descripcion={egresoProducto.clienteDestino.descripcion},egresoProducto.depositoDestino.oid={egresoProducto.depositoDestino.oid},egresoProducto.depositoOrigen.oid={egresoProducto.depositoOrigen.oid},egresoProducto.numero={egresoProducto.numero},fechaDesde={fechaDesde},fechaHasta={fechaHasta}"/>
  
    <@vc.htmlContent
  baseUrl="${request.contextPath}/deposito/egresoProducto/selectProductoBienSearch.action" 
  source="seleccionarProductoBien" 
  success="contentTrx" 
  failure="errorTrx"
  parameters="navigationId={navigationId},flowControl=change,navegacionIdBack={navegacionIdBack},egresoProducto.estado={estado},productoBien.oid={productoBien.oid},productoBien.descripcion={productoBien.descripcion},egresoProducto.tipoDestino={egresoProducto.tipoDestino},egresoProducto.clienteDestino.oid={egresoProducto.clienteDestino.oid},egresoProducto.clienteDestino.descripcion={egresoProducto.clienteDestino.descripcion},egresoProducto.depositoDestino.oid={egresoProducto.depositoDestino.oid},egresoProducto.depositoOrigen.oid={egresoProducto.depositoOrigen.oid},egresoProducto.numero={egresoProducto.numero},fechaDesde={fechaDesde},fechaHasta={fechaHasta}"/>
  
  
