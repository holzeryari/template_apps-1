<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>
<@s.hidden id="navigationId" name="navigationId"/>
<@s.hidden id="existencia.producto.rubro.oid" name="existencia.producto.rubro.oid"/>
<@s.hidden id="existencia.producto.rubro.descripcion" name="existencia.producto.rubro.descripcion"/>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
        	<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Datos del Producto</b></td>
			</tr>
		</table>
			
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
	    		<td class="textoCampo" colspan="4">&nbsp;</td>
	    	</tr>
			<tr>
				<td class="textoCampo">C&oacute;digo:</td>
	      		<td class="textoDato">
	      			<@s.textfield 
	      					templateDir="custontemplates" 
							id="existencia.producto.oid" 
							cssClass="textarea"
							cssStyle="width:160px" 
							name="existencia.producto.oid" 
							title="C&uacute;digo" />
				</td>	
				<td class="textoCampo">Descripci&oacute;n:</td>
	      		<td class="textoDato">
	      			<@s.textfield 
	      					templateDir="custontemplates" 
							id="existencia.producto.descripcion" 
							cssClass="textarea"
							cssStyle="width:160px" 
							name="existencia.producto.descripcion" 
							title="Descripci&uacute;n" />
					</td>							
	      		</tr>
				<tr>
					<td class="textoCampo">Dep&oacute;sito</td>
	      			<td class="textoDato">
							<@s.select 
								templateDir="custontemplates" 
								id="existencia.deposito.oid" 
								cssClass="textarea"
								cssStyle="width:165px" 
								name="existencia.deposito.oid"
								value="existencia.deposito.oid"  
								list="depositoList" 
								listKey="oid" 
								listValue="descripcion"
								title="Deposito"
	                        />
	               	</td>			
					<td class="textoCampo">Rubro:</td>
				    <td class="textoDato">
						<@s.property default="&nbsp;" escape=false value="existencia.producto.rubro.descripcion"/>								
						<@s.a templateDir="custontemplates" id="seleccionarRubro" name="seleccionarRubro" href="javascript://nop/">
							<img src="${request.contextPath}/common/images/buscar.gif" border="0" align="absmiddle" hspace="3" >
						</@s.a>		
					</td>
				</tr>			
				
				<tr>
	    		<tr>
	    		<td colspan="4" class="lineaGris" align="right">
	      			<input id="limpiar" type="button" name="btnVolver" value="Limpiar" class="boton"/>		
	      			<input id="buscar" type="button" name="btnVolver" value="Buscar" class="boton"/>
	    		</td>
			</tr>	
		</table>
	</div>
	<!-- Resultado Filtro -->
	<@s.if test="existenciaList!=null">
		<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
		<#assign existenciaProductoOid = 0 />
		<#assign existenciaProductoRubroOid = 0 />
		<@s.if test="existencia.producto != null && existencia.producto.oid != null">
			<#assign existenciaProductoOid = "${existencia.producto.oid}" />

			<@s.if test="existencia.producto.rubro != null && existencia.producto.rubro.oid != null">
				<#assign existenciaProductoRubroOid = "${existencia.producto.rubro.oid?c}" />
			</@s.if>
		</@s.if>

		<#assign existenciaDepositoOid = 0 />
		<@s.if test="existencia.deposito != null && existencia.deposito.oid != null">
			<#assign existenciaDepositoOid = "${existencia.deposito.oid?c}" />
		</@s.if>

		<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Existencias de productos encontradas</td>
					<td>
						<div class="alineacionDerecha">
							<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0145" 
								cssClass="item"
								href="${request.contextPath}/deposito/consultaExistencia/printXLS.action?existencia.producto.oid=${existenciaProductoOid}&existencia.producto.descripcion=${existencia.producto.descripcion}&existencia.deposito.oid=${existenciaDepositoOid}&existencia.producto.rubro.oid=${existenciaProductoRubroOid}&existencia.producto.rubro.descripcion=${existencia.producto.rubro.descripcion}">
								<img src="${request.contextPath}/common/images/imprimirExcel.gif" title="Exportar a Excel" align="absmiddle" border="0" hspace="3">
							</@security.a>
						</div>
						<div class="alineacionDerecha">
							<b>Imprimir</b>
							<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0145" 
								cssClass="item" 
								href="${request.contextPath}/deposito/consultaExistencia/printPDF.action?existencia.producto.oid=${existenciaProductoOid}&existencia.producto.descripcion=${existencia.producto.descripcion}&existencia.deposito.oid=${existenciaDepositoOid}&existencia.producto.rubro.oid=${existenciaProductoRubroOid}&existencia.producto.rubro.descripcion=${existencia.producto.rubro.descripcion}">
								<img src="${request.contextPath}/common/images/imprimirPdf.gif" title="Exportar a PDF" align="absmiddle" border="0" hspace="3">
							</@security.a>
						</div>	
					</td>
				</tr>
			</table>
			<@ajax.anchors target="contentTrx">			
         		<@display.table class="tablaDetalleCuerpo" name="existenciaList" id="existencia" pagesize=15 decorator="ar.com.riouruguay.web.actions.deposito.consultaExistencia.decorators.SubTotalDecorator" defaultsort=2 partialList=true size="recordSize" keepStatus=true excludedParams="resetFlag">	
          			<@display.column style=" text-align:center; display:block" title="Acciones" class="botoneraAnchoCon1">
          		
          			<div class="alineacion">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0480"
							enabled="movimiento.readable" 
							cssClass="item" 
							href="${request.contextPath}/deposito/consultaExistencia/verMovimientos.action?existencia.producto.oid=${existencia.producto.oid?c}&existencia.producto.descripcion=${existencia.producto.descripcion}&existencia.deposito.oid=${existencia.deposito.oid?c}&navigationId=${navigationId}">
							<img  src="${request.contextPath}/common/images/movimientoDeposito.gif" alt="Ver Movimientos" title="Ver Movimientos"  border="0">
						</@security.a>
						</div>
        				
					</@display.column>

					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" class="estiloNumero" property="producto.oid" title="C&oacute;digo" group=1/>
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator"  class="estiloTexto" property="producto.descripcion" title="Descripci&oacute;n" group=2 />
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator"  class="estiloTexto" property="deposito.descripcion" title="Dep&oacute;sito"/>
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator"  class="estiloTexto" property="producto.rubro.descripcion" title="Rubro" />
										
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" class="estiloNumero" property="cantidad" title="Cantidad" total=true/>

				</@display.table>
			</@ajax.anchors>
		</div>
	</@s.if>
			
	
<@vc.htmlContent 
  baseUrl="${request.contextPath}/deposito/consultaExistencia/selectRubroSearch.action" 
  source="seleccionarRubro" 
  success="contentTrx" 
  failure="errorTrx" 
    parameters="navigationId={navigationId},flowControl=change,navegacionIdBack={navigationId},existencia.producto.oid={existencia.producto.oid},existencia.producto.descripcion={existencia.producto.descripcion},existencia.deposito.oid={existencia.deposito.oid},existencia.producto.rubro.oid={existencia.producto.rubro.oid},existencia.producto.rubro.descripcion={existencia.producto.rubro.descripcion},existencia.producto.estado=2"/>


<@vc.htmlContent 
  baseUrl="${request.contextPath}/deposito/consultaExistencia/search.action"
  source="buscar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="resetFlag=true,navigationId={navigationId},flowControl=regis,existencia.producto.oid={existencia.producto.oid},existencia.producto.descripcion={existencia.producto.descripcion},existencia.deposito.oid={existencia.deposito.oid},existencia.producto.rubro.oid={existencia.producto.rubro.oid},existencia.producto.rubro.descripcion={existencia.producto.rubro.descripcion},existencia.producto.estado=2"/>
  		      

<@vc.htmlContent 
  baseUrl="${request.contextPath}/deposito/consultaExistencia/view.action"
  source="limpiar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=regis"/>
  
