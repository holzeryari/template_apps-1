<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>

<@s.hidden id="navigationId" name="navigationId"/>
<@s.hidden id="navegacionIdBack" name="navegacionIdBack"/>
<@s.hidden id="productoDuro" name="productoDuro"/>
<@s.hidden id="tipoMovimientoDuro" name="tipoMovimientoDuro"/>
<@s.hidden id="fechaDuras" name="fechaDuras"/>
<@s.hidden id="movimientoEgreso.clienteDestino.oid" name="movimientoEgreso.clienteDestino.oid"/>

  <@s.if test="productoDuro == true">
	<@s.hidden id="movimiento.producto.oid" name="movimiento.producto.oid"/>
	<@s.hidden id="movimiento.producto.descripcion" name="movimiento.producto.descripcion"/>
  </@s.if>
  
   <@s.if test="tipoMovimientoDuro == true">
	<@s.hidden id="movimiento.tipoMovimiento" name="movimiento.tipoMovimiento.ordinal()"/>	
  </@s.if>
  
<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
        	<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Datos del Producto</b></td>
			</tr>
		</table>
			
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
	    		<td class="textoCampo" colspan="4">&nbsp;</td>
	    	</tr>
			<tr>
				<td class="textoCampo">Cod. Producto:</td>
	      			<td class="textoDato">
	      			<@s.textfield 
	      					templateDir="custontemplates" 
							id="movimiento.producto.oid" 
							cssClass="textarea"
							cssStyle="width:160px" 
							name="movimiento.producto.oid" 
							title="C&uacute;digo" />
					</td>	
					<td class="textoCampo">Descripci&oacute;n:</td>
	      			<td class="textoDato">
	      			<@s.textfield 
	      					templateDir="custontemplates" 
							id="movimiento.producto.descripcion" 
							cssClass="textarea"
							cssStyle="width:160px" 
							name="movimiento.producto.descripcion" 
							title="Descripci&uacute;n" />
					</td>							
	      		</tr>
				<tr>
					<td class="textoCampo">Dep&oacute;sito</td>
	      			<td class="textoDato">
							<@s.select 
								templateDir="custontemplates" 
								id="movimiento.deposito.oid" 
								cssClass="textarea"
								cssStyle="width:165px" 
								name="movimiento.deposito.oid"
								value="movimiento.deposito.oid"  
								list="depositoList" 
								listKey="oid" 
								listValue="descripcion"
								title="Deposito"
	                        />
	               	</td>			
					<td class="textoCampo">Tipo Movimiento:</td>
				    <td class="textoDato">
				      	<@s.select 
							templateDir="custontemplates" 
							id="movimiento.tipoMovimiento" 
							cssClass="textarea"
							cssStyle="width:165px" 
							name="movimiento.tipoMovimiento" 
							list="tipoMovimientoList" 
							listKey="key" 
							listValue="description" 
							value="movimiento.tipoMovimiento.ordinal()"
							title="Tipo Movimiento"
							headerKey="0"
							headerValue="Todos"							
							/>
					</td>
				</tr>
				<tr>
					<td class="textoCampo">Fecha Desde:</td>
	      			<td class="textoDato">
						<@vc.rowCalendar 
						templateDir="custontemplates" 
						id="fechaDesde" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="fechaDesde" 
						title="Fecha Desde" />
					</td>
					<td class="textoCampo">Fecha Hasta:</td>
	      			<td class="textoDato">
					<@vc.rowCalendar 
						templateDir="custontemplates" 
						id="fechaHasta" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="fechaHasta" 
						title="Fecha Hasta" />
					</td>
				</tr>
				<#if movimientoEgreso?exists && 
					movimientoEgreso.egresoProducto?exists && 
					movimientoEgreso.egresoProducto.clienteDestino?exists>	
					<tr>
					<td class="textoCampo">Cliente:</td>
	      			<td class="textoDato" colspan="3">
	      				<@s.property default="&nbsp;" escape=false value="movimientoEgreso.egresoProducto.clienteDestino.descripcion"/>
	      			</td>
					</tr>
				</#if>	
				<tr>
					<td colspan="4" class="lineaGris" align="right">
	      				<input id="limpiar" type="button" name="btnVolver" value="Limpiar" class="boton"/>		
	      				<input id="buscar" type="button" name="btnVolver" value="Buscar" class="boton"/>
	    			</td>
				</tr>	
			</table>
		</div>
			
		<!-- Resultado Filtro -->
		<@s.if test="movimientoList!=null">
			<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
			
			<#assign movimientoProductoOid = "0" />
			<@s.if test="movimiento.producto != null && movimiento.producto.oid != null">
				<#assign movimientoProductoOid = "${movimiento.producto.oid?c}" />
			</@s.if>
			
			
			<#assign movimientoDepositoOid = "0" />
			<@s.if test="movimiento.deposito != null && movimiento.deposito.oid != null">
				<#assign movimientoDepositoOid = "${movimiento.deposito.oid?c}" />
			</@s.if>
			
			
			
			<#assign fechaD = "" />
			<@s.if test="fechaDesde != null">
				<#assign fechaD = "${fechaDesde?string('dd/MM/yyyy')}" />
			</@s.if>

			<#assign fechaH = "" />
			<@s.if test="fechaHasta != null">
				<#assign fechaH = "${fechaHasta?string('dd/MM/yyyy')}" />
			</@s.if>
			
			
			
			<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Movimientos de Productos encontrados</td>
					<td>					
						<div class="alineacionDerecha">						
							<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0183" 
								cssClass="item" 
								name="imprimir"
								href="${request.contextPath}/deposito/consultaMovimiento/printXLS.action?movimiento.producto.oid=${movimientoProductoOid}">												
								<img src="${request.contextPath}/common/images/imprimirExcel.gif" title="Exportar a Excel" align="absmiddle" border="0" hspace="3">
							</@security.a>						
						</div>
					</td>
					
				</tr>
				<div class="alineacionDerecha">
					
				</div>
			</table>
				
						
			<@ajax.anchors target="contentTrx">			
          		<@display.table class="tablaDetalleCuerpo" name="movimientoList" id="movimiento" pagesize=15 partialList=true size="recordSize" keepStatus=true excludedParams="resetFlag">
          			<@display.column style=" text-align:center; display:block" title="Acciones" class="botoneraAnchoCon1">
          		
  					<#-- Tipo 1: Ingreso -->
					<#if movimiento.tipoMovimiento.ordinal()== 1>
					
						<div class="alineacion">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0183" 
							enabled="movimiento.readable" 
							cssClass="item" 
							id="ver"
							name="ver"
							href="${request.contextPath}/deposito/consultaMovimiento/verIngreso.action?movimientoIngreso.ingresoProducto.oid=${movimiento.ingresoProducto.oid?c}&navigationId=${navigationId}">
							<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver"  border="0">
						</@security.a>
						</div>
					
					<#-- Tipo 2: Egreso -->
					<#elseif movimiento.tipoMovimiento.ordinal() == 2>
						<div class="alineacion">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0161" 
							enabled="movimiento.readable" 
							cssClass="item" 
							id="ver"
							name="ver"							
							href="${request.contextPath}/deposito/consultaMovimiento/verEgreso.action?movimientoEgreso.egresoProducto.oid=${movimiento.egresoProducto.oid?c}&navigationId=${navigationId}">
							<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver"  border="0">
						</@security.a>
						</div>
					
					<#-- Tipo 3: Ajuste -->
					<#elseif movimiento.tipoMovimiento.ordinal() == 3>
									
						<div class="alineacion">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0141" 
							enabled="movimiento.readable" 
							cssClass="item" 
							id="ver"
							name="ver"							
							href="${request.contextPath}/deposito/consultaMovimiento/verAjuste.action?movimientoAjuste.ajusteExistencia.oid=${movimiento.ajusteExistencia.oid?c}&navigationId=${navigationId}">
							<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver"  border="0">
						</@security.a>
						</div>
					
					<#-- Tipo 4: Recepcion -->
					<#elseif movimiento.tipoMovimiento.ordinal() == 4>
						<div class="alineacion">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0443" 
							enabled="movimiento.readable" 
							cssClass="item" 
							id="ver"
							name="ver"
							href="${request.contextPath}/deposito/consultaMovimiento/verRecepcion.action?movimientoRecepcionProducto.recepcionProductoBien.oid=${movimiento.recepcionProductoBien.oid?c}&navigationId=${navigationId}">
							<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver"  border="0">
						</@security.a>
						</div>
						
					<#-- Tipo 5: Devolucion -->
					<#elseif movimiento.tipoMovimiento.ordinal() == 5>
						<div class="alineacion">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0463" 
							enabled="movimiento.readable" 
							cssClass="item" 
							id="ver"
							name="ver"
							href="${request.contextPath}/deposito/consultaMovimiento/verDevolucion.action?movimientoDevolucionProducto.devolucionProductoBien.oid=${movimiento.devolucionProductoBien.oid?c}&navigationId=${navigationId}">
							<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver"  border="0">
						</@security.a>
						</div>
					</#if>		
						
					</@display.column>
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" class="estiloTexto" property="tipoMovimiento" title="Tipo Movimiento"/>
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" class="estiloTexto" property="fecha" format="{0,date,dd/MM/yyyy}"  title="Fecha"/>

					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" class="estiloNumero" property="producto.oid" title="Cod. Producto"/>
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" class="estiloTexto" property="producto.descripcion" title="Descripci&oacute;n"/>
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" class="estiloTexto" property="producto.rubro.descripcion" title="Rubro"/>
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" class="estiloTexto" property="deposito.descripcion" title="Dep&oacute;sito"/>
										
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" class="estiloNumero" property="ingreso" title="Cant. Ingresada"/>
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" class="estiloNumero" property="egreso" title="Cant. Egresada"/>
					

					<@display.footer>
						<tr>
							<td colspan=7 aling="right"><b>Existencia del producto a fecha hasta: </b></td>							
							<td colspan=2 style="text-align: right; font:bold 13px Verdana,Tahoma,Arial,sans-serif; "  id="tdTotalFacturas">
							<b >
							<#assign totalExistenciass = totalExistencia> 
								${totalExistenciass?string(",##0.00")}
							  </b></td>
						<tr>
					</@display.footer>
					
				</@display.table>
			</@ajax.anchors>
			</div>
		</@s.if>
		
	<@s.if test="productoDuro == true">
		<div id="capaBotonera" class="capaBotonera">
		<table id="tablaBotonera" class="tablaBotonera">
			<tr> 
				<td align="left">
					<input id="cancel" type="button" name="btnVolver" value="Volver" class="boton"/>
				</td>
			</tr>	
		</table>
	</div>
		
		
		 <@vc.htmlContent 
			  baseUrl="${request.contextPath}/compras/flowControl.action" 
			  source="cancel" 
			  success="contentTrx" 
			  failure="errorTrx" 
			  parameters="navigationId=${navegacionIdBack},flowControl=back"/>
	</@s.if>
      


<@vc.htmlContent 
  baseUrl="${request.contextPath}/deposito/consultaMovimiento/search.action"
  source="buscar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="resetFlag=true,navigationId={navigationId},flowControl=regis,tipoMovimientoDuro={tipoMovimientoDuro},movimiento.producto.oid={movimiento.producto.oid},movimiento.producto.descripcion={movimiento.producto.descripcion},movimiento.deposito.oid={movimiento.deposito.oid},movimiento.tipoMovimiento={movimiento.tipoMovimiento},fechaDesde={fechaDesde},fechaHasta={fechaHasta},productoDuro={productoDuro},navegacionIdBack={navegacionIdBack},fechaDuras={fechaDuras},movimientoEgreso.clienteDestino.oid={movimientoEgreso.clienteDestino.oid}"/>
  		      

<@vc.htmlContent 
  baseUrl="${request.contextPath}/deposito/consultaMovimiento/view.action"
  source="limpiar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=regis,productoDuro={productoDuro},navegacionIdBack={navegacionIdBack},tipoMovimientoDuro={tipoMovimientoDuro},fechaDuras={fechaDuras},movimientoEgreso.clienteDestino.oid={movimientoEgreso.clienteDestino.oid}"/>
  
