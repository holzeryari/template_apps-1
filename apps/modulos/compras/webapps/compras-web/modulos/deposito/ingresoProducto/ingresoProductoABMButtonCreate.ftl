<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>


<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancel" type="button" name="btnVolver" value="Cancelar" class="boton"/>
				<input id="store" type="button" name="btnAgregar" value="Agregar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/deposito/ingresoProducto/createIngresoProducto.action" 
  source="store" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="ingresoProducto.oid={ingresoProducto.oid},ingresoProducto.depositoIngreso.oid={ingresoProducto.depositoIngreso.oid},ingresoProducto.egresoProducto.oid={ingresoProducto.egresoProducto.oid},ingresoProducto.observaciones={ingresoProducto.observaciones}"/>
  
  <@vc.htmlContent 
    baseUrl="${request.contextPath}/compras/flowControl.action"  
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=buscar-ingresoProducto,flowControl=back"/>
  
    <@vc.htmlContent 
  baseUrl="${request.contextPath}/deposito/ingresoProducto/selectClienteCreate.action" 
  source="seleccionarCliente" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="ingresoProducto.oid={ingresoProducto.oid},ingresoProducto.egresoProducto.oid={ingresoProducto.egresoProducto.oid},ingresoProducto.egresoProducto.numero={ingresoProducto.egresoProducto.numero},ingresoProducto.depositoIngreso.oid={ingresoProducto.depositoIngreso.oid},ingresoProducto.observaciones={ingresoProducto.observaciones}"/>
  