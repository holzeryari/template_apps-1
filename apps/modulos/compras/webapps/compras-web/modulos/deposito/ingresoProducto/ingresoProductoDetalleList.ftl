<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<@ajax.anchors target="contentTrx">
<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
		<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Productos a Ingresar</td>
					<td>	
						<div align="right">
							<@s.a href="${request.contextPath}/deposito/ingresoProducto/selectProducto.action?ingresoProducto.oid=${ingresoProducto.oid?c}&ingresoProducto.depositoIngreso.oid=${ingresoProducto.depositoIngreso.oid?c}&ingresoProducto.depositoIngreso.descripcion=${ingresoProducto.depositoIngreso.descripcion}&navigationId=${navigationId}" templateDir="custontemplates" id="agregarIngresoProductoDetalle" name="agregarIngresoProductoDetalle" cssClass="ocultarIcono">
								<b>Agregar</b><img src="${request.contextPath}/common/images/agregar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
							</@s.a>			
						</div>
					</td>	
				</tr>
		</table>	
	
	<!-- Resultado Filtro -->						
	<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="ingresoProducto.ingresoProductoDetalleList" id="ingresoProductoDetalle" defaultsort=2 decorator="ar.com.riouruguay.web.actions.deposito.ingresoProducto.decorators.CantidadIngresadaDecorator">
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" class="botoneraAnchoCon3" title="Acciones">
		<a href="${request.contextPath}/deposito/ingresoProducto/readIngresoProductoDetalleView.action?ingresoProductoDetalle.oid=${ingresoProductoDetalle.oid?c}&navegacionIdBack=${navigationId}"><img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver" border="0"></a>
		&nbsp;
		
		<a href="${request.contextPath}/deposito/ingresoProducto/updateIngresoProductoDetalleView.action?ingresoProductoDetalle.oid=${ingresoProductoDetalle.oid?c}"><img  src="${request.contextPath}/common/images/modificar.gif" alt="Modificar" title="Modificar" border="0"></a>
		&nbsp;
		
		<a href="${request.contextPath}/deposito/ingresoProducto/deleteIngresoProductoDetalleView.action?ingresoProductoDetalle.oid=${ingresoProductoDetalle.oid?c}"><img  src="${request.contextPath}/common/images/eliminar.gif" alt="Eliminar" title="Eliminar" border="0"></a>
									
		</@display.column>
						
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="numero" title="Nro. Item" defaultorder="ascending"/>
	
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="producto.oid" title="Cod." />
	
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="producto.descripcion" title="Descripci&oacute;n" />
	
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="producto.rubro.descripcion" title="Rubro" />
	
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="producto.marca" title="Marca" />					
	
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="producto.modelo" title="Modelo" />
	
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="cantidadIngresada" title="Cant. a Ingresar" />
	</@display.table>
</div>
</@ajax.anchors>