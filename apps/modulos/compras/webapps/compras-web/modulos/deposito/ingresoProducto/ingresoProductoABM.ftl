<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<@s.hidden id="ingresoProducto.oid" name="ingresoProducto.oid"/>
<@s.hidden id="ingresoProducto.estado" name="ingresoProducto.estado.ordinal()"/>
<@s.hidden id="ingresoProducto.egresoProducto.oid" name="ingresoProducto.egresoProducto.oid"/>
<@s.hidden id="ingresoProducto.egresoProducto.numero" name="ingresoProducto.egresoProducto.numero"/>
<@s.hidden id="ingresoProducto.versionNumber" name="ingresoProducto.versionNumber"/>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
			<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos del Ingreso de Producto</b></td>
				<td>
					<div align="right">
						<@s.a templateDir="custontemplates" id="modificarIngresoProducto" name="modificarIngresoProducto" href="javascript://nop/" cssClass="ocultarIcono">
							<b>Modificar</b><img src="${request.contextPath}/common/images/modificar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
						</@s.a>
					</div>
				</td>
			</tr>
		</table>
		
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
			<tr>
      			<td class="textoCampo">N&uacute;mero:</td>
      			<td class="textoDato">
      				<@s.property default="&nbsp;" escape=false value="ingresoProducto.numero"/>
      			</td>
      			<td  class="textoCampo">Fecha:</td>
				<td class="textoDato">
					<@s.if test="ingresoProducto.fecha != null">
						<#assign fechaIngreso = ingresoProducto.fecha> 
						${fechaIngreso?string("dd/MM/yyyy")}	
					</@s.if>&nbsp;								
				</td>	      			
			</tr>	
			<tr>
      			<td class="textoCampo">Dep&oacute;sito Ingreso:</td>
	      		<td class="textoDato">		      						
						<@s.select 
							templateDir="custontemplates" 
							id="ingresoProducto.depositoIngreso.oid" 
							cssClass="textarea"
							cssStyle="width:165px" 
							name="ingresoProducto.depositoIngreso.oid" 
							list="depositoIngresoList" 
							listKey="oid" 
							listValue="descripcion" 
							value="oid"
							title="Deposito"
							headerKey="0" 
                            headerValue="Seleccionar"
                            value="ingresoProducto.depositoIngreso.oid"/>
                </td>		      		
				<td class="textoCampo">Egreso Asociado:</td>
      			<td class="textoDato">
      				<@s.property default="&nbsp;" escape=false value="ingresoProducto.egresoProducto.numero"/>
						<@s.a templateDir="custontemplates" id="seleccionarEgresoProducto" name="seleccionarEgresoProducto" href="javascript://nop/" cssClass="ocultarIcono">
							<img src="${request.contextPath}/common/images/buscar.gif" border="0" align="absmiddle" hspace="3" >
						</@s.a>
				</td>				
			</tr>
	    	<tr>
				<td class="textoCampo">Observaciones:</td>
	      		<td  class="textoDato"colspan="3">
					<@s.textarea	
							templateDir="custontemplates" 						  
							cols="89" rows="4"	      					
							cssClass="textarea"
							id="ingresoProducto.observaciones"							 
							name="ingresoProducto.observaciones"  
							label="Observaciones"														
							 />
				</td>					
	    	</tr>
			<tr>
				<td class="textoCampo">Estado:</td>
      			<td  class="textoDato"colspan="3">
					<@s.property default="&nbsp;" escape=false value="ingresoProducto.estado"/></td>
				</td>
			</tr>
			<tr><td class="textoCampo" colspan="4">&nbsp;</td></tr>
		</table>		
	</div>
	
	<@tiles.insertAttribute name="productos"/>
	
<@vc.htmlContent 
  baseUrl="${request.contextPath}/deposito/ingresoProducto/updateIngresoProductoView.action" 
  source="modificarIngresoProducto" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="ingresoProducto.oid={ingresoProducto.oid},ingresoProducto.versionNumber={ingresoProducto.versionNumber}"/>


  <@vc.htmlContent 
  baseUrl="${request.contextPath}/deposito/ingresoProducto/selectEgresoProductoCreate.action" 
  source="seleccionarEgresoProducto" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId=abm-ingresoProducto-create,flowControl=change,ingresoProducto.oid={ingresoProducto.oid},ingresoProducto.depositoIngreso.oid={ingresoProducto.depositoIngreso.oid},ingresoProducto.egresoProducto.oid={ingresoProducto.egresoProducto.oid},ingresoProducto.egresoProducto.numero={ingresoProducto.egresoProducto.numero},ingresoProducto.observaciones={ingresoProducto.observaciones},ingresoProducto.estado={ingresoProducto.estado}"/>
  


