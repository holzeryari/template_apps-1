<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancelar" type="button" name="btnVolver" value="Cancelar" class="boton"/>
				<input id="update" type="button" name="btnModificar" value="Modificar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/deposito/ingresoProducto/updateIngresoProducto.action" 
  source="update" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="ingresoProducto.oid={ingresoProducto.oid},ingresoProducto.observaciones={ingresoProducto.observaciones}"/>
  

<@vc.htmlContent 
    baseUrl="${request.contextPath}/compras/flowControl.action"  
  source="cancelar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=ingresoProducto-administrar,flowControl=back"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/deposito/ingresoProducto/selectEgresoProductoUpdate.action" 
  source="seleccionarCliente" 
  success="contentTrx" 
  failure="errorTrx" 
 parameters="ingresoProducto.oid={ingresoProducto.oid},ingresoProducto.depositoIngreso.oid={ingresoProducto.depositoIngreso.oid},ingresoProducto.egresoProducto.oid={ingresoProducto.egresoProducto.oid},ingresoProducto.observaciones={ingresoProducto.observaciones},ingresoProducto.estado={ingresoProducto.estado}"/>
  