<#assign disabedControls = stack.findValue("disabedControl") />
<#assign checked=0>
<#list disabedControls as disabedControl>
	<#if parameters.get("name")==disabedControl.name>
		<#assign checked=1>
<div<#rt/>
<#if parameters.id?if_exists != "">
 id="${parameters.id?html}"<#rt/>
</#if>
<#if parameters.cssClass?exists>
 class="${parameters.cssClass?html}"<#rt/>
</#if>
<#if parameters.cssStyle?exists>
 style="${parameters.cssStyle?html}"<#rt/>
</#if>
<#if parameters.title?exists>
 title="${parameters.title?html}"<#rt/>
</#if>
<#include "/${parameters.templateDir}/simple/common-attributes.ftl" />
><#rt/>
		<#break>
	</#if>
</#list>
<#if checked == 0>

<#include "/template/simple/a.ftl" />
</#if>

