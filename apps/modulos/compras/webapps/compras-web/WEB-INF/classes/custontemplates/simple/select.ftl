<#assign disabedControls = stack.findValue("disabedControl") />

<#assign checked=0>
<#assign vacio=1>
<#list disabedControls as disabedControl>
	<#if parameters.get("name")==disabedControl.name>
		<#assign checked=1>
<@s.iterator value="parameters.list">
        <#if parameters.listKey?exists>
            <#if stack.findValue(parameters.listKey)?exists>
              <#assign itemKey = stack.findValue(parameters.listKey)/>
              <#assign itemKeyStr = itemKey.toString()/>
            <#else>
              <#assign itemKey = ''/>
              <#assign itemKeyStr = ''/>
            </#if>
        <#else>
            <#assign itemKey = stack.findValue('top')/>
            <#assign itemKeyStr = itemKey.toString()/>
        </#if>
        <#if parameters.listValue?exists>
            <#if stack.findString(parameters.listValue)?exists>
              <#assign itemValue = stack.findString(parameters.listValue)/>
            <#else>
              <#assign itemValue = ''/>
            </#if>
        <#else>
            <#assign itemValue = stack.findString('top')/>
        </#if>
        <#if tag.contains(parameters.nameValue, itemKey) == true>

${itemValue?html} &nbsp;
		<#assign vacio=0>

        </#if>
</@s.iterator>
		<#if vacio == 1>
			&nbsp;
		</#if>

		<#break>
	</#if>
</#list>
<#if checked == 0>

<#include "/template/simple/select.ftl" />
</#if>
