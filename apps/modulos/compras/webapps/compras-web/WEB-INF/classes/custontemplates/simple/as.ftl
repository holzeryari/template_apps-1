<#macro divPut>
<div<#rt/>
<#if parameters.id?if_exists != "">
 id="div-${parameters.id?html}"<#rt/>
</#if>
<#if parameters.cssClass?exists>
 class="${parameters.cssClass?html}"<#rt/>
</#if>
<#if parameters.cssStyle?exists>
 style="${parameters.cssStyle?html}"<#rt/>
</#if>
<#if parameters.title?exists>
 title="${parameters.title?html}"<#rt/>
</#if>
<#include "common-attributes.ftl" />
><#rt/>
</#macro>
<#assign disabedControls = stack.findValue("disabedControl") />
<#assign checked=0>
<#if parameters.canPerform=="false" || parameters.enabled=="false" >
	<#assign checked=1>
<@divPut />
<#else>
	<#list disabedControls as disabedControl>
		<#if parameters.get("name")==disabedControl.name>
			<#assign checked=1>
<@divPut />
		<#break>
		</#if>
	</#list>	
</#if>
<#if checked == 0>
<#include "asView.ftl" />
</#if>