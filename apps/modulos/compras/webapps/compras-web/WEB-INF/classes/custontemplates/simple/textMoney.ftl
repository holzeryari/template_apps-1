<#setting locale="es_AR">
<#assign disabedControls = stack.findValue("disabedControl") />
<#assign checked=0>
<#list disabedControls as disabedControl>
	<#if parameters.get("name")==disabedControl.name>
		<#assign checked=1>
		<#if parameters.nameValue?exists>
			<#if stack.findValue(parameters.get("name"))?exists>
 				<#assign value = stack.findValue(parameters.get("name"))> 				
				${value?string(",##0.00")}
			<#else>
				&nbsp;
			</#if>
		<#else>
			&nbsp;
		</#if>
		<#break>
	</#if>
</#list>
<#if checked == 0>

<#include "textMoneyView.ftl" />
</#if>