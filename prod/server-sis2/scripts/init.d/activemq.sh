#!/bin/sh
# ActiveMQ service script

export JAVA_HOME=/java16

start() {
echo -n "Starting activemq:  "
/home/tomcat/activemq/bin/activemq start
sleep 2
}
stop() {
echo -n "Stopping activemq: "
/home/tomcat/activemq/bin/activemq stop

}

# See how we were called.
case "$1" in
start)
start
;;
stop)
stop
;;
restart)
stop
start
;;
*)
echo $"Usage: activemq {start|stop|restart}"
exit
esac

