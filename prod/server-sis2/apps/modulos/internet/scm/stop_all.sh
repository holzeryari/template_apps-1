echo "Stop All services"

ROOT_PATH=/home/tomcat/internet

${ROOT_PATH}/scm/stop_tomcat.sh cas-internet  
${ROOT_PATH}/scm/stop_tomcat.sh contabilidad-internet
${ROOT_PATH}/scm/stop_tomcat.sh compras-internet
${ROOT_PATH}/scm/stop_tomcat.sh rrhh-internet 
${ROOT_PATH}/scm/stop_tomcat.sh tesoreria-internet
${ROOT_PATH}/scm/stop_tomcat.sh tesoreria-ws-internet
${ROOT_PATH}/scm/stop_tomcat.sh emision-internet
${ROOT_PATH}/scm/stop_tomcat.sh emision-internet2
${ROOT_PATH}/scm/stop_tomcat.sh emision-ws-internet
${ROOT_PATH}/scm/stop_tomcat.sh desarrollo-territorial-internet 
${ROOT_PATH}/scm/stop_tomcat.sh cesvi-ws-internet
${ROOT_PATH}/scm/stop_tomcat.sh siniestros-internet
${ROOT_PATH}/scm/stop_tomcat.sh rusmovil-internet
${ROOT_PATH}/scm/stop_tomcat.sh auditoria-internet
${ROOT_PATH}/scm/stop_tomcat.sh contabilidad-internet

#/usr/bin/find /home/tomcat/*/temp/ -exec rm -Rf {} \;
#for i in /home/tomcat/legacy/tomcat.*; do $i stop; done
