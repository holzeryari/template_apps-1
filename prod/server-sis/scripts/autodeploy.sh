#!/bin/bash -x
export DIR_LOCAL=/home/tomcat/entregas/auto
export DIR_REMOTO=/home/tomcat/entregas/auto
source /usr/local/bin/obtenerNombreFunc.sh
#export HOST_REMOTO=192.168.200.72
#export HOST_REMOTO=ifxrusserver.ru
export HOST_REMOTO=server-apps-uat.ru

#rm -Rf $DIR_LOCAL/*.war
#scp root@$HOST_REMOTO:$DIR_REMOTO/*.war $DIR_LOCAL  && ssh root@$HOST_REMOTO "rm -Rf $DIR_REMOTO/*.war"

cd $DIR_LOCAL
for war in *.war
do
        case "$war" in
    tesoreria.war) echo tesoreria
	cp /home/tomcat/tesoreria-internet/webapps/tesoreria*.war /home/tomcat/tesoreria-internet/webapps/back_tesoreria_war
        rm -Rf /home/tomcat/tesoreria-*/webapps/tesoreria*
        cp tesoreria.war /home/tomcat/tesoreria-internet/webapps/tesoreria.war
        cp tesoreria.war /home/tomcat/tesoreria-prod/webapps/tesoreria.war
        cp tesoreria.war /home/tomcat/tesoreria-ws-internet/webapps/tesoreria.war
        cp tesoreria.war /home/tomcat/tesoreria-ws-intranet/webapps/tesoreria.war
        rm -Rf tesoreria.war
        ;;
    #tesoreria-ws.war) echo tesoreria-ws
       #cp /home/tomcat/tesoreria-ws-internet/webapps/tesoreria*.war /home/tomcat/tesoreria-ws-internet/webapps/back_tesoreria-ws_war
       #rm -Rf /home/tomcat/tesoreria-ws-*/webapps/tesoreria*
       #cp tesoreria-ws.war /home/tomcat/tesoreria-ws-internet/webapps/tesoreria-ws.war
       #cp tesoreria-ws.war /home/tomcat/tesoreria-ws-intranet/webapps/tesoreria-ws.war
       # rm -Rf tesoreria-ws.war
       # ;;
    emision-sis.war) echo emision
	cp /home/tomcat/emision-internet/webapps/emision-sis*.war /home/tomcat/emision-internet/webapps/back_emision-sis_war
        rm -Rf /home/tomcat/emision-*/webapps/emision-sis*
        cp emision-sis.war /home/tomcat/emision-internet/webapps/emision-sis.war
        cp emision-sis.war /home/tomcat/emision-prod/webapps/emision-sis.war
        rm -Rf emision-sis.war
        ;;
    notificaciones.war) echo notificaciones
	cp /home/tomcat/notificaciones-internet/webapps/notificaciones.war /home/tomcat/notificaciones-internet/webapps/back_notificaciones_war
	rm -Rf /home/tomcat/notificaciones-*/webapps/notificaciones*
	cp notificaciones.war /home/tomcat/notificaciones-internet/webapps/notificaciones.war
	cp notificaciones.war /home/tomcat/notificaciones-prod/webapps/notificaciones.war
	rm -Rf notificaciones.war
        ;;
    emision-ws.war) echo emision-ws
        cp /home/tomcat/emision-ws-internet/webapps/emision-ws*.war /home/tomcat/emision-ws-internet/webapps/back_emision-ws_war
        rm -Rf /home/tomcat/emision-ws-internet/webapps/emision-ws*
        cp emision-ws.war /home/tomcat/emision-ws-internet/webapps/emision-ws.war
        rm -Rf emision-ws.war
        ;;
    nuevozk.war) echo nuevozk
	cp /home/tomcat/emision-internet/webapps/nuevozk.war /home/tomcat/emision-internet/webapps/back_nuevozk_war
        rm -Rf /home/tomcat/emision-*/webapps/nuevozk*
        cp nuevozk.war /home/tomcat/emision-internet/webapps/nuevozk.war
        cp nuevozk.war /home/tomcat/emision-prod/webapps/nuevozk.war
        rm -Rf nuevozk.war
        ;;
    afip-ws.war) echo afip-ws
	cp /home/tomcat/emision-internet/webapps/afip-ws.war /home/tomcat/emision-internet/webapps/back_afip-ws_war
	rm -Rf /home/tomcat/emision-*/webapps/afip-ws*
	cp afip-ws.war /home/tomcat/emision-internet/webapps/afip-ws.war
	cp afip-ws.war /home/tomcat/emision-prod/webapps/afip-ws.war
	rm -Rf afip-ws.war
        ;;
    siniestro.war) echo siniestro
	cp /home/tomcat/siniestros-internet/webapps/siniestro.war /home/tomcat/siniestros-internet/webapps/back_siniestro_war
	cp /home/tomcat/siniestros-internet2/webapps/siniestro.war /home/tomcat/siniestros-internet2/webapps/back_siniestro_war
        rm -Rf /home/tomcat/siniestros-*/webapps/siniestro*
        cp siniestro.war /home/tomcat/siniestros-internet/webapps/siniestro.war
		cp siniestro.war /home/tomcat/siniestros-internet2/webapps/siniestro.war
        cp siniestro.war /home/tomcat/siniestros-prod/webapps/siniestro.war
        rm -Rf siniestro.war
        ;;
    auditoria.war) echo auditoria
	cp /home/tomcat/auditoria-internet/webapps/auditoria.war /home/tomcat/auditoria-internet/webapps/back_auditoria_war
        rm -Rf /home/tomcat/auditoria-*/webapps/auditoria*
        cp auditoria.war /home/tomcat/auditoria-internet/webapps/auditoria.war
        cp auditoria.war /home/tomcat/auditoria-prod/webapps/auditoria.war
        rm -Rf auditoria.war
        ;;
    contabilidad.war) echo contabilidad
	cp /home/tomcat/contabilidad-internet/webapps/contabilidad.war /home/tomcat/contabilidad-internet/webapps/back_contabilidad_war
        rm -Rf /home/tomcat/contabilidad-*/webapps/contabilidad*
        cp contabilidad.war /home/tomcat/contabilidad-internet/webapps/contabilidad.war
        cp contabilidad.war /home/tomcat/contabilidad-prod/webapps/contabilidad.war
        rm -Rf contabilidad.war
        ;;
    datos-comunes.war) echo datos-comunes
	cp /home/tomcat/datoscomunes-internet/webapps/datos-comunes.war /home/tomcat/datoscomunes-internet/webapps/back_datos-comunes_war
        rm -Rf /home/tomcat/datoscomunes-*/webapps/datos-comunes*
        cp datos-comunes.war /home/tomcat/datoscomunes-internet/webapps/datos-comunes.war
        cp datos-comunes.war /home/tomcat/datoscomunes-prod/webapps/datos-comunes.war
        rm -Rf datos-comunes.war
        ;;
    desarrolloTerritorial.war) echo desarrolloTerritorial
	cp /home/tomcat/desarrollo-territorial-internet/webapps/desarrolloTerritorial.war /home/tomcat/desarrollo-territorial-internet/webapps/back_desarrolloTerritorial_war
        rm -Rf /home/tomcat/desarrollo-territorial-*/webapps/desarrolloTerritorial*
        cp desarrolloTerritorial.war /home/tomcat/desarrollo-territorial-internet/webapps/desarrolloTerritorial.war
        cp desarrolloTerritorial.war /home/tomcat/desarrollo-territorial-prod/webapps/desarrolloTerritorial.war
        rm -Rf desarrolloTerritorial.war
        ;;
    security-sis.war) echo security-sis
	cp /home/tomcat/cas-internet/webapps/security-sis.war /home/tomcat/cas-internet/webapps/back_security-sis_war
        rm -Rf /home/tomcat/cas-*/webapps/security-sis*
        cp security-sis.war /home/tomcat/cas-prod/webapps/security-sis.war
        cp security-sis.war /home/tomcat/cas-internet/webapps/security-sis.war
        rm -Rf security-sis.war
        ;;
    procesos.war) echo 
        cp /home/tomcat/procesos-prod/webapps/procesos.war /home/tomcat/procesos-prod/webapps/back_procesos_war
        rm -Rf /home/tomcat/procesos-prod/webapps/procesos*
        cp procesos.war /home/tomcat/procesos-prod/webapps/procesos.war
        rm -Rf procesos.war
        ;;
    rrhh.war) echo rrhh
	cp /home/tomcat/rrhh-internet/webapps/rrhh.war /home/tomcat/rrhh-internet/webapps/back_rrhh_war
        rm -Rf /home/tomcat/rrhh-*/webapps/rrhh*
        cp rrhh.war /home/tomcat/rrhh-internet/webapps/rrhh.war
        cp rrhh.war /home/tomcat/rrhh-prod/webapps/rrhh.war
        rm -Rf rrhh.war
        ;;
    cesvi-ws-server.war) echo cesvi-ws
        cp /home/tomcat/cesvi-ws-internet/cesvi-ws-server.war /home/tomcat/cesvi-ws-internet/back_cesvi-ws-server_war
        rm -Rf /home/tomcat/cesvi-ws-*/webapps/cesvi-ws-server*	
        cp cesvi-ws-server.war /home/tomcat/cesvi-ws-internet/webapps/cesvi-ws-server.war
        rm -Rf cesvi-ws-server.war
        ;;
    rusmovil.war) echo rusmovil
	cp /home/tomcat/rusmovil-internet/webapps/rusmovil.war /home/tomcat/rusmovil-internet/webapps/back_rusmovil_war
        rm -Rf /home/tomcat/rusmovil-internet/webapps/movil*
        cp rusmovil.war /home/tomcat/rusmovil-internet/webapps/rusmovil.war
        rm -Rf rusmovil.war
        ;;
    cotizador.war) echo cotizador
	cp /home/tomcat/rusmovil-internet/webapps/cotizador.war /home/tomcat/rusmovil-internet/webapps/back_cotizador_war
        rm -Rf /home/tomcat/rusmovil-internet/webapps/cotizador*
        cp cotizador.war /home/tomcat/rusmovil-internet/webapps/cotizador.war
        rm -Rf cotizador.war
        ;;
    claims-ws.war) echo claims-ws
        cp /home/tomcat/claims-ws-internet/webapps/claims-ws.war /home/tomcat/claims-ws-internet/webapps/back_claims-ws_war
        rm -Rf /home/tomcat/claims-ws-internet/webapps/claims-ws*
        cp claims-ws.war /home/tomcat/claims-ws-internet/webapps/claims-ws.war
        rm -Rf claims-ws.war
        ;;
    infoauto-ws.war) echo infoauto-ws
        cp /home/tomcat/infoauto-ws-internet/webapps/infoauto-ws.war /home/tomcat/infoauto-ws-internet/webapps/back_infoauto-ws_war
        rm -Rf /home/tomcat/infoauto-ws-internet/webapps/infoauto-ws*
        cp infoauto-ws.war /home/tomcat/infoauto-ws-internet/webapps/infoauto-ws.war
        rm -Rf infoauto-ws.war
        ;;
    portal.war) echo portal
		cp /home/tomcat/cas-internet/webapps/portal.war /home/tomcat/cas-internet/webapps/back_portal_war
        rm -Rf /home/tomcat/cas-*/webapps/portal*
        cp portal.war /home/tomcat/cas-prod/webapps/portal.war
        cp portal.war /home/tomcat/cas-internet/webapps/portal.war
        rm -Rf portal.war
        ;;
		
        *) echo otro $war
        ;;
        esac
done

# Croneado con el usuario root:
# chown -R tomcat.tomcat /home/tomcat
# chmod 644 -R /home/tomcat/*/webapps/*.war
# chmod 644 -R /home/tomcat/*/logs/*
