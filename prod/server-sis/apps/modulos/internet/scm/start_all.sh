echo "Start All services"

ROOT_PATH=/home/tomcat/internet
ROOT_PATH_MS=/home/tomcat/microservices

${ROOT_PATH}/scm/start_tomcat.sh cas-internet
${ROOT_PATH}/scm/start_tomcat.sh contabilidad-internet
${ROOT_PATH}/scm/start_tomcat.sh compras-internet
${ROOT_PATH}/scm/start_tomcat.sh rrhh-internet
${ROOT_PATH}/scm/start_tomcat.sh tesoreria-internet
${ROOT_PATH}/scm/start_tomcat.sh tesoreria-ws-internet
${ROOT_PATH}/scm/start_tomcat.sh emision-internet
${ROOT_PATH}/scm/start_tomcat.sh emision-ws-internet
${ROOT_PATH}/scm/start_tomcat.sh desarrollo-territorial-internet
${ROOT_PATH}/scm/start_tomcat.sh cesvi-ws-internet
${ROOT_PATH}/scm/start_tomcat.sh siniestros-internet
${ROOT_PATH}/scm/start_tomcat.sh siniestros-internet2
${ROOT_PATH}/scm/start_tomcat.sh rusmovil-internet
${ROOT_PATH}/scm/start_tomcat.sh auditoria-internet
${ROOT_PATH}/scm/start_tomcat.sh contabilidad-internet
${ROOT_PATH}/scm/start_tomcat.sh api-rus-internet
${ROOT_PATH}/scm/start_tomcat.sh datoscomunes-internet
${ROOT_PATH}/scm/start_tomcat.sh notificaciones-internet
${ROOT_PATH}/scm/start_tomcat.sh rus-apigateway-internet
${ROOT_PATH}/scm/start_tomcat.sh api-crm-internet
${ROOT_PATH}/scm/start_tomcat.sh claims-ws-internet
${ROOT_PATH}/scm/start_tomcat.sh infoauto-ws-internet
#${ROOT_PATH_MS}/cobranza/recibosCobro/start.sh
${ROOT_PATH_MS}/emision/propuestas/start.sh
${ROOT_PATH_MS}/seguridad/apis/start.sh
#${ROOT_PATH_MS}/cobranza/recibos/start.sh
${ROOT_PATH_MS}/siniestro/detallesReclamo/start.sh
${ROOT_PATH}/scm/start_tomcat.sh api-erp-internet
