echo "Stop All services"

ROOT_PATH=/home/tomcat/internet

${ROOT_PATH}/scm/stop_tomcat.sh cas-internet  
${ROOT_PATH}/scm/stop_tomcat.sh contabilidad-internet
${ROOT_PATH}/scm/stop_tomcat.sh compras-internet
${ROOT_PATH}/scm/stop_tomcat.sh rrhh-internet 
${ROOT_PATH}/scm/stop_tomcat.sh tesoreria-internet
${ROOT_PATH}/scm/stop_tomcat.sh tesoreria-ws-internet
${ROOT_PATH}/scm/stop_tomcat.sh emision-internet
${ROOT_PATH}/scm/stop_tomcat.sh emision-ws-internet
${ROOT_PATH}/scm/stop_tomcat.sh desarrollo-territorial-internet 
${ROOT_PATH}/scm/stop_tomcat.sh cesvi-ws-internet
${ROOT_PATH}/scm/stop_tomcat.sh siniestros-internet
${ROOT_PATH}/scm/stop_tomcat.sh siniestros-internet2
${ROOT_PATH}/scm/stop_tomcat.sh rusmovil-internet
${ROOT_PATH}/scm/stop_tomcat.sh auditoria-internet
${ROOT_PATH}/scm/stop_tomcat.sh contabilidad-internet
${ROOT_PATH}/scm/stop_tomcat.sh api-rus-internet
${ROOT_PATH}/scm/stop_tomcat.sh datoscomunes-internet
${ROOT_PATH}/scm/stop_tomcat.sh notificaciones-internet
${ROOT_PATH}/scm/stop_tomcat.sh claims-ws-internet
${ROOT_PATH}/scm/stop_tomcat.sh infoauto-ws-internet
${ROOT_PATH}/scm/stop_tomcat.sh rus-apigateway-internet
${ROOT_PATH}/scm/stop_tomcat.sh api-crm-internet
kill $(ps aux | grep 'ms-cobranza-recibosCobro' | grep -v grep | awk '{print $2}')
kill $(ps aux | grep 'ms-emision-propuestas' | grep -v grep | awk '{print $2}')
kill $(ps aux | grep 'ms-seguridad-apis' | grep -v grep | awk '{print $2}')
kill $(ps aux | grep 'ms-cobranza-recibos' | grep -v grep | awk '{print $2}')
kill $(ps aux | grep 'ms-siniestro-detallesReclamo' | grep -v grep | awk '{print $2}')
kill $(ps aux | grep 'api-erp-internet' | grep -v grep | awk {print $1})
