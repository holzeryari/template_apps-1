#!/bin/bash 


USAGE="Uso: $0 instancia.  ej: $0 cas"


if [ "a" == "a"$1 ] ; then
   echo $USAGE
   exit 1
fi


ROOT_PATH=/home/tomcat
source $ROOT_PATH/$1/env.sh
TOMCAT_OWNER=tomcat; export TOMCAT_OWNER

echo -n "shutdown $1 Tomcat:  "
$CATALINA_HOME/bin/catalina.sh stop

echo "waiting 5 seconds"
sleep 5
TOMCAT_PID=`ps -ef | grep $TOMCAT_OWNER | grep -w "$ROOT_PATH/$1" | grep java | grep -v grep | awk '{print $2}' `

if [ "a" != "a"$TOMCAT_PID ] ; then
 echo "killing tomcat process: $TOMCAT_PID"
 kill -9 $TOMCAT_PID
fi

rm -Rf $ROOT_PATH/$1/logs/catalina.out

#/usr/bin/find $ROOT_PATH/$1/temp -type f -name "upload*" -exec rm -Rf {} \;
