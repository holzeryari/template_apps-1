echo "Stop All services"

ROOT_PATH=/home/tomcat/prod

#${ROOT_PATH}/activemq/bin/activemq stop
/etc/init.d/activemq.sh stop
${ROOT_PATH}/scm/stop_tomcat.sh cas-prod  
${ROOT_PATH}/scm/stop_tomcat.sh compras-prod
${ROOT_PATH}/scm/stop_tomcat.sh tesoreria-prod
${ROOT_PATH}/scm/stop_tomcat.sh procesos-prod
${ROOT_PATH}/scm/stop_tomcat.sh contabilidad-prod
${ROOT_PATH}/scm/stop_tomcat.sh desarrollo-territorial-prod
${ROOT_PATH}/scm/stop_tomcat.sh rrhh-prod
${ROOT_PATH}/scm/stop_tomcat.sh emision-prod
#${ROOT_PATH}/scm/stop_tomcat.sh emision-varias-prod
${ROOT_PATH}/scm/stop_tomcat.sh siniestros-prod
${ROOT_PATH}/scm/stop_tomcat.sh auditoria-prod
${ROOT_PATH}/scm/stop_tomcat.sh datoscomunes-prod
${ROOT_PATH}/scm/stop_tomcat.sh notificaciones-prod
kill $(ps aux | grep 'rus-apigateway-prod' | grep -v grep | awk '{print $2}')
