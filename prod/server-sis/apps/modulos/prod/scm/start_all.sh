echo "Starting All services"

ROOT_PATH=/home/tomcat/prod

rm -Rf ${ROOT_PATH}/*/temp/*tmp
rm -Rf ${ROOT_PATH}/*/work/Catalina/*

#${ROOT_PATH}/activemq/bin/activemq start
/etc/init.d/activemq.sh start
${ROOT_PATH}/scm/start_tomcat.sh cas-prod  
${ROOT_PATH}/scm/start_tomcat.sh compras-prod
${ROOT_PATH}/scm/start_tomcat.sh tesoreria-prod
${ROOT_PATH}/scm/start_tomcat.sh procesos-prod
${ROOT_PATH}/scm/start_tomcat.sh rrhh-prod 
${ROOT_PATH}/scm/start_tomcat.sh contabilidad-prod
${ROOT_PATH}/scm/start_tomcat.sh desarrollo-territorial-prod
${ROOT_PATH}/scm/start_tomcat.sh emision-prod
${ROOT_PATH}/scm/start_tomcat.sh auditoria-prod
${ROOT_PATH}/scm/start_tomcat.sh siniestros-prod 
${ROOT_PATH}/scm/start_tomcat.sh datoscomunes-prod
${ROOT_PATH}/scm/start_tomcat.sh notificaciones-prod
#${ROOT_PATH}/scm/start_tomcat.sh emision-varias-prod
#${ROOT_PATH}/rus-apigateway-interno/start.sh
